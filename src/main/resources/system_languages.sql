SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `SYSTEM_LANGUAGES` (
  `LANGUAGE_ID` int(1) NOT NULL AUTO_INCREMENT,
  `LANGUAGE` varchar(30) NOT NULL,
  PRIMARY KEY (`LANGUAGE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;


INSERT INTO `SYSTEM_LANGUAGES` (`LANGUAGE_ID`, `LANGUAGE`) VALUES
(-1, 'English'),
(-2, 'Russian'),
(-3, 'Turkish'),
(-4, 'Spanish'),
(-5, 'Persian'),
(-6, 'French'),
(-7, 'German'),
(-8, 'Japanese'),
(-9, 'Vietnamese'),
(-10, 'Chinese'),
(-11, 'Arabic'),
(-12, 'Indonesian'),
(-13, 'Portuguese'),
(-14, 'Italian'),
(-15, 'Ukrainian'),
(-16, 'Thai'),
(-17, 'Dutch'),
(-18, 'Korean'),
(-19, 'Hebrew'),
(-20, 'Polish'),
(-21, 'Greek'),
(-22, 'Romanian'),
(-23, 'Czech'),
(-24, 'Serbian'),
(-25, 'Swedish'),
(-26, 'Danish'),
(-27, 'Hungarian'),
(-28, 'Bulgarian'),
(-29, 'Finnish'),
(-30, 'Croatian'),
(-31, 'Slovak'),
(-32, 'Hindi'),
(-33, 'Norwegian Bokmal');




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
