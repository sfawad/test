/* ************************************************************************************************************************************************************ */
/* One time needs execution to rename columns */
ALTER Table family RENAME column family_name TO token;
ALTER Table family RENAME column info TO name;
