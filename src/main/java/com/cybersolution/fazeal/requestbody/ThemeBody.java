package com.cybersolution.fazeal.requestbody;

import javax.validation.constraints.NotNull;

import com.cybersolution.fazeal.customvalidators.ThemeNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeBody {
	@NotNull(message = AppConstants.THEME_NOT_NULL)
	@ThemeNameConstraint(message = AppConstants.THEME_FROM_LIST)
	String theme;
}
