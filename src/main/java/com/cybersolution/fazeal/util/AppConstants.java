package com.cybersolution.fazeal.util;

public final class AppConstants {

	private AppConstants() {

	}

	public static final String PERSONAL_PRIVATE = "PE_PR";
	public static final String PERSONAL_PUBLIC = "PE_PU";
	public static final String PERSONAL_CUSTOM = "PE_CU";
	public static final String BUSINESS_PUBLIC = "BU_PU";
	public static final String USER_AGENT = "User-Agent";
	public static final String SIGNIN = "SIGNIN";
	public static final String SIGNUP_CREATEFAMILY = "SIGNUP-CREATE FAMILY";
	public static final String SIGNUP_JOINFAMILY = "SIGNUP-JOIN FAMILY";
	public static final String CREATEFAMILY = "CREATE FAMILY";

	// Family Service Implementation Constants
	public static final String USER_PROFILE_PHOTO = "P";
	public static final String USER_COVER_PHOTO = "C";
	public static final String EMPTY = "";

	public static final String HAS_USER_OR_ADMIN_ROLE = "(hasRole('ROLE_USER') || hasRole('ROLE_ADMIN'))";
	public static final String AUTHENTICATED_USER = " && (authentication.principal.id.equals(#userId))";
	public static final String AUTHENTICATED_ADMIIN_USER = " && (authentication.principal.id.equals(#adminUserId))";
	public static final String HAS_ROLE_ROLE_ADMIN = "hasRole('ROLE_ADMIN')";

	// Family Controller constants
	public static final String FAMILY_ID = "familyId";
	public static final String TOKEN = "token";
	public static final String USER_ID = "userId";
	public static final String ADMIN_USER_ID = "adminUserId";
	public static final String USER_ID_TO_BE_UPDATED = "userIdToBeUpdated";
	public static final String IS_ADMIN = "isAdmin";
	public static final String RELATION_WITH_FAMILY = "relationWithFamily";
	public static final String ABOUT = "about";
	public static final String COUNTRY = "country";
	public static final String STATE = "state";
	public static final String CITY = "city";
	public static final String ZIP = "zip";
	public static final String ADDRESS2 = "address2";
	public static final String ADDRESS1 = "address1";
	public static final String PHONE = "phone";
	public static final String NAME = "name";

	// Voting Controllers constants
	public static final String OPTIONS = "options";
	public static final String VOTE_ID = "voteId";
	public static final String TYPE = "type";
	public static final String TITLE = "title";
	public static final String END_TIME = "endTime";

	// Shopping List Controllers constants
	public static final String SHOPPING_LIST_ID = "shoppingListId";
	public static final String ITEM_ID = "itemId";

	// Events Controller constants
	public static final String EVENT_ID = "eventId";
	public static final String IS_SHARED = "isShared";
	public static final String DATE = "date";
	public static final String EVENT_NAME = "eventName";
	public static final String START_TIME = "startTime";

	public static final String DEFAULT_COLOR = "#00000";

	// Album Controller constants
	public static final String ALBUM_ID = "albumId";
	public static final String ALBUM_NAME = "albumName";
	public static final String DESCRIPTION = "description";
	public static final String FAMILY_IDS = "familyIds";
	public static final String USER_IDS = "userIds";

	// Error Message Keys as constant
	public static final String LACK_OF_ADMIN_RIGHTS = "ERROR_USER_LACK_ADMIN_RIGHTS";

	// Image Controller constants
	public static final String PROFILE_PICTURE = "P";
	public static final String DASHBOARD_PICTURE = "D";
	public static final String FILE_IMAGE = "fileImage";
	public static final String DATE_FORMAT = "dd-M-yyyy";
	public static final String IMAGE_ID = "imageId";
	public static final String DATE_TIME_FORMATE = "dd-MM-yyyy HH:mm";
	public static final String EMAIL = "email";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String ERROR_USER_NOT_FOUND = "ERROR_USER_NOT_FOUND";
	public static final String ERROR_USER_NOT_EXISTS = "ERROR_USER_NOT_EXISTS";
	public static final String SIGNUP_JOIN_FAMILY_RESPONSE_MSG = "SIGNUP_JOIN_FAMILY_RESPONSE_MSG";
	public static final String SIGNUP_EMAIL_SUBJECT_JOIN_FAMILY_TEXT = "SIGNUP_EMAIL_SUBJECT_JOIN_FAMILY_TEXT";
	public static final String SIGNUP_CREATE_FAMILY_RESPONSE_MSG = "SIGNUP_CREATE_FAMILY_RESPONSE_MSG";
	public static final String ERROR_FAMILY_NOTFOUND = "ERROR_FAMILY_NOTFOUND";
	public static final String ERROR_FAMILY_TOKEN_MANDATORY = "ERROR_FAMILY_TOKEN_MANDATORY";
	public static final String ERROR_PHONE_NULL = "ERROR_PHONE_NULL";
	public static final String ERROR_DOB_MANDATORY = "ERROR_DOB_MANDATORY";
	public static final String ERROR_EMAIL_IN_USE = "ERROR_EMAIL_IN_USE";
	public static final String ERROR_USER_NAME = "ERROR_USER_NAME";
	public static final String ERROR_EMAIL_NULL = "ERROR_EMAIL_NULL";
	public static final String ERROR_USER_NAME_NULL = "ERROR_USER_NAME_NULL";
	public static final String DOMAIN_SRC = "<domainSrc>";
	public static final String SIGNUP_EMAIL_SUBJECT_CREATE_FAMILY_TEXT_LAST = "SIGNUP_EMAIL_SUBJECT_CREATE_FAMILY_TEXT_LAST";
	public static final String SIGNUP_EMAIL_SUBJECT_CREATE_FAMILY_TEXT = "SIGNUP_EMAIL_SUBJECT_CREATE_FAMILY_TEXT";
	public static final String SIGNUP_EMAIL_SUBJECT = "SIGNUP_EMAIL_SUBJECT";
	public static final String PASSWORD_UPDATED_SUCCESS = "PASSWORD_UPDATED_SUCCESS";
	public static final String PASSWORD_EMAIL_SUBJECT_RESET_DONE = "PASSWORD_EMAIL_SUBJECT_RESET_DONE";
	public static final String PASSWORD_ERROR_USER_PASSWORD_EMPTY = "PASSWORD_ERROR_USER_PASSWORD_EMPTY";
	public static final String PASSWORD_ERROR_USER_TOKEN_EMPTY = "PASSWORD_ERROR_USER_TOKEN_EMPTY";
	public static final String PASSWORD_ERROR_USER_TOKEN_INVALID = "PASSWORD_ERROR_USER_TOKEN_INVALID";
	public static final String PASSWORD_TOKEN_EXPIRED = "ERROR_PASSWORD_TOKEN_EXPIRED";
	public static final String PASSWORD_EMAIL_RESET_DONE_TEXT = "PASSWORD_EMAIL_RESET_DONE_TEXT";
	public static final String PASSWORD_EMAIL_SUBJECT_RESET = "PASSWORD_EMAIL_SUBJECT_RESET";
	public static final String PASSWORD_EMAIL_FROM = "PASSWORD_EMAIL_FROM";
	public static final String ENABLED = "enabled";
	public static final String ERROR_USER_UNAPPROVED = "ERROR_USER_UNAPPROVED";
	public static final String PASSWORD_EMAIL_SENT_SUCCESS = "PASSWORD_EMAIL_SENT_SUCCESS";
	public static final String BEARER = "Bearer";
	public static final String ERROR_INVALID_USER_PASSWORD = "ERROR_INVALID_USER_PASSWORD";
	public static final String DELETED = "deleted";
	public static final String ERROR_ROLE_NOT_FOUND = "ERROR_ROLE_NOT_FOUND";
	public static final String IS_VALID = "isValid";
	public static final String TOKEN_VERIFIED = "TOKEN_VERIFIED";
	public static final String TOKEN_IS_NOT_FOUND = "TOKEN_IS_NOT_FOUND";
	public static final String USER_PASSWORD_COMBINATION_OR_USER_NOT_FOUND = "USER_PASSWORD_COMBINATION_OR_USER_NOT_FOUND";
	public static final String FIRST_NAME = "firstname";
	public static final String LAST_NAME = "lastname";
	public static final String DATE_OF_BIRTH = "dob";
	public static final String GENDER = "gender";
	public static final String LANGUAGES = "languages";
	public static final String NICKNAME = "nickname";
	public static final String ABOUT_ME = "aboutme";
	public static final String ALL_PARAMS_MANDATORY = "ERROR_ALL_PARAMS_MANDATORY";
	public static final String PASSWORD_EMAIL_RESET_TEXT = "PASSWORD_EMAIL_RESET_TEXT";
	public static final String ALBUM_PICTURE = "U";
	public static final String EMAIL_NOTIFICATION_TYPE = "EMAIL";
	public static final String SMS_NOTIFICATION_TYPE = "SMS";
	public static final String PASSWORD_EMAIL_RESET = "PASSWORD_EMAIL_RESET";
	public static final String PASSWORD_EMAIL_RESET_DONE = "PASSWORD_EMAIL_RESET_DONE";
	public static final String JOIN_FAMILY_ADMIN = "JOIN_FAMILY_ADMIN";
	public static final String JOIN_FAMILY_USER = "JOIN_FAMILY_USER";
	public static final String JOIN_FAMILY_USER_SUCCESS = "JOIN_FAMILY_USER_SUCCESS";

	public static final String ERROR_SAVE_FILE = "Could not save file: fileName{} ";

	public static final String THEME = "theme";
	public static final String THEME_UPDATED_SUCCESSFULLY = "THEME_UPDATED_SUCCESSFULLY";

	public static final String ERR_ALL_PARAMS_MANDAORY = "ERROR_ALL_PARAMS_MANDATORY";
	public static final String ERR_INVALID_REQUEST = "ERROR_INVALID_REQUEST";
	public static final String ERROR_NO_VOTING_RECORD_FOUND = "ERROR_NO_VOTING_RECORD_FOUND";

	public static final String EVENTS = "EVENTS";
	public static final String BIRTHDAY = "BIRTHDAY";
	public static final String USER_CREATION = "USER_CREATION";

	public static final String FORMATED_DATE = "yyyy-MM-dd";
	public static final String FORMATED_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String NUMBER_ONE = "1";
	public static final String SPACE = " ";
	public static final String HYPHEN = "-";

	public static final String ERROR_FAM_ID_NOT_FOUND = "ERROR_FAM_ID_NOT_FOUND";
	public static final String IMAGES_SHARED = "IMAGES_SHARED";
	public static final String SLASH = "/";
	public static final String UNSERSCORE = "_";
	public static final String EMAIL_FROM = "EMAIL_FROM";
	public static final String DEFAULT = "default";
	public static final String ALBUM_COVER_UPDATED = "ALBUM_COVER_UPDATED";
	public static final String ERROR_ALBUM_COVER_FAILED = "ERROR_ALBUM_COVER_FAILED";
	public static final String LOCAL_PATH = "localPath";
	public static final String ALBUM_DELETED = "ALBUM_DELETED";

	// Messages for Refresh Token Implementations
	public static final String LOGOUT_MSG = "LOGOUT_MSG";
	public static final String REFRESH_TOKEN_EXPIRED_MSG = "REFRESH_TOKEN_EXPIRED_MSG";
	public static final String REFRESH_TOKEN_NOT_IN_DATABASE = "REFRESH_TOKEN_NOT_IN_DATABASE";
	public static final String ERROR_NO_RECORD_FOUND = "ERROR_NO_RECORD_FOUND";
	public static final String ERROR_INVALID_ALBUMNAME = "ERROR_INVALID_ALBUMNAME";
	public static final String ERROR_INVALID_REQUEST = "ERROR_INVALID_REQUEST";
	public static final String ERROR_ALBUM_NOTFOUND = "ERROR_ALBUM_NOTFOUND";
	public static final String ERROR_INVALID_USER_RIGHTS = "ERROR_INVALID_USER_RIGHTS";
	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String ALBUM_NAME_SIZE = "ALBUM_NAME_SIZE";
	public static final String ALBUM_TYPE_SIZE = "ALBUM_TYPE_SIZE";
	public static final String USERID_ISNULL = "USERID_ISNULL";
	public static final String ALBUMID_ISNULL = "ALBUMID_ISNULL";
	public static final String IMAGEID_ISNULL = "IMAGEID_ISNULL";
	public static final String ALBUM_ID_MIN = "ALBUM_ID_MIN";
	public static final String USER_ID_MIN = "USER_ID_MIN";
	public static final String JSON_NOT_CORRECT = "JSON_NOT_CORRECT";
	public static final String ERROR_USER_LACK_RIGHTS = "ERROR_USER_LACK_RIGHTS";
	public static final String DATE_ISNULL = "DATE_ISNULL";
	public static final String ERROR_SHOP_LIST_ID_INVALID = "ERROR_SHOP_LIST_ID_INVALID";
	public static final String ERROR_SHOP_LIST_FAMILYID_INVALID = "ERROR_SHOP_LIST__FAMILYID_INVALID";
	public static final String ERROR_SAVING_SHOPPING_LIST = "ERROR_SAVING_SHOPPING_LIST";
	public static final String ERROR_UPDATING_SHOPPING_LIST = "ERROR_UPDATING_SHOPPING_LIST";
	public static final String ERROR_IMAGE_NOTFOUND = "ERROR_IMAGE_NOTFOUND";
	public static final String ERROR_IMAGE_USER_ASSOCIATION = "ERROR_IMAGE_USER_ASSOCIATION";
	public static final String GENERAL_EXCEPTION = "GENERAL_EXCEPTION";
	public static final String PARAMS_INVALID = "PARAMS_INVALID";
	public static final String TYPE_MISMATCH = "TYPE_MISMATCH";
	public static final String DB_EXCEPTION = "DB_EXCEPTION";
	public static final String VALIDATION_FAILED = "VALIDATION_FAILED";
	public static final String INVALID_TYPE = "INVALID_TYPE";
	public static final String SHOPPINGLIST_DELETED = "SHOPPINGLIST_DELETED";
	public static final String SHOPPINGLIST_ITEM_DELETED = "SHOPPINGLIST_ITEM_DELETED";
	public static final String IMAGE_DELETED = "IMAGE_DELETED";
	public static final String EVENT_DELETED = "EVENT_DELETED";
	public static final String ERROR_INVALID_EVENT = "ERROR_INVALID_EVENT";
	public static final String ERROR_EVENT_USER_ASSOCIATION = "ERROR_EVENT_USER_ASSOCIATION";
	public static final String ERROR_INVALID_FAMILIES = "ERROR_INVALID_FAMILIES";
	public static final String ERROR_EVENT_NOT_FOUND = "ERROR_EVENT_NOT_FOUND";
	public static final String ERROR_ITEM_USER_ASSOCIATION = "ERROR_ITEM_USER_ASSOCIATION";
	public static final String ERROR_SHOP_LIST_ITEM_ID_NOT_FOUND = "ERROR_SHOP_LIST_ITEM_ID_NOT_FOUND";
	public static final String USERIDS_FAMILYIDS_SHOULD_BENULL = "USERIDS_FAMILYIDS_SHOULD_BENULL";
	public static final String ERROR_USER_FAMILY_PRIVACY = "ERROR_USER_FAMILY_PRIVACY";
	public static final String PRIVATE_WITH_NOT_NULL_LISTS = "PRIVATE_WITH_NOT_NULL_LISTS";
	public static final String CUSTOM_WITH_NULL_LISTS = "CUSTOM_WITH_NULL_LISTS";
	public static final String USER_HAVE_NO_RELATION_WITH_USERIDS_OR_FAMILYIDS = "USER_HAVE_NO_RELATION_WITH_USERIDS_OR_FAMILYIDS";
	public static final String UPDATED_PRIVATE = "UPDATED_PRIVATE";
	public static final String UPDATED_PUBLIC = "UPDATED_PUBLIC";
	public static final String UPDATED_CUSTOM = "UPDATED_CUSTOM";
	public static final String ALBUM_NAME_ISNULL = "ALBUM_NAME_ISNULL";
	public static final String ERROR_ALBUM_ALREADY_EXISTS = "ERROR_ALBUM_ALREADY_EXISTS";
	public static final String ERROR_IMAGE_LOCALPATH_ASSOCIATION = "ERROR_IMAGE_LOCALPATH_ASSOCIATION";
	public static final String THEME_FROM_LIST = "THEME_FROM_LIST";
	public static final String VOTING_TITLE_SIZE = "VOTING_TITLE_SIZE";
	public static final String THEME_NOT_NULL = "THEME_NOT_NULL";
	public static final String FAMILY_ID_POSITIVE = "FAMILY_ID_POSITIVE";
	public static final String VOTING_OPTIONS_ISNULL = "VOTING_OPTIONS_ISNULL";
	public static final String FAMILY_ID_ISNULL = "FAMILY_ID_ISNULL";
	public static final String VOTING_TYPE = "VOTING_TYPE";
	public static final String VOTING_ADD = "A";
	public static final String VOTING_REMOVE = "D";
	public static final String ERROR_VOTING_UPDATE_OPTIONS_TYPE = "ERROR_VOTING_UPDATE_OPTIONS_TYPE";
	public static final String USERID_ID_POSITIVE = "USERID_ID_POSITIVE";
	public static final String TITLE_ISNULL = "TITLE_ISNULL";
	public static final String VOTING_OPTIONS_INVALID = "VOTING_OPTIONS_INVALID";
	public static final String SHOPPINGLIST_NAME_INVALID = "SHOPPINGLIST_NAME_INVALID";
	public static final String SHOPPINGLIST_ITEM_NAME_INVALID = "SHOPPINGLIST_ITEM_NAME_INVALID";
	public static final String ERROR_USER_FAMILY_NOT_RELAION_EXISTS = "ERROR_USER_FAMILY_NOT_RELAION_EXISTS";
	public static final String SHOPPINGLIST_ID_ISNULL = "SHOPPINGLIST_ID_ISNULL";
	public static final String SHOPPINGLIST_ID_POSITIVE = "SHOPPINGLIST_ID_POSITIVE";

	public static final String EMAIL_INVALID = "EMAIL_INVALID";
	public static final String EMAIL_SIZE_INVALID = "EMAIL_SIZE_INVALID";
	public static final String USER_ID_INVALID_TYPE = "USER_ID_INVALID_TYPE";
	public static final String REFRESH_TOKEN_NOT_EMPTY = "REFRESH_TOKEN_NOT_EMPTY";
	public static final String TOKEN_NOT_EMPTY = "TOKEN_NOT_EMPTY";
	public static final String PASSWORD_NOT_EMPTY = "PASSWORD_NOT_EMPTY";
	public static final String PASSWORD_INVALID = "PASSWORD_INVALID";
	public static final String USER_NAME_NOT_EMPTY = "USER_NAME_NOT_EMPTY";
	public static final String USER_NAME_INVALID = "USER_NAME_INVALID";
	public static final String EMAIL_NOT_EMPTY = "EMAIL_NOT_EMPTY";
	public static final String DATE_OF_BIRTH_EMPTY = "DATE_OF_BIRTH_EMPTY";
	public static final String UPDATED_USER_ID_EMPTY = "UPDATED_USER_ID_EMPTY";

	public static final String BIND_MISMATCH = "BIND_MISMATCH";
	public static final String IMAGE_ISNULL = "IMAGE_ISNULL";
	public static final String TYPE_ISNULL = "TYPE_ISNULL";
	public static final String IMAGE_TYPE_NOT_FROM_LIST = "IMAGE_TYPE_NOT_FROM_LIST";
	public static final String ALBUM_NOT_EXIST = "ALBUM_NOT_EXIST";
	public static final String ERROR_INTERNAL = "ERROR_INTERNAL";
	public static final String SPECIAL_CHARS = "[$&+,:;=?@#|'<>.^*()%!-]";
	public static final String ALBUM_TYPE_NOT_VALID = "ALBUM_TYPE_NOT_VALID";
	public static final String INVALID_PARAMS = "INVALID_PARAMS";
	public static final String FILE_REGEX_TO_REMOVE = "[\\[|\\]|\\^|`]";
	public static final String SYSTEM_LANGUAGE_UPDATED_SUCCESSFULLY = "SYSTEM_LANGUAGE_UPDATED_SUCCESSFULLY";
	public static final String DEFAULT_LANGUAGE = "English";
	public static final String SOCIAL_MEDIA_LINKS_ADDED_SUCCESSFULLY = "SOCIAL_MEDIA_LINKS_ADDED_SUCCESSFULLY";
	public static final String ERROR_SAVING_SOCIAL_MEDIA_LINKS = "ERROR_SAVING_SOCIAL_MEDIA_LINKS";
	public static final String FAMILY_MEMBER_ADDED_SUCCESSFULLY = "FAMILY_MEMBER_ADDED_SUCCESSFULLY";
	public static final String ERROR_SAVING_FAMILY_MEMBER = "ERROR_SAVING_FAMILY_MEMBER";
	public static final String ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND = "ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND";
	public static final String ERROR_FAMILY_MEMBERS_NOTFOUND = "ERROR_FAMILY_MEMBERS_NOTFOUND";
	public static final String SOCIAL_MEDIA_ID = "socialMediaId";
	public static final String SOCIAL_MEDIA_LINK_DELETED = "SOCIAL_MEDIA_LINK_DELETED";
	public static final String ERROR_SOCIAL_MEDIA_LINK_USER_ASSOCIATION = "ERROR_SOCIAL_MEDIA_LINK_USER_ASSOCIATION";
	public static final String FAMILY_MEMBER_ID = "famMemberId";
	public static final String FAMILY_MEMBER_DELETED = "FAMILY_MEMBER_DELETED";
	public static final String ERROR_FAMILY_MEMBER_USER_ASSOCIATION = "ERROR_FAMILY_MEMBER_USER_ASSOCIATION";
	public static final String ERROR_UPDATING_SOCIAL_MEDIA_LINKS = "ERROR_UPDATING_SOCIAL_MEDIA_LINKS";
	public static final String SOCIAL_MEDIA_LINKS_UPDATED_SUCCESSFULLY = "SOCIAL_MEDIA_LINKS_UPDATED_SUCCESSFULLY";

	public static final String FAMILY_NAME_NOT_EMPTY = "FAMILY_NAME_NOT_EMPTY";
	public static final String PHONE_NOT_EMPTY = "PHONE_NOT_EMPTY";
	public static final String ADDRESS1_NOT_EMPTY = "ADDRESS1_NOT_EMPTY";
	public static final String CITY_NOT_EMPTY = "CITY_NOT_EMPTY";
	public static final String STATE_NOT_EMPTY = "STATE_NOT_EMPTY";
	public static final String ZIP_NOT_EMPTY = "ZIP_NOT_EMPTY";
	public static final String COUNTRY_NOT_EMPTY = "COUNTRY_NOT_EMPTY";
	public static final String RELATION_WITH_FAMILY_NOT_EMPTY = "RELATION_WITH_FAMILY_NOT_EMPTY";
	public static final String ADMIN_USER_ID_NOT_EMPTY = "ADMIN_USER_ID_NOT_EMPTY";
	public static final String IS_NEW_USER_ROLE_ADMIN_NOT_EMPTY = "IS_NEW_USER_ROLE_ADMIN_NOT_EMPTY";
	public static final String USER_ID_UPDATED_NOT_EMPTY = "USER_ID_UPDATED_NOT_EMPTY";

	public static final String FIRST_NAME_NOT_EMPTY = "FIRST_NAME_NOT_EMPTY";
	public static final String LAST_NAME_NOT_EMPTY = "LAST_NAME_NOT_EMPTY";

	public static final String GROUP_DASHBOARD = "GD";
	public static final String GROUP_PROFILE = "GP";
	public static final String PROFILE_DASHBOARD_SUCCESS = "PROFILE_DASHBOARD_SUCCESS";

	public static final String EVENT_NAME_NOT_EMPTY = "EVENT_NAME_NOT_EMPTY";
	public static final String EVENT_IS_SHARED_WITH_FAMILY_NOT_EMPTY = "EVENT_IS_SHARED_WITH_FAMILY_NOT_EMPTY";

	public static final String ERROR_FAMILY_NOTEXIST = "ERROR_FAMILY_NOTEXIST";
	public static final String GROUP_PROFILE_PICTURE = "GP";
	public static final String NAME_PREFIX_UPDATED_SUCCESSFULLY = "NAME_PREFIX_UPDATED_SUCCESSFULLY";
	public static final String NAME_PREFIX = "NAME_PREFIX";
	public static final String CONFIG_NAME = "name";
	public static final String PARAM_NAME = "PARAM_NAME";
	public static final String LANGUAGES_NOT_FROM_LIST = "LANGUAGES_NOT_FROM_LIST";
	public static final String SOCIAL_MEDIA_NOT_FROM_LIST = "SOCIAL_MEDIA_NOT_FROM_LIST";
	public static final String SOCIAL_MEDIA_USER_NAME_NOT_EMPTY = "SOCIAL_MEDIA_USER_NAME_NOT_EMPTY";

	public static final String NOT_IMAGE = "NOT_IMAGE";
	public static final String FILE_TYPE_IMAGE = "image";
	public static final String NAME_UPDATED_SUCCESSFULLY = "NAME_UPDATED_SUCCESSFULLY";
	public static final String DELETED_IMAGES_ALBUM = "Deleted images album";
	public static final String IMAGE_RESTORED = "IMAGE_RESTORED";
	public static final String IMAGE_NOT_DELETED = "IMAGE_NOT_DELETED";
}
