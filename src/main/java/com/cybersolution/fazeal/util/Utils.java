package com.cybersolution.fazeal.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

import com.cybersolution.fazeal.models.User;

public final class Utils {

	  private Utils() { 
		  
	  }

	/**
	   * @return formatted date
	   * @apiNote Used to format date and time as per required format
	   * @Param no param
	   */
	  public static String getCurrentDateTime() {
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	    Date date = new Date(System.currentTimeMillis());
	    return formatter.format(date);
	  }
	  
	  /**
	   * @param user
	   * @return user age as a period between now and  user.dob 
	   */
	  public static Period getUserAge(User user) {
			String[] userDob = user.getDob().split("T")[0].split("-");
			LocalDate l = LocalDate.of(Integer.valueOf(userDob[0]), Short.parseShort(userDob[1]),
					Short.parseShort(userDob[2]));
			LocalDate now = LocalDate.now();
			return Period.between(l, now);
		}
}
