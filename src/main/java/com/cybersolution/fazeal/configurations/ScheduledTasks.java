package com.cybersolution.fazeal.configurations;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cybersolution.fazeal.models.EventsRelation;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.NotificationsActivity;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.EventsRelationRespository;
import com.cybersolution.fazeal.repository.NotificationsActivityRespository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	@Autowired
	EventsRelationRespository eventsRelation;

	@Autowired
	NotificationsActivityRespository notifsRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	Messages messages;

	@Autowired
	EmailService emailService;

	@Scheduled(cron = "0 0 0 * * ?", zone = "")
	// @Scheduled(fixedDelay = 8600000, initialDelay = 5000)

	public void createPerDayNotifications() {

		createEventsNotifs();
		createUserJoiningNotifs();
		createBirthdaysNotifs();

	}

	public void createEventsNotifs() {

		List<NotificationsActivity> notifs = new ArrayList<>();
		try {
			notifs = notifsRepo.findNotificationsByType(AppConstants.EVENTS);
			if (!notifs.isEmpty()) {
				notifsRepo.deleteAll(notifs);
				notifs = new ArrayList<>();
			}

			// code for fetching latest events for the day.
			List<EventsRelation> events = eventsRelation.findAll();

			SimpleDateFormat dateFormatter = new SimpleDateFormat(AppConstants.FORMATED_DATE);
			SimpleDateFormat timeFormatter = new SimpleDateFormat(AppConstants.FORMATED_DATE_TIME);

			Date date = new Timestamp(System.currentTimeMillis());
			date = new Timestamp(timeFormatter.parse(date.toString()).getTime());
			Date dateTime = new Timestamp(dateFormatter.parse(date.toString()).getTime());

			for (EventsRelation event : events) {
				NotificationsActivity ntoifsActivity = new NotificationsActivity();
				Date date1 = new Timestamp(dateFormatter.parse(event.getEdate()).getTime());
				Date time1 = new Timestamp(timeFormatter.parse(event.getEstart()).getTime());
				if (date1.equals(dateTime) && time1.compareTo(date) > 0) {
					ntoifsActivity.setActivityType(AppConstants.EVENTS);
					ntoifsActivity.setActivity(event.getEvent());
					if (event.getIsShared().equalsIgnoreCase(AppConstants.NUMBER_ONE)) {
						ntoifsActivity.setFamilyId(event.getFamilyId());
					} else {
						ntoifsActivity.setFamilyId(null);
					}
					ntoifsActivity.setUserId(event.getUid());
					notifs.add(ntoifsActivity);
				}
			}

			if (!notifs.isEmpty()) {
				notifsRepo.saveAll(notifs);
			}
		} catch (Exception ec) {
			log.error("Error Occured when Calling  createEventsNotifs {} ", ec.getMessage());
		}
	}

	public void createUserJoiningNotifs() {

		List<User> users = new ArrayList<>();
		List<NotificationsActivity> notifsList = new ArrayList<>();
		try {
			notifsList = notifsRepo.findNotificationsByType(AppConstants.USER_CREATION);
			if (!notifsList.isEmpty()) {
				notifsRepo.deleteAll(notifsList);
				notifsList = new ArrayList<>();
			}

			SimpleDateFormat dateFormatter = new SimpleDateFormat(AppConstants.FORMATED_DATE);

			Date date = new Timestamp(System.currentTimeMillis());

			Calendar c = Calendar.getInstance();
			c.setTime(dateFormatter.parse(date.toString()));
			c.add(Calendar.DATE, -1); // number of days to add
			String date1 = dateFormatter.format(c.getTime());

			users = userRepo.getUserByCreatedAt(date1);
			for (User user1 : users) {
				for (Family familiy : user1.getFamilies()) {
					NotificationsActivity notifs = new NotificationsActivity();
					notifs.setActivity(AppConstants.USER_CREATION);
					notifs.setActivityType(AppConstants.USER_CREATION);
					notifs.setUserId(user1.getId());
					notifs.setFamilyId(familiy.getFamilyId());
					notifsList.add(notifs);
				}
			}

			if (!notifsList.isEmpty()) {
				notifsRepo.saveAll(notifsList);
			}
		} catch (Exception ec) {
			log.error("Error Occured while calling  createUserJoiningNotifs {} ", ec.getMessage());
		}
	}

	public void createBirthdaysNotifs() {

		List<User> users = new ArrayList<>();
		List<NotificationsActivity> notifsList = new ArrayList<>();
		try {
			notifsList = notifsRepo.findNotificationsByType(AppConstants.BIRTHDAY);
			if (!notifsList.isEmpty()) {
				notifsRepo.deleteAll(notifsList);
				notifsList = new ArrayList<>();
			}

			Date date = new Timestamp(System.currentTimeMillis());
			String[] datex = date.toString().split(AppConstants.SPACE)[0].split(AppConstants.HYPHEN);

			users = userRepo.getUserByDOB(datex[1] + AppConstants.HYPHEN + datex[2]);
			for (User user1 : users) {
				for (Family familiy : user1.getFamilies()) {
					NotificationsActivity notifs = new NotificationsActivity();
					notifs.setActivity(AppConstants.BIRTHDAY);
					notifs.setActivityType(AppConstants.BIRTHDAY);
					notifs.setUserId(user1.getId());
					notifs.setFamilyId(familiy.getFamilyId());
					notifsList.add(notifs);
				}

				// Email message
				SimpleMailMessage birthdayEmail = new SimpleMailMessage();
				birthdayEmail.setFrom(messages.get("PASSWORD_EMAIL_FROM"));
				birthdayEmail.setTo(user1.getEmail());
				birthdayEmail.setSubject(
						messages.get("PASSWORD_EMAIL_SUBJECT_BITHDAY") + AppConstants.HYPHEN + user1.getFirstName());
				birthdayEmail.setText(messages.get("PASSWORD_EMAIL_BBIRTHDAY_TEXT"));

			}

			if (!notifsList.isEmpty()) {
				notifsRepo.saveAll(notifsList);
			}
		} catch (Exception ec) {
			log.error("Error Occured While calling createBirthdayNotifs {} ", ec.getMessage());
		}
	}

}
