package com.cybersolution.fazeal.request;

import com.cybersolution.fazeal.util.AppConstants;
import lombok.*;

import javax.validation.constraints.NotBlank;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TokenRefreshRequest {
    @NotBlank(message = AppConstants.REFRESH_TOKEN_NOT_EMPTY)
    private String refreshToken;
}
