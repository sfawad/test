package com.cybersolution.fazeal.request;

import com.cybersolution.fazeal.util.AppConstants;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LogOutRequest {
    @NotNull(message = AppConstants.USER_ID_INVALID_TYPE)
    private Long userId;

}