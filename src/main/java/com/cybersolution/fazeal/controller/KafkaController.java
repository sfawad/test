package com.cybersolution.fazeal.controller;


import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.service.KafkaService;
import com.cybersolution.fazeal.util.AppConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Api(value = "Kafka Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/topic")
public class KafkaController {

    @Autowired
    private KafkaService kafkaService;


    @PostMapping(value = "/notification/publish",consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(AppConstants.HAS_ROLE_ROLE_ADMIN)
    public ResponseEntity<Object> authenticateUser(@RequestBody Notification notificationRequest) {
        kafkaService.publishToTopic(notificationRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
