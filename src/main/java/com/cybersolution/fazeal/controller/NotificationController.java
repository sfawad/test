package com.cybersolution.fazeal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.NotificationsActivity;
import com.cybersolution.fazeal.service.NotificationService;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

/**
 * @author Faizan Events controller for fetching notifications
 */

@Api(value = "Notification Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/notifications")
public class NotificationController {

	@Autowired
	NotificationService notificationService;

	/**
	 * @return ResponseEntity (Notifications)
	 * @apiNote Used to get notifications for user
	 * @Param Integer id
	 */
	@GetMapping("/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<List<NotificationsActivity>> getNotificationsForUser(
			@PathVariable(AppConstants.USER_ID) Long userId) {
		List<NotificationsActivity> notification = notificationService.getNotificationsForUsers(userId);
		return ResponseEntity.ok(notification);
	}

}
