
package com.cybersolution.fazeal.controller;

import java.util.List;

import com.cybersolution.fazeal.pojo.CreateEventPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.cybersolution.fazeal.models.Events;
import com.cybersolution.fazeal.models.EventsCurrentUserResponse;
import com.cybersolution.fazeal.models.EventsRelationResponse;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.repository.EventsFamilyUserRespository;
import com.cybersolution.fazeal.repository.EventsRelationRespository;
import com.cybersolution.fazeal.repository.EventsRespository;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EventService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author Faizan Events controller for fetching deleting updating and saving
 *         events for users
 */

@Api(value = "Events Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@Validated
@RestController
@RequestMapping("/api/v1/events")
public class EventsController {
	@Autowired
	EventService eventService;

	@Autowired
	EventsRespository eventRepo;

	@Autowired
	Messages messages;

	@Autowired
	EventsFamilyUserRespository eventsFamilyUserRespository;

	@Autowired
	FamilyRespository familyRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	EventsRelationRespository eventsRelRepo;

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to event by id
	 * @Param Integer id
	 */
	@GetMapping("/get/eventId/{eventId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getEvent(@PathVariable(AppConstants.EVENT_ID) Long eventId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		Events events = eventService.findByEventId(eventId);
		if (!ObjectUtils.isEmpty(events)) {
			if (userId.equals(events.getUserId())) {

				return ResponseEntity.ok(events);
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_EVENT_NOT_FOUND)));
		}

	}

	/**
	 * @return Boolean value
	 * @apiNote Used to delete events
	 * @Param Integer id (eventId)
	 */
	@PostMapping("/deleteEvent/{eventId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteEvent(@PathVariable(AppConstants.EVENT_ID) Long eventId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		Events events = eventService.findByEventId(eventId);
		if (!ObjectUtils.isEmpty(events)) {
			if (userId.equals(events.getUserId())) {

				boolean event = eventService.deleteEvent(eventId);
				if (event) {
					return ResponseEntity.status(HttpStatus.OK)
							.body(new MessageResponse(messages.get(AppConstants.EVENT_DELETED)));

				} else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
				}
			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_EVENT_USER_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_EVENT_NOT_FOUND)));
		}

	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get user's Events Relation Response
	 * @Param Integer userId
	 */
	@GetMapping("/get/userId/plainEventsResponse/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getUserEventsPlainResponse(@PathVariable(AppConstants.USER_ID) Long userId) {

		List<EventsRelationResponse> eventsRelList = eventService.getUserEventsPlainResponse(userId);

		return ResponseEntity.ok(eventsRelList);

	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get user's events
	 * @Param Integer userId
	 */
	/*
	 * This api is not consumed in Frontend and it will used in futrue for other
	 * features
	 */
	@GetMapping("/get/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getUserEvents(@PathVariable(AppConstants.USER_ID) Long userId) {

		EventsCurrentUserResponse eventsDetails = eventService.getUserEvents(userId);
		if (ObjectUtils.isEmpty(eventsDetails.getFamilyEvents())
				&& ObjectUtils.isEmpty(eventsDetails.getUserPrivateEvents())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
		}
		return ResponseEntity.ok(eventsDetails);
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get user's and Family (shared) events altogether
	 * @Param Integer familyId
	 * @Param Integer userId
	 */

	@GetMapping("/get/familyId/{familyId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getUserWithFamilyEvents(@PathVariable(AppConstants.FAMILY_ID) Long familyId) {
		List<Events> famEvent = eventService.getUserWithFamilyEvents(familyId);
		if (ObjectUtils.isEmpty(famEvent)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
		}
		return ResponseEntity.ok(famEvent);
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to update events
	 * @Param Integer id
	 * @Param String event
	 * @Param String date
	 * @Param String startTime
	 * @Param String endTime
	 * @Param boolean isShared
	 * @Param Integer userId
	 */
	@PostMapping("/update/{eventId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateUserEvent(@PathVariable(AppConstants.EVENT_ID) Long id,
			@RequestParam(required = false) String eventName, @RequestParam(required = false) String date,
			@RequestParam(required = false) String startTime, @RequestParam(required = false) String endTime,
			@RequestParam(AppConstants.USER_ID) Long userId, @RequestParam(required = false) Long[] familyId,
			@RequestParam(AppConstants.IS_SHARED) boolean isShared) {
		Events events = eventService.findByEventId(id);
		if (null != events) {
			if (events.getUserId().equals(userId)) {
				eventService.setEventDetails(events, date, startTime, endTime, isShared, eventName);
				if (isShared) {
					if (null != familyId && familyId.length > 0) {
						eventService.deleteEventsFamilyUser(id);
						eventService.saveEventsFamilyUser(events, userId, familyId);
					} else {
						return ResponseEntity.badRequest()
								.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_FAMILIES)));
					}
				} else {
					eventService.deleteEventsFamilyUser(id);
				}
				return ResponseEntity.ok(events);
			} else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get(AppConstants.ERROR_EVENT_USER_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_EVENT)));
		}
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to save events
	 * @Param IntegeruserId
	 * @Param String event
	 * @Param String date
	 * @Param String startTime
	 * @Param String endTime
	 * @Param boolean isShared
	 * @Param Integer familyId
	 */
	@PostMapping("/save/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> saveUserEvent(@PathVariable(AppConstants.USER_ID) Long userId, @Valid @RequestBody CreateEventPojo createEventPojo) {
		
		Events events = eventService.saveEvent(userId,createEventPojo);
		if (Boolean.TRUE.equals(createEventPojo.getIsShared())) {
			 eventService.saveEventsFamilyUser(events, userId, createEventPojo.getFamilyIds());
		}
		return ResponseEntity.ok(events);
	}

}
