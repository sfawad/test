package com.cybersolution.fazeal.controller;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.AlbumCoverPojo;
import com.cybersolution.fazeal.pojo.CreateAlbumModelPoJo;
import com.cybersolution.fazeal.pojo.UpdateAlbumModelPoJo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.security.services.UserDetailsImpl;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "Album Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/album")
@Validated
public class AlbumController {
	@Autowired
	private AlbumService albumService;

	@Autowired
	UserService userService;

	@Autowired
	Messages messages;

	/**
	 * @apiNote /api/v1/album/create called to create album with params described
	 *          and return AlbumModel of the created album
	 * @param date
	 * @param userId,      of the owner of the created album
	 * @param albumName,   of album
	 * @param description, of album
	 * @param familyIds,   family ids where album to be shared with
	 * @param userIds,     user ids where album to be shared with
	 * @param type,        type of album from PE_PR personal private, PE_PU personal
	 *                     public (shared to user families), PE_CU (seleted users &
	 *                     families to be shared with)
	 * @return null in case of error with BAD_REQUEST status, and album model with
	 *         OK status in case of success call.
	 */
	@PostMapping(value = "/create", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> createNewAlbum(@RequestBody @Valid CreateAlbumModelPoJo albumModelPoJo) {
		AlbumModel newAlbum = createAlbum(albumModelPoJo.getDate(), albumModelPoJo.getUserId(),
				albumModelPoJo.getAlbumName(), albumModelPoJo.getDescription().trim(), albumModelPoJo.getType());
		try {
			User user = userService.getById(albumModelPoJo.getUserId());
			AlbumModel albumModel = albumService.findAlbumByAlbumNameAndUserId(albumModelPoJo.getAlbumName(),
					albumModelPoJo.getUserId());
			if (albumModel != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_ALBUM_ALREADY_EXISTS)));
			}
			if (AppConstants.PERSONAL_PRIVATE.equalsIgnoreCase(albumModelPoJo.getType())
					&& (!albumModelPoJo.getFamilyIds().isEmpty() || !albumModelPoJo.getUserIds().isEmpty())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.PRIVATE_WITH_NOT_NULL_LISTS)));
			} else if (AppConstants.PERSONAL_CUSTOM.equalsIgnoreCase(albumModelPoJo.getType())
					&& (albumModelPoJo.getFamilyIds().isEmpty() && albumModelPoJo.getUserIds().isEmpty())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.CUSTOM_WITH_NULL_LISTS)));
			} else if (AppConstants.PERSONAL_PRIVATE.equalsIgnoreCase(albumModelPoJo.getType())
					&& (albumModelPoJo.getFamilyIds().isEmpty() && albumModelPoJo.getUserIds().isEmpty())) {
				newAlbum = albumService.createPrivateAlbum(newAlbum);
			} else if (AppConstants.PERSONAL_PUBLIC.equalsIgnoreCase(albumModelPoJo.getType())) {
				newAlbum = albumService.createPublicToAllUserFamiliesAlbum(newAlbum, user);
			} else if (AppConstants.PERSONAL_CUSTOM.equalsIgnoreCase(albumModelPoJo.getType())
					&& (!albumModelPoJo.getFamilyIds().isEmpty() || !albumModelPoJo.getUserIds().isEmpty())) {
				newAlbum = albumService.createSharedToFamiliesAndUsersAlbum(albumModelPoJo.getFamilyIds(),
						albumModelPoJo.getUserIds(), user, newAlbum);
				if (newAlbum == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(
							messages.get(AppConstants.USER_HAVE_NO_RELATION_WITH_USERIDS_OR_FAMILYIDS)));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERR_ALL_PARAMS_MANDAORY)));
			}
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERR_INVALID_REQUEST)));
		}
		return ResponseEntity.ok(newAlbum);
	}

	/**
	 * 
	 * @param date
	 * @param userId
	 * @param albumName
	 * @param description
	 * @param type
	 * @return AlbumModel
	 */
	private AlbumModel createAlbum(Date date, Long userId, String albumName, String description, String type) {
		return AlbumModel.builder().userId(userId).date(date).albumName(albumName).description(description).type(type)
				.build();
	}

	/**
	 * @apiNote /api/v1/album/delete/{id} to delete album where the logged in user
	 *          is the owner of the album
	 * @param id, album id to be deleted
	 * @return true if deleted, false if not deleted or id parameter is null
	 */
	@PostMapping(value = "/delete/{albumId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<MessageResponse> deleteAlbumById(@PathVariable(AppConstants.ALBUM_ID) Long albumId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		boolean status = albumService.deleteByAlbumId(albumId, auth.getName());
		if (status) {
			return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.ALBUM_DELETED)));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_ALBUM_NOTFOUND)));
		}
	}

	/**
	 * @apiNote /api/v1/album/update/{id} to update album with params described and
	 *          return the updated album model
	 * @param albumId,     of the album to be updated
	 * @param date,        date of update
	 * @param userId,      userId of the owner of the album
	 * @param albumName,   new album name
	 * @param description, new description
	 * @param familyIds,   {@value optional} ids of the families to share album with
	 * @param userIds,     {@value optional} ids of the users to share album with
	 * @param type,        type of the album
	 * @return saved album model
	 */
	@PostMapping(value = "/update/{albumId}/userId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateAlbum(@PathVariable(AppConstants.ALBUM_ID) Long albumId,
			@PathVariable(AppConstants.USER_ID) Long userId, @Valid @RequestBody UpdateAlbumModelPoJo albumModelPoJo) {
		try {
			User user = userService.getById(userId);
			AlbumModel oldAlbum = albumService.getByAlbumId(albumId);

			if (ObjectUtils.isEmpty(oldAlbum)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_ALBUM_NOTFOUND)));
			} else if (!oldAlbum.getUserId().equals(userId)) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_USER_RIGHTS)));
			}
			if (AppConstants.PERSONAL_PRIVATE.equalsIgnoreCase(albumModelPoJo.getType())
					&& (!albumModelPoJo.getFamilyIds().isEmpty() || !albumModelPoJo.getUserIds().isEmpty())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.PRIVATE_WITH_NOT_NULL_LISTS)));
			} else if (AppConstants.PERSONAL_CUSTOM.equalsIgnoreCase(albumModelPoJo.getType())
					&& (albumModelPoJo.getFamilyIds().isEmpty() && albumModelPoJo.getUserIds().isEmpty())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.CUSTOM_WITH_NULL_LISTS)));
			} else if (AppConstants.PERSONAL_PRIVATE.equalsIgnoreCase(albumModelPoJo.getType())
					&& (albumModelPoJo.getFamilyIds().isEmpty() && albumModelPoJo.getUserIds().isEmpty())) {
				albumService.updateAlbumSetPrivateAlbum(oldAlbum, albumModelPoJo.getAlbumName(),
						albumModelPoJo.getDescription(), AppConstants.PERSONAL_PRIVATE, albumModelPoJo.getDate(), user);
				return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.UPDATED_PRIVATE)));
			} else if (AppConstants.PERSONAL_PUBLIC.equalsIgnoreCase(albumModelPoJo.getType())) {
				albumService.updateAlbumSetPublicToAllUserFamiliesAlbum(oldAlbum, albumModelPoJo.getAlbumName(),
						albumModelPoJo.getDescription(), albumModelPoJo.getType(), albumModelPoJo.getDate(), user);
				return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.UPDATED_PUBLIC)));
			} else if (AppConstants.PERSONAL_CUSTOM.equalsIgnoreCase(albumModelPoJo.getType())
					&& (!albumModelPoJo.getFamilyIds().isEmpty() && !albumModelPoJo.getUserIds().isEmpty())) {
				if (albumService.updateAlbumSetSharedToSelectedFamiliesAndUsers(user, oldAlbum, albumModelPoJo) == null)
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(
							messages.get(AppConstants.USER_HAVE_NO_RELATION_WITH_USERIDS_OR_FAMILYIDS)));
				return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.UPDATED_CUSTOM)));
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERR_ALL_PARAMS_MANDAORY)));
			}
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_ALBUM_NOTFOUND)));
		}
	}

	/**
	 * @param id
	 * @return ResponseEntity
	 * @apiNote used to get Albums Id by UserId
	 */
	@GetMapping(value = "/byUserId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<List<AlbumModel>> getAlbumsIdByUserId(@PathVariable(AppConstants.USER_ID) Long userId) {
		return ResponseEntity.ok(albumService.findByUserId(userId));
	}

	/**
	 * 
	 * @param albumId        Long
	 * @param albumModelPoJo
	 * @return String
	 */
	@PostMapping(value = "/setCoverphoto/{albumId}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<MessageResponse> setAlbumCoberphoto(@PathVariable(AppConstants.ALBUM_ID) Long albumId,
			@Valid @RequestBody AlbumCoverPojo albumCoverPojo) {
		if (albumService.updateAlbumCover(albumId, albumCoverPojo.getUserId(), albumCoverPojo.getImageId()))
			return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.ALBUM_COVER_UPDATED)));
		else
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_ALBUM_COVER_FAILED)));

	}

	/**
	 * 
	 * @param albumId Long
	 * @return AlbumModel
	 */
	@GetMapping(value = "/{albumId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getAlbumByAlbumId(@PathVariable(AppConstants.ALBUM_ID) Long albumId) {
		UserDetailsImpl principal = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		AlbumModel album = albumService.getByAlbumId(albumId);
		if (ObjectUtils.isEmpty(album)) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_NO_RECORD_FOUND)));
		}
		if (album.getUserId().equals(principal.getId()))
			return ResponseEntity.ok(album);
		else
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_LACK_RIGHTS)));
	}

	/**
	 * 
	 * @param albumName String
	 * @param userId    Long
	 * @return AlbumModel
	 */
	@GetMapping(value = "/searchByAlbumName/{albumName}/userId/{userId}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getAlbumByAlbumName(@PathVariable(AppConstants.ALBUM_NAME) String albumName,
			@PathVariable(AppConstants.USER_ID) Long userId) {
		AlbumModel albumModel = albumService.findAlbumByAlbumNameAndUserId(albumName, userId);
		if (!ObjectUtils.isEmpty(albumModel) && !ObjectUtils.isEmpty(albumModel.getAlbumId())) {
			return ResponseEntity.ok(albumModel);
		} else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_NO_RECORD_FOUND)));
		}
	}

	/**
	 * 
	 * @param albumName String
	 * @param userId    Long
	 * @return List<AlbumModel>
	 */
	@GetMapping(value = "/containsName/{albumName}/userId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getAlbumIfContainsName(@PathVariable(AppConstants.ALBUM_NAME) String albumName,
			@PathVariable(AppConstants.USER_ID) Long userId) {
		List<AlbumModel> albumModel = albumService.findAlbumIfContainsNameWithUserId(albumName, userId);
		if (!albumModel.isEmpty()) {
			return ResponseEntity.ok(albumModel);
		} else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_NO_RECORD_FOUND)));
		}
	}
}