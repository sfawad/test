package com.cybersolution.fazeal.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.GroupProfileDashboardImagePojo;
import com.cybersolution.fazeal.pojo.ImageRelationPojo;
import com.cybersolution.fazeal.pojo.ImageUploadPojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageModelFamiliesService;
import com.cybersolution.fazeal.service.ImageModelUserService;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "Image Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/images")
public class ImageController {
	@Autowired
	private ImageService imageService;
	@Autowired
	private UserService userService;
	@Autowired
	private ImageModelFamiliesService imageModelFamiliesService;
	@Autowired
	private ImageModelUserService imageModelUserService;
	@Autowired
	private Messages messages;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private FamilyService familyService;

	/**
	 * @param date
	 * @param multipartFile
	 * @param userId
	 * @param type          - C for cover photo, P for profile picture and F for
	 *                      family photos
	 * @param albumName
	 * @param description
	 * @return ResponseEntity (File name)
	 * @apiNote Used to Upload Image
	 * @throws IOException
	 */
	@PostMapping("/upload/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> uploadImage(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @ModelAttribute ImageUploadPojo imageUploadPojo) {

		String fileNameReturned = AppConstants.EMPTY;
		AlbumModel albumModel = null;

		if (imageUploadPojo.getFileImage() != null && (imageUploadPojo.getFileImage().getSize() > 0)) {
			String contentType = imageUploadPojo.getFileImage().getContentType();
			if (contentType != null && !contentType.startsWith(AppConstants.FILE_TYPE_IMAGE)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.NOT_IMAGE)));
			}

			if (AppConstants.ALBUM_PICTURE.equalsIgnoreCase(imageUploadPojo.getType())) {
				albumModel = albumService.findAlbumByAlbumNameAndUserId(imageUploadPojo.getAlbumName(), userId);
				if (albumModel == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ALBUM_NOT_EXIST)));
				}
			}

			fileNameReturned = imageService.uploadImages(imageUploadPojo.getDate(), imageUploadPojo.getFileImage(),
					userId, imageUploadPojo.getType(), imageUploadPojo.getAlbumName(), imageUploadPojo.getDescription(),
					fileNameReturned, albumModel);

			if (ObjectUtils.isEmpty(fileNameReturned)) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_INTERNAL)));
			}

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.IMAGE_ISNULL)));
		}
		return ResponseEntity.ok(new MessageResponse(fileNameReturned));
	}

	/**
	 * @param userId
	 * @param familyIds
	 * @param imageId
	 * @param userIds
	 * @return ResponseEntity (Message )
	 * @apiNote used to set the privacy for Images
	 */
	@PostMapping("/update/{imageId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> updateImageToSharewithFamilyandUSer(
			@PathVariable(AppConstants.USER_ID) Long userId, @PathVariable(AppConstants.IMAGE_ID) Long imageId,
			@Valid @RequestBody ImageRelationPojo imageRelationPojo) {
		ImageModel image = imageService.getImageById(imageId);
		if (image == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
		if (userId.equals(image.getUserId())) {
			User user = userService.getById(userId);
			if (AppConstants.PERSONAL_PRIVATE.equalsIgnoreCase(imageRelationPojo.getType())) {
				if (!imageRelationPojo.getFamilyIds().isEmpty() || !imageRelationPojo.getUserIds().isEmpty()) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.USERIDS_FAMILYIDS_SHOULD_BENULL)));
				}
				clearImageRelations(imageId);
				image.setType(imageRelationPojo.getType());
				imageService.save(image);
				return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.IMAGES_SHARED)));
			} else if (AppConstants.PERSONAL_PUBLIC.equalsIgnoreCase(imageRelationPojo.getType())) {
				clearImageRelations(imageId);
				if (imageService.imagesSharedWithUsersAndFamilies(
						user.getFamilies().stream().map(Family::getFamilyId).collect(Collectors.toList()), image, null,
						user, imageRelationPojo.getType())) {
					return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.IMAGES_SHARED)));
				}
			} else if (AppConstants.PERSONAL_CUSTOM.equalsIgnoreCase(imageRelationPojo.getType())) {
				if (!imageRelationPojo.getFamilyIds().isEmpty() || !imageRelationPojo.getUserIds().isEmpty()) {
					clearImageRelations(imageId);
					if (imageService.imagesSharedWithUsersAndFamilies(imageRelationPojo.getFamilyIds(), image,
							imageRelationPojo.getUserIds(), user, imageRelationPojo.getType())) {
						return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.IMAGES_SHARED)));
					}
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(
							messages.get(AppConstants.USER_HAVE_NO_RELATION_WITH_USERIDS_OR_FAMILYIDS)));
				} else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_FAMILY_PRIVACY)));
				}
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_USER_RIGHTS)));
	}

	private void clearImageRelations(Long imageId) {
		imageModelFamiliesService.delete(imageId);
		imageModelUserService.delete(imageId);
	}

	/**
	 * @param userId
	 * @return ResponseEntity (images )
	 * @apiNote used to get the Images with UserId
	 */
	@GetMapping("/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<List<ImageModel>> getUserImages(@PathVariable(AppConstants.USER_ID) Long userId) {
		List<ImageModel> images = imageService.findByUserId(userId);
		return new ResponseEntity<>(images, HttpStatus.OK);
	}

	/**
	 * @param userId and type
	 * @return ResponseEntity (images )
	 * @apiNote used to get the Images with UserId and Type
	 */
	@GetMapping("/{userId}/byType/{type}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getUserImagesByUserIdAndType(@PathVariable(AppConstants.USER_ID) Long userId,
			@PathVariable(AppConstants.TYPE) String[] type) {
		List<ImageModel> images;
		images = imageService.findByUserIdAndType(userId, type);
		if (ObjectUtils.isEmpty(images)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
		}
		return ResponseEntity.ok(images);
	}

	/**
	 * @param userId
	 * @return ResponseEntity (albumMap )
	 * @apiNote used to get the Album Images which is shared with Family and Users
	 *          and Own Images with UserId and Type
	 */
	@GetMapping("/albums/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<ConcurrentHashMap<String, Set<AlbumModel>>> getAlbumImages(
			@PathVariable(AppConstants.USER_ID) Long userId) {
		ConcurrentHashMap<String, Set<AlbumModel>> albumMap;
		albumMap = imageService.getAlbumImagesWithUserId(userId);
		return ResponseEntity.ok(albumMap);
	}

	/**
	 * @param userId Long
	 * @param imageId Long
	 * @return ResponseEntity<MessageResponse> success | failure message response
	 * @apiNote to mark image as delete
	 */
	@PostMapping("/delete/{imageId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> deleteImage(@PathVariable(AppConstants.IMAGE_ID) Long imageId,
			@PathVariable(AppConstants.USER_ID) Long userId) {
		try {
			User user = userService.getById(userId);
			if (ObjectUtils.isEmpty(user)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)));
			}
			ImageModel imageModel = imageService.getImageById(imageId);
			if (imageModel != null) {
				if (userId.equals(imageModel.getUserId())) {
					imageService.markImagedelete(imageModel, userId);
					return ResponseEntity.status(HttpStatus.OK)
							.body(new MessageResponse(messages.get(AppConstants.IMAGE_DELETED)));
				} else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_USER_ASSOCIATION)));
				}
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
			}
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(messages.get(AppConstants.ERROR_INTERNAL)));
		}
	}

	/**
	 * 
	 * @param localPath String
	 * @return ImageModel
	 */
	@GetMapping("/byLocalPath/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getImageByImageLocalPath(@RequestParam(AppConstants.LOCAL_PATH) String localPath,
			@PathVariable(AppConstants.USER_ID) Long userId) {
		ImageModel imageModel = imageService.getImgByImageLocalPath(localPath);
		if (!ObjectUtils.isEmpty(imageModel)) {
			if (userId.equals(imageModel.getUserId())) {
				ImageModel imageModelPath = imageService.getImageByImageLocalPath(localPath, userId);
				if (ObjectUtils.isEmpty(imageModelPath)) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
				}
				return ResponseEntity.ok(imageModelPath);
			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_LOCALPATH_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
		}
	}

	@PostMapping("/groupImage/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> uploadGroupProfileOrDashboardImage(
			@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @ModelAttribute GroupProfileDashboardImagePojo groupPDImagePojo) {
		if (groupPDImagePojo.getFileImage() != null && (groupPDImagePojo.getFileImage().getSize() > 0)) {
			String contentType = groupPDImagePojo.getFileImage().getContentType();
			if (contentType != null && (contentType.startsWith(AppConstants.FILE_TYPE_IMAGE)
					|| contentType.startsWith(AppConstants.GROUP_PROFILE.toLowerCase())
					|| contentType.startsWith(AppConstants.GROUP_DASHBOARD.toLowerCase()))) {
				Family family = familyService.getFamilyById(groupPDImagePojo.getFamilyId());
				if (null == family) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTEXIST)));
				}
				FamilyUserRoles userRole = familyService.getRoleForUserInFamily(groupPDImagePojo.getFamilyId(), userId);
				if (userRole == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_FAMILY_NOT_RELAION_EXISTS)));
				} else if (!ERole.ROLE_ADMIN.toString().equalsIgnoreCase(userRole.getRole())) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_LACK_RIGHTS)));
				}
				imageService.uploadFamilyGroupProfileOrDashboardImage(groupPDImagePojo.getFileImage(), family,
						groupPDImagePojo.getType(), groupPDImagePojo.getDate(), userId);
				return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.PROFILE_DASHBOARD_SUCCESS)));
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.NOT_IMAGE)));
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.IMAGE_ISNULL)));
		}
	}

	/**
	 * @param userId Long
	 * @param imageId Long
	 * @return ResponseEntity<MessageResponse> success | failure message response
	 * @apiNote to un-mark image as delete
	 */
	@PostMapping("/restore/{imageId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> restoreImageToAlbum(@PathVariable(AppConstants.IMAGE_ID) Long imageId,
			@PathVariable(AppConstants.USER_ID) Long userId) {
		try {
			User user = userService.getById(userId);
			if (ObjectUtils.isEmpty(user)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)));
			}
			ImageModel imageModel = imageService.getImageById(imageId);
			if (imageModel != null) {
				if (userId.equals(imageModel.getUserId())) {
					if (imageService.restoreImageToAlbum(imageModel))
						return ResponseEntity.status(HttpStatus.OK)
								.body(new MessageResponse(messages.get(AppConstants.IMAGE_RESTORED)));
					else
						return ResponseEntity.status(HttpStatus.OK)
								.body(new MessageResponse(messages.get(AppConstants.IMAGE_NOT_DELETED)));
				} else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_USER_ASSOCIATION)));
				}
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_IMAGE_NOTFOUND)));
			}
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MessageResponse(messages.get(AppConstants.ERROR_INTERNAL)));
		}
	}

}