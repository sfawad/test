package com.cybersolution.fazeal.controller;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cybersolution.fazeal.customvalidators.PasswordConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.exception.ResourceNotFoundException;
import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.LoginRequest;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.pojo.UserUpdateDetails;
import com.cybersolution.fazeal.request.LogOutRequest;
import com.cybersolution.fazeal.request.TokenRefreshRequest;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.security.services.RefreshTokenService;
import com.cybersolution.fazeal.service.AuthService;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.KafkaService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "User Auth Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@Validated
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	Messages messages;

	@Autowired
	EmailService emailService;

	@Autowired
	FamilyService familyService;

	@Autowired
	FamilyController familyController;

	@Autowired
	AuditLogsHelper auditLogsHelper;

	@Autowired
	AuthService authService;

	@Autowired
	RefreshTokenService refreshTokenService;

	@Value("${origin.domain}")
	private String domain;

	@Autowired
	KafkaService kafkaService;

	@Autowired
	Notification notification;

	/**
	 * Register new user with option of join family or create family
	 * 
	 * @param signUpRequest SignupRequest
	 * @param httpReq       HttpServletRequest
	 * @return
	 */
	@PostMapping("/signup")
	public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest,
			HttpServletRequest httpReq) {

		if (Boolean.TRUE.equals(authService.isUserExistsWithUsername(signUpRequest.getUsername()))) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NAME)));
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getEmail())
				&& Boolean.TRUE.equals(authService.isUserExistsWithEmail(signUpRequest.getEmail()))) {
			return ResponseEntity.badRequest()
					.body((new MessageResponse(messages.get(AppConstants.ERROR_EMAIL_IN_USE))));
		}
		if (signUpRequest.isCreateFamilyAccount() && ObjectUtils.isEmpty(signUpRequest.getPhone())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get(AppConstants.ERROR_PHONE_NULL)));
		}
		if (!signUpRequest.isCreateFamilyAccount()) {
			if (ObjectUtils.isEmpty(signUpRequest.getFamilyToken())) {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_TOKEN_MANDATORY)));
			} else {
				if (ObjectUtils.isEmpty(familyService.getFamilyInfoFromToken(signUpRequest.getFamilyToken()))) {
					return ResponseEntity.badRequest()
							.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)));
				}
			}
		}
		User user = authService.createNewUser(signUpRequest);
		Family family = null;
		if (!ObjectUtils.isEmpty(user)) {
			if (signUpRequest.isCreateFamilyAccount()) {
				if (familyService.isUserAgeValidToCreateFamily(user)) {
					family = familyService.createNewFamilyForNewUserWhenRegister(user);
				} else {
					return ResponseEntity.badRequest()
							.body(new MessageResponse(messages.get("ERROR_FAMILY_CREATION_MIN_AGE")));
				}
			} else {
				familyController.requestJoinUserToFamily(user.getId(), signUpRequest.getFamilyToken());
			}
		} else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_NOT_SAVED_SERVER_ERROR")));
		}
		if (signUpRequest.isCreateFamilyAccount()) {
			if (!ObjectUtils.isEmpty(user.getEmail()) && null != family)
				notification = Notification.builder().emailId(user.getEmail()).mobileNumber(user.getPhone())
						.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).familyToken(family.getToken())
						.messageType(AppConstants.SIGNUP_CREATEFAMILY).build();
			kafkaService.publishToTopic(notification);

			auditLogsHelper.saveAuditLogs(user.getId(), AppConstants.SIGNUP_CREATEFAMILY, httpReq, Boolean.TRUE,
					AppConstants.EMPTY);
			return ResponseEntity.ok(new MessageResponse(
					MessageFormat.format(messages.get(AppConstants.SIGNUP_CREATE_FAMILY_RESPONSE_MSG),
							!ObjectUtils.isEmpty(family) && !ObjectUtils.isEmpty(family.getToken()) ? family.getToken()
									: AppConstants.EMPTY)));
		} else {
			kafkaService.publishToTopic(Notification.builder().emailId(user.getEmail())
					.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).mobileNumber(user.getPhone())
					.messageType(AppConstants.SIGNUP_JOINFAMILY).build());
			auditLogsHelper.saveAuditLogs(user.getId(), AppConstants.SIGNUP_JOINFAMILY, httpReq, Boolean.TRUE,
					AppConstants.EMPTY);
			return ResponseEntity.ok((new MessageResponse(messages.get(AppConstants.SIGNUP_JOIN_FAMILY_RESPONSE_MSG))));
		}
	}

	/**
	 * 
	 * @return all users
	 */
	@GetMapping("/users/all/{userId}")
	@PreAuthorize(value = AppConstants.HAS_ROLE_ROLE_ADMIN + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<List<User>> listAllUsers(@PathVariable(value = AppConstants.USER_ID)  Long userId) {
		return ResponseEntity.ok(authService.findAll());
	}

	/**
	 * 
	 * @param userId Long
	 * @return all user Families List<FamilyModel>
	 */
	@GetMapping("/users/all/byUserId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> listAllFamiliesAndUsersByUserId(@PathVariable(value = AppConstants.USER_ID)  Long userId) {
		User user = authService.getUserById(userId);
		if (!ObjectUtils.isEmpty(user)) {
			Map<String, List<UserResponse>> familyUserMap = authService.listAllFamiliesAndUsersByUserId(user);
			return ResponseEntity.ok(familyUserMap);
		} else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)));
		}
	}

	/**
	 * 
	 * @param userId Long
	 * @return userResponse
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/users/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> findUserById(@PathVariable(value = AppConstants.USER_ID) Long userId) {
		User user = authService.getUserById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		UserResponse userResponse = authService.findUserById(userId, user);
		return ResponseEntity.ok().body(userResponse);
	}

	/**
	 * PUT to POST UPDATED
	 * 
	 * @param userId      Long
	 * @param email       String
	 * @param password    String
	 * @param firstName   String
	 * @param lastName    String
	 * @param dateOfBirth String
	 * @param gender      String
	 * @param aboutMe     String
	 * @param languages   String
	 * @param nickname    String
	 * @return updated User Model
	 * @throws ResourceNotFoundException
	 */
	@PostMapping("/update/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateUser(@PathVariable(value = AppConstants.USER_ID) Long userId,
			@Valid @RequestBody UserUpdateDetails userDetails) {
		User user = authService.updateUserDetails(userId, userDetails);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		return ResponseEntity.ok(user);
	}

	/**
	 * PUT TO POST UPDATED
	 * 
	 * @param userId            Long
	 * @param userIdToBeUpdated Long
	 * @return Disable/Enable user in Fazeal app (Admin rights)
	 * @throws ResourceNotFoundException
	 */
	@PostMapping("/updateEnbaled/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateUserEnabled(@PathVariable(AppConstants.USER_ID) Long userId,
													@RequestParam(AppConstants.USER_ID_TO_BE_UPDATED) @NotNull(message = AppConstants.UPDATED_USER_ID_EMPTY) Long userIdToBeUpdated) {
		User updatedUser = authService.changeEnabledStatus(userIdToBeUpdated);
		if (ObjectUtils.isEmpty(updatedUser)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		return ResponseEntity.ok(updatedUser);
	}

//changed return data  to handle error messages in FE
	/**
	 * DELETE TO POST
	 * 
	 * @param userId Long
	 * @return Ok status with true/false if user deleted
	 */
	@Transactional
	@PostMapping("/delete/{userId}")
	@PreAuthorize(value = AppConstants.HAS_ROLE_ROLE_ADMIN + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteUser(@PathVariable(AppConstants.USER_ID) Long userId) {
		return ResponseEntity.ok(authService.deleteUserById(userId));
	}

	// removed enabled from login request since we need to check at DB level if user
	// is enabled or disabled
	/**
	 * 
	 * @param loginRequest userName,password String
	 * @param httpReq
	 * @return JwtResponse on success with OK status, bad request when fail
	 */
	@PostMapping("/signin")
	public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
			HttpServletRequest httpReq) {
		JwtResponse jwtResponse = authService.loginByUsernameAndPassword(loginRequest.getUsername(),
				loginRequest.getPassword());
		if (!ObjectUtils.isEmpty(jwtResponse)) {
			auditLogsHelper.saveAuditLogs(jwtResponse.getId(), AppConstants.SIGNIN, httpReq, Boolean.TRUE,
					AppConstants.EMPTY);
			return ResponseEntity.ok(jwtResponse);
		}
		return ResponseEntity.status(401)
				.body(new MessageResponse(messages.get(AppConstants.USER_PASSWORD_COMBINATION_OR_USER_NOT_FOUND)));
	}

	@PostMapping("/refreshtoken")
	public ResponseEntity<?> refreshToken(@Valid @RequestBody TokenRefreshRequest request) {
		String requestRefreshToken = request.getRefreshToken();
		return ResponseEntity.ok(authService.refreshAccessToken(requestRefreshToken));
	}

	@PostMapping("/logout")
	public ResponseEntity<MessageResponse> logoutUser( @RequestBody @Valid LogOutRequest logOutRequest) {
		refreshTokenService.deleteByUserId(logOutRequest.getUserId());
		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.LOGOUT_MSG)));
	}

	/**
	 * PUT TO POST UPDATED
	 * 
	 * @param email String
	 * @return OK status when reset token generated
	 */
	@PostMapping("/forgot/byEmail/{email}")
	public ResponseEntity<MessageResponse> forgotPassword(@PathVariable(AppConstants.EMAIL) @Email(message = AppConstants.EMAIL_INVALID) @Size(max = 50,message = AppConstants.EMAIL_SIZE_INVALID) String email) {

		User user = authService.generateTokenToResetUserPassword(email);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)));
		}

		notification = Notification.builder().emailId(user.getEmail())
				.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).resetToken(user.getResetToken())
				.messageType(AppConstants.PASSWORD_EMAIL_RESET).build();
		kafkaService.publishToTopic(notification);

		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.PASSWORD_EMAIL_SENT_SUCCESS)));

	}

//update return type
	/**
	 * 
	 * @param token String
	 * @return Ok status when token verified, BadRequest when not verified
	 */
	@GetMapping("/verifyToken/{token}")
	public ResponseEntity<Object> verifyResetToken(@PathVariable(value = AppConstants.TOKEN) @NotBlank(message = AppConstants.TOKEN_NOT_EMPTY) String token) {
		User user = authService.getUserByResetToken(token);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get(AppConstants.TOKEN_IS_NOT_FOUND)));
		} else {
			return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.TOKEN_VERIFIED)));
		}
	}

	/**
	 * 
	 * @param token    String
	 * @param password String new password
	 * @return Ok status when password reset done correctly
	 */
	@PostMapping("/reset")
	public ResponseEntity<MessageResponse> resetPassword(@RequestParam(AppConstants.TOKEN) @NotBlank(message = AppConstants.TOKEN_NOT_EMPTY) String token,
			@RequestParam(AppConstants.PASSWORD) @PasswordConstraint(message = AppConstants.PASSWORD_NOT_EMPTY) String password) {
		User user = authService.getUserByResetToken(token);
		if (ObjectUtils.isEmpty(user))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.PASSWORD_ERROR_USER_TOKEN_INVALID)));

		if (authService.isTokenExpired(user))
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body(new MessageResponse(messages.get(AppConstants.PASSWORD_TOKEN_EXPIRED)));

		authService.updateUserPassword(password, user);
		notification = Notification.builder().emailId(user.getEmail())
				.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE)
				.messageType(AppConstants.PASSWORD_EMAIL_RESET_DONE).build();
		kafkaService.publishToTopic(notification);
		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.PASSWORD_UPDATED_SUCCESS)));
	}

}