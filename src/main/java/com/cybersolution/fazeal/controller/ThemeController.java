package com.cybersolution.fazeal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.requestbody.ThemeBody;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.ThemeService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "Theme Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/theme")
public class ThemeController {
	@Autowired
	private ThemeService themeService;

	@Autowired
	Messages messages;

	/**
	 * 
	 * @param userId
	 * @param theme
	 * @return String response of updating user theme
	 */
	@PostMapping("/setUserTheme/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> updateUserTheme(@PathVariable(value = AppConstants.USER_ID) Long userId,
			@Valid @RequestBody ThemeBody theme) {
		if (!themeService.updateUserTheme(userId, theme.getTheme())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.THEME_UPDATED_SUCCESSFULLY)));
	}

	@GetMapping("/getUserTheme/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> getUserTheme(@PathVariable(value = AppConstants.USER_ID) Long userId) {
		return ResponseEntity.ok(new MessageResponse(themeService.getUserTheme(userId)));
	}

}
