package com.cybersolution.fazeal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.ShoppingListService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

/**
 * @author Faizan Events controller for family shopping list
 */

@Api(value = "Shopping List Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/shoppingList")
@Validated
public class ShoppingListController {
	@Autowired
	Messages messages;

	@Autowired
	ShoppingListService shoppingListService;
	
	@Autowired
	ShoppingListEntriesRepository shopItemsListRepo;
	
	@Autowired
	UserService userService;

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to get shopping list by id
	 * @Param Long id
	 */
	@GetMapping("/byId/{shoppingListId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getShoppingListById(
			@PathVariable(AppConstants.SHOPPING_LIST_ID) Long shoppingListId) {
		
		ShoppingList shopList = shoppingListService.findById(shoppingListId);

		if (null == shopList) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SHOP_LIST_ID_INVALID)));
		}

		return ResponseEntity.ok(shopList);
	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to get shopping list by family id
	 * @Param Long id
	 */
	@GetMapping("/byFamilyId/{familyId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getShoppingListByFamilyId(@PathVariable(AppConstants.FAMILY_ID) Long familyId,@PathVariable(AppConstants.USER_ID) Long userId) {
		boolean userFamilyRel= userService.isUserInFamily(userId, familyId );
		if(userFamilyRel) {
		List<ShoppingList> shopList = shoppingListService.getShoppingListForFamilyOnId(familyId);
		if (ObjectUtils.isEmpty(shopList)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SHOP_LIST_FAMILYID_INVALID)));

		}
		return ResponseEntity.ok(shopList);
		}else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_FAMILY_NOT_RELAION_EXISTS)));
		}
	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to save shopping list by family id
	 * @Param ShoppingListRequest
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value="/create/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> createShoppingList(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @RequestBody ShoppingListRequest shopListRequest) {

		ShoppingList shoppingList = shoppingListService.saveShoppingList(userId, shopListRequest);
		if (shoppingList == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SAVING_SHOPPING_LIST)));
		}

		return ResponseEntity.ok(shoppingList);

	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to update shopping list by family id only status and shoplist
	 *          name
	 * @Param ShoppingListUpdateRequest
	 */
	@PostMapping("/update/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateShoppingList(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @RequestBody ShoppingListUpdateRequest shopListRequest) {

		ShoppingList shoppingList = shoppingListService.updateShoppingList(userId, shopListRequest);
		if (shoppingList == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_UPDATING_SHOPPING_LIST)));

		}
		return ResponseEntity.ok(shoppingList);
	}

	/**
	 * @return Map (deleted,true)
	 * @apiNote Used to delete items from shopping list
	 * @Param Long id (itemId)
	 */
	@PostMapping("/deleteItems/{itemId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteShoppingListItems(@PathVariable(AppConstants.ITEM_ID) Long itemId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		ShoppingListEntries shopListItems = shopItemsListRepo.getByItemId(itemId);
		if (!ObjectUtils.isEmpty(shopListItems)) {
			if (shopListItems.getUserId().equals(userId)) {

				boolean status = shoppingListService.deleteShoppingListItemByItemId(itemId);
				if (status) {
					return ResponseEntity.status(HttpStatus.OK)
							.body(new MessageResponse(messages.get(AppConstants.SHOPPINGLIST_ITEM_DELETED)));

				} else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND)
							.body(new MessageResponse(messages.get(AppConstants.ERR_INVALID_REQUEST)));
				}
			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_ITEM_USER_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SHOP_LIST_ITEM_ID_NOT_FOUND)));
		}

	}

	/**
	 * @return Map (deleted,true)
	 * @apiNote Used to delete items from shopping list
	 * @Param Long id (itemId)
	 */
	@PostMapping("/deleteList/{shoppingListId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteShoppingList(@PathVariable(AppConstants.SHOPPING_LIST_ID) Long shoppingListId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		boolean status = shoppingListService.deleteShoppingListByShoppingListId(shoppingListId);
		if (status) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new MessageResponse(messages.get(AppConstants.SHOPPINGLIST_DELETED)));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERR_INVALID_REQUEST)));
		}
	}

}