package com.cybersolution.fazeal.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.pojo.UpdateNamePojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "User Information Settings Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/userInformation")
@Validated
public class UserInformationSettingsController {

	@Autowired
	Messages messages;

	@Autowired
	private UserService userService;

	/**
	 * 
	 * @return all prefixes
	 */
	@PostMapping("/namePrefixes/{userId}")
	@PreAuthorize(value = AppConstants.HAS_ROLE_ROLE_ADMIN + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> listAllNamePrefixes(@PathVariable(AppConstants.USER_ID) Long userId,
			@RequestParam(AppConstants.CONFIG_NAME) @NotBlank(message=AppConstants.PARAM_NAME)String name ) {
		return ResponseEntity.ok(userService.findAll(name));
	}
	
	@PostMapping("/updateName/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateName(@PathVariable(value = AppConstants.USER_ID) Long userId,
			@Valid @RequestBody UpdateNamePojo updateNamePojo) {

		if (!userService.setNames(userId, updateNamePojo.getNamePrefix(), updateNamePojo.getFirstName(),
				updateNamePojo.getLastName())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}

		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.NAME_UPDATED_SUCCESSFULLY)));
	}

}