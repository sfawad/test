package com.cybersolution.fazeal.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyResponse;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.FamilyCreatePoJo;
import com.cybersolution.fazeal.pojo.FamilyUpdatePoJo;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

/**
 * @author Faizan Family controller for fetching family details, saving and
 *         updating
 */

@Api(value = "Family Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@Validated
@RestController
@RequestMapping("/api/v1/family")
public class FamilyController {
	@Autowired
	FamilyService familyService;
	@Autowired
	Messages messages;
	@Autowired
	UserService userService;
	@Autowired
	AuditLogsHelper auditLogsHelper;

	@Value("${origin.domain}")
	private String domain = "";

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get family by id
	 * @Param Integer id
	 */
	@GetMapping("/byId/{familyId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getFamilyById(@PathVariable(AppConstants.FAMILY_ID) Long familyId) {

		Family family = familyService.getFamilyById(familyId);
		if (ObjectUtils.isEmpty(family)) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_FAMILY_NAME_INVALID")));
		}
		return new ResponseEntity<>(family, HttpStatus.OK);
	}

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get family by token
	 * @Param String name
	 */
	@GetMapping("/byToken/{token}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getFamilyByToken(@PathVariable(value = AppConstants.TOKEN) String token) {

		Family family = familyService.getFamilyInfoFromToken(token);
		if (ObjectUtils.isEmpty(family)) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND) + token));
		}
		FamilyResponse familyRes = familyService.getFamilyResponseByToken(family, token);
		return ResponseEntity.ok(familyRes);
	}

	/**
	 * Family Information
	 * 
	 * @param Integer id
	 * @return ResponseE.ntity (Family)
	 * @apiNote Used to update family by id
	 * @Param String info
	 * @Param Long phone
	 * @Param String address1
	 * @Param String address2
	 * @Param String city
	 * @Param String state
	 * @Param String zip
	 * @Param String country
	 * @param String about
	 */
	@PostMapping("/update/byId/{familyId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> updateFamilyInfo(@PathVariable(value = AppConstants.FAMILY_ID) Long familyId,
			@PathVariable(value = AppConstants.USER_ID) Long userId,
			@Valid @RequestBody FamilyUpdatePoJo familyUpdatePoJo) {

		Family family = familyService.getFamilyById(familyId);
		User user = userService.getById(userId);

		if (!ObjectUtils.isEmpty(family) && !ObjectUtils.isEmpty(user)) {
			boolean saved = familyService.updateFamilyInfo(familyId, familyUpdatePoJo, family, userId);
			if (!saved) {
				return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_RELATION_MANDATORY")));
			}
		} else if (ObjectUtils.isEmpty(family)) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND_UPDATE") + familyId));
		} else if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		return ResponseEntity.ok(new MessageResponse(messages.get("FAMILY_INFO_UPDATED_SUCCESSFULLY")));
	}

	/**
	 * @param String name
	 * @return ResponseEntity (Family)
	 * @apiNote Used to save family
	 * @Param String info
	 * @Param Long phone
	 * @Param String address1
	 * @Param String address2
	 * @Param String city
	 * @Param String state
	 * @Param String zip
	 * @Param String country
	 * @Param String relation
	 * @param String about
	 */
	@PostMapping("/save/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> createFamilyByFamilyAdmin(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @RequestBody FamilyCreatePoJo familyCreatePoJo, HttpServletRequest httpReq) {

		User user = userService.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		if (!ObjectUtils.isEmpty(user.getDob()) && !familyService.isUserAgeValidToCreateFamily(user)) {
			auditLogsHelper.saveAuditLogs(user.getId(), AppConstants.CREATEFAMILY, httpReq, false,
					messages.get("ERROR_FAMILY_CREATION_MIN_AGE"));
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_FAMILY_CREATION_MIN_AGE")));
		}

		Family family = familyService.createNewFamily(familyCreatePoJo, user);
		if (ObjectUtils.isEmpty(family.getFamilyId())) {
			auditLogsHelper.saveAuditLogs(user.getId(), AppConstants.CREATEFAMILY, httpReq, false,
					messages.get(AppConstants.ERROR_FAM_ID_NOT_FOUND));
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAM_ID_NOT_FOUND)));
		}

		auditLogsHelper.saveAuditLogs(user.getId(), AppConstants.CREATEFAMILY, httpReq, true, AppConstants.EMPTY);
		return ResponseEntity.ok(new MessageResponse(messages.get("FAMILY_CREATED_SUCCESSFULLY")));
	}

	/**
	 * @param Integer userId
	 * @return ResponseEntity (Family)
	 * @apiNote Used to request to join a family
	 * @Param String familyName
	 */
	@PostMapping("/requestFamily/join/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<MessageResponse> requestJoinUserToFamily(@PathVariable(AppConstants.USER_ID) Long userId,
			@RequestParam(AppConstants.TOKEN) @NotBlank(message = AppConstants.TOKEN_NOT_EMPTY) String token) {

		Family family = familyService.getFamilyInfoFromToken(token);
		User user = userService.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		if (!ObjectUtils.isEmpty(family)) {
			if (!family.getPendingUser().contains(user) && !user.getFamilies().contains(family)) {
				if (familyService.isUserRequestToJoinFamilyAdded(user, family)) {
					return ResponseEntity.ok(new MessageResponse(messages.get("FAMILY_JOINING_REQUEST")));
				} else {
					return ResponseEntity.badRequest().body(new MessageResponse(messages.get("FAMILY_UPDATE_FAILED")));
				}
			} else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("INFO_USER_IS_IN_FAMILY_PENDING_LIST")));
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)));
	}

	/**
	 * @param Integer userId
	 * @return ResponseEntity (Family)
	 * @apiNote Admin rights required for user to accept the incoming joining
	 *          request by another user
	 * @Param String familyName
	 */
	@PostMapping("/associateUser/user/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_ADMIIN_USER)
	public ResponseEntity<MessageResponse> associateUserToFamily(@PathVariable(AppConstants.USER_ID) Long userId,
			@RequestParam(AppConstants.TOKEN) @NotBlank(message=AppConstants.TOKEN_NOT_EMPTY) String token,
			@RequestParam(AppConstants.ADMIN_USER_ID) @NotNull(message=AppConstants.ADMIN_USER_ID_NOT_EMPTY) Long adminUserId) {

		Family family = familyService.getFamilyInfoFromToken(token);
		User user = userService.getById(userId);
		if (!ObjectUtils.isEmpty(family)) {
			if (!ObjectUtils.isEmpty(user) && !user.getFamilies().contains(family)) {
				if (!familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())) {
					return ResponseEntity.badRequest()
							.body(new MessageResponse(messages.get(AppConstants.LACK_OF_ADMIN_RIGHTS)));
				}
				if (familyService.associateUserToFamily(userId, family)) {
					return ResponseEntity.ok(new MessageResponse(messages.get("USER_ADDED_TO_FAMILY_MEMBERS")));
				} else {
					return ResponseEntity.badRequest()
							.body(new MessageResponse(messages.get("ERROR_USER_NOT_FOUND_FOR_APPROVAL")));
				}
			} else {
				return ResponseEntity.badRequest().body(new MessageResponse(messages.get("USER_ALREADY_IN_FAMILY")));
			}
		}
		return ResponseEntity.badRequest().body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)));
	}

	/**
	 * @param Integer userId
	 * @param Intger  userIdToRemove
	 * @return ResponseEntity (Family)
	 * @apiNote Admin rights required for user to disassociate user from family
	 * @Param String familyName
	 */
	@PostMapping("/disassociateUser/user/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_ADMIIN_USER)
	public ResponseEntity<MessageResponse> disassociateUserFromFamily(
			@PathVariable(AppConstants.USER_ID) Long userIdToRemove, @RequestParam(AppConstants.TOKEN) @NotBlank(message = AppConstants.TOKEN_NOT_EMPTY) String token,
			@RequestParam(AppConstants.ADMIN_USER_ID) @NotNull(message = AppConstants.ADMIN_USER_ID_NOT_EMPTY) Long adminUserId) {

		Family family = familyService.getFamilyInfoFromToken(token);
		if (!ObjectUtils.isEmpty(family)) {
			if (!familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())) {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get(AppConstants.LACK_OF_ADMIN_RIGHTS)));
			}

			if (familyService.disassociatUserFromFamily(userIdToRemove, token, family)) {
				return ResponseEntity.ok(new MessageResponse(messages.get("USER_REMOVED_FROM_FAMILY_PENDING_LIST")));
			} else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("ERROR_NO_USER_OR_USER_HAS_NO_RELATION_TO_FAMILY")));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)));
		}
	}

	/**
	 * @param Integer userId
	 * @param Intger  userIdToBeUpdated
	 * @param Boolean isAdmin = if u want to update user to admin or user
	 * @return ResponseEntity (Family)
	 * @apiNote Admin rights required to update user role in family
	 * @Param Integer familyId
	 */
	@PostMapping("/updateUserFamilyRole/{adminUserId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_ADMIIN_USER)
	public ResponseEntity<MessageResponse> updateUserRoleInFamily(
			@PathVariable(value = AppConstants.ADMIN_USER_ID) Long adminUserId,
			@RequestParam(AppConstants.IS_ADMIN) @NotNull(message = AppConstants.IS_NEW_USER_ROLE_ADMIN_NOT_EMPTY) boolean isNewUserRoleAdmin,
			@RequestParam(AppConstants.USER_ID_TO_BE_UPDATED) @NotNull(message = AppConstants.USER_ID_UPDATED_NOT_EMPTY) Long userIdToBeUpdated,
			@RequestParam(AppConstants.FAMILY_ID) @NotNull(message = AppConstants.FAMILY_ID_ISNULL) Long familyId) {


		User user = userService.getById(adminUserId);
		if (!ObjectUtils.isEmpty(user)) {
			Family family = familyService.getFamilyById(familyId);
			if (!ObjectUtils.isEmpty(family)) {
				if (!familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())) {
					return ResponseEntity.badRequest()
							.body(new MessageResponse(messages.get(AppConstants.LACK_OF_ADMIN_RIGHTS)));
				}
				familyService.changeUserRoleInFamily(isNewUserRoleAdmin, userIdToBeUpdated, family);
			} else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
		}
		return ResponseEntity.ok(new MessageResponse(messages.get("UPDATE_USER_FAMILY_ROLE")));
	}

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get all roles for users in a family on basis of familyId
	 * @Param Integer familyId
	 */
	@GetMapping("/allRolesInFamily/byFamilyId/{familyId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE)
	public ResponseEntity<Object> getAllRolesOfUsersInFamilyByFamilyId(
			@PathVariable(value = AppConstants.FAMILY_ID) Long familyId) {

		List<FamilyUserRoles> rolesOfUser = familyService.getAllUserRolesInFamily(familyId);
		if (ObjectUtils.isEmpty(rolesOfUser)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get("ERROR_NO_RECORD_FOUND")));
		}
		return ResponseEntity.ok(rolesOfUser);
	}

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get role for users in a family on basis of familyId
	 * @Param Integer familyId
	 */
	@GetMapping("/getUserRoleInFamily/byUserId/{userId}/familyId/{familyId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getRoleOfUsersInFamilyById(@PathVariable(value = AppConstants.USER_ID) Long userId,
			@PathVariable(value = AppConstants.FAMILY_ID) Long familyId) {

		FamilyUserRoles rolesOfUser = familyService.getRoleForUserInFamily(familyId, userId);
		if (ObjectUtils.isEmpty(rolesOfUser)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get("ERROR_NO_RECORD_FOUND")));
		}
		return ResponseEntity.ok(rolesOfUser);
	}

	/**
	 * @return ResponseEntity (Set<Family>)
	 * @apiNote Used to get families of user
	 * @Param Integer userId
	 */
	@GetMapping("/familiesbyuserid/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getUserFamiliesById(@PathVariable(value = AppConstants.USER_ID) Long userId) {

		User user = userService.getById(userId);
		if (ObjectUtils.isEmpty(user) || ObjectUtils.isEmpty(user.getFamilies())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}

		Set<Family> userFamilies = user.getFamilies();
		return ResponseEntity.ok(userFamilies);
	}

}