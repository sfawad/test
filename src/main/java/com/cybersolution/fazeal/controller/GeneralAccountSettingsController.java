package com.cybersolution.fazeal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.FamilyMemberRequest;
import com.cybersolution.fazeal.models.FamilyMembers;
import com.cybersolution.fazeal.models.SystemLanguages;
import com.cybersolution.fazeal.models.SocialMediaLinks;
import com.cybersolution.fazeal.models.SocialMediaLinksRequest;
import com.cybersolution.fazeal.pojo.AddSystemLanguagePojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.GeneralAccountSettingsService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

@Api(value = "General Account Info  Settings Controller")
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/generalAccount")
@Validated
public class GeneralAccountSettingsController {

	@Autowired
	Messages messages;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private GeneralAccountSettingsService generalAccountSettingsService;

	/**
	 
	 * @apiNote Used to add system language 
	 * @param userId   Long
	 * @param SystemLanguages   String
	 * @return ResponseEntity boolean
	 */
	@PostMapping("/addSystemLanguage/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> addSystemLanguage (@PathVariable(value = AppConstants.USER_ID) Long userId,
			@Valid @RequestBody AddSystemLanguagePojo addSystemLanguagePojo) {
		
		if (!userService.setSystemLanguage(userId, addSystemLanguagePojo.getSystemLanguage())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse(messages.get(AppConstants.ERROR_USER_NOT_FOUND)));
		}
		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.SYSTEM_LANGUAGE_UPDATED_SUCCESSFULLY)));
	}
	
	/**
	 
	 * @apiNote Used to get system language 
	 * @param userId   Long
	 * @return String systemLanguage
	 */
	@GetMapping("/getSystemLanguage/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getSystemLanguage (@PathVariable(value = AppConstants.USER_ID) Long userId) {
		
		return ResponseEntity.ok(new MessageResponse(userService.getSystemLanguage(userId)));
	}
	
	/**
	 * @return ResponseEntity (Social media links of User)
	 * @apiNote Used to add Social media links
	 * @param Long userId
	 * @Param SocialMediaLinksRequest
	 */
	@PostMapping(value="/socialMediaLinks/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> addSocialMediaLinks(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @RequestBody SocialMediaLinksRequest socialMediaLinksRequest) {

		SocialMediaLinks socialMediaLinks = generalAccountSettingsService.saveSocialMediaLinks(userId, socialMediaLinksRequest);
		if (socialMediaLinks == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SAVING_SOCIAL_MEDIA_LINKS)));
		}

		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.SOCIAL_MEDIA_LINKS_ADDED_SUCCESSFULLY)));

	}
	
	
	/**
	 * @return ResponseEntity Social media link
	 * @apiNote Used to get Social media link  by its id
	 * @Param socialMediaId
	 * @param userId
	 */
	@GetMapping(value="/getSocialMediaLinks/byId/{socialMediaId}/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getSocialMediaLinks(@PathVariable(AppConstants.USER_ID) Long userId,
			@PathVariable(AppConstants.SOCIAL_MEDIA_ID) Long socialMediaId) {

		SocialMediaLinks socialMediaLinks = generalAccountSettingsService.findBySocialMediaId(socialMediaId);
		
		if (!ObjectUtils.isEmpty(socialMediaLinks)) {
			if (userId.equals(socialMediaLinks.getUserId())) {

				return ResponseEntity.ok(socialMediaLinks);
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)));
		}
		
	}
	
	
	/**
	 * @return ResponseEntity Social media link
	 * @apiNote Used to get All Social media links by userId
	 * @param userId
	 */
	@GetMapping(value="/getUsersSocialMediaLinks/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getUsersAllSocialMediaLinks(@PathVariable(AppConstants.USER_ID) Long userId) {
		
		List<SocialMediaLinks> socialMediaLinks = generalAccountSettingsService.getSocialMediaLinks(userId);
		if (socialMediaLinks == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)));
		}

		return ResponseEntity.ok(socialMediaLinks);
		 
	}
	
	/**
	 * @return Boolean value
	 * @apiNote Used to delete Social media link
	 * @Param Long id (socialMediaId)
	 * @param Long userId
	 */
	@PostMapping("/deleteSocMediaLink/{socialMediaId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteSocialMediaLink(@PathVariable(AppConstants.SOCIAL_MEDIA_ID) Long socialMediaId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		SocialMediaLinks socialMediaLinks = generalAccountSettingsService.findBySocialMediaId(socialMediaId);
		if (!ObjectUtils.isEmpty(socialMediaLinks)) {
			if (userId.equals(socialMediaLinks.getUserId())) {

				if (generalAccountSettingsService.deleteSocMediaLink(socialMediaId)) {
					return ResponseEntity.status(HttpStatus.OK)
							.body(new MessageResponse(messages.get(AppConstants.SOCIAL_MEDIA_LINK_DELETED)));

				} else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
				}
			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINK_USER_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)));
		}

	}
	

	/**
	 * @return ResponseEntity (FamilyMembers for User)
	 * @apiNote Used to add Family Member
	 * @Param FamilyMemberRequest
	 */
	@PostMapping(value="/addFamilyMember/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> addFamilyMember(@PathVariable(AppConstants.USER_ID) Long userId,
			@Valid @RequestBody FamilyMemberRequest familyMemberRequest) {

		FamilyMembers familyMembers = generalAccountSettingsService.saveFamilyMembers(userId, familyMemberRequest);
		if (familyMembers == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_SAVING_FAMILY_MEMBER)));
		}

		return ResponseEntity.ok(new MessageResponse(messages.get(AppConstants.FAMILY_MEMBER_ADDED_SUCCESSFULLY)));

	}
	
	
	/**
	 * @return ResponseEntity (FamilyMembers for User)
	 * @apiNote Used to get All Family Members of User
	 * @Param Long userId
	 */
	@GetMapping(value="/getUsersFamMember/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getUsersAllFamilyMembers(@PathVariable(AppConstants.USER_ID) Long userId) {

		List<FamilyMembers> familyMembers = generalAccountSettingsService.getFamilyMembers(userId);
		if (ObjectUtils.isEmpty(familyMembers)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)));
		}

		return ResponseEntity.ok(familyMembers);

	}
	
	/**
	 * @return ResponseEntity (Family member)
	 * @apiNote Used to get Family Member  with its id
	 * @Param Long family member Id
	 * @param Long userId
	 */
	@GetMapping(value="/getFamilyMember/byId/{famMemberId}/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> getFamilyMember(@PathVariable(AppConstants.USER_ID) Long userId,
			@PathVariable(AppConstants.FAMILY_MEMBER_ID) Long famMemberId) {

		FamilyMembers familyMember = generalAccountSettingsService.findByfamilyMemberId(famMemberId);
		
		if (!ObjectUtils.isEmpty(familyMember)) {
			if (userId.equals(familyMember.getUserId())) {

				return ResponseEntity.ok(familyMember);
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)));
		}
		
	}
	
	/**
	 * @return Boolean value
	 * @apiNote Used to delete Family member
	 * @Param Long id (family member Id)
	 * @param Long userId
	 */
	@PostMapping("/deleteFamMember/{famMemberId}/userId/{userId}")
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> deleteFamilyMember(@PathVariable(AppConstants.FAMILY_MEMBER_ID) Long famMemberId,
			@PathVariable(AppConstants.USER_ID) Long userId) {

		FamilyMembers familyMembers = generalAccountSettingsService.findByfamilyMemberId(famMemberId);
		if (!ObjectUtils.isEmpty(familyMembers)) {
			if (userId.equals(familyMembers.getUserId())) {

				if (generalAccountSettingsService.deleteFamilyMember(famMemberId)) {
					return ResponseEntity.status(HttpStatus.OK)
							.body(new MessageResponse(messages.get(AppConstants.FAMILY_MEMBER_DELETED)));

				} else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND)
							.body(new MessageResponse(messages.get(AppConstants.ERROR_INVALID_REQUEST)));
				}
			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_MEMBER_USER_ASSOCIATION)));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)));
		}

	}
	
	/**
	 * 
	 * @return all System Languages
	 */
	@GetMapping("/systemLanguages/{userId}")
	@PreAuthorize(value = AppConstants.HAS_ROLE_ROLE_ADMIN + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<List<SystemLanguages>> listAllSystemLanguages(@PathVariable(AppConstants.USER_ID) Long userId) {
		return ResponseEntity.ok(generalAccountSettingsService.findAll());
	}
	
	
}