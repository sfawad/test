package com.cybersolution.fazeal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.Voting;
import com.cybersolution.fazeal.models.VotingResponse;
import com.cybersolution.fazeal.pojo.SaveVotingPojo;
import com.cybersolution.fazeal.pojo.UpdateVotingPojo;
import com.cybersolution.fazeal.pojo.UpdateVotingSelectionPojo;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.VotingService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

import io.swagger.annotations.Api;

/**
 * @author Faizan - Voting controller to vote and see the results
 */

@CrossOrigin(origins = "*", maxAge = 4300)
@Api(value = "Voting Controller")
@RestController
@RequestMapping("/api/v1/voting")
@Validated
public class VotingController {
	@Autowired
	Messages messages;

	@Autowired
	private VotingService votingService;

	@Autowired
	private UserService userService;

	/**
	 * list all family voting's by familyId
	 * 
	 * @param familyId Long
	 * @return all family voting's by familyId
	 */
	@GetMapping(value="/byFamilyId/{familyId}/userId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE +  AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> listAllVotingsByFamilyId(
			@PathVariable(value = AppConstants.FAMILY_ID) Long familyId, @PathVariable(value = AppConstants.USER_ID) Long userId) {
		
		boolean userFamilyRel= userService.isUserInFamily(userId, familyId );
			if(userFamilyRel) {
		List<VotingResponse> votingRespList = votingService.getAllFamilyVotingsByFamilyId(familyId);
		if (votingRespList.isEmpty()) {
			return ResponseEntity.badRequest().body(messages.get("ERROR_NO_VOTING_RECORD_FOUND"));
		} else {
			return ResponseEntity.ok(votingRespList);
		}
		}else {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_FAMILY_NOT_RELAION_EXISTS")));
		}
	}

	/**
	 * @param userId   Long
	 * @param familyId Long
	 * @param title    voting title
	 * @param options  user options
	 * @return saved user voting
	 */
	@PostMapping(value="/save/userId/{userId}",produces = { MediaType.APPLICATION_JSON_VALUE } )
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> saveUserVoting(@PathVariable(AppConstants.USER_ID) Long userId, @RequestBody @Valid SaveVotingPojo saveVotingPojo) {

		if (!userService.isUserInFamily(userId, saveVotingPojo.getFamilyId())) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_FAMILY_NOT_RELAION_EXISTS")));
		}
		return ResponseEntity.ok(votingService.saveUserVoting(userId, saveVotingPojo.getFamilyId(), saveVotingPojo.getTitle(), saveVotingPojo.getOptions()));
	}

	/**
	 * @author Faizan,,,, can you add description of this method
	 * @param voteId  voting Id long
	 * @param options String[]
	 * @param userId  long
	 * @param type    'A' for (increase) adding votes, 'D' (decrease) remove vote
	 * @return the updated vote
	 */
	@PostMapping(value="/updateoptions/{voteId}/userId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateVotingOptionsByVoteId(@PathVariable(AppConstants.VOTE_ID) Long voteId,@PathVariable(AppConstants.USER_ID) Long userId,
			@RequestBody @Valid UpdateVotingPojo updateVotingPojo) {

		if (AppConstants.VOTING_ADD.equalsIgnoreCase(updateVotingPojo.getType()) || AppConstants.VOTING_REMOVE.equalsIgnoreCase(updateVotingPojo.getType())) {
		Voting voting = votingService.updateVotingOptionsByVoteId(voteId, updateVotingPojo.getOptions(), userId, updateVotingPojo.getType());
		if (null == voting) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_OR_NO_VOTING_OR_USER_NOT_VALID")));
		}
		return ResponseEntity.ok(voting);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new MessageResponse(messages.get(AppConstants.ERROR_VOTING_UPDATE_OPTIONS_TYPE)));
		}
	}

	/**
	 * @author Faizan,,,, can you add description of this method
	 * @param voteId voting id Long
	 * @param userId Long of user who own the voting
	 * @param option String option to be updated
	 * @return
	 */
	@PostMapping(value="/vote/byOptionId/{voteId}/userId/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = AppConstants.HAS_USER_OR_ADMIN_ROLE + AppConstants.AUTHENTICATED_USER)
	public ResponseEntity<Object> updateVoting(@PathVariable(AppConstants.VOTE_ID) Long voteId,
			@PathVariable(AppConstants.USER_ID) Long userId,   @RequestBody @Valid UpdateVotingSelectionPojo updateVotingSelectionPojo) {

		Voting voting = votingService.updateVotingByIdUserIdOption(voteId, userId, updateVotingSelectionPojo.getOptions());
		if (voting == null) {
			return ResponseEntity.badRequest().body(new MessageResponse(
					messages.get("ERROR_USER_NOT_EXIST_OR_NOT_IN_FAMILY_OR_ALREADY_VOTED_OR_VOTE_NOT_FOUND")));
		}
		return ResponseEntity.ok(voting);
	}

}