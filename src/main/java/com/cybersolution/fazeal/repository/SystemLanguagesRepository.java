package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.SystemLanguages;

@Repository
public interface SystemLanguagesRepository extends JpaRepository<SystemLanguages, Long> {

	@Query(value = "select language from SystemLanguages")
	String[] findByLanguage();

}
