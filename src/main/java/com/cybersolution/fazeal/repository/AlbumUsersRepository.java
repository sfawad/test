package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.AlbumModelUser;

@Repository
public interface AlbumUsersRepository extends JpaRepository<AlbumModelUser, Long> {

}
