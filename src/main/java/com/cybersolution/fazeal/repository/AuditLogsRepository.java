package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.AuditLogs;

@Repository
public interface AuditLogsRepository extends JpaRepository<AuditLogs, Long> {

  @Query("Select c from AuditLogs c where c.userId=?1")
  public List<AuditLogs> findAuditLogsByUserId(@Param("id") Long id);

}
