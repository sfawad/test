package com.cybersolution.fazeal.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cybersolution.fazeal.models.FamilyMembers;

public interface GeneralAccountSettingsFamilyRepository extends JpaRepository<FamilyMembers, Long> {

	@Query(value = "Select c from FamilyMembers c where c.userId = ?1")
	List<FamilyMembers> findByUserId(Long userId);

	@Query(value = "Select c from FamilyMembers c where c.id = ?1")
	FamilyMembers findByFamMembersId(Long famMemberId);

	
}