package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.VotingOptions;


@Repository
public interface VotingOptionsRepository extends JpaRepository<VotingOptions, Long> {
	
}
