package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ConfigData;

@Repository
public interface ConfigDataRepository extends JpaRepository<ConfigData, Long> {

	 @Query(value = "select * from config_data where name = :namePrefix", nativeQuery= true)
     ConfigData findByNamePrefix(String namePrefix);

}
