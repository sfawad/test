package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Voting;


@Repository
public interface VotingRepository extends JpaRepository<Voting, Long> {
	
	@Query(value = "Select c from Voting c where c.familyId = ?1")
	List<Voting> getByFamilyId(Long familyId);
	
}
