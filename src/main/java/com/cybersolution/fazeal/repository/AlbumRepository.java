package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.AlbumModel;

@Repository
public interface AlbumRepository extends JpaRepository<AlbumModel, Long> {

	List<AlbumModel> findAll();

	@Query("Select c from AlbumModel c where c.userId=?1")
	public List<AlbumModel> findByUserId(@Param("id") Long id);

	@Query("Select c from AlbumModel c where c.albumName=?1")
	public AlbumModel findByAlbumName(String albumName);

	@Query("Select c from AlbumModel c where c.albumName=?1 and c.userId=?2")
	public AlbumModel findByAlbumNameAndUserId(String albumName, Long userId);

	@Query("Select c from AlbumModel c where c.albumId=?1 and c.userId=?2")
	public AlbumModel findByAlbumIdAndUserId(Long albumId, Long userId);

	@Query("Select c from AlbumModel c where c.albumId in (select albumId from ImageModel where id in"
			+ "(SELECT imagemodelId from ImageModelUser d where  d.userId=:userSharedId)) and c.userId <> :userSharedId")
	public List<AlbumModel> findAlbumsByUsersIDS(Long userSharedId);

	@Query("Select c from AlbumModel c where c.albumId in (select albumId from ImageModel where id in"
			+ "(SELECT imagemodelId from ImageModelFamilies d where  d.familyId=:familyId)) and c.userId <> :userId")
	public List<AlbumModel> findAlbumsByFamilyIDS(Long familyId, Long userId);

	@Query(value = "Select albumId from AlbumModel p where p.userId=?1")
	public List<Long> findAlbumsIdByUserId(Long userId);

	@Query("Select c from AlbumModel c where c.albumId=?1")
	AlbumModel getByAlbumId(Long id);

	@Query("Select c from AlbumModel c where c.albumName like %?1% and c.userId=?2")
	public List<AlbumModel> findAlbumIfContainsNameWithUserId(String albumName, Long userId);

	@Query("Select count(a) from AlbumModel a where a.albumName =:name and a.userId=:userId")
	public int countAlbumByName(@Param("name") String name, @Param("userId") Long userId);
}
