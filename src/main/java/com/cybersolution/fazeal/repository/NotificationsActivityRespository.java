package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.NotificationsActivity;

@Repository 
public interface NotificationsActivityRespository extends JpaRepository<NotificationsActivity, Long>{


	 @Query(value = "select * from notifications_activity where user_id = :userId ", nativeQuery= true)
	  public List<NotificationsActivity> findNotificationsForUser(@Param("userId") Long userId);
	 
	 @Query(value = "select * from notifications_activity where activity_type = :type", nativeQuery= true)
	  public List<NotificationsActivity> findNotificationsByType(@Param("type") String type);

}
