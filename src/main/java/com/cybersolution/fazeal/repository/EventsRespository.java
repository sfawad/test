package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Events;

@Repository 
public interface EventsRespository extends JpaRepository<Events, Long>{
	
	@Query("Select c from Events c where c.userId = ?1 ")
	List<Events> findByUserId(Long userId);
	
	@Query("Select c from Events c where c.userId = ?1 and c.isShared=0")
	List<Events> findUnsharedEventsForUserId(Long userId);

	Long countById(Long id);
}
