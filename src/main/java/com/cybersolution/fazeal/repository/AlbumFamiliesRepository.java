package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.AlbumModelFamilies;

@Repository
public interface AlbumFamiliesRepository extends JpaRepository<AlbumModelFamilies, Long> {

}
