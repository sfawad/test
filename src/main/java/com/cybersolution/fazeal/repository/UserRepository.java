package com.cybersolution.fazeal.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	@Query(value = "Select * from users where id in (select user_id from user_family where family_id = :familyId) and id <> :userId", nativeQuery = true)
	List<User> getUsersListForAFamily(@Param("familyId") Long familyId, @Param("userId") Long userId);

	@Query(value = "Select * from users where email= :email", nativeQuery = true)
	User getUserFromEmail(@Param("email") String email);

	@Query(value = "Select * from users where reset_token= :token", nativeQuery = true)
	User getUserFromToken(@Param("token") String token);

	@Query(value = "select * from users where DATE(created_at) = :created_at", nativeQuery = true)
	List<User> getUserByCreatedAt(@Param("created_at") String createdAt);

	@Query(value = "select * from users where DATE(dob) like %:dob%", nativeQuery = true)
	List<User> getUserByDOB(@Param("dob") String dob);

	@Query(value = "Select * from users where id in (select user_id from user_family where family_id = :familyId)", nativeQuery = true)
	List<User> getAllUsersListForAFamily(@Param("familyId") Long familyId);

	@Query(value = "Select * from users where id = :userId", nativeQuery = true)
	User findByUserId(Long userId);

}
