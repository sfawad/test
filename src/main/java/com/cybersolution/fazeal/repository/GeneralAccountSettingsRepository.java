package com.cybersolution.fazeal.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cybersolution.fazeal.models.SocialMediaLinks;

public interface GeneralAccountSettingsRepository extends JpaRepository<SocialMediaLinks, Long> {

	@Query(value = "Select c from SocialMediaLinks c where c.userId = ?1")
	List<SocialMediaLinks> findByUserId(Long userId);

	Long countById(Long socialMediaId);

	@Query(value = "Select c from SocialMediaLinks c where c.id = ?1")
	SocialMediaLinks findBySocialMediaId(Long socialMediaId);

}