package com.cybersolution.fazeal.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ShoppingListEntries;

@Repository
public interface ShoppingListEntriesRepository extends JpaRepository<ShoppingListEntries, Long> {

  @Query(value = "Select c from ShoppingListEntries c where c.shoppinglistId = ?1")
  Set<ShoppingListEntries> getShoppingListEntriesByListId(Long shoppingListId);

  @Query(value = "Select c from ShoppingListEntries c where c.id = ?1")
  ShoppingListEntries getByItemId(Long itemId);

}
