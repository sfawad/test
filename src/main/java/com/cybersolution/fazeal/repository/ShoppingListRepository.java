package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ShoppingList;

@Repository
public interface ShoppingListRepository extends JpaRepository<ShoppingList, Long> {

	@Query(value = "Select c from ShoppingList c where c.familyId = ?1")
	List<ShoppingList> getShoppingListForFamilyOnId(Long familyId);

	@Query(value = "Select c from ShoppingList c where c.id = ?1")
	ShoppingList getByShoppingListId(Long shoppingListId);

}
