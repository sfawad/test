package com.cybersolution.fazeal.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);

	Role getByName(ERole name);
}
