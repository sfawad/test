package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.VotingOptionsSelection;


@Repository
public interface VotingOptionsSelectionRepository extends JpaRepository<VotingOptionsSelection, Long> {
	
}
