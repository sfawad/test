package com.cybersolution.fazeal.repository;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Family;

@Repository
public interface FamilyRespository extends JpaRepository<Family, Long> {

	@Query(value = "Select c from Family c where c.familyId = ?1")
	public Family getFamilyById(Long id);

	@Query(value = "Select c from Family c where c.token = ?1")
	public Family getFamilyInfoFromToken(String token);

	@Query(value = "SELECT  familyId, name FROM Family WHERE name LIKE %?1% and isGroup = 1", nativeQuery = true)
	public Map<Integer, String> getGroupsByName(@Param("name") String name);
}
