package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ImageModelUser;

@Repository
public interface ImageUsersRepository extends JpaRepository<ImageModelUser, Long> {

	@Modifying
	@Query("DELETE from ImageModelUser c where  c.imagemodelId=:imagemodelId")
	Integer deleteImg(Long imagemodelId);

	@Query("SELECT imagemodelId from ImageModelUser c where  c.userId=:userId")
	Long[] getImageByUseryId(Long userId);
}
