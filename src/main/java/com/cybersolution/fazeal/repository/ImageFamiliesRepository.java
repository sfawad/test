package com.cybersolution.fazeal.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModelFamilies;

@Repository
public interface ImageFamiliesRepository extends JpaRepository<ImageModelFamilies, Long> {

  @Modifying
  @Query("DELETE from ImageModelFamilies c where  c.imagemodelId=:imageId")
  Integer deleteImgFamily(Long imageId);

  @Query("SELECT c from ImageModelFamilies c where  c.imagemodelId=:familyIds")
  List<ImageModelFamilies> getImageModel(Set<Family> familyIds);

  @Query("SELECT imagemodelId from ImageModelFamilies c where  c.familyId=:familyId")
  Long[] getImageByFamilyId(Long familyId);

}
