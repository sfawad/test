package com.cybersolution.fazeal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cybersolution.fazeal.models.ImageModel;

public interface ImageRepository extends JpaRepository<ImageModel, Long> {
	List<ImageModel> findAll();

	@Query("Select c from ImageModel c where c.userId=?1")
	List<ImageModel> findByUserId(Long id);

	@Transactional
	@Modifying
	@Query("Update ImageModel c set c.userId=null where c.userId=?1")
	Integer setUserIdNull(Long userId);

	Long countById(Long id);

	@Query(nativeQuery = true, value = "SELECT * FROM image_model as e WHERE e.user_id =:id and e.type IN (:type)")
	List<ImageModel> findByUserIdAndType(@Param("id") Long id, @Param("type") String[] type);

	@Query("select c from ImageModel c where c.albumId=?1")
	public List<ImageModel> getByAlbumId(Long albumId);

	@Query("Select c from ImageModel c where c.localPath=?1 and c.userId=?1 ")
	ImageModel getByLocalPathAndUserId(String localPath, Long userId);

	ImageModel getByLocalPath(String localPath);
}
