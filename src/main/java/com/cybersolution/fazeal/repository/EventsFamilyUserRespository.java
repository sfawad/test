package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.EventsFamilyUser;

@Repository 
public interface EventsFamilyUserRespository extends JpaRepository<EventsFamilyUser, Long>{

	 @Query(value = "select * from events_user_family where event_id = ?1", nativeQuery= true)
	 List<EventsFamilyUser> findEventsByEventId(Long eventId);
	 
	 @Query(value = "select * from events_user_family where family_id = ?1", nativeQuery= true)
	 List<EventsFamilyUser> findEventsByFamilyId(Long familyId);
	 
	 @Query(value = "select * from events_user_family where family_id = ?1 and user_id = ?2", nativeQuery= true)
	 List<EventsFamilyUser> findEventsByFamilyAndUserId(Long familyId, Long userId);
}
