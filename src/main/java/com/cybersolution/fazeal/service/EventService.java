package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.Events;
import com.cybersolution.fazeal.models.EventsCurrentUserResponse;
import com.cybersolution.fazeal.models.EventsRelationResponse;
import com.cybersolution.fazeal.pojo.CreateEventPojo;

public interface EventService {

  boolean updateEvent(Long id, String event, boolean isShared);

  List<Events> findByUserId(Long userId);

  Events findByEventId(Long eventId);

  boolean deleteEvent(Long eventId);

  List<EventsRelationResponse> getUserEventsPlainResponse(Long userId);

  EventsCurrentUserResponse getUserEvents(Long userId);

  List<Events> getUserWithFamilyEvents(Long familyId);

  void setEventDetails(Events events, String date, String endTime, String startTime, boolean isShared, String event);

  void deleteEventsFamilyUser(Long eventId);

  void saveEventsFamilyUser(Events events, Long userId, Long[] familyId);

  Events saveEvent(Long userId, CreateEventPojo createEventPojo);
}
