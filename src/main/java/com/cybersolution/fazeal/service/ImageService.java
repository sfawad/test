package com.cybersolution.fazeal.service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.multipart.MultipartFile;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.User;

public interface ImageService {

	public String uploadImages(Date date, MultipartFile multipartFiles, Long userId, String type, String album,
			String fileNameReturned, String description, AlbumModel albumModel);

	public boolean imagesSharedWithUsersAndFamilies(List<Long> familyIds, ImageModel image, List<Long> userIds,
			User user, String string);

	public List<ImageModel> findByUserId(Long userId);

	public List<ImageModel> findByUserIdAndType(Long id, String[] type);

	public ImageModel getImageById(Long imageId);

	public ConcurrentHashMap<String, Set<AlbumModel>> getAlbumImagesWithUserId(Long userId);

	public void deleteByImgId(ImageModel image);

	public ImageModel getImageByImageLocalPath(String localPath, Long userId);

	public ImageModel getImgByImageLocalPath(String localPath);

	public void save(ImageModel image);

	public void uploadFamilyGroupProfileOrDashboardImage(MultipartFile fileImage, Family family, String type, Date date,
			Long userId);

	public void markImagedelete(ImageModel image, Long userId);

	public AlbumModel getDeletedAlbumForUser(Long userId);

	boolean restoreImageToAlbum(ImageModel image);
}
