package com.cybersolution.fazeal.service;

public interface ThemeService {
	boolean updateUserTheme(Long userId, String theme);

	String getUserTheme(Long userId);
}
