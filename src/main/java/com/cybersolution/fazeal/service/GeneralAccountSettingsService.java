package com.cybersolution.fazeal.service;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.FamilyMemberRequest;
import com.cybersolution.fazeal.models.FamilyMembers;
import com.cybersolution.fazeal.models.SystemLanguages;
import com.cybersolution.fazeal.models.SocialMediaLinks;
import com.cybersolution.fazeal.models.SocialMediaLinksRequest;

@Service
public interface GeneralAccountSettingsService {

	SocialMediaLinks saveSocialMediaLinks(Long userId, @Valid SocialMediaLinksRequest socialMediaLinksRequest);

	List<SocialMediaLinks> getSocialMediaLinks(Long userId);

	SocialMediaLinks findBySocialMediaId(Long socialMediaId);

	boolean deleteSocMediaLink(Long socialMediaId);

	FamilyMembers saveFamilyMembers(Long userId, @Valid FamilyMemberRequest familyMemberRequest);

	List<FamilyMembers> getFamilyMembers(Long userId);

	FamilyMembers findByfamilyMemberId(Long famMemberId);

	boolean deleteFamilyMember(Long famMemberId);

	List<SystemLanguages> findAll();

	

}