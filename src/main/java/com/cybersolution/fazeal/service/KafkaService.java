package com.cybersolution.fazeal.service;

import com.cybersolution.fazeal.pojo.Notification;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface KafkaService {

   void publishToTopic(Notification message);
}
