package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.NotificationsActivity;

public interface NotificationService {

  public List<NotificationsActivity> getNotificationsForUsers(Long userId);
}
