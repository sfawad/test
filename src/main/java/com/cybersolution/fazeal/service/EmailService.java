package com.cybersolution.fazeal.service;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {
	
	void sendEmail(SimpleMailMessage email);
	void sendMail(String from, String to, String subject, String msg);
}
