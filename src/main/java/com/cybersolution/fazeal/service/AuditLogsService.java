package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.AuditLogs;

public interface AuditLogsService {

  public List<AuditLogs> findAuditLogsByUserId(Long userId);
  
  void save(AuditLogs auditLogs);


}
