package com.cybersolution.fazeal.service;

import java.util.Optional;

import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Role;

public interface RoleService {
	public Optional<Role> getUserRoleFromDB(ERole role);
}
