package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyResponse;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.FamilyCreatePoJo;
import com.cybersolution.fazeal.pojo.FamilySaveInfoPoJo;
import com.cybersolution.fazeal.pojo.FamilyUpdatePoJo;

public interface FamilyService {

	public Family getFamilyById(Long id);

	public Family getFamilyInfoFromToken(String token);

	public Family save(Family family);

	public Family saveFamilyInfo(FamilySaveInfoPoJo familySaveInfoPoJo);

	public FamilyResponse getFamilyResponseByToken(Family family, String token);

	public boolean updateFamilyInfo(Long familyId, FamilyUpdatePoJo familyUpdatePoJo, Family family, Long userId);

	public Family createNewFamily(FamilyCreatePoJo familyCreatePoJo, User user);

	public boolean isUserAgeValidToCreateFamily(User user);

	public boolean isUserRequestToJoinFamilyAdded(User userId, Family family);

	public boolean isUserAdminforFamily(Long adminUserId, Long familyId);

	public boolean associateUserToFamily(Long userId, Family family);

	public boolean disassociatUserFromFamily(Long userIdToRemove, String token, Family family);

	public void changeUserRoleInFamily(boolean isNewUserRoleAdmin, Long userIdToBeUpdated, Family family);

	public List<FamilyUserRoles> getAllUserRolesInFamily(Long id);

	public FamilyUserRoles getRoleForUserInFamily(Long familyId, Long userId);

	public Long generateFamilyToken(Long familyId);

	public Family createNewFamilyForNewUserWhenRegister(User user);
}
