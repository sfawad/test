package com.cybersolution.fazeal.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.cybersolution.fazeal.util.AppConstants;
import com.google.common.io.Files;

@Service
public class AmazonClient {

	private AmazonS3 s3client;

	@Value("${amazonProperties.endpointUrl}")
	private String endpointUrl;
	@Value("${amazonProperties.bucketName}")
	private String bucketName;
	@Value("${amazonProperties.accessKey}")
	private String accessKey;
	@Value("${amazonProperties.secretKey}")
	private String secretKey;

	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.s3client = new AmazonS3Client(credentials);
	}

	public String uploadFile(MultipartFile multipartFile, Long userId, String folderName) {
		String fileName = "";
		try {
			File file = convertMultiPartToFile(multipartFile);
			fileName = generateFileName(multipartFile);
			if (ObjectUtils.isEmpty(folderName)) {
				fileName = userId + AppConstants.SLASH + folderName + AppConstants.SLASH + fileName;
			} else {
				fileName = userId + AppConstants.SLASH + fileName;
			}
			uploadFileTos3bucket(fileName, file);
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		try {
			fos.write(file.getBytes());
		} finally {
			fos.close();
		}
		return convFile;
	}

	private String generateFileName(MultipartFile multiPart) {
		if (!ObjectUtils.isEmpty(multiPart) && !ObjectUtils.isEmpty(multiPart.getOriginalFilename()))
			return new Date().getTime() + AppConstants.HYPHEN
					+ multiPart.getOriginalFilename().replace(AppConstants.SPACE, AppConstants.UNSERSCORE);
		else
			return "";
	}

	private void uploadFileTos3bucket(String fileName, File file) {
		s3client.putObject(
				new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
	}

	public String deleteFileFromS3Bucket(String fileName) {
		s3client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
		return "Successfully deleted";
	}

	public String getExtensionByApacheCommonLib(String filename) {
		return Files.getFileExtension(filename);
	}
}
