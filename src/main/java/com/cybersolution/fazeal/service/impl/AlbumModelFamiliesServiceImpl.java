package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.repository.AlbumFamiliesRepository;
import com.cybersolution.fazeal.service.AlbumModelFamiliesService;

@Service
@Transactional
public class AlbumModelFamiliesServiceImpl implements AlbumModelFamiliesService {

  @Autowired
  AlbumFamiliesRepository albumFamiliesRepository;

  @Override
  public AlbumModelFamilies addFamily(AlbumModelFamilies families) {
    return albumFamiliesRepository.save(families);
  }
}