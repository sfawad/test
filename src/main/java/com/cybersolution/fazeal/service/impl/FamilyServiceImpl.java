package com.cybersolution.fazeal.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyResponse;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.FamilyCreatePoJo;
import com.cybersolution.fazeal.pojo.FamilySaveInfoPoJo;
import com.cybersolution.fazeal.pojo.FamilyUpdatePoJo;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.util.AppConstants;
import com.cybersolution.fazeal.util.Utils;

@Service
public class FamilyServiceImpl implements FamilyService {
	private static final String ROLEADMIN = "ROLE_ADMIN";

	@Autowired
	private FamilyRespository familyRespository;
	@Autowired
	private ImageRepository imageRepo;
	@Autowired
	private FamilyUserRoleRespository familyUsersRoleRepo;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepo;
	@Autowired
	private EmailService emailService;
	@Autowired
	private Messages messages;
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	KafkaService kafkaService;

	@Autowired
	Notification notification;

	@Value("${family-create-token-counter}")
	private Long token = 0L;
	@Value("${family-create-minage}")
	private Integer minAge;
	@Value("${origin.domain}")
	private String domain;

	/**
	 * @param id Long family Id
	 * @return Family by id
	 */
	@Override
	public Family getFamilyById(Long id) {
		return familyRespository.getFamilyById(id);
	}

	/**
	 * @param id String family token
	 * @return Family by token
	 */
	@Override
	public Family getFamilyInfoFromToken(String token) {
		return familyRespository.getFamilyInfoFromToken(token);
	}

	/**
	 * @param Family family
	 * @return Family saved
	 */
	@Override
	public Family save(Family family) {
		return familyRespository.save(family);
	}

	/**
	 * @param String name
	 * @param Long   phone
	 * @param String address1
	 * @param String address2
	 * @param String city
	 * @param String state
	 * @param String zip
	 * @param String country
	 * @return Family saveFamilyInfo
	 */
	@Override
	public Family saveFamilyInfo(FamilySaveInfoPoJo familySaveInfoPoJo) {
		return familyRespository.save(new Family(familySaveInfoPoJo.getName(), familySaveInfoPoJo.getPhone(),
				familySaveInfoPoJo.getAddress1(), familySaveInfoPoJo.getAddress2(), familySaveInfoPoJo.getCity(),
				familySaveInfoPoJo.getState(), familySaveInfoPoJo.getZip(), familySaveInfoPoJo.getCountry()));
	}

	/**
	 * @param Family family
	 * @param String token
	 * @return FamilyResponse
	 */
	@Override
	public FamilyResponse getFamilyResponseByToken(Family family, String token) {
		FamilyResponse familyRes = FamilyResponse.builder().familyId(family.getFamilyId()).token(family.getToken())
				.name(family.getName()).phone(family.getPhone()).address1(family.getAddress1())
				.address2(family.getAddress2()).city(family.getCity()).country(family.getCountry())
				.state(family.getState()).zip(family.getZip()).isGrp(family.isGrp()).isPub(family.isPub())
				.dashboardImage(family.getDashboardImage()).profileImage(family.getProfileImage()).build();

		Set<UserResponse> usersList = new HashSet<>();
		Set<User> users = family.getPendingUser();
		for (User user : users) {
			List<ImageModel> imgList = imageRepo.findByUserIdAndType(user.getId(),
					new String[] { AppConstants.USER_COVER_PHOTO, AppConstants.USER_PROFILE_PHOTO });
			UserResponse userResponse = UserResponse.builder().dob(user.getDob()).email(user.getEmail())
					.enabled(user.isEnabled()).firstName(user.getFirstName()).gender(user.getGender()).id(user.getId())
					.lastName(user.getLastName()).phone(user.getPhone()).username(user.getUsername()).build();

			if (!imgList.isEmpty()) {
				for (ImageModel img : imgList) {
					if (img.getType().equalsIgnoreCase(AppConstants.USER_COVER_PHOTO)) {
						userResponse.setCoverPhoto(img.getLocalPath());
					} else if (img.getType().equalsIgnoreCase(AppConstants.USER_PROFILE_PHOTO)) {
						userResponse.setProfilePicture(img.getLocalPath());
					}
				}
			}
			usersList.add(userResponse);
		}
		familyRes.setPendingUser(usersList);
		return familyRes;
	}

	/**
	 * @param String name
	 * @param Long   phone
	 * @param String address1
	 * @param String address2
	 * @param String city
	 * @param String state
	 * @param String zip
	 * @param String country
	 * @param Long   userId
	 * @param String relationWithFamily
	 * @param String about
	 * @param Family family
	 * @return boolean false if not saved, true if saved and updated
	 */
	@Override
	public boolean updateFamilyInfo(Long familyId, FamilyUpdatePoJo familyUpdatePoJo, Family family, Long userId) {

		FamilyUserRoles usersRoles = familyUsersRoleRepo.findRoleForUserInFamily(familyId, userId);
		if (!ObjectUtils.isEmpty(usersRoles.getRelation())) {
			if (ObjectUtils.isEmpty(familyUpdatePoJo.getRelationWithFamily())) {
				return false;
			} else {
				usersRoles.setRelation(familyUpdatePoJo.getRelationWithFamily());
				familyUsersRoleRepo.save(usersRoles);
			}
		}
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getName()))
			family.setName(familyUpdatePoJo.getName());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getAddress1()))
			family.setAddress1(familyUpdatePoJo.getAddress1());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getAbout()))
			family.setAbout(familyUpdatePoJo.getAbout());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getAddress2()))
			family.setAddress2(familyUpdatePoJo.getAddress2());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getCity()))
			family.setCity(familyUpdatePoJo.getCity());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getState()))
			family.setState(familyUpdatePoJo.getState());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getZip()))
			family.setZip(familyUpdatePoJo.getZip());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getCountry()))
			family.setCountry(familyUpdatePoJo.getCountry());
		if (!ObjectUtils.isEmpty(familyUpdatePoJo.getPhone()))
			family.setPhone(familyUpdatePoJo.getPhone());

		familyRespository.save(family);
		return true;
	}

	/**
	 * @param String name
	 * @param Long   phone
	 * @param String address1
	 * @param String address2
	 * @param String city
	 * @param String state
	 * @param String zip
	 * @param String country
	 * @param Long   userId
	 * @param String about
	 * @param String relationWithFamily
	 * @param User   user
	 * @return Family saved family
	 */
	@Override
	public Family createNewFamily(FamilyCreatePoJo familyCreatePoJo, User user) {

		Family family = Family.builder().name(familyCreatePoJo.getName()).phone(familyCreatePoJo.getPhone())
				.address1(familyCreatePoJo.getAddress1()).address2(familyCreatePoJo.getAddress2())
				.city(familyCreatePoJo.getCity()).state(familyCreatePoJo.getState()).zip(familyCreatePoJo.getZip())
				.country(familyCreatePoJo.getCountry()).about(familyCreatePoJo.getAbout()).build();
		familyRespository.save(family);
		Set<Role> roles = new HashSet<>();
		Role userRole = roleRepo.getByName(ERole.ROLE_ADMIN);
		if (!ObjectUtils.isEmpty(userRole))
			roles.add(userRole);
		user.setRoles(roles);
		Set<Family> families = user.getFamilies();
		Set<Family> newUserFamilies = new HashSet<>();
		newUserFamilies.add(family);
		newUserFamilies.addAll(families);
		user.setFamilies(newUserFamilies);
		user.setEnabled(true);
		userRepository.save(user);
		if (!ObjectUtils.isEmpty(family.getFamilyId())) {
			FamilyUserRoles usersRoles = new FamilyUserRoles(family.getFamilyId(), user.getId(),
					userRole.getName().toString(), familyCreatePoJo.getRelationWithFamily());
			familyUsersRoleRepo.save(usersRoles);

			Long newFamilyToken = generateFamilyToken(family.getFamilyId());
			family.setToken(newFamilyToken.toString());
			familyRespository.save(family);
		}
		return family;
	}

	/**
	 * @param user
	 * @return if user capable to create a family by age
	 */
	public boolean isUserAgeValidToCreateFamily(User user) {

		String[] userDob = user.getDob().split("T")[0].split("-");
		LocalDate l = LocalDate.of(Integer.valueOf(userDob[0]), Short.parseShort(userDob[1]),
				Short.parseShort(userDob[2]));
		LocalDate now = LocalDate.now();
		return Period.between(l, now).getYears() >= minAge;
	}

	/**
	 * @param Long   userId
	 * @param Family family
	 * @return boolean true if user added to the pending list of a family, false if
	 *         not added for a reason user not exist with ID
	 */
	@Override
	public synchronized boolean isUserRequestToJoinFamilyAdded(User user, Family family) {

		Set<User> newPendingList = new HashSet<>();
		newPendingList.addAll(family.getPendingUser());
		newPendingList.add(user);
		family.setPendingUser(newPendingList);
		Family saved = familyRespository.save(family);
		if (!ObjectUtils.isEmpty(saved)) {
			if (!ObjectUtils.isEmpty(user.getEmail())) {
				notification = Notification.builder().emailId(user.getEmail())
						.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).mobileNumber(user.getPhone())
						.messageType(AppConstants.JOIN_FAMILY_USER).build();
				kafkaService.publishToTopic(notification);
			}
			List<FamilyUserRoles> famUserRolesList = familyUsersRoleRepo
					.findAllUserInFamilyWithRespectToRoles(family.getFamilyId(), ROLEADMIN);
			for (FamilyUserRoles famUserRole : famUserRolesList) {
				User user1 = userRepository.getById(famUserRole.getUserId());
				if (!ObjectUtils.isEmpty(user1.getEmail()))
					notification = Notification.builder().emailId(user1.getEmail())
							.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).mobileNumber(user1.getPhone())
							.messageType(AppConstants.JOIN_FAMILY_ADMIN).build();
				kafkaService.publishToTopic(notification);
			}
			return true;
		}
		return false;
	}

	/**
	 * @param Long adminUserId
	 * @param Long familyId
	 * @return boolean true if user admin, false if not admin for a family
	 */
	@Override
	public boolean isUserAdminforFamily(Long adminUserId, Long familyId) {
		FamilyUserRoles adminUser = familyUsersRoleRepo.findRoleForUserInFamily(familyId, adminUserId);
		return !ObjectUtils.isEmpty(adminUser) && !ObjectUtils.isEmpty(adminUser.getRole())
				&& adminUser.getRole().equalsIgnoreCase(ROLEADMIN);
	}

	/**
	 * @param Long   userId
	 * @param Family family
	 * @return boolean false if user not associated from family, true if associated
	 *         from family, send email to notify user if user email exist.
	 */
	@Override
	public boolean associateUserToFamily(Long userId, Family family) {

		boolean associated = false;
		User user = userRepository.getById(userId);
		if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(family.getPendingUser())
				&& family.getPendingUser().contains(user) && !user.getFamilies().contains(family)) {
			family.setPendingUser(family.getPendingUser().stream().filter(userIn -> !userIn.getId().equals(userId))
					.collect(Collectors.toSet()));
			familyRespository.save(family);

			Set<Family> families = user.getFamilies();

			Set<Family> newUserFamilies = new HashSet<>();
			newUserFamilies.add(family);
			newUserFamilies.addAll(families);
			user.setFamilies(newUserFamilies);
			userRepository.save(user);

			Role userRole = roleRepo.getByName(ERole.ROLE_USER);
			FamilyUserRoles rolesOfUser = familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(), userId);
			if (ObjectUtils.isEmpty(rolesOfUser)) {
				FamilyUserRoles usersRoles = new FamilyUserRoles(family.getFamilyId(), user.getId(),
						userRole.getName().toString(), AppConstants.EMPTY);
				familyUsersRoleRepo.save(usersRoles);
			} else {
				rolesOfUser.setRole(userRole.getName().toString());
			}

			if (!ObjectUtils.isEmpty(user.getEmail())) {
				notification = Notification.builder().emailId(user.getEmail())
						.notificationType(AppConstants.EMAIL_NOTIFICATION_TYPE).mobileNumber(user.getPhone())
						.messageType(AppConstants.JOIN_FAMILY_USER_SUCCESS).build();
				kafkaService.publishToTopic(notification);
			}
			associated = true;
		}
		return associated;
	}

	/**
	 * @param Long   userIdToRemove
	 * @param String token
	 * @param Family family
	 * @return boolean false if user not disassociated from family, true if
	 *         disassociated from family
	 */
	@Override
	public boolean disassociatUserFromFamily(Long userIdToRemove, String token, Family family) {

		boolean removed = false;
		User user = userRepository.getById(userIdToRemove);
		if ((!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user.getFamilies())
				&& user.getFamilies().contains(family))
				|| (!ObjectUtils.isEmpty(user) && family.getPendingUser().contains(user))) {
			Set<User> familyPendingUsers = family.getPendingUser().stream()
					.filter(userIn -> !userIn.getId().equals(userIdToRemove)).collect(Collectors.toSet());
			family.setPendingUser(familyPendingUsers);
			Set<Family> families = user.getFamilies().stream().filter(fam -> !fam.getToken().equalsIgnoreCase(token))
					.collect(Collectors.toSet());
			user.setFamilies(families);
			userRepository.save(user);
			familyRespository.save(family);
			removed = true;
		}
		return removed;
	}

	/**
	 * @param boolean isNewUserRoleAdmin
	 * @param Long    userIdToBeUpdated
	 * @param Family  family Update user role in Family
	 */
	@Override
	public void changeUserRoleInFamily(boolean isNewUserRoleAdmin, Long userIdToBeUpdated, Family family) {

		Role userRole;
		if (isNewUserRoleAdmin) {
			userRole = roleRepo.getByName(ERole.ROLE_ADMIN);
		} else {
			userRole = roleRepo.getByName(ERole.ROLE_USER);
		}

		FamilyUserRoles rolesOfUser = familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(),
				userIdToBeUpdated);
		if (!ObjectUtils.isEmpty(rolesOfUser) && !ObjectUtils.isEmpty(userRole)) {
			rolesOfUser.setRole(userRole.getName().toString());
			familyUsersRoleRepo.save(rolesOfUser);
		}
	}

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get all roles for users in a family on basis of familyId
	 * @Param Integer familyId
	 */
	@Override
	public List<FamilyUserRoles> getAllUserRolesInFamily(Long id) {
		return familyUsersRoleRepo.findAllUserRolesInFamily(id);
	}

	/**
	 * @return ResponseEntity (Family)
	 * @apiNote Used to get role for users in a family on basis of familyId
	 * @Param Integer familyId
	 */
	@Override
	public FamilyUserRoles getRoleForUserInFamily(Long familyId, Long userId) {
		return familyUsersRoleRepo.findRoleForUserInFamily(familyId, userId);
	}

	/**
	 * 
	 * @param familyId Long
	 * @return Long familyToken
	 */
	public Long generateFamilyToken(Long familyId) {
		return token + familyId;
	}

	/**
	 * this method to be used when new user register on Fazeal
	 * 
	 * @param user User
	 * @return Family family with id and token
	 */
	@Override
	public Family createNewFamilyForNewUserWhenRegister(User user) {
		Period diff = Utils.getUserAge(user);
		if (diff.getYears() < minAge) {
			return null;
		}

		Family family = new Family();
		family = save(family);
		if (!ObjectUtils.isEmpty(family.getFamilyId())) {
			family.setToken(String.valueOf(generateFamilyToken(family.getFamilyId())));
			family = save(family);

			user.setFamilies(Set.of(family));
			Role FamilyAdminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException(messages.get(AppConstants.ERROR_ROLE_NOT_FOUND)));
			FamilyUserRoles usersRoles = FamilyUserRoles.builder().familyId(family.getFamilyId()).userId(user.getId())
					.role(FamilyAdminRole.getName().toString()).build();
			familyUsersRoleRepo.save(usersRoles);
			return family;
		}
		return null;
	}

}
