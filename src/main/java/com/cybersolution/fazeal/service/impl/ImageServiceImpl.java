package com.cybersolution.fazeal.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.ImageModelFamilies;
import com.cybersolution.fazeal.models.ImageModelUser;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.AlbumRepository;
import com.cybersolution.fazeal.repository.ImageFamiliesRepository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.ImageUsersRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.AmazonClient;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageModelFamiliesService;
import com.cybersolution.fazeal.service.ImageModelUserService;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.util.AppConstants;
import com.google.common.io.Files;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	ImageRepository imageRepository;

	@Autowired
	private AmazonClient amazonClient;

	@Autowired
	private UserService userService;

	@Autowired
	private AlbumService albumService;

	@Autowired
	private ImageModelFamiliesService imageModelFamiliesService;

	@Autowired
	private ImageModelUserService imageModelUserService;

	@Autowired
	AlbumRepository albumRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ImageFamiliesRepository imageFamiliesRepository;

	@Autowired
	ImageUsersRepository imageUsersRepository;

	@Autowired
	private FamilyService familyService;

	/**
	 * @param date
	 * @param multipartFile
	 * @param userId
	 * @param type          - C for cover photo, P for profile picture and F for
	 *                      family photos
	 * @param album
	 * @param description
	 * @return String (File name)
	 * @apiNote Used to Upload Image
	 */
	@Override
	public String uploadImages(Date date, MultipartFile multipartFiles, Long userId, String type, String albumName,
			String description, String fileNameReturned, AlbumModel albumModel) {

		String fileName = StringUtils.cleanPath(multipartFiles.getOriginalFilename());
		String fileNamefromUpload = AppConstants.EMPTY;

		if (type.equalsIgnoreCase(AppConstants.DASHBOARD_PICTURE)
				|| type.equalsIgnoreCase(AppConstants.PROFILE_PICTURE)) {
			List<ImageModel> img = imageRepository.findByUserIdAndType(userId, new String[] { type });
			img.stream().forEach(image -> markImagedelete(image, userId));
		}
		ImageModel image = new ImageModel();

		image.setDate(date);
		String extension = Files.getFileExtension(fileName);

		image.setExtension(extension);
		User uploadingUser = userService.getById(userId);

		image.setUserId(uploadingUser.getId());

		image.setUserName(uploadingUser.getUsername());
		image.setType(type);
		image.setDescription(
				description != null ? description.replaceAll(AppConstants.SPECIAL_CHARS, AppConstants.EMPTY)
						: AppConstants.EMPTY);
		ImageModel imageSaved = imageRepository.save(image);
		if (!ObjectUtils.isEmpty(imageSaved.getId())) {

			String uploadPath = amazonClient.uploadFile(multipartFiles, uploadingUser.getId(), albumName);

			fileNamefromUpload = uploadPath.replaceAll(AppConstants.FILE_REGEX_TO_REMOVE, AppConstants.EMPTY);
			imageSaved.setLocalPath(fileNamefromUpload);
			imageRepository.save(image);

			if (null != albumModel) {

				Set<ImageModel> imagesSet = albumModel.getAlbumImages();
				imagesSet.add(imageSaved);
				albumModel.setAlbumImages(imagesSet);

				albumRepository.save(albumModel);
			}
		}
		return fileNamefromUpload;

	}

	/**
	 * @param user
	 * @param familyIds
	 * @param imageId
	 * @param userIds
	 * @return ResponseEntity (Message )
	 * @apiNote used to set the privacy for Images
	 */
	@Override
	public boolean imagesSharedWithUsersAndFamilies(List<Long> familyIds, ImageModel image, List<Long> userIds,
			User user, String type) {
		try {
			List<Long> familyIdsHasRelationWithUser = user.getFamilies().stream()
					.filter(familyIn -> filterExist(familyIn.getFamilyId(), familyIds)).map(Family::getFamilyId)
					.collect(Collectors.toList());

			Set<Long> userFamiliesToGetUserIdsFrom = user.getFamilies().stream()
					.filter(family -> !filterExist(family.getFamilyId(), familyIdsHasRelationWithUser))
					.map(Family::getFamilyId).collect(Collectors.toSet());

			Set<User> userList = new HashSet<>();
			userFamiliesToGetUserIdsFrom.forEach(familyId -> {
				List<User> eachFamilyUsersList = userRepository.getUsersListForAFamily(familyId, user.getId()).stream()
						.filter(userInFamily -> checkIfImageSharedToFamily(userInFamily, userFamiliesToGetUserIdsFrom))
						.collect(Collectors.toList());

				if (!ObjectUtils.isEmpty(eachFamilyUsersList))
					userList.addAll(eachFamilyUsersList);
			});
			Set<Long> usersToShare = userList.stream().map(User::getId)
					.filter(userIdInList -> filterExist(userIdInList, userIds)).collect(Collectors.toSet());
			if (usersToShare.isEmpty() && familyIdsHasRelationWithUser.isEmpty())
				return false;
			image.setType(type);
			imageRepository.save(image);
			usersToShare.stream()
					.forEach(userIdFiltered -> createShareRelationWithUsers(image.getId(), userIdFiltered));
			familyIdsHasRelationWithUser.stream()
					.forEach(familyId -> createShareRelationWithFamilies(image.getId(), familyId));
		} catch (Exception e) {

			return false;
		}
		return true;
	}

	/**
	 * @param userId
	 * @return ResponseEntity (images )
	 * @apiNote used to get the Images with UserId
	 */
	@Override
	public List<ImageModel> findByUserId(Long userId) {
		return imageRepository.findByUserId(userId);
	}

	/**
	 * @param userId and type
	 * @return images
	 * @apiNote used to get the Images with UserId and Type
	 */
	@Override
	public List<ImageModel> findByUserIdAndType(Long id, String[] type) {
		return imageRepository.findByUserIdAndType(id, type);
	}

	/**
	 * @param imageId
	 * @return images
	 * @apiNote used to get the Images with imageId
	 */
	@Override
	public ImageModel getImageById(Long imageId) {
		if (imageRepository.existsById(imageId)) {
			return imageRepository.getById(imageId);
		}
		return null;
	}

	/**
	 * @param userId
	 * @return ResponseEntity (albumMap )
	 * @apiNote used to get the Album Images which is shared with Family and Users
	 *          and Own Images with UserId and Type
	 */
	@Override
	public ConcurrentHashMap<String, Set<AlbumModel>> getAlbumImagesWithUserId(Long userId) {
		User user = userService.getById(userId);
		ConcurrentHashMap<String, Set<AlbumModel>> albumMap = new ConcurrentHashMap<>();
		Set<AlbumModel> albums = new HashSet<>(albumService.findByUserId(userId));
		Set<Family> families = user.getFamilies();
		albumMap.put("userAlbums", albums);
		Set<AlbumModel> familySharedAlbums = new HashSet<>();
		Set<AlbumModel> userSharedAlbums = new HashSet<>();
		for (Family family : families) {
			Long[] familyAlbum = imageModelFamiliesService.getfamilyImagesByFamId(family.getFamilyId());
			if (familyAlbum.length > 0) {
				List<AlbumModel> albumFam = albumService.getAlbumsByFamilyIds(family.getFamilyId(), userId);
				if (!albumFam.isEmpty()) {
					for (AlbumModel albumModel : albumFam) {
						AlbumModel copy = new AlbumModel(albumModel);
						copy.setAlbumImages(new HashSet<>());
						Set<ImageModel> albumModelImages = new HashSet<>();
						albumModelImages.addAll(albumModel.getAlbumImages());
						Set<ImageModel> albumImages = new HashSet<>();
						for (ImageModel imgModel : albumModel.getAlbumImages()) {
							boolean isExists = false;
							for (int i = 0; i < familyAlbum.length; i++) {
								if (imgModel.getId().equals(familyAlbum[i])) {
									isExists = true;
								}
							}
							if (!isExists) {
								albumModelImages.remove(imgModel);
							}
						}
						albumModelImages.stream().forEach(albumImages::add);
						copy.setAlbumImages(albumImages);
						familySharedAlbums.add(copy);
					}
				}
			}
		}
		albumMap.put("familySharedAlbums", familySharedAlbums);
		Long[] userImgs = imageModelUserService.getUserImagesIdsByUserId(userId);
		if (userImgs.length > 0) {
			List<AlbumModel> albumUser = albumService.getAlbumsByUserIds(userId);
			if (!albumUser.isEmpty()) {
				for (AlbumModel userAlbumModel : albumUser) {
					Set<ImageModel> albumModelImages = new HashSet<>();
					AlbumModel copy = new AlbumModel(userAlbumModel);
					copy.setAlbumImages(new HashSet<>());
					albumModelImages.addAll(userAlbumModel.getAlbumImages());
					Set<ImageModel> albumImages = new HashSet<>();
					for (ImageModel imgMod : userAlbumModel.getAlbumImages()) {
						boolean isExists = false;
						for (int i = 0; i < userImgs.length; i++) {
							if (imgMod.getId().equals(userImgs[i])) {
								isExists = true;
							}
						}
						if (!isExists) {
							albumImages.remove(imgMod);
						}
					}
					albumModelImages.stream().forEach(albumImages::add);
					copy.setAlbumImages(albumImages);
					userSharedAlbums.add(copy);
//					albumImages.stream().forEach(albumsUserList::add);
				}
			}
		}
		albumMap.put("userSharedAlbums", userSharedAlbums);
		return albumMap;
	}

	/**
	 * @param id
	 * @return ResponseEntity (boolean value )
	 * @apiNote used to delete Image
	 */
	@Override
	public void deleteByImgId(ImageModel image) {
		amazonClient.deleteFileFromS3Bucket(image.getLocalPath());
		imageRepository.deleteById(image.getId());
	}

	/**
	 * @param idInList userId Long
	 * @param ids      userIds
	 * @return boolean true if id in ids and if not return false
	 */
	public boolean filterExist(Long idInList, List<Long> ids) {

		if (ids.isEmpty())
			return false;
		for (Long id : ids) {
			if (id != null && id.equals(idInList))
				return true;
		}
		return false;
	}

	/**
	 * @param userInFamily                 logged in user
	 * @param userFamiliesToGetUserIdsFrom familiIds where to check if user
	 *                                     join-family for each one
	 * @return true if album is shared with that user family from the list to be
	 *         filtered in stream where is this method called
	 */
	public boolean checkIfImageSharedToFamily(User userInFamily, Set<Long> userFamiliesToGetUserIdsFrom) {
		for (Long id : userFamiliesToGetUserIdsFrom)
			for (Family userFamilyIn : userInFamily.getFamilies())
				if (userFamilyIn.getFamilyId().equals(id))
					return true;
		return false;
	}

	/**
	 * @param familyIds
	 * @param imageId
	 * @apiNote Used to save shared Families In ImageModelFamilies repository
	 */
	public void createShareRelationWithFamilies(Long imageId, Long familyId) {

		ImageModelFamilies imageModelFamilies = new ImageModelFamilies();
		imageModelFamilies.setFamilyId(familyId);
		imageModelFamilies.setImagemodelId(imageId);
		imageFamiliesRepository.save(imageModelFamilies);
		List<FamilyUserRoles> allUserRolesInFamily = familyService.getAllUserRolesInFamily(familyId);
		if (allUserRolesInFamily == null)
			return;
		familyService.getAllUserRolesInFamily(familyId).stream()
				.forEach(role -> createShareRelationWithUsers(imageId, role.getUserId()));
	}

	/**
	 * @param familyIds
	 * @param imageId
	 * @apiNote Used to save shared Users In ImageModelUser repository
	 */
	public void createShareRelationWithUsers(Long imageId, Long userId) {
		ImageModelUser imageModelUsers = new ImageModelUser();
		imageModelUsers.setUserId(userId);
		imageModelUsers.setImagemodelId(imageId);
		imageUsersRepository.save(imageModelUsers);
	}

	/**
	 * @param localPath String
	 * @return ImageModel
	 */
	@Override
	public ImageModel getImageByImageLocalPath(String localPath, Long userId) {
		return imageRepository.getByLocalPathAndUserId(localPath, userId);
	}

	/**
	 * @param localPath String
	 * @return ImageModel
	 */
	@Override
	public ImageModel getImgByImageLocalPath(String localPath) {
		return imageRepository.getByLocalPath(localPath);
	}

	/**
	 * @param image ImageModel
	 */
	@Override
	public void save(ImageModel image) {
		imageRepository.save(image);
	}

	/**
	 * @param MultipartFile image file
	 * @param Family
	 * @param String        type (GP: group profile, GD: group dashboard)
	 * @param Date          date
	 * @param Long          userId to upload profile or dashboard image for a
	 *                      specific (family|group)
	 */
	@Override
	public void uploadFamilyGroupProfileOrDashboardImage(MultipartFile fileImage, Family family, String type, Date date,
			Long userId) {
		String fileName = StringUtils.cleanPath(fileImage.getOriginalFilename());
		ImageModel imageToBeDeleted = null;
		if (AppConstants.GROUP_PROFILE_PICTURE.equalsIgnoreCase(type)) {
			imageToBeDeleted = this.getImgByImageLocalPath(family.getProfileImage());
		} else {
			imageToBeDeleted = this.getImgByImageLocalPath(family.getDashboardImage());
		}
		if (imageToBeDeleted != null) {
			this.deleteByImgId(imageToBeDeleted);
		}
		ImageModel image = new ImageModel();
		image.setDate(date);
		String extension = Files.getFileExtension(fileName);
		image.setExtension(extension);
		User uploadingUser = userService.getById(userId);
		if (uploadingUser != null)
			image.setUserName(uploadingUser.getUsername());
		image.setType(type);
		ImageModel imageSaved = imageRepository.save(image);
		if (!ObjectUtils.isEmpty(imageSaved)) {
			String uploadPath = amazonClient.uploadFile(fileImage, userId, family.getName());
			String fileNamefromUpload = uploadPath.replaceAll(AppConstants.FILE_REGEX_TO_REMOVE, AppConstants.EMPTY);
			imageSaved.setLocalPath(fileNamefromUpload);
			imageRepository.save(image);
			if (AppConstants.GROUP_PROFILE_PICTURE.equalsIgnoreCase(type)) {
				family.setProfileImage(fileNamefromUpload);
			} else {
				family.setDashboardImage(fileNamefromUpload);
			}
			familyService.save(family);
		}
	}

	/**
	 * mark an image model to be moved to deleted album
	 * 
	 * @param ImageModel image
	 * @param Long       userId
	 */
	@Override
	public void markImagedelete(ImageModel image, Long userId) {
		getDeletedAlbumForUser(userId);
		image.setMarkedToBeDeleted(Boolean.TRUE);
		imageRepository.save(image);
	}

	/**
	 * return Albumodel deleted album if exist, and create new deleted album if not
	 * exist
	 * 
	 * @param Long userId
	 */
	@Override
	public AlbumModel getDeletedAlbumForUser(Long userId) {
		AlbumModel deletedAlbum;
		if (albumService.userDontHaveDeletedAlbum(userId)) {
			deletedAlbum = albumService.createPrivateAlbum(AlbumModel.builder().userId(userId)
					.type(AppConstants.DELETED).albumName(AppConstants.DELETED.toUpperCase()).date(new Date())
					.description(AppConstants.DELETED_IMAGES_ALBUM).build());
		} else {
			deletedAlbum = albumService.findAlbumByAlbumNameAndUserId(AppConstants.DELETED.toUpperCase(), userId);
		}
		return deletedAlbum;
	}

	/**
	 * return true if image restored, false if not deleted
	 * 
	 * @param ImageModel image
	 */
	@Override
	public boolean restoreImageToAlbum(ImageModel image) {
		if (Boolean.TRUE.equals(image.getMarkedToBeDeleted())) {
			image.setMarkedToBeDeleted(false);
			imageRepository.save(image);
			return true;
		}
		return false;
	}

}
