package com.cybersolution.fazeal.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.FamilyMemberRequest;
import com.cybersolution.fazeal.models.FamilyMembers;
import com.cybersolution.fazeal.models.SocialMediaLinks;
import com.cybersolution.fazeal.models.SocialMediaLinksRequest;
import com.cybersolution.fazeal.models.SystemLanguages;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.GeneralAccountSettingsFamilyRepository;
import com.cybersolution.fazeal.repository.GeneralAccountSettingsRepository;
import com.cybersolution.fazeal.repository.SystemLanguagesRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.GeneralAccountSettingsService;

@Service
public class GeneralAccountSettingsServiceImpl implements GeneralAccountSettingsService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	GeneralAccountSettingsRepository generalAccountSettingsRepository;

	@Autowired
	GeneralAccountSettingsFamilyRepository generalAccountSettingsFamilyRepository;
	
	@Autowired
	SystemLanguagesRepository systemLanguagesRepository;
	
	/**
	 * @return Social media links of User
	 * @apiNote Used to add Social media links
	 * @Param SocialMediaLinksRequest
	 * @param Long userId
	 */
	@Override
	public SocialMediaLinks saveSocialMediaLinks(Long userId, SocialMediaLinksRequest socialMediaLinksRequest) {

		User user = userRepository.getById(userId);
		SocialMediaLinks socialMediaLinks = null;
		Set<SocialMediaLinks> socialMediaLinksListSet = new HashSet<>();
		for (String socialMedia : socialMediaLinksRequest.getSocialMediaLinksMap().keySet()) {
			socialMediaLinks = SocialMediaLinks.builder().userId(userId).socialMedia(socialMedia)
					.socialMedialLink(socialMediaLinksRequest.getSocialMediaLinksMap().get(socialMedia)).build();

			socialMediaLinksListSet.add(socialMediaLinks);
			user.getSocialMediaLinksList().addAll(socialMediaLinksListSet);
			generalAccountSettingsRepository.save(socialMediaLinks);
		}

		userRepository.save(user);

		return socialMediaLinks;
	}

	/**
	 * @return Social media links
	 * @apiNote Used to get Social media link by userId
	 * @param userId
	 */
	@Override
	public List<SocialMediaLinks> getSocialMediaLinks(Long userId) {
		return generalAccountSettingsRepository.findByUserId(userId);
	}

	/**
	 * @return Social media links
	 * @apiNote Used to get Social media link by its id
	 * @Param socialMediaId
	 */
	@Override
	public SocialMediaLinks findBySocialMediaId(Long socialMediaId) {
		return generalAccountSettingsRepository.findBySocialMediaId(socialMediaId);
	}

	/**
	 * @return Boolean value
	 * @apiNote Used to delete Social media link
	 * @Param Long id (socialMediaId)
	 */
	@Override
	public boolean deleteSocMediaLink(Long socialMediaId) {
		SocialMediaLinks socialMediaLinks = findBySocialMediaId(socialMediaId);
		if (!ObjectUtils.isEmpty(socialMediaLinks)) {
			generalAccountSettingsRepository.deleteById(socialMediaLinks.getId());
			return true;
		}

		return false;

	}
	
	@Override
	public FamilyMembers saveFamilyMembers(Long userId, @Valid FamilyMemberRequest familyMemberRequest) {
		User user = userRepository.getById(userId);
		FamilyMembers familyMembers=null;
		Set<FamilyMembers> familyMembersListSet = new HashSet<>();
		for (String relation : familyMemberRequest.getFamilyMemberMap().keySet()) {
			 familyMembers= FamilyMembers.builder().userId(userId).relation(relation).familyMemberName(
					familyMemberRequest.getFamilyMemberMap().get(relation)).build();
	    
			familyMembersListSet.add(familyMembers);
	     user.getFamilyMembersList().addAll(familyMembersListSet);
	     generalAccountSettingsFamilyRepository.save(familyMembers);
		}
		
		 userRepository.save(user);
		
		return familyMembers;
	}
	@Override
	public List<FamilyMembers> getFamilyMembers(Long userId) {
		return generalAccountSettingsFamilyRepository.findByUserId(userId);
	}
	
	@Override
	public FamilyMembers findByfamilyMemberId(Long famMemberId) {
		return generalAccountSettingsFamilyRepository.findByFamMembersId(famMemberId);
	}
	@Override
	public boolean deleteFamilyMember(Long famMemberId) {
		FamilyMembers familyMembers = findByfamilyMemberId(famMemberId);
		if (!ObjectUtils.isEmpty(familyMembers)) {
			generalAccountSettingsFamilyRepository.deleteById(familyMembers.getId());
			return true;
		}

		return false;
	}

	@Override
	public List<SystemLanguages> findAll() {
		return systemLanguagesRepository.findAll() ;
	}

}