package com.cybersolution.fazeal.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.service.RoleService;

public class RoleServiceImple implements RoleService {
	@Autowired
	RoleRepository roleRepository;

	@Override
	public Optional<Role> getUserRoleFromDB(ERole role) {
		return roleRepository.findByName(role);
	}
}
