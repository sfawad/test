package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.repository.AlbumUsersRepository;
import com.cybersolution.fazeal.service.AlbumModelUserService;

@Service
@Transactional
public class AlbumModelUserServiceImpl implements AlbumModelUserService {

  @Autowired
  AlbumUsersRepository albumUsersRepository;

  @Override
  public AlbumModelUser addUser(AlbumModelUser userModels) {

    return albumUsersRepository.save(userModels);
  }
}
