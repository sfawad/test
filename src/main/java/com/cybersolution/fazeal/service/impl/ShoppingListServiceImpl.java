package com.cybersolution.fazeal.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.repository.ShoppingListRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.ShoppingListService;
import com.cybersolution.fazeal.util.Utils;

@Service
public class ShoppingListServiceImpl implements ShoppingListService {

	@Autowired
	ShoppingListRepository shopListRepo;

	@Autowired
	Messages messages;
	@Autowired
	UserRepository userRepo;

	@Autowired
	FamilyRespository familyRepo;

	@Autowired
	ShoppingListEntriesRepository shopItemsListRepo;

	@Autowired
	ShoppingListService shoppingListService;

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to get shopping list by id
	 * @Param Long id
	 */
	@Override
	public ShoppingList findById(Long id) {
		if (shopListRepo.existsById(id)) {
			return shopListRepo.getById(id);
		}
		return null;
	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to get shopping list by family id
	 * @Param Long id
	 */
	@Override
	public List<ShoppingList> getShoppingListForFamilyOnId(Long familyId) {

		return shopListRepo.getShoppingListForFamilyOnId(familyId);
	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to save shopping list by family id
	 * @Param ShoppingListRequest
	 */
	@Override

	public ShoppingList saveShoppingList(Long userId, ShoppingListRequest shopListRequest) {

		User user = userRepo.getById(userId);

		boolean userFamilyRelationExists = false;
		if (ObjectUtils.isEmpty(user)) {
			return null;
		}

		for (Family family : user.getFamilies()) {
			if (family.getFamilyId().equals(shopListRequest.getFamilyId())) {
				userFamilyRelationExists = true;
			}
		}

		if (!userFamilyRelationExists) {
			return null;
		}

		Set<ShoppingListEntries> shopListSet = new HashSet<>();
		for (String itemsName : shopListRequest.getItemMap().keySet()) {
			ShoppingListEntries shopListEntries = new ShoppingListEntries(itemsName,
					shopListRequest.getItemMap().get(itemsName), userId, user.getUsername());
			shopListSet.add(shopListEntries);
			shopItemsListRepo.save(shopListEntries);
		}

		ShoppingList shoppingList = ShoppingList.builder().shoppinglistName(shopListRequest.getShoppinglistName())
				.familyId(shopListRequest.getFamilyId()).dateCreate(Utils.getCurrentDateTime())
				.isActive(shopListRequest.isActive()).updatedAt(null).build();
		shoppingList.setShoppingItemsList(shopListSet);
		shoppingList = shopListRepo.save(shoppingList);
		return shoppingList;
	}

	/**
	 * @return ResponseEntity (Shopping List for Family)
	 * @apiNote Used to update shopping list by family id only status and shoplist
	 *          name
	 * @Param ShoppingListUpdateRequest
	 */
	@Override
	public ShoppingList updateShoppingList(Long userId, ShoppingListUpdateRequest shoppingListUpdateRequest) {
		ShoppingList shopList = shoppingListService.findById(shoppingListUpdateRequest.getId());

		if (ObjectUtils.isEmpty(shopList)) {
			return null;
		}

		User user = userRepo.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return null;
		}

		Set<ShoppingListEntries> shopListSet = new HashSet<>();
		if (!ObjectUtils.isEmpty(shoppingListUpdateRequest.getItemMap())) {
			Set<ShoppingListEntries> shopListEntries = shopItemsListRepo
					.getShoppingListEntriesByListId(shoppingListUpdateRequest.getId());
			shopItemsListRepo.deleteAll(shopListEntries);
			for (String itemsName : shoppingListUpdateRequest.getItemMap().keySet()) {
				ShoppingListEntries shopListEntry = new ShoppingListEntries(itemsName,
						shoppingListUpdateRequest.getItemMap().get(itemsName), userId, user.getUsername());

				shopListSet.add(shopListEntry);

				shopItemsListRepo.save(shopListEntry);
			}
		}
		shopList.getShoppingItemsList().clear();
		shopList.setActive(shoppingListUpdateRequest.isListStatus());
		shopList.getShoppingItemsList().addAll(shopListSet);
		shopList.setShoppinglistName(shoppingListUpdateRequest.getShoppingListName());
		shopList.setUpdatedAt(Utils.getCurrentDateTime());
		shopList = shopListRepo.save(shopList);
		return shopList;
	}

	/**
	 * @return Map (deleted,true)
	 * @apiNote Used to delete items from shopping list
	 * @Param Long id (itemId)
	 */
	@Override
	public boolean deleteShoppingListByShoppingListId(Long shoppingListId) {

		ShoppingList shopList = shopListRepo.getByShoppingListId(shoppingListId);
		if (null != shopList) {
			shopListRepo.deleteById(shopList.getId());
			return true;
		}

		return false;
	}

	/**
	 * @return Map (deleted,true)
	 * @apiNote Used to delete items from shopping list
	 * @Param Long id (itemId)
	 */
	@Override
	public boolean deleteShoppingListItemByItemId(Long itemId) {
		ShoppingListEntries shopListItems = shopItemsListRepo.getByItemId(itemId);
		if (null != shopListItems) {
			shopItemsListRepo.deleteById(shopListItems.getId());
			return true;
		}

		return false;

	}

}
