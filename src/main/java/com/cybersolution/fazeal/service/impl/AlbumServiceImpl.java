package com.cybersolution.fazeal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.UpdateAlbumModelPoJo;
import com.cybersolution.fazeal.repository.AlbumFamiliesRepository;
import com.cybersolution.fazeal.repository.AlbumRepository;
import com.cybersolution.fazeal.repository.AlbumUsersRepository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.util.AppConstants;

@Service
public class AlbumServiceImpl implements AlbumService {
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private AlbumFamiliesRepository albumFamiliesRepository;
	@Autowired
	private AlbumUsersRepository albumUsersRepository;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private ImageRepository imageRepository;
	@Autowired
	private ImageService imageService;
	@Autowired
	private FamilyService familyService;

	/**
	 * @param album id Long
	 * @return all user albums
	 */
	@Override
	public List<AlbumModel> findByUserId(Long userId) {
		return albumRepository.findByUserId(userId);
	}

	/**
	 * list album where user with user id is the owner of this album
	 * 
	 * @param userId    long
	 * @param albumName string
	 * @return albumModel by albumName and userId where is the user own the album
	 */
	@Override
	public AlbumModel findAlbumByAlbumNameAndUserId(String albumName, Long userId) {
		return albumRepository.findByAlbumNameAndUserId(albumName, userId);
	}

	/**
	 * create private album with no relationships with users or families
	 * 
	 * @param newAlbum
	 * @return saved albumModel
	 */
	@Override
	public AlbumModel createPrivateAlbum(AlbumModel newAlbum) {
		return albumRepository.save(newAlbum);
	}

	/**
	 * create album shared with all families of a user
	 * 
	 * @param newAlbum, albumModel to be created and shared to all families of a
	 *                  user
	 * @param user,     logged in user to share album all families
	 * @return saved AlbumModel
	 */
	@Override
	public AlbumModel createPublicToAllUserFamiliesAlbum(AlbumModel newAlbum, User user) {
		AlbumModel saved;
		saved = albumRepository.save(newAlbum);
		Set<Family> userFamilies = user.getFamilies();
		userFamilies.forEach(family -> createFamilyAlbumRelationship(saved, family.getFamilyId()));
		return saved;
	}

	/**
	 * create album shared to families as well with users, where users not in those
	 * families that album is shared with
	 * 
	 * @param familiIds  {@Nullable}, id's of families where user want to share
	 *                   album with
	 * @param userIds    {@Nullable}, id's of users where user want to share album
	 *                   with
	 * @param UserModel, logged in user
	 * @param newAlbum,  AlbumModel to be saved and shared with selected families
	 *                   and users
	 * @return saved AlbumModel
	 */
	@Override
	public AlbumModel createSharedToFamiliesAndUsersAlbum(List<Long> familyIds, List<Long> userIds, User user,
			AlbumModel newAlbum) {

		AlbumModel saved;
		List<Long> familyIdsHasRelationWithUser = user.getFamilies().stream()
				.filter(familyIn -> filterExist(familyIn.getFamilyId(), familyIds)).map(Family::getFamilyId)
				.collect(Collectors.toList());

		Set<Long> userFamiliesToGetUserIdsFrom = user.getFamilies().stream()
				.filter(family -> !filterExist(family.getFamilyId(), familyIdsHasRelationWithUser))
				.map(Family::getFamilyId).collect(Collectors.toSet());

		Set<User> userList = new HashSet<>();
		userFamiliesToGetUserIdsFrom.forEach(familyId -> {
			List<User> eachFamilyUsersList = userRepo.getUsersListForAFamily(familyId, user.getId()).stream()
					.filter(userInFamily -> checkIfAlbumSharedToFamily(userInFamily, userFamiliesToGetUserIdsFrom))
					.collect(Collectors.toList());

			if (null != eachFamilyUsersList)
				userList.addAll(eachFamilyUsersList);
		});

		Set<Long> usersToShare = userList.stream().map(User::getId)
				.filter(userIdInList -> filterExist(userIdInList, userIds)).collect(Collectors.toSet());
		if (usersToShare.isEmpty() && familyIdsHasRelationWithUser.isEmpty())
			return null;
		saved = albumRepository.save(newAlbum);
		usersToShare.stream().forEach(userIdFiltered -> createUserAlbumRelationship(saved, userIdFiltered));
		familyIdsHasRelationWithUser.stream().forEach(familyId -> createFamilyAlbumRelationship(saved, familyId));
		return saved;
	}

	/**
	 * @param userInFamily                 logged in user
	 * @param userFamiliesToGetUserIdsFrom familiIds where to check if user
	 *                                     join-family for each one
	 * @return true if album is shared with that user family from the list to be
	 *         filtered in stream where is this method called
	 */
	private boolean checkIfAlbumSharedToFamily(User userInFamily, Set<Long> userFamiliesToGetUserIdsFrom) {
		for (Long id : userFamiliesToGetUserIdsFrom)
			for (Family userFamilyIn : userInFamily.getFamilies())
				if (userFamilyIn.getFamilyId().equals(id))
					return true;
		return false;
	}

	/**
	 * @param idInList userId Long
	 * @param ids      userIds
	 * @return boolean true if id in ids and if not return false
	 */
	private boolean filterExist(Long idInList, List<Long> ids) {
		if (ids.isEmpty())
			return false;
		for (Long id : ids) {
			if (id != null && id.equals(idInList)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * save relationship between album and family in db
	 * 
	 * @param newAlbum AlbumModel to create share relation with familyId
	 * @param familyId Long
	 */
	private void createFamilyAlbumRelationship(AlbumModel newAlbum, Long familyId) {
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies();
		albumModelFamilies.setFamilyId(familyId);
		albumModelFamilies.setAlbumId(newAlbum.getAlbumId());
		albumFamiliesRepository.save(albumModelFamilies);
		familyService.getAllUserRolesInFamily(familyId).stream()
				.forEach(role -> createUserAlbumRelationship(newAlbum, role.getUserId()));
	}

	/**
	 * save relationship between album and user in db
	 * 
	 * @param newAlbum AlbumModel to create share relation with userId
	 * @param userIdIn Long
	 */
	private void createUserAlbumRelationship(AlbumModel newAlbum, Long userIdIn) {
		AlbumModelUser albumModelUsers = new AlbumModelUser();
		albumModelUsers.setUserId(userIdIn);
		albumModelUsers.setAlbumId(newAlbum.getAlbumId());
		albumUsersRepository.save(albumModelUsers);
	}

	/**
	 * delete album by id, and that when the user is the owner
	 * 
	 * @param id       album id Long
	 * @param userName return true if album deleted (where user is the owner of this
	 *                 album) false if not or album not exist
	 */
	@Override
	public boolean deleteByAlbumId(Long id, String userName) {
		boolean deleted = false;
		Optional<User> user = userRepo.findByUsername(userName);
		AlbumModel albumModel = albumRepository.getByAlbumId(id);
		if (albumModel != null && (user.isPresent() && !user.get().getId().equals(albumModel.getUserId()))
				|| albumModel == null) {
			return deleted;
		}
		List<ImageModel> images = imageRepository.getByAlbumId(id);
		images.stream().forEach(image -> {
			if (image != null)
				imageService.markImagedelete(image, user.get().getId());//////
		});
		albumRepository.deleteById(id);
		deleted = true;
		return deleted;
	}

	/**
	 * get AlbumModel by album id
	 * 
	 * @param id album id Long
	 * @return AlbumModel
	 */
	@Override
	public AlbumModel getByAlbumId(Long id) {
		return albumRepository.getById(id);
	}

	/**
	 * update album set album private with no share relationship
	 * 
	 * @param oldAlbum    AlbumModel from db to be updated
	 * @param albumName   String new album name
	 * @param description String new description
	 * @param type        String new album type
	 * @param date        Date update date of album
	 * @return AlbumModel updated model with the new data not shared and will be
	 *         private for a user
	 */
	@Override
	public AlbumModel updateAlbumSetPrivateAlbum(AlbumModel oldAlbum, String albumName, String description, String type,
			Date date, User user) {
		AlbumModel updated;

		if (!ObjectUtils.isEmpty(oldAlbum.getAlbumFamiliesList())) {
			oldAlbum.getAlbumFamiliesList().clear();
			oldAlbum.getAlbumFamiliesList().addAll(new HashSet<>());
		}
		if (!ObjectUtils.isEmpty(oldAlbum.getAlbumUserList())) {
			oldAlbum.getAlbumUserList().clear();
			oldAlbum.getAlbumUserList().addAll(new HashSet<>());
		}
		oldAlbum.setAlbumName(albumName);
		oldAlbum.setDescription(description);
		oldAlbum.setType(type);
		oldAlbum.setUpdateDate(date);
		updated = albumRepository.save(oldAlbum);
		List<ImageModel> images = imageRepository.getByAlbumId(oldAlbum.getAlbumId());
		images.stream().forEach(
				image -> imageService.imagesSharedWithUsersAndFamilies(List.of(), image, List.of(), user, type));
		return updated;
	}

	/**
	 * update album set the album shared with all user families
	 * 
	 * @param oldAlbum    AlbumModel from db to be updated
	 * @param albumName   String new album name
	 * @param description String new description
	 * @param type        String new album type
	 * @param date        Date update date of album
	 * @param user        User logged in user
	 * @return AlbumModel updated model with the new data, will be shared with user
	 *         families
	 */
	@Override
	public AlbumModel updateAlbumSetPublicToAllUserFamiliesAlbum(AlbumModel oldAlbum, String albumName,
			String description, String type, Date date, User user) {
		AlbumModel updated;

		if (!ObjectUtils.isEmpty(oldAlbum.getAlbumFamiliesList())) {
			oldAlbum.getAlbumFamiliesList().clear();
			oldAlbum.getAlbumFamiliesList().addAll(new HashSet<>());
		}
		if (!ObjectUtils.isEmpty(oldAlbum.getAlbumUserList())) {
			oldAlbum.getAlbumUserList().clear();
			oldAlbum.getAlbumUserList().addAll(new HashSet<>());
		}

		oldAlbum.setDescription(description);
		oldAlbum.setType(type);
		oldAlbum.setUpdateDate(date);
		updated = albumRepository.save(oldAlbum);

		Set<Family> userFamilies = user.getFamilies();
		List<Long> userFamiliesIds = new ArrayList<>();
		userFamilies.forEach(family -> {
			createFamilyAlbumRelationship(updated, family.getFamilyId());
			userFamiliesIds.add(family.getFamilyId());
		});
		List<ImageModel> images = imageRepository.getByAlbumId(oldAlbum.getAlbumId());
		images.stream().forEach(
				image -> imageService.imagesSharedWithUsersAndFamilies(userFamiliesIds, image, List.of(), user, type));
		return updated;
	}

	/**
	 * update album share with selected users|families of a user
	 * 
	 * @param familyIds   List<Long> families id's where album will be shared with
	 * @param userIds     List<Long> users id's where album will be shared with
	 * @param oldAlbum    AlbumModel from db to be updated
	 * @param albumName   String new album name
	 * @param description String new description
	 * @param type        String new album type
	 * @param date        Date update date of album
	 * @param user        User logged in user
	 * @return AlbumModel updated model with the new data, will be shared with user
	 *         selected families and users
	 */
	@Override
	public AlbumModel updateAlbumSetSharedToSelectedFamiliesAndUsers(User user, AlbumModel oldAlbum,
			UpdateAlbumModelPoJo albumModelPoJo) {

		AlbumModel updated;
		oldAlbum.getAlbumFamiliesList().clear();
		oldAlbum.getAlbumUserList().clear();
		oldAlbum.setAlbumName(albumModelPoJo.getAlbumName());
		oldAlbum.setDescription(albumModelPoJo.getDescription());
		oldAlbum.setType(albumModelPoJo.getType());
		oldAlbum.setUpdateDate(albumModelPoJo.getDate());
		updated = albumRepository.save(oldAlbum);

		List<Long> familyIdsHasRelationWithUser = user.getFamilies().stream()
				.filter(familyIn -> filterExist(familyIn.getFamilyId(), albumModelPoJo.getFamilyIds()))
				.map(Family::getFamilyId).collect(Collectors.toList());

		Set<Long> userFamiliesToGetUserIdsFrom = user.getFamilies().stream()
				.filter(family -> !filterExist(family.getFamilyId(), familyIdsHasRelationWithUser))
				.map(Family::getFamilyId).collect(Collectors.toSet());

		Set<User> userList = new HashSet<>();
		userFamiliesToGetUserIdsFrom.forEach(familyId -> {
			List<User> eachFamilyUsersList = userRepo.getUsersListForAFamily(familyId, user.getId()).stream()
					.filter(userInFamily -> checkIfAlbumSharedToFamily(userInFamily, userFamiliesToGetUserIdsFrom))
					.collect(Collectors.toList());

			if (null != eachFamilyUsersList)
				userList.addAll(eachFamilyUsersList);
		});

		Set<Long> usersToShare = userList.stream().map(User::getId)
				.filter(userIdInList -> filterExist(userIdInList, albumModelPoJo.getUserIds()))
				.collect(Collectors.toSet());
		if (usersToShare.isEmpty() && familyIdsHasRelationWithUser.isEmpty())
			return null;
		usersToShare.stream().forEach(userIdFiltered -> createUserAlbumRelationship(oldAlbum, userIdFiltered));

		familyIdsHasRelationWithUser.forEach(familyId -> createFamilyAlbumRelationship(oldAlbum, familyId));
		return updated;
	}

	@Override
	public boolean updateAlbumCover(Long albumId, Long userId, Long imageId) {
		AlbumModel album = albumRepository.getById(albumId);
		ImageModel image = imageRepository.getById(imageId);
		if ((ObjectUtils.isEmpty(album) || ObjectUtils.isEmpty(image))
				|| (!album.getUserId().equals(userId) || !album.getAlbumImages().contains(image)))
			return false;
		album.setCoverPhoto(image.getLocalPath());
		albumRepository.save(album);
		return true;
	}

	@Override
	public List<AlbumModel> findAlbumIfContainsNameWithUserId(String albumName, Long userId) {
		return albumRepository.findAlbumIfContainsNameWithUserId(albumName, userId);
	}

	@Override
	public AlbumModel findByAlbumIdAndUserId(Long albumId, Long userId) {
		return albumRepository.findByAlbumIdAndUserId(albumId, userId);
	}

	@Override
	public List<AlbumModel> getAlbumsByFamilyIds(Long familyId, Long userId) {
		return albumRepository.findAlbumsByFamilyIDS(familyId, userId);
	}

	@Override
	public List<AlbumModel> getAlbumsByUserIds(Long usersId) {
		return albumRepository.findAlbumsByUsersIDS(usersId);
	}

	@Override
	public boolean userDontHaveDeletedAlbum(Long userId) {
		return albumRepository.countAlbumByName(AppConstants.DELETED.toUpperCase(), userId) == 0;
	}

	@Override
	public AlbumModel save(AlbumModel deletedAlbum) {
		return albumRepository.save(deletedAlbum);
	}
}
