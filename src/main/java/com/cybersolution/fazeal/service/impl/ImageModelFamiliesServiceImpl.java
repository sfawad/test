package com.cybersolution.fazeal.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModelFamilies;
import com.cybersolution.fazeal.repository.ImageFamiliesRepository;
import com.cybersolution.fazeal.service.ImageModelFamiliesService;

@Service
@Transactional
public class ImageModelFamiliesServiceImpl implements ImageModelFamiliesService {

  @Autowired
  ImageFamiliesRepository imageFamiliesRepository;

  @Override
  public Integer delete(Long id) {
    return imageFamiliesRepository.deleteImgFamily(id);
  }

  @Override
  public ImageModelFamilies findByFamId(Long famId) {
    return imageFamiliesRepository.getById(famId);
  }

  @Override
  public List<ImageModelFamilies> fetchfamilyImages(Set<Family> familyIds) {
    return imageFamiliesRepository.getImageModel(familyIds);
  }

  @Override
  public Long[] getfamilyImagesByFamId(Long familyId) {
    return imageFamiliesRepository.getImageByFamilyId(familyId);
  }

}
