package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybersolution.fazeal.repository.ImageUsersRepository;
import com.cybersolution.fazeal.service.ImageModelUserService;

@Service
@Transactional
public class ImageModelUserServiceImpl implements ImageModelUserService {

  @Autowired
  ImageUsersRepository imageUsersRepository;

  @Override
  public Integer delete(Long id) {
    return imageUsersRepository.deleteImg(id);
  }

  @Override
  public Long[] getUserImagesIdsByUserId(Long userId) {
    return imageUsersRepository.getImageByUseryId(userId);
  }

}
