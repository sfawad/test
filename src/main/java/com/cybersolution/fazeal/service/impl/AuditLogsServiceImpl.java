package com.cybersolution.fazeal.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.AuditLogs;
import com.cybersolution.fazeal.repository.AuditLogsRepository;
import com.cybersolution.fazeal.service.AuditLogsService;

@Service
public class AuditLogsServiceImpl implements AuditLogsService {

	private static final Logger LOGGER = LogManager.getLogger(AuditLogsServiceImpl.class);

	@Autowired
	private AuditLogsRepository auditLogsRepo;

	@Override
	public List<AuditLogs> findAuditLogsByUserId(Long userId) {
		return auditLogsRepo.findAuditLogsByUserId(userId);

	}

	@Override
	public void save(AuditLogs auditLogs) {
		try {
			auditLogsRepo.save(auditLogs);
		} catch (Exception ec) {
			LOGGER.error("Error while saving audit logs for user: {} {}", auditLogs.getUserId(), ec.getMessage());
		}
	}

}
