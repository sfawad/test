package com.cybersolution.fazeal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.NotificationsActivity;
import com.cybersolution.fazeal.repository.NotificationsActivityRespository;
import com.cybersolution.fazeal.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

  @Autowired
  NotificationsActivityRespository notifsRepo;

  @Override
  public List<NotificationsActivity> getNotificationsForUsers(Long userId) {
    return  notifsRepo.findNotificationsForUser(userId);
  }

}
