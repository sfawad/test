package com.cybersolution.fazeal.service.impl;

import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.service.KafkaService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KafkaServiceImpl implements KafkaService {


    @Value("${kafka.notification.topicName}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;


    @Override
    public void publishToTopic(Notification message)  {

        log.info("publish message to kafka topic" + message);

        kafkaTemplate.send(topicName,message);

    }



}
