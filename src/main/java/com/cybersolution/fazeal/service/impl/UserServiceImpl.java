package com.cybersolution.fazeal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.ConfigData;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.ConfigDataRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.util.AppConstants;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repo;
	
	@Autowired
	ConfigDataRepository configDataRepository;

	@Override
	public User getById(Long id) {
		return repo.getById(id);
	}

	@Override
	public List<User> getUsersListForAFamily(Long familyId, Long userId) {
		return repo.getUsersListForAFamily(familyId, userId);
	}

	public boolean isUserInFamily(Long userId, Long familyId) {
		List<User> allUsersListForAFamily = repo.getAllUsersListForAFamily(familyId);
		return allUsersListForAFamily.stream().anyMatch(user -> user.getId().equals(userId));
	}

	/**
	 * @apiNote Used to add system language 
	 * @param userId   Long
	 * @param systemLanguage   String
	 * @return boolean value
	 */
	@Override
	public boolean setSystemLanguage(Long userId, String systemLanguage) {
		User user= repo.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return false;
		}
		if (!ObjectUtils.isEmpty(systemLanguage)) {
			user.setSystemLanguage(systemLanguage);
		   repo.save(user);
		}
		return true;
	}

	/**
	 * @apiNote Used to get system language 
	 * @param userId   Long
	 * @return String systemLanguage
	 */
	@Override
	public String getSystemLanguage(Long userId) {
		User user = repo.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return AppConstants.DEFAULT_LANGUAGE;
		}
		return user.getSystemLanguage();
	}


	@Override
	public String[] findAll(String namePrefix) {
		ConfigData configData=  configDataRepository.findByNamePrefix(namePrefix);
		return configData.getValue().split(",");
	}

	@Override
	public boolean setNames(Long userId, String namePrefix, String firstName, String lastName) {
		User user= repo.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return false;
		}
		user.setNamePrefix(namePrefix);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		repo.save(user);
		return true;
	}
	
	
}
