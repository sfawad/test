package com.cybersolution.fazeal.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.cybersolution.fazeal.pojo.CreateEventPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.Events;
import com.cybersolution.fazeal.models.EventsCurrentUserResponse;
import com.cybersolution.fazeal.models.EventsFamilyResponse;
import com.cybersolution.fazeal.models.EventsFamilyUser;
import com.cybersolution.fazeal.models.EventsRelation;
import com.cybersolution.fazeal.models.EventsRelationResponse;
import com.cybersolution.fazeal.models.EventsResponse;
import com.cybersolution.fazeal.models.EventsUserResponse;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.EventsFamilyUserRespository;
import com.cybersolution.fazeal.repository.EventsRelationRespository;
import com.cybersolution.fazeal.repository.EventsRespository;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EventService;
import com.cybersolution.fazeal.util.AppConstants;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventsRespository eventRepo;

	@Autowired
	EventsFamilyUserRespository eventsFamilyUserRespository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	FamilyRespository familyRespository;

	@Autowired
	EventsRelationRespository eventsRelationRespository;

	private static final String[] COLOR = { "#FF0000", "#0000FF", "#008000", "#ADD8E6", "#B0E0E6", "#FFC0CB", "#FF69B4",
			"#FFFF00", "#FFA500", "#800080", "#808080", "#A9A9A9", "#FFD700", "#FFFACD", "#DB7093", "#E6E6FA",
			"#8B0000", "#FF6347", "#FFDAB9", "#9ACD32", "#556B2F", "#00FFFF", "#E0FFFF", "#5F9EA0", "#F5DEB3",
			"#BC8F8F", "#A0522D", "#FAEBD7", "#2F4F4F" };

	/**
	 * @return Boolean
	 * @apiNote Used to update Event
	 * @Param Long id (eventId)
	 * @Param String event
	 * @Param Boolean isShared
	 */
	@Override
	public boolean updateEvent(Long id, String event, boolean isShared) {
		Events eventUpd = eventRepo.getById(id);
		if (!ObjectUtils.isEmpty(eventUpd.getId())) {
			eventUpd.setEvent(event);
			eventUpd.setShared(isShared);
			eventRepo.save(eventUpd);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get Event by user ID
	 * @Param Long userId
	 */
	@Override
	public List<Events> findByUserId(Long userId) {
		return eventRepo.findByUserId(userId);
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get Event by event ID
	 * @Param Long id (eventId)
	 */
	@Override
	public Events findByEventId(Long eventId) {
		if (eventRepo.existsById(eventId)) {
			return eventRepo.getById(eventId);
		}
		return null;

	}

	/**
	 * @return Map (deleted,true)
	 * @apiNote Used to delete Event by event ID
	 * @Param Long eventId
	 */
	@Override
	public boolean deleteEvent(Long eventId) {
		Long count = eventRepo.countById(eventId);
		if (count > 0) {
			Events event = findByEventId(eventId);
			List<EventsFamilyUser> eventsFamUser = eventsFamilyUserRespository.findEventsByEventId(eventId);
			if (null != eventsFamUser)
				for (EventsFamilyUser eventsShared : eventsFamUser) {
					eventsFamilyUserRespository.deleteById(eventsShared.getId());
				}
			eventRepo.delete(event);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get Event by User ID
	 * @Param Long userId
	 */
	@Override
	public List<EventsRelationResponse> getUserEventsPlainResponse(Long userId) {
		List<EventsRelation> eventsShared = new ArrayList<>();
		List<EventsRelationResponse> eventsRelList = new ArrayList<>();

		User user = userRepository.findByUserId(userId);

		if (null != user) {
			Set<Family> families = user.getFamilies();
			for (Family family : families) {
				eventsShared = eventsRelationRespository.findSharedEventsForFamily(family.getFamilyId());
			}
			List<EventsRelation> eventsPrivate = eventsRelationRespository.findPrivateEventsForUser(userId);
			eventsShared.addAll(eventsPrivate);
			Long userIdCheck = 0L;
			String colors = AppConstants.DEFAULT_COLOR;
			for (int i = 0; i < eventsShared.size(); i++) {
				EventsRelationResponse eventsRelRes = new EventsRelationResponse();
				if (ObjectUtils.isEmpty(userIdCheck) || !userIdCheck.equals(eventsShared.get(i).getUid())) {
					userIdCheck = eventsShared.get(i).getUid();
					colors = COLOR[i];
				}
				eventsRelRes.setColor(colors);
				eventsRelRes.setEid(eventsShared.get(i).getEid());
				eventsRelRes.setEdate(eventsShared.get(i).getEdate());
				eventsRelRes.setEend(eventsShared.get(i).getEend());
				eventsRelRes.setEstart(eventsShared.get(i).getEstart());
				eventsRelRes.setEvent(eventsShared.get(i).getEvent());
				eventsRelRes.setFamilyId(eventsShared.get(i).getFamilyId());
				eventsRelRes.setFamilyName(eventsShared.get(i).getFamilyName());
				eventsRelRes.setIsShared(eventsShared.get(i).getIsShared());
				eventsRelRes.setUemail(eventsShared.get(i).getUemail());
				eventsRelRes.setUid(eventsShared.get(i).getUid());
				eventsRelRes.setUsername(eventsShared.get(i).getUsername());
				eventsRelList.add(eventsRelRes);
			}
		}
		return eventsRelList;
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get Events by user ID
	 * @Param Long userId
	 */
	@Override
	public EventsCurrentUserResponse getUserEvents(Long userId) {
		User user = userRepository.findByUserId(userId);
		EventsCurrentUserResponse eventsDetails = new EventsCurrentUserResponse();
		if (null != user) {
			List<Events> eventsUnsharedForuser = eventRepo.findUnsharedEventsForUserId(userId);
			List<EventsResponse> eventsListResponselist = new ArrayList<>();
			for (Events event : eventsUnsharedForuser) {
				EventsResponse eventsRes = new EventsResponse();
				eventsRes.setId(event.getId());
				eventsRes.setDate(event.getDate());
				eventsRes.setTitle(event.getEvent());
				eventsRes.setStart(event.getEventStart());
				eventsRes.setEnd(event.getEventEnd());
				eventsListResponselist.add(eventsRes);
			}
			eventsDetails.setUserPrivateEvents(eventsListResponselist);
			Set<Family> families = user.getFamilies();
			List<EventsFamilyResponse> eventsFamilyRespList = new ArrayList<>();
			for (Family family : families) {
				EventsFamilyResponse eventsFamilyResp = new EventsFamilyResponse();
				eventsFamilyResp.setFamilyId(family.getFamilyId());
				eventsFamilyResp.setFamilyName(family.getName());
				List<User> usersList = userRepository.getAllUsersListForAFamily(family.getFamilyId());
				List<EventsUserResponse> eventsUserRespList = new ArrayList<>();
				for (int i = 0; i < usersList.size(); i++) {
					EventsUserResponse eventsUserResp = new EventsUserResponse();
					eventsUserResp.setUId(usersList.get(i).getId());
					eventsUserResp.setUemail(usersList.get(i).getEmail());
					eventsUserResp.setUsername(usersList.get(i).getUsername());
					eventsUserResp.setColor(COLOR[i]);
					eventsUserResp.setCurrentUser(false);
					if (usersList.get(i).getId().equals(user.getId())) {
						eventsUserResp.setCurrentUser(true);
					}
					List<EventsResponse> eventsList = new ArrayList<>();
					List<EventsFamilyUser> eventsFamUser = eventsFamilyUserRespository
							.findEventsByFamilyAndUserId(family.getFamilyId(), usersList.get(i).getId());
					for (EventsFamilyUser eventsRel : eventsFamUser) {
						Events events = eventsRel.getEvents();
						EventsResponse eventsRes = new EventsResponse();
						eventsRes.setId(events.getId());
						eventsRes.setDate(events.getDate());
						eventsRes.setTitle(events.getEvent());
						eventsRes.setStart(events.getEventStart());
						eventsRes.setEnd(events.getEventEnd());
						eventsList.add(eventsRes);
					}
					eventsUserResp.setEvents(eventsList);
					eventsUserRespList.add(eventsUserResp);
				}
				eventsFamilyResp.setUserEvents(eventsUserRespList);
				eventsFamilyRespList.add(eventsFamilyResp);
			}
			eventsDetails.setFamilyEvents(eventsFamilyRespList);
		}
		return eventsDetails;
	}

	/**
	 * @return ResponseEntity (Event)
	 * @apiNote Used to get user's Family (shared) events
	 * @Param Long familyId
	 */
	@Override
	public List<Events> getUserWithFamilyEvents(Long familyId) {
		List<Events> famEvent = new ArrayList<>();
		List<EventsFamilyUser> eventsRel = eventsFamilyUserRespository.findEventsByFamilyId(familyId);
		for (EventsFamilyUser eventsFamily : eventsRel) {
			famEvent.add(eventsFamily.getEvents());
		}
		return famEvent;
	}

	/**
	 * @return void
	 * @apiNote Used to set events details
	 * @Param Events events (event object)
	 * @Param String date (event creation date)
	 * @Param String endTime (event end time)
	 * @Param String startTime (event start time)
	 * @Param Boolean isShared (event shared to family)
	 * @Param String event (event description)
	 */
	@Override
	public void setEventDetails(Events events, String date, String endTime, String startTime, boolean isShared,
			String event) {
		if (!ObjectUtils.isEmpty(event)) {
			events.setEvent(event);
		}
		if (!ObjectUtils.isEmpty(date)) {
			events.setDate(date);
		}
		if (!ObjectUtils.isEmpty(startTime)) {
			events.setEventStart(startTime);
		}
		if (!ObjectUtils.isEmpty(endTime)) {
			events.setEventEnd(endTime);
		}
		events.setShared(isShared);
		eventRepo.save(events);
	}

	/**
	 * @return void
	 * @apiNote Used to delete events shared between Family and User
	 * @Param Long eventId
	 */
	@Override
	public void deleteEventsFamilyUser(Long eventId) {
		for (EventsFamilyUser eventsFamilyUser : eventsFamilyUserRespository.findEventsByEventId(eventId)) {
			eventsFamilyUserRespository.delete(eventsFamilyUser);
		}
	}

	/**
	 * @return void
	 * @apiNote Used to save events shared between Family and User
	 * @Param Events events (event object)
	 * @Param Long userId
	 * @Param Long[] familyId
	 */
	@Override
	public void saveEventsFamilyUser(Events events, Long userId, Long[] familyId) {
		for (int i = 0; i < familyId.length; i++) {
			EventsFamilyUser entity = new EventsFamilyUser(userRepository.getById(userId),
					familyRespository.getById(familyId[i]), events);
			eventsFamilyUserRespository.save(entity);
		}
	}

	/**
	 * @return Events
	 * @apiNote Used to save event
	 * @Param Long userId
	 * @Param String event
	 * @Param String date
	 * @Param String startTime
	 * @Param String endTime
	 * @Param boolean isShared
	 */
	@Override
	public Events saveEvent(Long userId,CreateEventPojo createEventPojo) {
		Events events = Events.builder().date(createEventPojo.getDate()).event(createEventPojo.getEventName()).eventStart(createEventPojo.getStartTime()).eventEnd(createEventPojo.getEndTime())
				.userId(userId).isShared(createEventPojo.getIsShared()).build();
		return eventRepo.save(events);
	}
}
