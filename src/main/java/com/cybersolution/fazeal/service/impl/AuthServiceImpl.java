package com.cybersolution.fazeal.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.controller.FamilyController;
import com.cybersolution.fazeal.exception.TokenRefreshException;
import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.RefreshToken;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.UserUpdateDetails;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.response.TokenRefreshResponse;
import com.cybersolution.fazeal.security.jwt.JwtUtils;
import com.cybersolution.fazeal.security.services.RefreshTokenService;
import com.cybersolution.fazeal.security.services.UserDetailsImpl;
import com.cybersolution.fazeal.service.AuthService;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.util.AppConstants;
import com.cybersolution.fazeal.util.Utils;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	RefreshTokenService refreshTokenService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ImageRepository imageRepo;

	@Autowired
	Messages messages;

	@Autowired
	EmailService emailService;

	@Autowired
	FamilyService famService;

	@Autowired
	FamilyController familyController;

	@Autowired
	FamilyUserRoleRespository familyUsersRoleRepo;

	@Autowired
	AuditLogsHelper auditLogsHelper;

	@Value("${family-create-minage}")
	private Integer minAge = 0;

	@Value("${family-create-token-counter}")
	private Integer token = 0;

	@Value("${origin.domain}")
	private String domain = "";

	@Value("${ciui.app.jwtExpirationMs}")
	Long resetTokenTime;

	/**
	 * @return List<User> from DB
	 */
	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	/**
	 * @param User return User saved
	 */
	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	/**
	 * @param userId Long
	 * @return User by userId
	 */
	@Override
	public User getUserById(Long userId) {
		return userRepository.getById(userId);
	}

	/**
	 * @param email String
	 * @return boolean true if email exist, false if not exist
	 */
	@Override
	public boolean isUserExistsWithEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	/**
	 * @param userName String
	 * @return User by userName
	 */
	@Override
	public boolean isUserExistsWithUsername(String userName) {
		return userRepository.existsByUsername(userName);
	}

	/**
	 * @param SignUpRequest
	 * @return User
	 */
	@Override
	public User createNewUser(SignupRequest signUpRequest) {
		User user;
		user = User.builder().username(signUpRequest.getUsername())
				.password(encoder.encode(signUpRequest.getPassword())).enabled(true).build();
		if (!ObjectUtils.isEmpty(signUpRequest.getEmail())) {
			user.setEmail(signUpRequest.getEmail());
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getDob())) {
			user.setDob(signUpRequest.getDob());
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getFirstName())) {
			user.setFirstName(signUpRequest.getFirstName());
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getLastName())) {
			user.setLastName(signUpRequest.getLastName());
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getGender())) {
			user.setGender(signUpRequest.getGender());
		}
		if (!ObjectUtils.isEmpty(signUpRequest.getPhone())) {
			user.setPhone(signUpRequest.getPhone());
		}

		Role userRole = roleRepository.findByName(ERole.ROLE_USER)
				.orElseThrow(() -> new RuntimeException(messages.get(AppConstants.ERROR_ROLE_NOT_FOUND)));
		user.setCreatedAt(Utils.getCurrentDateTime());
		user.setRoles(Set.of(userRole));
		return saveUser(user);
	}

	/**
	 * @param User
	 * @return Map<String, List<UserResponse>> for a user
	 */
	@Override
	public Map<String, List<UserResponse>> listAllFamiliesAndUsersByUserId(User user) {
		Map<String, List<UserResponse>> familyUserMap = new HashMap<>();
		user.getFamilies().stream().forEach(family -> {

		});
		for (Family family : user.getFamilies()) {
			List<UserResponse> users = new ArrayList<>();
			List<User> usersList = userRepository.getUsersListForAFamily(family.getFamilyId(), user.getId());
			for (User userInFamily : usersList) {
				List<ImageModel> imgList = imageRepo.findByUserIdAndType(userInFamily.getId(),
						new String[] { "C", "P" });
				UserResponse userResponse = createUserResponse(userInFamily);
				if (!imgList.isEmpty()) {
					for (ImageModel img : imgList) {
						if (img.getType().equalsIgnoreCase("C")) {
							userResponse.setCoverPhoto(img.getLocalPath());
						} else if (img.getType().equalsIgnoreCase("P")) {
							userResponse.setProfilePicture(img.getLocalPath());
						}
					}
				}
				users.add(userResponse);
			}
			familyUserMap.put(family.getToken(), users);
		}
		return familyUserMap;
	}

	/**
	 * @param Long userId
	 * @param User
	 * @return UserResponse
	 */
	@Override
	public UserResponse findUserById(Long userId, User userInFamily) {
		UserResponse userResponse = null;
		List<ImageModel> imgList = imageRepo.findByUserIdAndType(userId, new String[] { "C", "P" });
		userResponse = createUserResponse(userInFamily);
		if (!imgList.isEmpty()) {
			for (ImageModel img : imgList) {
				if (img.getType().equalsIgnoreCase("C")) {
					userResponse.setCoverPhoto(img.getLocalPath());
				} else if (img.getType().equalsIgnoreCase("P")) {
					userResponse.setProfilePicture(img.getLocalPath());
				}
			}
		}
		return userResponse;
	}

	/**
	 * @param userInFamily
	 * @return UserResponse
	 */
	private UserResponse createUserResponse(User userInFamily) {
		return UserResponse.builder().dob(userInFamily.getDob()).email(userInFamily.getEmail())
				.enabled(userInFamily.isEnabled()).families(userInFamily.getFamilies())
				.firstName(userInFamily.getFirstName()).lastName(userInFamily.getLastName())
				.gender(userInFamily.getGender()).id(userInFamily.getId()).phone(userInFamily.getPhone())
				.username(userInFamily.getUsername()).namePrefix(userInFamily.getNamePrefix()).build();
	}

	/**
	 * @param String token
	 * @return User by reset token
	 */
	@Override
	public User getUserByResetToken(String token) {
		return userRepository.getUserFromToken(token);
	}

	/**
	 * @param User
	 * @return boolean check if resetToken expired or not
	 */
	@Override
	public boolean isTokenExpired(User user) {
		Long updatedTime = null;
		boolean ifExpired = false;
		SimpleDateFormat formatter = new SimpleDateFormat(AppConstants.DATE_TIME_FORMATE);
		String dateInString = user.getUpdatedAt();
		try {
			Date date = formatter.parse(dateInString);
			updatedTime = date.getTime();
			updatedTime = Long.sum(updatedTime, resetTokenTime);
		} catch (Exception ec) {
			ifExpired = true;
		}
		Long currentTime = System.currentTimeMillis();
		if (!ObjectUtils.isEmpty(updatedTime) && updatedTime < currentTime) {
			ifExpired = false;
		}
		return ifExpired;
	}

	/**
	 * @param String password
	 * @param User   user
	 */
	@Override
	public void updateUserPassword(String password, User user) {
		user.setResetToken(null);
		user.setUpdatedAt(Utils.getCurrentDateTime());
		user.setPassword(encoder.encode(password));
		saveUser(user);
	}

	/**
	 * @param Long userId
	 * @return boolean true if user deleted, flase if not
	 */
	@Override
	public boolean deleteUserById(Long userId) {
		User user = userRepository.getById(userId);
		if (!ObjectUtils.isEmpty(user)) {
			userRepository.delete(user);
			return true;
		}
		return false;
	}

	/**
	 * @param String email
	 * @return User with generated password token
	 */
	@Override
	public User generateTokenToResetUserPassword(String email) {
		User user = userRepository.getUserFromEmail(email);
		if (ObjectUtils.isEmpty(user)) {
			return null;
		}
		String generatedToken = UUID.randomUUID().toString();
		user.setResetToken(generatedToken);
		user.setUpdatedAt(Utils.getCurrentDateTime());
		userRepository.save(user);
		return user;
	}

	/**
	 * @param Long userId
	 * @return User with changed enable from true to false and back by Fazeal admin
	 */
	@Override
	public User changeEnabledStatus(Long userId) {
		User user = userRepository.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return null;
		}
		user.setEnabled(!user.isEnabled());
		return userRepository.save(user);
	}

	/**
	 * @param String username
	 * @param String password
	 * @return JwtResponse null if not valid, and valid JwtResponse if logged in
	 *         successfully
	 */
	@Override
	public JwtResponse loginByUsernameAndPassword(String username, String password) {
		Optional<User> opUser = userRepository.findByUsername(username);
		if (opUser.isPresent()) {
			User user = opUser.get();
			if (user.isEnabled()) {
				Authentication authentication;
				try {
					authentication = authenticationManager
							.authenticate(new UsernamePasswordAuthenticationToken(username, password));
				} catch (Exception ec) {
					return null;
				}
				SecurityContextHolder.getContext().setAuthentication(authentication);
				UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
				List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
						.collect(Collectors.toList());
				String jwt = jwtUtils.generateJwtToken(userDetails);
				RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
				return new JwtResponse(jwt, refreshToken.getToken(), AppConstants.BEARER, userDetails.getId(),
						userDetails.getUsername(), userDetails.getEmail(), roles, user.getFirstName(),
						user.getLastName(), user.getTheme());
			}
		}
		return null;
	}

	/**
	 * @param Long   userId,
	 * @param email,
	 * @param String password,
	 * @param String firstName,
	 * @param String lastName,
	 * @param String dateOfBirth,
	 * @param String nickname,
	 * @param String gender, String aboutMe,
	 * @param String languages
	 * @return User updated and saved
	 */
	@Override
	public User updateUserDetails(Long userId, UserUpdateDetails userDetails) {
		User user = userRepository.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return null;
		}
		if (!ObjectUtils.isEmpty(userDetails.getEmail()))
			user.setEmail(userDetails.getEmail());
		if (!ObjectUtils.isEmpty(userDetails.getPassword()))
			user.setPassword(encoder.encode(userDetails.getPassword()));
		if (!ObjectUtils.isEmpty(userDetails.getFirstName()))
			user.setFirstName(userDetails.getFirstName());
		if (!ObjectUtils.isEmpty(userDetails.getLastName()))
			user.setLastName(userDetails.getLastName());
		if (!ObjectUtils.isEmpty(userDetails.getDateOfBirth()))
			user.setDob(userDetails.getDateOfBirth());
		if (!ObjectUtils.isEmpty(userDetails.getGender()))
			user.setGender(userDetails.getGender());
		if (!ObjectUtils.isEmpty(userDetails.getAboutMe()))
			user.setAboutMe(userDetails.getAboutMe());
		if (!ObjectUtils.isEmpty(userDetails.getLanguages()))
			user.setLanguages(userDetails.getLanguages());
		if (!ObjectUtils.isEmpty(userDetails.getNickname()))
			user.setNickname(userDetails.getNickname());
		return userRepository.save(user);
	}

	@Override
	public TokenRefreshResponse refreshAccessToken(String refreshToken) {
		return refreshTokenService.findByToken(refreshToken).map(refreshTokenService::verifyExpiration)
				.map(RefreshToken::getUser).map(user -> {
					String refToken = jwtUtils.generateTokenFromUsername(user.getUsername());
					RefreshToken refreshTokenObject = refreshTokenService.createRefreshToken(user.getId());
					return new TokenRefreshResponse(refToken, refreshTokenObject.getToken());
				}).orElseThrow(() -> new TokenRefreshException(refreshToken,
						messages.get(AppConstants.REFRESH_TOKEN_NOT_IN_DATABASE)));
	}

}
