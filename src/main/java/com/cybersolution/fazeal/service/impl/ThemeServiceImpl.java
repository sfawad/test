package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.ThemeService;
import com.cybersolution.fazeal.util.AppConstants;

@Service
public class ThemeServiceImpl implements ThemeService {

	@Autowired
	UserRepository userRepository;

	/**
	 * @param Long   userId
	 * @param String theme
	 * @return boolean true on success, false on fail if user not exist
	 */
	@Override
	public boolean updateUserTheme(Long userId, String theme) {
		User user = userRepository.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return false;
		}
		user.setTheme(theme);
		userRepository.save(user);
		return true;
	}

	/**
	 * @param userId Long
	 * @return String theme of a user
	 */
	@Override
	public String getUserTheme(Long userId) {
		User user = userRepository.getById(userId);
		if (ObjectUtils.isEmpty(user)) {
			return AppConstants.DEFAULT;
		}
		return user.getTheme();
	}

}
