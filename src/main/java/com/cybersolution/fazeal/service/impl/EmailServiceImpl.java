package com.cybersolution.fazeal.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService{
	private static final Logger logger = LogManager.getLogger(EmailServiceImpl.class);
	
	@Autowired
	public JavaMailSender mailSender;
	
	@Override
	public void sendEmail(SimpleMailMessage email) {
		mailSender.send(email);
		
	}
	
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    
	@Override
	public void sendMail(String from, String to, String subject, String msg) {
		// Commented this as we will have email being sent to user in seperate application, from here we will only drop message to KAFKA
		/*
		 * try {
		 * 
		 * if(!ObjectUtils.isEmpty(to)) { MimeMessage message =
		 * mailSender.createMimeMessage(); MimeMessageHelper helper = new
		 * MimeMessageHelper(message, false, "utf-8");
		 * 
		 * message.setContent(msg, "text/html"); helper.setTo(to);
		 * helper.setSubject(subject); helper.setFrom(from);
		 * 
		 * mailSender.send(message); } } catch (MessagingException ex) {
		 * logger.log(Level.ERROR, ex.getMessage()); }
		 */	}

	
}
