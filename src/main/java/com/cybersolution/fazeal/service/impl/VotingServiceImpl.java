package com.cybersolution.fazeal.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.Voting;
import com.cybersolution.fazeal.models.VotingOptions;
import com.cybersolution.fazeal.models.VotingOptionsResponse;
import com.cybersolution.fazeal.models.VotingOptionsSelection;
import com.cybersolution.fazeal.models.VotingResponse;
import com.cybersolution.fazeal.repository.VotingOptionsRepository;
import com.cybersolution.fazeal.repository.VotingRepository;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.VotingService;
import com.google.gson.Gson;

@Service
public class VotingServiceImpl implements VotingService {

	@Autowired
	private VotingRepository votingRepo;

	@Autowired
	private VotingOptionsRepository votingOptionsRepo;

	@Autowired
	private Messages messages;

	@Autowired
	private UserService userService;

	/**
	 * list all family votings by family id
	 * 
	 * @param familyId Long return List<Voting> of family by familyId
	 */
	@Override
	public List<VotingResponse> getAllFamilyVotingsByFamilyId(Long familyId) {

		List<Voting> votingsList = votingRepo.getByFamilyId(familyId);
		if (votingsList == null) {
			return Collections.emptyList();
		}

		List<VotingResponse> votingRespList = new ArrayList<>();
		for (Voting voting : votingsList) {

			VotingResponse votingResp;
			Gson gson = new Gson();
			String votingListResp = gson.toJson(voting);
			votingResp = gson.fromJson(votingListResp, VotingResponse.class);

			Map<Long, Long> countMap = new HashMap<>();
			Map<Long, Double> percentageMap = new HashMap<>();
			Long totalCount = 0L;
			Set<VotingOptions> votingOptions = voting.getVotingDetails();
			for (VotingOptions votingOptes : votingOptions) {
				totalCount += votingOptes.getVotingSelection().size();
				countMap.put(votingOptes.getId(), (long) votingOptes.getVotingSelection().size());
			}
			for (Map.Entry<Long, Long> map1 : countMap.entrySet()) {
				percentageMap.put(map1.getKey(),
						calculatePercentage(map1.getValue().doubleValue(), totalCount.doubleValue()));
			}

			for (VotingOptionsResponse votingOptes : votingResp.getVotingDetails()) {
				votingOptes.setVoteCounts(countMap.get(votingOptes.getId()));
				votingOptes.setVotePercentage(percentageMap.get(votingOptes.getId()));
			}
			votingRespList.add(votingResp);
		}
		return votingRespList;
	}

	/**
	 * calculate voting percentage
	 * 
	 * @param obtained double
	 * @param total    double
	 * @return obtained * 100 / total
	 */
	public double calculatePercentage(double obtained, double total) {
		return obtained * 100 / total;
	}

	/*
	 * return Timestamp of currentTime formated
	 */
	private String getCurrentDateTime() {
		Date date = new Timestamp(System.currentTimeMillis());
		String returnTime = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			returnTime = new Timestamp(formatter.parse(date.toString()).getTime()).toString();

		} catch (Exception ec) {
		}
		return returnTime;
	}

	/**
	 * save voting of a user
	 * 
	 * @param userId   long userId of the voting user
	 * @param familyId long, where voting is related to this family
	 * @param title    String, title of voting
	 * @param options  String[], of votings
	 * @return voting model saved
	 */
	@Override
	public Voting saveUserVoting(Long userId, Long familyId, String title, List<String> options) {

		Set<VotingOptions> voteOptions = new HashSet<>();
		for (int i = 0; i < options.size(); i++) {
			VotingOptions votingOptions = new VotingOptions(options.get(i));
			votingOptionsRepo.save(votingOptions);
			voteOptions.add(votingOptions);
		}
		Voting voting = new Voting(userId, familyId, title, getCurrentDateTime());
		voting.setVotingDetails(voteOptions);
		votingRepo.saveAndFlush(voting);
		return voting;
	}

	/**
	 * update voting for a user check if user own the voting and Add or Decrease the
	 * voting by length of options
	 * 
	 * @param id,     Long vote id
	 * @param options String[] to be updated
	 * @param userId  Long, user own a voting to update
	 * @param type    String {@value "A" | "D"} adding, decreasing the update by
	 *                options return the updated voting
	 */
	
	@Override
	public Voting updateVotingOptionsByVoteId(Long id, List<String> options, Long userId, String type) {

		User user = userService.getById(userId);
		if (user == null) {
			return null;
		}

		Voting voting = votingRepo.getById(id);
		if (ObjectUtils.isEmpty(voting.getId())) {
			return null;
		}

		if (!voting.getUserId().equals(userId)) {
			return null;
		}

		if ("A".equalsIgnoreCase(type)) {
			Set<VotingOptions> votingOptions = voting.getVotingDetails();
			for (int i = 0; i < options.size(); i++) {
				boolean isPresent = false;
				if (!votingOptions.isEmpty()) {
					for (VotingOptions votingOption : votingOptions) {
						if (votingOption.getOptions().equalsIgnoreCase(options.get(i))) {
							isPresent = true;
						}
					}
				}
				if (!isPresent) {
					VotingOptions voteoption = new VotingOptions(options.get(i));
					votingOptionsRepo.save(voteoption);
					votingOptions.add(voteoption);
				}
			}
			votingRepo.save(voting);
		} else if ("D".equalsIgnoreCase(type)) {
			Set<VotingOptions> votingOptions = voting.getVotingDetails();
			for (int i = 0; i < options.size(); i++) {
				for (VotingOptions votingOption : votingOptions) {
					if (null != votingOptions && !votingOptions.isEmpty()
							&& votingOption.getOptions().equalsIgnoreCase(options.get(i))) {
						votingOptions.remove(votingOption);
						break;
					}
				}
			}
			votingRepo.save(voting);
		}
		return voting;
	}

	/**
	 * @param id     Long voting id
	 * @param userId Long user own the voting
	 * @param option String to update option
	 * @return updated voting
	 */
	@Override
	public Voting updateVotingByIdUserIdOption(Long id, Long userId, String option) {

		User user = userService.getById(userId);
		if (user == null) {
			return null;
		}
		Voting voting = votingRepo.getById(id);
		if (ObjectUtils.isEmpty(voting.getId())) {
			return null;
		}

		if (!voting.getUserId().equals(user.getId())) {
			return null;
		}
		
		if (null != voting) {
			boolean userEligible = false;
			Long votingFamily = voting.getFamilyId();
			for (Family family : user.getFamilies()) {
				if (family.getFamilyId().equals(votingFamily)) {
					userEligible = true;
				}
			}

			if (!userEligible) {
				return null;
			}

			Set<VotingOptions> votingOptions = voting.getVotingDetails();

			for (VotingOptions votingOptes : votingOptions) {
				Set<VotingOptionsSelection> votOptsSel = votingOptes.getVotingSelection();
				for (VotingOptionsSelection voteOptSel : votOptsSel) {
					if (voteOptSel.getUserId().equals(userId)) {
						return null;
					}
				}
			}
			for (VotingOptions votingOptes : votingOptions) {
				if (votingOptes.getOptions().equalsIgnoreCase(option)) {
					Set<VotingOptionsSelection> votOptsSel = votingOptes.getVotingSelection();
					VotingOptionsSelection votingOptionsSelect = new VotingOptionsSelection(userId, id);
					votOptsSel.add(votingOptionsSelect);
					break;
				}
			}

			votingRepo.save(voting);
		} else {
			return null;
		}
		return voting;
	}

}
