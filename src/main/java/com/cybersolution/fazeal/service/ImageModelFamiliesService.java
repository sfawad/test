package com.cybersolution.fazeal.service;

import java.util.List;
import java.util.Set;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModelFamilies;

public interface ImageModelFamiliesService {

  ImageModelFamilies findByFamId(Long famId);

  Integer delete(Long id);

  List<ImageModelFamilies> fetchfamilyImages(Set<Family> familyIds);

  Long[] getfamilyImagesByFamId(Long familyId);

}
