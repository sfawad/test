package com.cybersolution.fazeal.service;

import java.util.List;
import java.util.Map;

import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.UserUpdateDetails;
import com.cybersolution.fazeal.response.TokenRefreshResponse;

public interface AuthService {

	public List<User> findAll();

	public User saveUser(User user);

	public User getUserById(Long userId);

	public User createNewUser(SignupRequest signUpRequest);

	public Map<String, List<UserResponse>> listAllFamiliesAndUsersByUserId(User user);

	public boolean isUserExistsWithUsername(String userName);

	public boolean isUserExistsWithEmail(String email);

	public UserResponse findUserById(Long userId, User user);

	public User getUserByResetToken(String token);

	boolean isTokenExpired(User user);

	public void updateUserPassword(String password, User user);

	public boolean deleteUserById(Long userId);

	public User generateTokenToResetUserPassword(String email);

	public User changeEnabledStatus(Long userId);

	public JwtResponse loginByUsernameAndPassword(String username, String password);

	public User updateUserDetails(Long userId, UserUpdateDetails userDetails);

	public TokenRefreshResponse refreshAccessToken(String refreshToken);

}
