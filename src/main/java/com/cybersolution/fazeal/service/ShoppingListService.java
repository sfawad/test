package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;

public interface ShoppingListService {

	ShoppingList findById(Long id);

	ShoppingList saveShoppingList(Long userId, ShoppingListRequest shopList);

	List<ShoppingList> getShoppingListForFamilyOnId(Long familyId);

	boolean deleteShoppingListByShoppingListId(Long shoppingListId);

	ShoppingList updateShoppingList(Long userId, ShoppingListUpdateRequest shoppingListUpdateRequest);

	boolean deleteShoppingListItemByItemId(Long itemId);

}
