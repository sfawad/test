package com.cybersolution.fazeal.service;

import com.cybersolution.fazeal.models.AlbumModelFamilies;

public interface AlbumModelFamiliesService {

  AlbumModelFamilies addFamily(AlbumModelFamilies families);
}
