package com.cybersolution.fazeal.service;

import java.util.Date;
import java.util.List;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.UpdateAlbumModelPoJo;

public interface AlbumService {

	public AlbumModel findAlbumByAlbumNameAndUserId(String albumName, Long userId);

	public AlbumModel createPrivateAlbum(AlbumModel newAlbum);

	public AlbumModel createPublicToAllUserFamiliesAlbum(AlbumModel newAlbum, User user);

	public AlbumModel createSharedToFamiliesAndUsersAlbum(List<Long> familyIds, List<Long> userIds, User user,
			AlbumModel newAlbum);

	public boolean deleteByAlbumId(Long id, String userName);

	public AlbumModel getByAlbumId(Long id);

	AlbumModel updateAlbumSetPrivateAlbum(AlbumModel newAlbum, String albumName, String description, String type,
			Date date, User user);

	AlbumModel updateAlbumSetPublicToAllUserFamiliesAlbum(AlbumModel newAlbum, String albumName, String description,
			String type, Date date, User user);

	public AlbumModel updateAlbumSetSharedToSelectedFamiliesAndUsers(User user, AlbumModel oldAlbum,
			UpdateAlbumModelPoJo albumModelPoJo);

	List<AlbumModel> findByUserId(Long id);

	public boolean updateAlbumCover(Long albumId, Long userId, Long imageId);

	public List<AlbumModel> findAlbumIfContainsNameWithUserId(String albumName, Long userId);

	public AlbumModel findByAlbumIdAndUserId(Long albumId, Long userId);

	public List<AlbumModel> getAlbumsByFamilyIds(Long familyId, Long userId);

	public List<AlbumModel> getAlbumsByUserIds(Long usersId);

	public boolean userDontHaveDeletedAlbum(Long userId);

	public AlbumModel save(AlbumModel deletedAlbum);

}
