package com.cybersolution.fazeal.service;

public interface ImageModelUserService {

  Integer delete(Long id);

  Long[] getUserImagesIdsByUserId(Long userId);
}
