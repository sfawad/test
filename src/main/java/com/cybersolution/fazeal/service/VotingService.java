package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.Voting;
import com.cybersolution.fazeal.models.VotingResponse;

public interface VotingService {

  public List<VotingResponse> getAllFamilyVotingsByFamilyId(Long familyId);

  public Voting saveUserVoting(Long userId, Long familyId, String title, List<String> options);

  public Voting updateVotingOptionsByVoteId(Long id, List<String> options, Long userId, String type);

  public Voting updateVotingByIdUserIdOption(Long id, Long userId, String option);


}
