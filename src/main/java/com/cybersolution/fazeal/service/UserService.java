package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.User;

public interface UserService {

	User getById(Long id);

	List<User> getUsersListForAFamily(Long familyId, Long userId);

	public boolean isUserInFamily(Long userId, Long familyId);

	boolean setSystemLanguage(Long userId, String systemLanguage);

	String getSystemLanguage(Long userId);

	String[] findAll(String namePrefix);

	boolean setNames(Long userId,String namePrefix, String firstName, String lastName);

}
