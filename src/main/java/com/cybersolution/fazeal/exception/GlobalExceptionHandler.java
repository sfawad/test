package com.cybersolution.fazeal.exception;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import javax.persistence.OptimisticLockException;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.cybersolution.fazeal.models.ErrorDetails;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;
import com.fasterxml.jackson.databind.JsonMappingException;

@ControllerAdvice
public class GlobalExceptionHandler {
	@Autowired
	Messages messages;


	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> ConstraintViolationExceptionHandler(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), Arrays.stream(ex.getMessage().split(","))
						.map(err -> messages.get(StringUtils.substringAfter(StringUtils.deleteWhitespace(err),":")))
				        .collect(Collectors.toList())
				        .toString(),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> globleExcpetionHandler(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.GENERAL_EXCEPTION),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<Object> accessDeniedExcpetionHandler(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Object> methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.PARAMS_INVALID),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.TYPE_MISMATCH),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(JsonMappingException.class)
	public ResponseEntity<Object> jsonMappingExceptionHandling(HttpMessageNotReadableException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(),
				messages.get(AppConstants.JSON_NOT_CORRECT) + ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<Object> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ResourceNotFoundException.class.getName(),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(OptimisticLockException.class)
	public ResponseEntity<Object> optimisticLockException(OptimisticLockException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.DB_EXCEPTION),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ConversionFailedException.class)
	public ResponseEntity<Object> conversionFailedException(ConversionFailedException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.DB_EXCEPTION),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> illegalArgumentException(IllegalArgumentException	 ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), messages.get(AppConstants.DB_EXCEPTION),
				request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
}
