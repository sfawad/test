package com.cybersolution.fazeal.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class EventsUserResponse {
	
	private Long uId;
	private String username;
	private String uemail;
	private List<EventsResponse> events;
	private String color;
	private boolean isCurrentUser;
}
