package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

  private Long id;
  private String username;
  private String email;
  private boolean enabled;
  private String createdAt;
  private String firstName;
  private String lastName;
  private String dob;
  private String gender;
  private String phone;
  private Set<Family> families = new HashSet<>();
  private String profilePicture;
  private String coverPhoto;
  private String namePrefix;
}
