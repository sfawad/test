package com.cybersolution.fazeal.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user_setting_shared_family")
public class UserSettingSharedFamily implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_id")
	private Long userId;
	@Column(name = "shared_family_id")
	private Long sharedFamilyId;

	public UserSettingSharedFamily(long userId, long sharedFamilyId) {
		super();
		this.userId = userId;
		this.sharedFamilyId = sharedFamilyId;
	}

}