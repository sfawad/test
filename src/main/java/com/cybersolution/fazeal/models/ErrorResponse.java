package com.cybersolution.fazeal.models;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement(name = "error")
public class ErrorResponse 
{
  private Date date;

public ErrorResponse(Date date, String message, List<String> details) {
    super();
    this.message = message;
    this.details = details;
    this.date = date;
  }
 
  //General error message about nature of error
  private String message;
 
  //Specific errors in API request processing
  private List<String> details;
 
  //Getter and setters
}