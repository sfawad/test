package com.cybersolution.fazeal.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class EventsFamilyResponse {

  private Long familyId;
  private String familyName;
  private List<EventsUserResponse> userEvents;
}
