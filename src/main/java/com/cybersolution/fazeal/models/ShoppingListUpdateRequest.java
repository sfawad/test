package com.cybersolution.fazeal.models;

import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.cybersolution.fazeal.customvalidators.ShoppingListNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShoppingListUpdateRequest {


	@NotNull(message = AppConstants.SHOPPINGLIST_ID_ISNULL)
	@Positive(message = AppConstants.SHOPPINGLIST_ID_POSITIVE)
	private Long id;

	@NotBlank
	@ShoppingListNameConstraint(message=AppConstants.SHOPPINGLIST_NAME_INVALID)  
	private String shoppingListName;

	private boolean listStatus;

	// Shopping list items
	private Map<@ShoppingListNameConstraint(message = AppConstants.SHOPPINGLIST_ITEM_NAME_INVALID) String, Boolean> itemMap;

}
