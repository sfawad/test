package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class VotingResponse {

  private Long id;
  private Long userId; // user who created it
  private Long familyId; // family
  private String info;
  private String createdAt;
  private Set<VotingOptionsResponse> votingDetails = new HashSet<>();

}
