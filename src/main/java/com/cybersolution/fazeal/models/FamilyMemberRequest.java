package com.cybersolution.fazeal.models;

import java.util.Map;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FamilyMemberRequest {
	// Family Members
		private Map<@NotBlank String, @NotBlank String> familyMemberMap;
}
