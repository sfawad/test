package com.cybersolution.fazeal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "shoppinglist_items")
public class ShoppingListEntries {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String item;
	@Column(name = "item_status")
	private boolean itemStatus;

	private Long userId;

	private String userName;
	@Column(name = "shoppinglist_id")
	private Long shoppinglistId;

	public ShoppingListEntries(String item, boolean itemStatus, Long userId, String userName) {
		super();
		this.item = item;
		this.itemStatus = itemStatus;
		this.userId = userId;
		this.userName = userName;
	}
}
