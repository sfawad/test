package com.cybersolution.fazeal.models;

import java.util.Map;


import com.cybersolution.fazeal.customvalidators.SocialMediaConstraint;
import com.cybersolution.fazeal.customvalidators.UserNameConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.cybersolution.fazeal.util.AppConstants;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SocialMediaLinksRequest {

	// Social Media links
	private Map<@SocialMediaConstraint(message=AppConstants.SOCIAL_MEDIA_NOT_FROM_LIST) String, @UserNameConstraint(message=AppConstants.SOCIAL_MEDIA_USER_NAME_NOT_EMPTY) String> socialMediaLinksMap;

}
