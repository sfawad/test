package com.cybersolution.fazeal.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "image_model")
public class ImageModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Date date;
	private String extension;

	@Column(columnDefinition = "TEXT")
	private String localPath;
	@Column(name = "user_id")
	private Long userId;
	private String type;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "album_id")
	private Long albumId;
	private String description;
	@Column(name = "post_id")
	private Long postId;
	@Column(name = "mark_to_be_deleted")
	private Boolean markedToBeDeleted;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "imagemodel_id")
	private Set<ImageModelFamilies> imageFamiliesList = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "imagemodel_id")
	private Set<ImageModelUser> imageUserList = new HashSet<>();

}