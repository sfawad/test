package com.cybersolution.fazeal.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "family")
public class Family implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long familyId;
	private String token;
	private String name;
	private String phone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String about;
	@Column(name = "is_group")
	private boolean isGrp;
	@Column(name = "is_public")
	private boolean isPub;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_pending_family", joinColumns = @JoinColumn(name = "family_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> pendingUser = new HashSet<>();

	@Column(name = "profile_image")
	private String profileImage;
	@Column(name = "dashboard_image")
	private String dashboardImage;

	public Family(String name, String phone, String address1, String address2, String city, String state, String zip,
			String country) {
		super();
		this.name = name;
		this.phone = phone;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
	}
}
