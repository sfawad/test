package com.cybersolution.fazeal.models;

import javax.validation.constraints.*;

import com.cybersolution.fazeal.customvalidators.PasswordConstraint;
import com.cybersolution.fazeal.customvalidators.SignUpNameConstraint;
import com.cybersolution.fazeal.customvalidators.UserNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SignupRequest {
    @UserNameConstraint(message = AppConstants.USER_NAME_NOT_EMPTY)
    private String username;

    @NotBlank(message = AppConstants.EMAIL_NOT_EMPTY )
    @Size(max = 50,message = AppConstants.EMAIL_SIZE_INVALID)
    @Email(message = AppConstants.EMAIL_INVALID)
    private String email;
    
    private String role;

    @PasswordConstraint(message = AppConstants.PASSWORD_NOT_EMPTY)
    private String password;
    
    private boolean enabled;

    @SignUpNameConstraint(message = AppConstants.FIRST_NAME_NOT_EMPTY)
    private String firstName;

    @SignUpNameConstraint(message = AppConstants.LAST_NAME_NOT_EMPTY)
    private String lastName;

    @NotBlank(message = AppConstants.DATE_OF_BIRTH_EMPTY)
    private String dob;
    
    private String  gender;  
    
    private boolean createFamilyAccount = false; // Join family or create (Boolean true - create family/false - join family)
    
    private String  familyToken;
    
    private String  phone;
   
}
