package com.cybersolution.fazeal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "image_model_families")
public class ImageModelFamilies {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "imagemodel_id")
	private Long imagemodelId;
	@Column(name = "family_id")
	private Long familyId;

	public ImageModelFamilies(Long imagemodelId, long familyId) {
		super();
		this.imagemodelId = imagemodelId;
		this.familyId = familyId;
	}
}