package com.cybersolution.fazeal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "event_records")
public class EventsRelation {

  @Id
  private Long eid;
  private Long uid;
  private String uemail;
  private String username;
  @Column(name = "family_name")
  private String familyName;
  private String event;
  private String edate;
  private String estart;
  private String eend;
  @Column(name = "is_shared")
  private String isShared;
  @Column(name = "family_id")
  private Long familyId;

}
