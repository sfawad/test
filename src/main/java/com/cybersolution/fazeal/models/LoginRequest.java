package com.cybersolution.fazeal.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.cybersolution.fazeal.customvalidators.PasswordConstraint;
import com.cybersolution.fazeal.customvalidators.UserNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LoginRequest {
	@UserNameConstraint(message = AppConstants.USER_NAME_INVALID)
	private String username;

	@PasswordConstraint(message = AppConstants.PASSWORD_INVALID)
	private String password;
}
