package com.cybersolution.fazeal.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class EventsRelationResponse {

  private Long eid;
  private Long uid;
  private String uemail;
  private String username;
  private String familyName;
  private String event;
  private String edate;
  private String estart;
  private String eend;
  private String isShared;
  private Long familyId;
  private String color;

}
