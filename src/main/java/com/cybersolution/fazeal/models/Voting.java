package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "voting")
public class Voting {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long userId; // user who created it
	private Long familyId; // family
	private String info;
	@Column(name = "created_at")
	private String createdAt;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "voting_id")
	private Set<VotingOptions> votingDetails = new HashSet<>();

	public Voting(@NotBlank Long userId, Long familyId, String info, String createdAt) {
		super();
		this.userId = userId;
		this.familyId = familyId;
		this.info = info;
		this.createdAt = createdAt;
	}

}
