package com.cybersolution.fazeal.models;

import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.cybersolution.fazeal.customvalidators.ShoppingListNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShoppingListRequest {

	@NotBlank
	@ShoppingListNameConstraint(message=AppConstants.SHOPPINGLIST_NAME_INVALID)  
	private String shoppinglistName;

	@NotNull(message = AppConstants.FAMILY_ID_ISNULL)
	@Positive(message = AppConstants.FAMILY_ID_POSITIVE)
	private Long familyId;
	
	private boolean isActive;

	// Shopping list items
	private Map<@ShoppingListNameConstraint(message = AppConstants.SHOPPINGLIST_ITEM_NAME_INVALID) String, Boolean> itemMap;

}
