package com.cybersolution.fazeal.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "album_model")
public class AlbumModel {

	public AlbumModel(AlbumModel albumModel) {
		this.albumName = albumModel.getAlbumName();
		this.albumId = albumModel.getAlbumId();
		this.description = albumModel.getDescription();
		this.date = albumModel.getDate();
		this.userId = albumModel.getUserId();
		this.type = albumModel.getType();
		this.albumFamiliesList = albumModel.getAlbumFamiliesList();
		this.albumUserList = albumModel.getAlbumUserList();
		this.coverPhoto = albumModel.getCoverPhoto();
		this.albumImages = albumModel.getAlbumImages();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "album_id")
	@NotFound(action = NotFoundAction.IGNORE)
	private Long albumId;
	@Column(name = "album_name")
	private String albumName;
	private String description;
	private Date date;
	@Column(name = "update_date")
	private Date updateDate;
	@Column(name = "user_id")
	private Long userId;
	private String type;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "album_id")
	private Set<ImageModel> albumImages = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "album_id")
	private Set<AlbumModelFamilies> albumFamiliesList = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "album_id")
	private Set<AlbumModelUser> albumUserList = new HashSet<>();

	@Column(name = "cover_photo")
	private String coverPhoto;
}
