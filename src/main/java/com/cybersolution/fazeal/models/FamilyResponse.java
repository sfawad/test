package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
/**
 * 
 * @author Khalil
 * @FamilyResponse Retruned data on family Controller
 */
public class FamilyResponse {
	private Long familyId;
	private String token;
	private String name;
	private String phone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private boolean isPub;
	private boolean isGrp;
	private String profileImage;
	private String dashboardImage;
	private Set<UserResponse> pendingUser = new HashSet<>();
}
