package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "shopping")
public class ShoppingList {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String shoppinglistName;

	private Long familyId;
	@Column(name = "date_create")
	private String dateCreate;
	@Column(name = "is_active")
	private boolean isActive;
	@Column(name = "updated_at")
	private String updatedAt;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "shoppinglist_id")
	private Set<ShoppingListEntries> shoppingItemsList = new HashSet<>();

	public ShoppingList(String shoppinglistName, Long familyId, String dateCreate, boolean isActive, String updatedAt) {
		super();
		this.shoppinglistName = shoppinglistName;
		this.familyId = familyId;
		this.dateCreate = dateCreate;
		this.isActive = isActive;
		this.updatedAt = updatedAt;
	}

}
