package com.cybersolution.fazeal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "voting_selection")
public class VotingOptionsSelection {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;
	@Column(name = "voting_options_id")
	private Long votingOptionsId;

	public VotingOptionsSelection(Long userId, Long votingOptionsId) {
		super();
		this.userId = userId;
		this.votingOptionsId = votingOptionsId;
	}

}
