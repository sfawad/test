package com.cybersolution.fazeal.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "family_users_roles")
public class FamilyUserRoles {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Long familyId;
	
	private Long userId;
	
	private String role;
	
	// Relation with family
	private String relation;

	public FamilyUserRoles(Long familyId, Long userId, String role, String relation) {
		super();
		this.familyId = familyId;
		this.userId = userId;
		this.role = role;
		this.relation = relation;
	}
}
