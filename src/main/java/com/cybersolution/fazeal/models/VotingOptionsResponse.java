package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor

public class VotingOptionsResponse {

  private Long id;

  private String options;

  private Long votingId;

  private Set<VotingOptionsSelectionResponse> votingSelection = new HashSet<>();

  private Long voteCounts;

  private double votePercentage;

}
