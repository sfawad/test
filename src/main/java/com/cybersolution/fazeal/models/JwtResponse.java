package com.cybersolution.fazeal.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponse {
	private String accessToken;
	private String refreshToken;
	private String tokenType = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;
	private String firstName;
	private String lastName;
	private String theme;
}
