package com.cybersolution.fazeal.models;

public enum ERole {
	ROLE_USER,
    ROLE_ADMIN
}
