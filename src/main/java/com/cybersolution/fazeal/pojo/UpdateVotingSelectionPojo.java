package com.cybersolution.fazeal.pojo;

import javax.validation.constraints.NotEmpty;

import com.cybersolution.fazeal.customvalidators.VotingOptionNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateVotingSelectionPojo {
	
	@NotEmpty(message = AppConstants.VOTING_OPTIONS_ISNULL)
	@VotingOptionNameConstraint(message = AppConstants.VOTING_OPTIONS_INVALID) 
	String options; 
	
}
