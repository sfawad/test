package com.cybersolution.fazeal.pojo;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.cybersolution.fazeal.customvalidators.VotingOptionsConstraint;
import com.cybersolution.fazeal.customvalidators.VotingTypeConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateVotingPojo {
	
	@NotEmpty(message = AppConstants.VOTING_OPTIONS_ISNULL)
	@VotingOptionsConstraint(message = AppConstants.VOTING_OPTIONS_INVALID) 
	List<String> options; 
	
	@VotingTypeConstraint(message = AppConstants.VOTING_TYPE)
	String type;
	
}
