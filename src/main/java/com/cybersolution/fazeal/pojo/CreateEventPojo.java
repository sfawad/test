package com.cybersolution.fazeal.pojo;



import com.cybersolution.fazeal.util.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateEventPojo {
    @NotBlank(message=AppConstants.EVENT_NAME_NOT_EMPTY)
    private String eventName;
    private String date;
    private String startTime;
    private String endTime;
    @NotNull(message=AppConstants.EVENT_IS_SHARED_WITH_FAMILY_NOT_EMPTY)
    private Boolean isShared;
    private Long[] familyIds;
}
