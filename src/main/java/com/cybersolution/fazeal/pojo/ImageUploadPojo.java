package com.cybersolution.fazeal.pojo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.cybersolution.fazeal.customvalidators.AlbumNameConstraint;
import com.cybersolution.fazeal.customvalidators.ImageTypeConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ImageUploadPojo {
	@NotNull
	@DateTimeFormat(pattern = AppConstants.DATE_TIME_PATTERN)
	Date date;

	@ImageTypeConstraint(message = AppConstants.IMAGE_TYPE_NOT_FROM_LIST)
	String type;

	@AlbumNameConstraint(message = AppConstants.ALBUM_NAME_SIZE)
	String albumName;

	MultipartFile fileImage;

	String description;
}
