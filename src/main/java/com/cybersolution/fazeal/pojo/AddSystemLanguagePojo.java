package com.cybersolution.fazeal.pojo;

import java.io.Serializable;

import com.cybersolution.fazeal.customvalidators.SystemLanguagesConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class AddSystemLanguagePojo implements Serializable {

	@SystemLanguagesConstraint(message=AppConstants.LANGUAGES_NOT_FROM_LIST)
	private String systemLanguage;

}
