package com.cybersolution.fazeal.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Notification {

    private String emailId;
    private String mobileNumber;
    private String notificationType;
    private String familyToken;
    private String resetToken;
    private String messageType;

}
