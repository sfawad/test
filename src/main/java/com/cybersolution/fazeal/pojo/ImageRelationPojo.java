package com.cybersolution.fazeal.pojo;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cybersolution.fazeal.customvalidators.AlbumTypeConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ImageRelationPojo {
	@AlbumTypeConstraint(message = AppConstants.ALBUM_TYPE_NOT_VALID)
	private String type;
	List<Long> familyIds;
	List<Long> userIds;
}
