package com.cybersolution.fazeal.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class FamilyUpdatePoJo {

	private String name;
	private String phone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String relationWithFamily;
	private String about;

}
