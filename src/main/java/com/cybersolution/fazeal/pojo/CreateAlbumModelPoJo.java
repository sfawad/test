package com.cybersolution.fazeal.pojo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.cybersolution.fazeal.customvalidators.AlbumNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class CreateAlbumModelPoJo {

	@NotNull(message = AppConstants.USERID_ISNULL)
	Long userId;
	private List<Long> familyIds;
	private List<Long> userIds;
	@NotNull(message = AppConstants.ALBUM_NAME_ISNULL)
	@AlbumNameConstraint(message = AppConstants.ALBUM_NAME_SIZE)
	private String albumName;
	private String description;
	@NotNull(message = AppConstants.ALBUM_TYPE_SIZE)
	@Size(min = 5, max = 5, message = AppConstants.ALBUM_TYPE_SIZE)
	private String type;
	@NotNull(message = AppConstants.DATE_ISNULL)
	@DateTimeFormat(pattern = AppConstants.DATE_TIME_PATTERN)
	private Date date;

}
