package com.cybersolution.fazeal.pojo;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.cybersolution.fazeal.customvalidators.VotingOptionsConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveVotingPojo {
	
	@NotNull(message = AppConstants.FAMILY_ID_ISNULL)
	@Positive(message = AppConstants.FAMILY_ID_POSITIVE)
	Long familyId;
	
	@NotNull(message = AppConstants.TITLE_ISNULL)
	@Size(min = 4, max = 10, message = AppConstants.VOTING_TITLE_SIZE)
	String title;
	
    @NotEmpty(message = AppConstants.VOTING_OPTIONS_ISNULL)
	@VotingOptionsConstraint(message = AppConstants.VOTING_OPTIONS_INVALID) 
	List<String> options; 
	
}
