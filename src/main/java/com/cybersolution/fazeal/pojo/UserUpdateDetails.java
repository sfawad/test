package com.cybersolution.fazeal.pojo;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.cybersolution.fazeal.util.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class UserUpdateDetails implements Serializable {

	@Size(max = 50,message = AppConstants.EMAIL_SIZE_INVALID)
	@Email(message=AppConstants.EMAIL_INVALID)
	private String email;

	private String password;

	private String firstName;

	private String lastName;

	private String dateOfBirth;

	private String nickname;
	private String gender;

	private String aboutMe;

	private String languages;

}
