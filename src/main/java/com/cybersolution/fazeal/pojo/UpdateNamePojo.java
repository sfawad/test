package com.cybersolution.fazeal.pojo;

import com.cybersolution.fazeal.customvalidators.NamePrefixConstraint;
import com.cybersolution.fazeal.customvalidators.SignUpNameConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateNamePojo {
	
	
	@NamePrefixConstraint(message=AppConstants.NAME_PREFIX)
	private String namePrefix;

	@SignUpNameConstraint(message = AppConstants.FIRST_NAME_NOT_EMPTY)
    private String firstName;

    @SignUpNameConstraint(message = AppConstants.LAST_NAME_NOT_EMPTY)
    private String lastName;
}
