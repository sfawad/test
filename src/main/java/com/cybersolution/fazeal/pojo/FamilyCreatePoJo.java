package com.cybersolution.fazeal.pojo;

import com.cybersolution.fazeal.util.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class FamilyCreatePoJo {

	@NotBlank(message = AppConstants.FAMILY_NAME_NOT_EMPTY)
	private String name;
	@NotBlank(message = AppConstants.PHONE_NOT_EMPTY)
	private String phone;
	@NotBlank(message = AppConstants.ADDRESS1_NOT_EMPTY)
	private String address1;
	private String address2;
	@NotBlank(message = AppConstants.CITY_NOT_EMPTY)
	private String city;
	@NotBlank(message = AppConstants.STATE_NOT_EMPTY)
	private String state;
	@NotBlank(message = AppConstants.ZIP_NOT_EMPTY)
	private String zip;
	@NotBlank(message = AppConstants.COUNTRY_NOT_EMPTY)
	private String country;
	private String about;
	@NotBlank(message = AppConstants.RELATION_WITH_FAMILY_NOT_EMPTY)
	private String relationWithFamily;

}
