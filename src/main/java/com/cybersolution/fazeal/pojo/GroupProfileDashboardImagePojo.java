package com.cybersolution.fazeal.pojo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.opsworks.model.App;
import com.cybersolution.fazeal.customvalidators.GroupProfileDashboardImageTypeConstraint;
import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GroupProfileDashboardImagePojo {
	@NotNull
	@DateTimeFormat(pattern = AppConstants.DATE_TIME_PATTERN)
	Date date;

	@GroupProfileDashboardImageTypeConstraint(message = AppConstants.IMAGE_TYPE_NOT_FROM_LIST)
	String type;

	MultipartFile fileImage;

	@NotNull(message = AppConstants.FAMILY_ID_ISNULL)
	Long familyId;
}
