package com.cybersolution.fazeal.pojo;

import javax.validation.constraints.NotNull;

import com.cybersolution.fazeal.util.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AlbumCoverPojo {
	@NotNull(message = AppConstants.USERID_ISNULL)
	Long userId;
	@NotNull(message = AppConstants.IMAGEID_ISNULL)
	Long imageId;
}
