package com.cybersolution.fazeal.fileio;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cybersolution.fazeal.util.AppConstants;

public class FileUploadUtil {

	private FileUploadUtil() {
		// not called
	}

	public static void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws IOException {
		Path uploadPath = Paths.get(uploadDir);

		if (!Files.exists(uploadPath)) {
			Files.createDirectories(uploadPath);
		}

		try (InputStream inputStream = multipartFile.getInputStream()) {
			Path filePath = uploadPath.resolve(fileName);
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new IOException(AppConstants.ERROR_SAVE_FILE, e);
		}
	}

	@Transactional
	public static void deleteImage(String imagePath) throws IOException {
		Path file = Paths.get(imagePath);
		if (!Files.isDirectory(file)) {
			Files.delete(file);
		}
	}

	public static File resizeImage(MultipartFile image, Integer width, Integer height, String extension)
			throws IOException {
		InputStream is = new ByteArrayInputStream(image.getBytes());
		BufferedImage bufferedImage = ImageIO.read(is);
		BufferedImage resized = Scalr.resize(bufferedImage, Scalr.Method.AUTOMATIC, Scalr.Mode.FIT_EXACT, width, height,
				Scalr.OP_ANTIALIAS);
		File outputfile = new File("a." + extension);
		ImageIO.write(resized, extension, outputfile);
		return outputfile;
	}

}
