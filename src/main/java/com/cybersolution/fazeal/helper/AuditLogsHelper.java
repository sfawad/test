package com.cybersolution.fazeal.helper;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.cybersolution.fazeal.models.AuditLogs;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.AuditLogsService;
import com.cybersolution.fazeal.util.AppConstants;
import com.cybersolution.fazeal.util.Utils;

@Component
public class AuditLogsHelper {

  private static final Logger LOGGER = LogManager.getLogger(AuditLogsHelper.class);

  @Autowired
  UserRepository userRepo;

  @Autowired
  AuditLogsService auditLogsService;

  /**
   * @return void
   * @apiNote Used to save audit logs for a user
   * @Param userId
   * @param action
   * @param deviceType
   */
  public void saveAuditLogs(Long userId, String action, HttpServletRequest httpReq, boolean isSuccess, String reason) {

    LOGGER.traceEntry("saveAuditLogs() --> ", userId + " " + action);
    User user = userRepo.getById(userId);

    AuditLogs auditLogs = new AuditLogs(null, userId, user.getUsername(), Utils.getCurrentDateTime(), action,
        getDeviceType(httpReq), isSuccess, reason);
    auditLogsService.save(auditLogs);
    LOGGER.traceExit();
  }

  /**
   * @return device details
   * @apiNote Used to get device details from HttpServletRequest for logging in audit logs
   * @Param HttpServletRequest
   */
  public String getDeviceType(HttpServletRequest httpReq) {
    LOGGER.traceEntry("getDeviceType() --> ", httpReq);
    String device = null;
    if(!ObjectUtils.isEmpty(httpReq) && !ObjectUtils.isEmpty(httpReq.getHeader(AppConstants.USER_AGENT))) {
      LOGGER.info("User Agent Details {}: ", httpReq.getHeader(AppConstants.USER_AGENT));
      String[] deviceDetails = httpReq.getHeader(AppConstants.USER_AGENT).split(" ");
      if(deviceDetails.length > 1)
        device = deviceDetails[0] + deviceDetails[1];
      else
        device = deviceDetails[0];
    }
    LOGGER.traceExit();
    return device;
  }
}