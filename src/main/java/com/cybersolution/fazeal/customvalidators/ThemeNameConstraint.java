package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = ThemeNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface ThemeNameConstraint {
    String message() default AppConstants.THEME_FROM_LIST;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
