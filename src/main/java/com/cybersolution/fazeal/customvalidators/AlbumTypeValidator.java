package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class AlbumTypeValidator implements ConstraintValidator<AlbumTypeConstraint, String> {

	@Value(value = "#{'${album.type}'.split(',')}")
	List<String> types;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return types.contains(value);
	}

}
