package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class SocialMediaValidator implements ConstraintValidator<SocialMediaConstraint, String> {

	@Value(value = "#{'${social.media.websites}'.split(',')}")
	List<String> websites;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value != null && websites.contains(value);
	}

}
