package com.cybersolution.fazeal.customvalidators;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class VotingOptionsValidator implements ConstraintValidator<VotingOptionsConstraint, List<String>> {

	@Value(value = "${voting.option.regex}")
	String regex;

	@Override
	public boolean isValid(List<String> value, ConstraintValidatorContext csontext) {
		Pattern pattern = Pattern.compile(regex);
		List<Matcher> notAcceptedPattern = value.stream().map(val -> pattern.matcher(val.trim()))
				.filter(matcher -> matcher.find()).collect(Collectors.toList());
		List<String> notAcceptedLength = value.stream().filter(val -> !(val.length() > 2 && val.length() < 40))
				.collect(Collectors.toList());
		return notAcceptedPattern.size() < 1 && notAcceptedLength.size() <1;
	}
}