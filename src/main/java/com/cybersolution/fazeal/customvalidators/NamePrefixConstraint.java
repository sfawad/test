package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = NamePrefixValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface NamePrefixConstraint {
    String message() default AppConstants.NAME_PREFIX;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
