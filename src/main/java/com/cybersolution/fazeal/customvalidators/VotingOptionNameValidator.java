package com.cybersolution.fazeal.customvalidators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class VotingOptionNameValidator implements ConstraintValidator<VotingOptionNameConstraint, String> {

	@Value(value = "${votingoption.name.regex}")
	String regex;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(value==null) {
			return value!=null;
		}
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value);
		return !matcher.find() && (value.length() > 3 && value.length() < 40);
	}

}
