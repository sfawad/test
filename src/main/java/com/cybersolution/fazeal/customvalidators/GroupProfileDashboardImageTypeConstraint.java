package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = GroupProfileDashboardImageTypeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupProfileDashboardImageTypeConstraint {
	String message() default AppConstants.IMAGE_TYPE_NOT_FROM_LIST;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
