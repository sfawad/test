package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = VotingTypeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface VotingTypeConstraint {
    String message() default AppConstants.VOTING_TYPE;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
