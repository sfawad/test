package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = AlbumTypeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface AlbumTypeConstraint {
    String message() default AppConstants.THEME_FROM_LIST;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
