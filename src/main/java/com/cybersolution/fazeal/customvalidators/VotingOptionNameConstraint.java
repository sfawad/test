package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = VotingOptionNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface VotingOptionNameConstraint {
	 String message() default AppConstants.SHOPPINGLIST_NAME_INVALID;
	    Class<?>[] groups() default {};
	    Class<? extends Payload>[] payload() default {};
}
