package com.cybersolution.fazeal.customvalidators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

import com.cybersolution.fazeal.util.AppConstants;

public class AlbumNameValidator implements ConstraintValidator<AlbumNameConstraint, String> {

	@Value(value = "${album.name.regex}")
	String regex;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null)
			return value != null;
		if(value.equals(AppConstants.DASHBOARD_PICTURE) || value.equals(AppConstants.PROFILE_PICTURE))
			return true;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value);
		return !matcher.find() && (value.length() > 2 && value.length() < 40);
	}

}
