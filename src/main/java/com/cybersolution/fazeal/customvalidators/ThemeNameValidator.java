package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class ThemeNameValidator implements ConstraintValidator<ThemeNameConstraint, String> {

	@Value(value = "#{'${theme.names}'.split(',')}")
	List<String> themes;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return themes.contains(value);
	}

}
