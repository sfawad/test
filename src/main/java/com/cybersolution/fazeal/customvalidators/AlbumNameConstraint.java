package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Constraint(validatedBy = AlbumNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface AlbumNameConstraint {
	 String message() default AppConstants.ALBUM_NAME_SIZE;
	    Class<?>[] groups() default {};
	    Class<? extends Payload>[] payload() default {};
}
