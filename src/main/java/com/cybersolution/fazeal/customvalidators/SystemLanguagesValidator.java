package com.cybersolution.fazeal.customvalidators;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cybersolution.fazeal.repository.SystemLanguagesRepository;

public class SystemLanguagesValidator implements ConstraintValidator<SystemLanguagesConstraint, String> {

	
	@Autowired
	SystemLanguagesRepository systemLanguagesRepository;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		String[] pattern = systemLanguagesRepository.findByLanguage();
		List<String> languages=Arrays.asList(pattern);
		return value != null && languages.contains(value);
	}

}
