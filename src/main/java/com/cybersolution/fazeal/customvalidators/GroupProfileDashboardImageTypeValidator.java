package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class GroupProfileDashboardImageTypeValidator implements ConstraintValidator<GroupProfileDashboardImageTypeConstraint, String> {

	@Value(value = "#{'${group.image.type}'.split(',')}")
	List<String> types;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value != null && types.contains(value);
	}

}
