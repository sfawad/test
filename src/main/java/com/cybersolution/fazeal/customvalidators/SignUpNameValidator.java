package com.cybersolution.fazeal.customvalidators;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpNameValidator implements ConstraintValidator<SignUpNameConstraint, String> {

	@Value(value = "${auth.signup.name.regex}")
	String regex;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value.trim());
		return value != null && matcher.matches();
	}

}
