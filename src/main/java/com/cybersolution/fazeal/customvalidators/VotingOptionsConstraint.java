package com.cybersolution.fazeal.customvalidators;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cybersolution.fazeal.util.AppConstants;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = VotingOptionsValidator.class)
public @interface VotingOptionsConstraint {
	 String message() default AppConstants.VOTING_OPTIONS_ISNULL;
	    Class<?>[] groups() default {};
	    Class<? extends Payload>[] payload() default {};
}