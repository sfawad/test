package com.cybersolution.fazeal.customvalidators;

import com.cybersolution.fazeal.util.AppConstants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PasswordValidator.class)
public @interface PasswordConstraint {
	 String message() default AppConstants.PASSWORD_NOT_EMPTY;
	    Class<?>[] groups() default {};
	    Class<? extends Payload>[] payload() default {};
}