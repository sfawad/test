package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class NamePrefixValidator implements ConstraintValidator<NamePrefixConstraint, String> {

	@Value(value = "#{'${users.name.prefix}'.split(',')}")
	List<String> prefix;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value != null && prefix.contains(value);
	}

}
