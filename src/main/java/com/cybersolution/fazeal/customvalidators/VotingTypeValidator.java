package com.cybersolution.fazeal.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class VotingTypeValidator implements ConstraintValidator<VotingTypeConstraint, String> {

	@Value(value = "#{'${voting.type}'.split(',')}")
	List<String> type;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value != null && type.contains(value);
	}

}
