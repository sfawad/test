package com.cybersolution.fazeal.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.HttpStatusRequestRejectedHandler;
import org.springframework.security.web.firewall.RequestRejectedHandler;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.cybersolution.fazeal.security.jwt.AuthEntryPointJwt;
import com.cybersolution.fazeal.security.jwt.AuthTokenFilter;
import com.cybersolution.fazeal.security.services.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
     securedEnabled = true,
     jsr250Enabled = true,
    prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  UserDetailsServiceImpl userDetailsService;

  @Autowired
  protected AuthEntryPointJwt unauthorizedHandler;

  @Value("${origin.domain}")
  private String domain;

  @Value("${origin.support}")
  private String support;

  @Value("${origin.help}")
  private String help;

  @Value("${origin.api}")
  private String api;

  @Value("${isDEVMode}")
  private boolean isDev = false;

  @Bean
  public AuthTokenFilter authenticationJwtTokenFilter() {
    return new AuthTokenFilter();
  }

  @Override
  public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
    authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
        .antMatchers("/api/v1/auth/**").permitAll().antMatchers("/api/v1/events/**").permitAll().antMatchers("/api/v1/topic/**").permitAll()
        .antMatchers("/api/v1/images/**").permitAll().antMatchers("/api/v1/family/**").permitAll()
        .antMatchers("/api/v1/notifications/**").permitAll().antMatchers("/api/v1/shoppingList/**").permitAll()
        .antMatchers("/api/v1/voting/**").permitAll().antMatchers("/api/v1/generalAccount/**").permitAll().antMatchers("/api/v1/userInformation/**").permitAll().antMatchers("/error").permitAll().anyRequest().authenticated();
    http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  public CorsFilter corsFilter() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    final CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin(domain);
    config.addAllowedOrigin(support);
    config.addAllowedOrigin(help);
    config.addAllowedOrigin(api);
    config.setAllowedMethods(Arrays.asList(HttpMethod.GET.name(), HttpMethod.PUT.name(), HttpMethod.POST.name(),
        HttpMethod.OPTIONS.name(), HttpMethod.DELETE.name()));
    config.setAllowedHeaders(Arrays.asList("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
    source.registerCorsConfiguration("/**", config);
    return new CorsFilter(source);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    if(isDev) web.ignoring().antMatchers("/v2/api-docs", "/configuration/**", "/swagger-resources/**",
        "/swagger-ui.html", "/webjars/**", "/api-docs/**");
    web.httpFirewall(configureFirewall());
  }

  @Bean
  public HttpFirewall configureFirewall() {
      StrictHttpFirewall strictHttpFirewall = new StrictHttpFirewall();
      strictHttpFirewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT"));
      return strictHttpFirewall;
  }

  @Bean
  public RequestRejectedHandler requestRejectedHandler() {
		return new HttpStatusRequestRejectedHandler();
	}
}
