package com.cybersolution.fazeal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.client.RestTemplate;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.GroupProfileDashboardImagePojo;
import com.cybersolution.fazeal.pojo.ImageRelationPojo;
import com.cybersolution.fazeal.pojo.ImageUploadPojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageModelFamiliesService;
import com.cybersolution.fazeal.service.ImageModelUserService;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ImageControllerTest {

	@InjectMocks
	private ImageController imageController;

	@Mock
	private ImageService imageService;

	@Mock
	private UserService userService;

	@Mock
	public RestTemplate restTemplate;

	@Mock
	public ImageModelFamiliesService imageModelFamiliesService;

	@Mock
	public ImageModelUserService imageModelUserService;

	@Mock
	public AlbumService albumService;

	@Mock
	private Messages messages;

	@Mock
	private FamilyService familyService;

	public static final String FILE_NAME = "fileNameReturned";
	public static final String NAME_OF_THE_FILE = "NameOfTheFile";
	public static final String FILE_TYPE = "image/png";
	public static final String DESCRIPTION = "short desc";
	public static final String ALBUM_NAME = "Album";
	public static final String LOCAL_PATH = "1/albummodel/dog.png";

	Date date = new Date(System.currentTimeMillis());
	List<Long> familyIds = new ArrayList<>();
	List<Long> userIds = new ArrayList<>();

	String[] type = { "P", "D" };
	Family family1 = new Family(1L, "about", null, "1L", "token13", "fam1", "line1", "line2", "city", "state", null,
			false, false, null, null, null);
	Set<Family> families = Set.of(family1);

	@Test
	public void uploadImageTestCase() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("P").albumImages(null).build();
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 1L)).thenReturn(album);
		when(imageService.uploadImages(date, file, 1L, "c", "My Album", "description", FILE_NAME, album))
				.thenReturn(FILE_NAME);
		ImageUploadPojo imageUploadPojo = ImageUploadPojo.builder().date(date).fileImage(file).type("C")
				.description(DESCRIPTION).albumName(ALBUM_NAME).build();
		ResponseEntity response = imageController.uploadImage(2L, imageUploadPojo);
		assertNotNull(response);
	}

	@Test
	public void uploadImageFailToSaveTestCase() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("P").albumImages(null).build();
		when(messages.get("ERROR_INTERNAL")).thenReturn("Error: Something went wrong.");
		when(imageService.uploadImages(date, file, 1L, "c", "My Album", DESCRIPTION, FILE_NAME, album)).thenReturn("");
		ImageUploadPojo imageUploadPojo = ImageUploadPojo.builder().date(date).fileImage(file).type("P")
				.description(DESCRIPTION).albumName(ALBUM_NAME).build();
		MessageResponse response = (MessageResponse) imageController.uploadImage(2L, imageUploadPojo).getBody();
		String body = String.valueOf(response.getMessage());
		assertTrue(body.contains("Error: Something went wrong."));
	}

	@Test
	public void uploadImageMissingDataTestCase() throws IOException {
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("P").albumImages(null).build();
		when(messages.get(AppConstants.IMAGE_ISNULL)).thenReturn("Error: Please check input parameters.");
		when(imageService.uploadImages(null, null, null, null, null, DESCRIPTION, FILE_NAME, album)).thenReturn(null);
		ImageUploadPojo imageUploadPojo = ImageUploadPojo.builder().date(date).fileImage(null).type("c")
				.description(DESCRIPTION).albumName(ALBUM_NAME).build();
		MessageResponse response = (MessageResponse) imageController.uploadImage(2L, imageUploadPojo).getBody();
		String body = String.valueOf(response.getMessage());
		assertTrue(body.contains("Error: Please check input parameters."));
	}

	@Test
	public void getImagesListByIdTestCase() {
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		List<ImageModel> images = new ArrayList<>();
		images.add(imageModel);
		Long userId = imageModel.getUserId();
		when(imageService.findByUserId(userId)).thenReturn(images);
		ResponseEntity<?> result = imageController.getUserImages(Long.valueOf(1));
		Object response = result.getBody();
		assertEquals(images, response);
	}

	@Test
	public void getImageslistUserImagesByTypeTestCase() {
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("jpg").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		List<ImageModel> images = new ArrayList<>();
		images.add(imageModel);
		Long userId = imageModel.getUserId();
		when(imageService.findByUserIdAndType(userId, type)).thenReturn(images);
		ResponseEntity<?> result = imageController.getUserImagesByUserIdAndType(Long.valueOf(1), type);
		Object response = result.getBody();
		assertEquals(images, response);
	}

	@Test
	public void testListAlbumImages() {
		AlbumModelFamilies albumModelFamilies1 = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 1L, 230L);

		AlbumModelUser albumModelUser1 = new AlbumModelUser(null, 1L, 200L);
		AlbumModelUser albumModelUser2 = new AlbumModelUser(null, 1L, 230L);

		AlbumModel userAlbums = AlbumModel.builder().albumId(1L).albumName("AlbumName").description("AlbumDescription")
				.date(date).userId(20L).type("PE_PU").albumUserList(Set.of(albumModelUser1, albumModelUser2))
				.albumFamiliesList(Set.of(albumModelFamilies1, albumModelFamilies2)).build();

		AlbumModel familySharedAlbums = AlbumModel.builder().albumId(1L).albumName("AlbumName")
				.description("AlbumDescription").date(date).userId(20L).type("PE_PU")
				.albumUserList(Set.of(albumModelUser1, albumModelUser2))
				.albumFamiliesList(Set.of(albumModelFamilies1, albumModelFamilies2)).build();

		AlbumModel userSharedAlbums = AlbumModel.builder().albumId(1L).albumName("AlbumName")
				.description("AlbumDescription").date(date).userId(20L).type("PE_PU")
				.albumUserList(Set.of(albumModelUser1, albumModelUser2))
				.albumFamiliesList(Set.of(albumModelFamilies1, albumModelFamilies2)).build();

		ConcurrentHashMap albumMap = new ConcurrentHashMap<>();
		albumMap.put("userAlbums", userAlbums);
		albumMap.put("familySharedAlbums", familySharedAlbums);
		albumMap.put("userSharedAlbums", userSharedAlbums);

		when(imageService.getAlbumImagesWithUserId(1L)).thenReturn(albumMap);

		ResponseEntity<?> result = imageController.getAlbumImages(Long.valueOf(1));
		Object response = result.getBody();

		assertEquals(albumMap, response);
	}

	@Test
	public void updateImageToSharewithFamilyandUSerNullTest() {
		Long userId = null;
		Long imageId = null;
		familyIds.add(1L);
		familyIds.add(2L);
		userIds.add(1L);
		userIds.add(2L);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds).build();
		ResponseEntity actual = imageController.updateImageToSharewithFamilyandUSer(userId, imageId, imageRelationPojo);
		assertEquals(HttpStatus.BAD_REQUEST, actual.getStatusCode());
	}

	@Test
	public void updateImageToSharewithFamilyandUserPrivateNullTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(LOCAL_PATH).build();
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		familyIds.add(1L);
		familyIds.add(2L);
		userIds.add(1L);
		userIds.add(2L);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds)
				.type("PE_PR").build();

		when(imageService.getImageById(imageModel.getId())).thenReturn(imageModel);
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), imageModel.getId(),
				imageRelationPojo);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void updateImageToSharewithFamilyandUserPrivateSuccessTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(LOCAL_PATH).build();
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds)
				.type("PE_PR").build();

		when(imageService.getImageById(imageModel.getId())).thenReturn(imageModel);
		when(userService.getById(user.getId())).thenReturn(user);
		when(imageModelFamiliesService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		when(imageModelUserService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), imageModel.getId(),
				imageRelationPojo);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void updateImageToSharewithFamilyandUserCustomSuccessTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(LOCAL_PATH).build();
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		familyIds.add(1L);
		familyIds.add(2L);
		userIds.add(1L);
		userIds.add(2L);
		String type = "PE_CU";
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds)
				.type("PE_CU").build();

		when(imageService.getImageById(imageModel.getId())).thenReturn(imageModel);
		when(userService.getById(user.getId())).thenReturn(user);
		when(imageModelFamiliesService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		when(imageModelUserService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		when(imageService.imagesSharedWithUsersAndFamilies(familyIds, imageModel, userIds, user, type))
				.thenReturn(true);
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), imageModel.getId(),
				imageRelationPojo);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void updateImageToSharewithPublicSuccessTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(LOCAL_PATH).build();
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		String type = "PE_PU";
		familyIds.add(1L);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds)
				.type("PE_PU").build();

		when(imageService.getImageById(imageModel.getId())).thenReturn(imageModel);
		when(userService.getById(user.getId())).thenReturn(user);
		when(imageModelFamiliesService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		when(imageModelUserService.delete(imageModel.getId())).thenReturn(Integer.valueOf(1));
		when(imageService.imagesSharedWithUsersAndFamilies(familyIds, imageModel, null, user, type)).thenReturn(true);
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), imageModel.getId(),
				imageRelationPojo);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void updateImageToSharewithFamilyandUSerNotOwnerImgIdTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(LOCAL_PATH).build();
		User user = new User(2L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		familyIds.add(1L);
		familyIds.add(2L);
		userIds.add(1L);
		userIds.add(2L);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds).build();

		when(messages.get("ERROR_INVALID_USER_RIGHTS")).thenReturn("User doesnot have rights");
		when(imageService.getImageById(imageModel.getId())).thenReturn(imageModel);
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), imageModel.getId(),
				imageRelationPojo);
		String body = String.valueOf(response.getBody());
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void getImageslistUserImagesByTypeNullTestCase() {
		when(imageService.findByUserIdAndType(1L, type)).thenReturn(null);
		ResponseEntity<?> response = imageController.getUserImagesByUserIdAndType(Long.valueOf(1), type);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void updateImageToSharewithFamilyandUSerNoImgIdTest() {
		User user = new User(2L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null, families, null,
				null, "theme", "eng", null, null, null);
		familyIds.add(1L);
		familyIds.add(2L);
		userIds.add(1L);
		userIds.add(2L);
		ImageRelationPojo imageRelationPojo = ImageRelationPojo.builder().userIds(userIds).familyIds(familyIds).build();

		when(messages.get("ERROR_INVALID_USER_RIGHTS")).thenReturn("User doesnot have rights");
		when(imageService.getImageById(1L)).thenReturn(null);
		ResponseEntity response = imageController.updateImageToSharewithFamilyandUSer(user.getId(), 1L,
				imageRelationPojo);
		String body = String.valueOf(response.getBody());
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void getImageByImageLocalPathTestNull() {
		String localPath = "";
		when(imageService.getImageByImageLocalPath(localPath, 1L)).thenReturn(null);
		when(messages.get("ERROR_IMAGE_NOTFOUND")).thenReturn("Image not found");
		MessageResponse result = (MessageResponse) imageController.getImageByImageLocalPath(localPath, 1L).getBody();
		assertTrue(result.getMessage().toString().contains("Image not found"));
	}

	@Test
	public void getImageByImageLocalPathTestSuccess() {
		String localPath = "imagePath";
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).build();
		when(imageService.getImgByImageLocalPath(localPath)).thenReturn(imageModel);
		when(imageService.getImageByImageLocalPath(localPath, 1L)).thenReturn(imageModel);
		ImageModel result = (ImageModel) imageController.getImageByImageLocalPath(localPath, 1L).getBody();
		assertNotNull(result);
		assertEquals(imageModel, result);
	}

	@Test
	public void getImageByImageLocalPathTestFail() {
		String localPath = "imagePath";
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).build();
		when(imageService.getImgByImageLocalPath(localPath)).thenReturn(imageModel);
		when(imageService.getImageByImageLocalPath(localPath, 1L)).thenReturn(null);
		ResponseEntity<Object> result = imageController.getImageByImageLocalPath(localPath, 1L);
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}

	@Test
	public void deleteImageUserNotFoundTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		when(userService.getById(userId)).thenReturn(null);
		ResponseEntity<MessageResponse> response = imageController.deleteImage(imageId, userId);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void deleteImageImageNotFoundTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		User user = User.builder().id(userId).username("Test").build();
		when(messages.get("IMAGE_DELETED")).thenReturn("Image Deleted Successfully");
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(null);

		ResponseEntity<MessageResponse> response = imageController.deleteImage(imageId, userId);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void deleteImageImageNotOwnedByUserTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		User user = User.builder().id(userId).username("Test").build();
		when(messages.get("IMAGE_DELETED")).thenReturn("Image Deleted Successfully");
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(imageModel);

		ResponseEntity<MessageResponse> response = imageController.deleteImage(imageId, userId);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void deleteImageImageSuccessTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		ImageModel imageModel = ImageModel.builder().userId(2L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		User user = User.builder().id(userId).username("Test").build();
		when(messages.get("IMAGE_DELETED")).thenReturn("Image Deleted Successfully");
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(imageModel);

		ResponseEntity<MessageResponse> response = imageController.deleteImage(imageId, userId);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void uploadImageToAlbumFailTestCase() throws IOException {
		FileInputStream inputFile = new FileInputStream("cs-test.png");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 1L)).thenReturn(null);
		when(messages.get(AppConstants.ALBUM_NOT_EXIST)).thenReturn("Error: Please type a valid album name");
		ImageUploadPojo imageUploadPojo = ImageUploadPojo.builder().date(date).fileImage(file).type("U")
				.description(DESCRIPTION).albumName(ALBUM_NAME).build();

		MessageResponse response = (MessageResponse) imageController.uploadImage(2L, imageUploadPojo).getBody();
		assertTrue(response.getMessage().toString().contains("Error: Please type a valid album name"));

	}

	@Test
	public void getImageByImageLocalPathTestNullImagModel() {
		String localPath = "nullImage";
		when(imageService.getImgByImageLocalPath(localPath)).thenReturn(null);
		when(messages.get("ERROR_IMAGE_NOTFOUND")).thenReturn("Image not found");
		MessageResponse result = (MessageResponse) imageController.getImageByImageLocalPath(localPath, 1L).getBody();
		assertTrue(result.getMessage().toString().contains("Image not found"));
	}

	@Test
	public void getImageByImageLocalPathTestDifferentUserId() {
		String localPath = "differentUserId";
		ImageModel imageModel = ImageModel.builder().id(1L).userId(1L).build();
		when(imageService.getImgByImageLocalPath(localPath)).thenReturn(imageModel);
		when(messages.get("ERROR_IMAGE_LOCALPATH_ASSOCIATION")).thenReturn(
				"Error: the Image passed in request does not have relation with the user. Please check again");
		MessageResponse result = (MessageResponse) imageController.getImageByImageLocalPath(localPath, 2L).getBody();
		assertTrue(result.getMessage().contains(
				"Error: the Image passed in request does not have relation with the user. Please check again"));
	}

	@Test
	public void uploadGroupProfileOrDashboardImageNoImageFail() throws IOException {
		FileInputStream inputFile = null;
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void uploadGroupProfileOrDashboardImageNoFamilyWithIdFail() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void uploadGroupProfileOrDashboardImageNoFamilyRoleFail() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		when(familyService.getFamilyById(1L)).thenReturn(family1);
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void uploadGroupProfileOrDashboardImageNoFamilyNoAdminRoleFail() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		when(familyService.getFamilyById(1L)).thenReturn(family1);
		when(familyService.getRoleForUserInFamily(1L, 1L))
				.thenReturn(FamilyUserRoles.builder().role(ERole.ROLE_USER.toString()).build());
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void uploadGroupProfileOrDashboardImageSuccess() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, FILE_TYPE, inputFile);
		when(familyService.getFamilyById(1L)).thenReturn(family1);
		when(familyService.getRoleForUserInFamily(1L, 1L))
				.thenReturn(FamilyUserRoles.builder().role(ERole.ROLE_ADMIN.toString()).build());
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void uploadGroupProfileOrDashboardImageTextFileFailTestCase() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, "txt", inputFile);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 1L)).thenReturn(null);
		when(messages.get(AppConstants.NOT_IMAGE)).thenReturn("Not valid file type");
		GroupProfileDashboardImagePojo gp = GroupProfileDashboardImagePojo.builder().fileImage(file).date(date)
				.familyId(1L).build();
		ResponseEntity<MessageResponse> response = imageController.uploadGroupProfileOrDashboardImage(1L, gp);
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void uploadImageToAlbumTextFileFailTestCase() throws IOException {
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", NAME_OF_THE_FILE, "txt", inputFile);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 1L)).thenReturn(null);
		when(messages.get(AppConstants.NOT_IMAGE)).thenReturn("Not valid file type");
		ImageUploadPojo imageUploadPojo = ImageUploadPojo.builder().date(date).fileImage(file).type("U")
				.description(DESCRIPTION).albumName(ALBUM_NAME).build();

		MessageResponse response = (MessageResponse) imageController.uploadImage(2L, imageUploadPojo).getBody();
		assertTrue(response.getMessage().toString().contains("Not valid file type"));
	}

	@Test
	public void restoreImageUserNotFoundTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		when(userService.getById(userId)).thenReturn(null);
		ResponseEntity<MessageResponse> response = imageController.restoreImageToAlbum(imageId, userId);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void restoreImageImageNotFoundTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		User user = User.builder().id(userId).username("Test").build();
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(null);

		ResponseEntity<MessageResponse> response = imageController.restoreImageToAlbum(imageId, userId);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void restoreImageImageNotOwnedByUserTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		User user = User.builder().id(userId).username("Test").build();
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(imageModel);

		ResponseEntity<MessageResponse> response = imageController.restoreImageToAlbum(imageId, userId);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void restoreImageImageOwnedDeletedTestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		ImageModel imageModel = ImageModel.builder().userId(2L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		User user = User.builder().id(userId).username("Test").build();
		when(messages.get("IMAGE_DELETED")).thenReturn("Image Deleted Successfully");
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(imageModel);
		when(imageService.restoreImageToAlbum(imageModel)).thenReturn(false);

		ResponseEntity<MessageResponse> response = imageController.restoreImageToAlbum(imageId, userId);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void restoreImageImageSuccessestCase() {
		Long imageId = 3L;
		Long userId = 2L;
		ImageModel imageModel = ImageModel.builder().userId(2L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(LOCAL_PATH).build();
		User user = User.builder().id(userId).username("Test").build();
		when(userService.getById(userId)).thenReturn(user);
		when(imageService.getImageById(imageId)).thenReturn(imageModel);
		when(imageService.restoreImageToAlbum(imageModel)).thenReturn(true);

		ResponseEntity<MessageResponse> response = imageController.restoreImageToAlbum(imageId, userId);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}