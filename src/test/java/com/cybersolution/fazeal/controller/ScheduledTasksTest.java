package com.cybersolution.fazeal.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.cybersolution.fazeal.configurations.ScheduledTasks;
import com.cybersolution.fazeal.models.EventsRelation;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.EventsFamilyUserRespository;
import com.cybersolution.fazeal.repository.EventsRelationRespository;
import com.cybersolution.fazeal.repository.EventsRespository;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.NotificationsActivityRespository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ScheduledTasksTest {

	@InjectMocks
	private NotificationController notifController;

	@Mock
	EventsRespository eventRepository;

	@Mock
	UserRepository usersRepository;

	@Mock
	FamilyRespository familyRepository;

	@Mock
	EventsFamilyUserRespository eventFamUserRepository;

	@Mock
	EventsRelationRespository eventsRelation;

	@Mock
	NotificationsActivityRespository notifsRepo;

	@Mock
	Messages messages;

	@Mock
	EmailService emailService;

	@Mock
	ScheduledTasks scheduledTasks;

	SimpleDateFormat dateFormatter = new SimpleDateFormat(AppConstants.DATE_FORMAT);
	SimpleDateFormat timeFormatter = new SimpleDateFormat(AppConstants.DATE_TIME_FORMATE);

	@Test
	public void createEventsNotifsTest() throws ParseException {

		List<EventsRelation> eventRelList = new ArrayList<>();
		Date date = new Timestamp(System.currentTimeMillis());
		date = new Timestamp(timeFormatter.parse(date.toString()).getTime());
		Date dateTime = new Timestamp(dateFormatter.parse(date.toString()).getTime());
		EventsRelation eventRel = new EventsRelation(5L, 8L, "2useewr2@resti.api", "user2family", "Family2",
				"test event", dateTime.toString(), date.toString(), "2021-09-28 20:00:00.0", "1", 8L);
		eventRelList.add(eventRel);

		when(notifsRepo.findNotificationsByType("")).thenReturn(null);
		when(eventsRelation.findAll()).thenReturn(eventRelList);
		scheduledTasks.createEventsNotifs();

		verify(scheduledTasks).createEventsNotifs();
	}

	@Test
	public void createBirthdayNotifsTest() throws Exception {

		List<User> usersList = new ArrayList<>();
		Date date = new Timestamp(System.currentTimeMillis());
		date = new Timestamp(timeFormatter.parse(date.toString()).getTime());
		User user = new User("user1", "email1.cc.com", "Ew22232wwwWESD", true);
		usersList.add(user);
		when(notifsRepo.findNotificationsByType("")).thenReturn(null);
		when(usersRepository.getUserByDOB("")).thenReturn(usersList);
		when(messages.get("PASSWORD_EMAIL_FROM")).thenReturn("");
		when(messages.get("PASSWORD_EMAIL_SUBJECT_BITHDAY")).thenReturn("");
		when(messages.get("PASSWORD_EMAIL_BBIRTHDAY_TEXT")).thenReturn("");

		scheduledTasks.createBirthdaysNotifs();
		verify(scheduledTasks).createBirthdaysNotifs();
	}

	@Test
	public void createUserJoiningNotifsTest() throws Exception {

		List<User> usersList = new ArrayList<>();
		Date date = new Timestamp(System.currentTimeMillis());
		date = new Timestamp(timeFormatter.parse(date.toString()).getTime());
		User user = new User("user1", "email1.cc.com", "Ew22232wwwWESD", true);
		usersList.add(user);
		when(notifsRepo.findNotificationsByType("")).thenReturn(null);
		when(usersRepository.getUserByCreatedAt("")).thenReturn(usersList);

		scheduledTasks.createUserJoiningNotifs();
		verify(scheduledTasks).createUserJoiningNotifs();
	}

}
