package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.pojo.UpdateNamePojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
class UserInformationSettingsControllerTest {
	@InjectMocks
	private UserInformationSettingsController userInformationSettingsController;

	@Mock
	private Messages messages;

	@Mock
	private UserService userService;

	@Test
	public void listAllNamePrefixesTest() {
		String[] namePrefixesLists= {"miss","mr"};
		when(userService.findAll("namePrefix")).thenReturn(namePrefixesLists);
		ResponseEntity response=userInformationSettingsController.listAllNamePrefixes(1L,"namePrefix");
		assertEquals(response.getBody(),namePrefixesLists);
	}
	@Test
	public void updateNameFailTest() {
		Long userId=1L;
		String firstName="maxxw";
		String lastName="Well";
		UpdateNamePojo updateNamePojo= new UpdateNamePojo();
		updateNamePojo.setFirstName(firstName);
		updateNamePojo.setLastName(lastName);
		updateNamePojo.setNamePrefix("mr");
		when(userService.setNames(userId,"mr",firstName,lastName)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn("Error: User not found to be approved.");
		MessageResponse result = (MessageResponse) userInformationSettingsController.updateName(userId, updateNamePojo).getBody();
		assertTrue(result.getMessage().contains("Error: User not found to be approved."));
	}
	@Test
	public void updateNamesuccessTest() {
		Long userId=1L;
		String firstName="maxxw";
		String lastName="Well";
		UpdateNamePojo updateNamePojo= new UpdateNamePojo();
		updateNamePojo.setFirstName(firstName);
		updateNamePojo.setLastName(lastName);
		updateNamePojo.setNamePrefix("mr");
		when(userService.setNames(userId,"mr",firstName,lastName)).thenReturn(true);
		when(messages.get(AppConstants.NAME_UPDATED_SUCCESSFULLY)).thenReturn("Name updated successfully");
		MessageResponse result = (MessageResponse) userInformationSettingsController.updateName(userId, updateNamePojo).getBody();
		assertTrue(result.getMessage().contains("Name updated successfully"));
	}
	
}
