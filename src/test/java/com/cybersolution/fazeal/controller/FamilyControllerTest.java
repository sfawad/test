package com.cybersolution.fazeal.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.exception.ResourceNotFoundException;
import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyResponse;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.FamilyCreatePoJo;
import com.cybersolution.fazeal.pojo.FamilyUpdatePoJo;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;


@RunWith(MockitoJUnitRunner.Silent.class)
public class FamilyControllerTest {

	@InjectMocks
	private FamilyController familyController;

	@Mock
	FamilyService familyService;
	@Mock
	Messages messages;
	@Mock
	private UserService userService;
	@Mock
	private AuditLogsHelper auditLogsHelper;

	public static final String ALL_PARAMS_MANDATORY_ERROR = "ERROR_ALL_PARAMS_MANDATORY";
	public static final String INPUT_PARAMS_NOT_VALID = "Input params not valid";
	public static final String ERROR_FAMILY_NOTFOUND = "ERROR_FAMILY_NOTFOUND";
	public static final String FAMILY_NOT_FOUND = "Family not found";
	public static final String TOKEN = "token";
	public static final String FAMILY_NOT_FOUND_NAME = "Family not found for name: ";
	public static final String ERROR_USER_NOT_FOUND = "ERROR_USER_NOT_FOUND";
	public static final String ERROR_ONE_PARAM_NULL = "Error: One of the params is null";
	public static final String PARAMS_IS_NULL = "params is null";
	public static final String FAMILY_TOKEN = "familyToken";
	public static final String ERROR_USER_LACK_ADMIN_RIGHTS = "ERROR_USER_LACK_ADMIN_RIGHTS";
	public static final String ERROR_NO_ADMIN_PREVILEGE = "Error: User doesn't have Admin previlege to perfrom this activity";
	public static final String NO_RECORD_FOUND = "Error: No record found";

	@Test
	public void testGetFamilyByIdTestCaseSuccess() throws Exception {
		Family family = createFamily();
		when(familyService.getFamilyById(1L)).thenReturn(family);
		ResponseEntity<?> result = familyController.getFamilyById(Long.valueOf(1));
		Family response = (Family) result.getBody();
		assertEquals(family, response);
	}

	@Test
	public void testGetFamilyByTokenSuccess() {
		String token = "someToken";
		Family family = createFamily();
		FamilyResponse familyResponse = FamilyResponse.builder().familyId(1L).token(token).name("TestFamilyName")
				.phone("123456L").address1("address1").build();
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(familyService.getFamilyResponseByToken(family, token)).thenReturn(familyResponse);
		ResponseEntity<?> result = familyController.getFamilyByToken(token);
		assertEquals(result.getBody(), familyResponse);
	}

	@Test
	public void testGetFamilyByTokenNullMessageFail() {
		String token = "";
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(null);
		ResponseEntity<?> result = familyController.getFamilyByToken(TOKEN);

		assertTrue(result.getBody().toString().contains(FAMILY_NOT_FOUND));
	}

	@Test
	public void testGetFamilyByTokenNoFamilyFail() {
		String token = TOKEN;
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(null);
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND_NAME + token);
		ResponseEntity<?> result = familyController.getFamilyByToken(TOKEN);
		String body = String.valueOf(result.getBody());
		assertTrue(body.contains(FAMILY_NOT_FOUND));
	}

	@Test
	public void testUpdateFamilySuccess() {
		Family family = createFamily();
		User user = createUser();
		FamilyUpdatePoJo familyUpdatePoJo = getFamilyUpdateInfo();
		when(messages.get("FAMILY_INFO_UPDATED_SUCCESSFULLY")).thenReturn("Family informations updated successfully.");
		when(familyService.getFamilyById(1L)).thenReturn(family);
		when(userService.getById(Long.valueOf(1))).thenReturn(user);
		when(familyService.updateFamilyInfo(family.getFamilyId(), familyUpdatePoJo, family, 1L)).thenReturn(true);
		ResponseEntity<MessageResponse> result = familyController.updateFamilyInfo(Long.valueOf(1), 1L,
				familyUpdatePoJo);
		MessageResponse response = result.getBody();
		assertTrue(response.getMessage().contains("Family informations updated successfully."));
	}

	@Test
	public void testUpdateFamilyNoFamilyFail() {
		FamilyUpdatePoJo familyUpdatePoJo = getFamilyUpdateInfo();
		when(messages.get(ERROR_USER_NOT_FOUND)).thenReturn("Error: User not found!");
		when(familyService.getFamilyById(1L)).thenReturn(createFamily());
		when(userService.getById(Long.valueOf(1))).thenReturn(null);
		ResponseEntity<?> result = familyController.updateFamilyInfo(Long.valueOf(1), 1L, familyUpdatePoJo);
		String body = String.valueOf(result.getBody());
		assertTrue(body.contains("Error: User not found!"));
	}

	@Test
	public void testUpdateFamilyNoUserFail() {
		User user = createUser();
		FamilyUpdatePoJo familyUpdatePoJo = getFamilyUpdateInfo();
		when(messages.get("ERROR_FAMILY_NOTFOUND_UPDATE")).thenReturn("Family not found while updating by Id:");
		when(familyService.getFamilyById(1L)).thenReturn(null);
		when(userService.getById(Long.valueOf(1))).thenReturn(user);
		ResponseEntity<?> result = familyController.updateFamilyInfo(Long.valueOf(1), 1L, familyUpdatePoJo);
		String body = String.valueOf(result.getBody());
		assertTrue(body.contains("Family not found while updating"));
	}

	@Test
	public void testCreateFamilyByFamilyAdminSuccess() {
		Family family = createFamily();
		User user = createUser();
		FamilyCreatePoJo familyCreatePoJo = getFamilyCreateInfo();
		when(messages.get("FAMILY_CREATED_SUCCESSFULLY")).thenReturn("New family created successfully");
		when(userService.getById(Long.valueOf(1))).thenReturn(user);
		when(familyService.isUserAgeValidToCreateFamily(user)).thenReturn(true);
		when(familyService.createNewFamily(familyCreatePoJo, user)).thenReturn(family);

		ResponseEntity<MessageResponse> result = familyController.createFamilyByFamilyAdmin(Long.valueOf(1),
				familyCreatePoJo, null);
		MessageResponse response = result.getBody();
		assertTrue(response.getMessage().contains("New family created successfully"));
	}


	@Test
	public void testCreateFamilyByFamilyAdminAgeCheckFail() {
		User user = createUser();
		FamilyCreatePoJo familyCreatePoJo = getFamilyCreateInfo();
		when(messages.get("ERROR_FAMILY_CREATION_MIN_AGE")).thenReturn("Error: User can't create family age not valid");
		when(userService.getById(Long.valueOf(1))).thenReturn(user);
		when(familyService.isUserAgeValidToCreateFamily(user)).thenReturn(false);
		ResponseEntity<?> result = familyController.createFamilyByFamilyAdmin(Long.valueOf(1), familyCreatePoJo, null);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains("can't create family age not valid"));
	}

	@Test
	public void testCreateFamilyByFamilyAdminUserNotFoundForIdFail() {
		FamilyCreatePoJo familyCreatePoJo = getFamilyCreateInfo();
		when(messages.get(ERROR_USER_NOT_FOUND)).thenReturn("Error: User not found for id: 1");
		when(userService.getById(Long.valueOf(1))).thenReturn(null);
		ResponseEntity<?> result = familyController.createFamilyByFamilyAdmin(Long.valueOf(1), familyCreatePoJo, null);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains("User not found for id: 1"));
	}

	@Test
	public void testRequestJoinUserToFamilyNoFamilyFoundFail() {
		String token = FAMILY_TOKEN;
		when(userService.getById(1L)).thenReturn(createUser());
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(null);
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND_NAME);
		ResponseEntity<MessageResponse> result = familyController.requestJoinUserToFamily(1L, token);
		String body = result.getBody().toString();
		assertTrue(body.contains(FAMILY_NOT_FOUND_NAME));
	}

	@Test
	public void testRequestJoinUserToFamilySuccess() {
		String token = FAMILY_TOKEN;
		User user = createUser();
		user.setId(1L);
		Family family = createFamily();
		when(userService.getById(1L)).thenReturn(user);
		when(familyService.isUserRequestToJoinFamilyAdded(user, family)).thenReturn(true);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(messages.get("FAMILY_JOINING_REQUEST"))
				.thenReturn("A request has been made to family administrator; to welcome you in family.");
		ResponseEntity<MessageResponse> result = familyController.requestJoinUserToFamily(1L, token);
		String body = result.getBody().toString();
		assertTrue(body.contains("request has been made to family"));
	}

	@Test
	public void testDisassociateUserFromFamilyNoFamilyFail() {
		String token = FAMILY_TOKEN;
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(null);
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND_NAME);
		ResponseEntity<?> result = familyController.disassociateUserFromFamily(1L, token, 30L);
		String body = result.getBody().toString();
		assertTrue(body.contains(FAMILY_NOT_FOUND_NAME));
	}

	@Test
	public void testDisassociateUserFromFamilyNotFamilyAdminFail() {
		String token = FAMILY_TOKEN;
		Family family = createFamily();
		Long adminUserId = 1L;
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(false);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(messages.get(ERROR_USER_LACK_ADMIN_RIGHTS)).thenReturn(ERROR_NO_ADMIN_PREVILEGE);
		ResponseEntity<?> result = familyController.disassociateUserFromFamily(1L, token, 30L);
		String body = result.getBody().toString();
		assertTrue(body.contains(ERROR_NO_ADMIN_PREVILEGE));
	}

	@Test
	public void testDisassociateUserFromFamilyNoUserFamilyRelationFail() {
		String token = FAMILY_TOKEN;
		Family family = createFamily();
		Long adminUserId = 30L;
		Long userIdToRemove = 1L;

		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(true);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(familyService.disassociatUserFromFamily(userIdToRemove, token, family)).thenReturn(false);
		when(messages.get("ERROR_NO_USER_OR_USER_HAS_NO_RELATION_TO_FAMILY"))
				.thenReturn("Error: User Not found, or have no relation with Family");
		ResponseEntity<?> result = familyController.disassociateUserFromFamily(userIdToRemove, token, adminUserId);
		String body = result.getBody().toString();
		assertTrue(body.contains("have no relation with Family"));
	}

	@Test
	public void testDisassociateUserFromFamilySuccess() {
		String token = FAMILY_TOKEN;
		Family family = createFamily();
		Long adminUserId = 30L;
		Long userIdToRemove = 1L;
		when(messages.get("USER_REMOVED_FROM_FAMILY_PENDING_LIST"))
				.thenReturn("User removed from family pending list successfully");
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(true);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(familyService.disassociatUserFromFamily(userIdToRemove, token, family)).thenReturn(true);
		ResponseEntity<?> result = familyController.disassociateUserFromFamily(userIdToRemove, token, adminUserId);
		String body = result.getBody().toString();
		assertTrue(body.contains("User removed from family pending list successfully"));
	}

	@Test
	public void testGetUserFamiliesByUserIdTestSuccessCase() {
		Family family = createFamily();
		Set<Family> families = Set.of(family);
		User user = new User("test", "email@email.com", "passEnc", true);
		user.setFamilies(families);
		when(userService.getById(1L)).thenReturn(user);
		ResponseEntity<?> result = familyController.getUserFamiliesById(Long.valueOf(1));
		@SuppressWarnings("unchecked")
		Set<Family> response = (Set<Family>) result.getBody();
		assertEquals(families, response);
	}

	@Test
	public void testGetUserFamiliesByUserIdTestNoUserWithIdCase() {
		Long userId = 100L;
		when(userService.getById(userId)).thenReturn(null);
		when(messages.get(ERROR_USER_NOT_FOUND)).thenReturn("Error: No User with id ::");
		ResponseEntity<?> result = familyController.getUserFamiliesById(userId);
		Object response = result.getBody();
		assertEquals("MessageResponse(message=Error: No User with id ::)", response.toString());
	}

	@Test
	public void testGetUserFamiliesByUserIdTestNullUserFamilyListCase() {
		User user = new User("test", "email@email.com", "passEnc", true);
		user.setFamilies(null);
		Long userId = 1L;
		when(userService.getById(userId)).thenReturn(user);
		when(messages.get(ERROR_USER_NOT_FOUND)).thenReturn("Error: No user families list found!");
		ResponseEntity<?> result = familyController.getUserFamiliesById(userId);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains("Error: No user families list"));
	}

	@Test
	public void testGetRoleOfUsersInFamilyByIdSuccess() {
		FamilyUserRoles familyUserRole = FamilyUserRoles.builder().id(1L).familyId(20L).userId(39L).build();
		when(familyService.getRoleForUserInFamily(1L, 2L)).thenReturn(familyUserRole);
		ResponseEntity<?> result = familyController.getRoleOfUsersInFamilyById(2L, 1L);
		assertEquals(familyUserRole, result.getBody());
	}

	@Test
	public void testGetRoleOfUsersInFamilyByIdFail() {
		when(familyService.getRoleForUserInFamily(1L, 2L)).thenReturn(null);
		when(messages.get("ERROR_NO_RECORD_FOUND")).thenReturn(NO_RECORD_FOUND);
		ResponseEntity<?> result = familyController.getRoleOfUsersInFamilyById(2L, 1L);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains(NO_RECORD_FOUND));
	}

	@Test
	public void testGetAllRolesOfUsersInFamilyByFamilyIdSuccess() {
		FamilyUserRoles familyUserRole = FamilyUserRoles.builder().id(1L).familyId(20L).userId(39L).build();
		List<FamilyUserRoles> roles = List.of(familyUserRole);
		when(familyService.getAllUserRolesInFamily(1L)).thenReturn(roles);
		ResponseEntity<?> result = familyController.getAllRolesOfUsersInFamilyByFamilyId(1L);
		assertEquals(roles, result.getBody());
	}

	@Test
	public void testGetAllRolesOfUsersInFamilyByFamilyIdFail() {
		when(familyService.getAllUserRolesInFamily(1L)).thenReturn(null);
		when(messages.get("ERROR_NO_RECORD_FOUND")).thenReturn(NO_RECORD_FOUND);
		ResponseEntity<?> result = familyController.getAllRolesOfUsersInFamilyByFamilyId(1L);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains(NO_RECORD_FOUND));
	}

	@Test
	public void testAssociateUserToFamilyNoFamilyFail() {
		Long userId = 34L;
		String token = TOKEN;
		Long adminUserId = 30L;
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(null);
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND_NAME + token);
		ResponseEntity<?> response = familyController.associateUserToFamily(userId, token, adminUserId);
		String result = String.valueOf(response.getBody());
		assertTrue(result.contains(FAMILY_NOT_FOUND_NAME));
	}

	@Test
	public void testAssociateUserToFamilyNotAdminFail() {
		Long userId = 34L;
		String token = TOKEN;
		Long adminUserId = 30L;
		User user = createUser();
		user.setId(userId);
		Family family = createFamily();
		when(userService.getById(userId)).thenReturn(user);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(messages.get(ERROR_USER_LACK_ADMIN_RIGHTS)).thenReturn(ERROR_NO_ADMIN_PREVILEGE);
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(false);

		ResponseEntity<?> response = familyController.associateUserToFamily(userId, token, adminUserId);
		String result = String.valueOf(response.getBody());
		assertTrue(result.contains(ERROR_NO_ADMIN_PREVILEGE));
	}

	@Test
	public void testAssociateUserToFamilyUserNotFoundFail() {
		Long userId = 34L;
		String token = TOKEN;
		Long adminUserId = 30L;
		Family family = createFamily();
		User user = createUser();
		user.setId(userId);
		when(userService.getById(userId)).thenReturn(user);
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(messages.get("ERROR_USER_NOT_FOUND_FOR_APPROVAL")).thenReturn("Error: User not found to be approved.");
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(true);
		when(familyService.associateUserToFamily(userId, family)).thenReturn(false);
		ResponseEntity<?> response = familyController.associateUserToFamily(userId, token, adminUserId);
		String result = String.valueOf(response.getBody());
		assertTrue(result.contains("Error: User not found to be approved."));
	}

	@Test
	public void testAssociateUserToFamilySuccess() {
		Long userId = 34L;
		String token = TOKEN;
		Long adminUserId = 30L;
		User user = createUser();
		user.setId(userId);
		Family family = createFamily();
		when(userService.getById(userId)).thenReturn(user);
		when(messages.get("USER_ADDED_TO_FAMILY_MEMBERS"))
				.thenReturn("User added successfully to family members, welcome to our family.");
		when(familyService.getFamilyInfoFromToken(token)).thenReturn(family);
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(true);
		when(familyService.associateUserToFamily(userId, family)).thenReturn(true);
		ResponseEntity<MessageResponse> result = familyController.associateUserToFamily(userId, token, adminUserId);
		MessageResponse response = result.getBody();
		assertTrue(response.getMessage().contains("User added successfully to family members, welcome to our family."));
	}

	@Test
	public void testUpdateUserRoleInFamilyUserNotFoundFail() throws ResourceNotFoundException {
		Long adminUserId = 1L;
		boolean isNewUserRoleAdmin = true;
		Long userIdToBeUpdated = 32L;
		Long familyId = 23L;
		when(messages.get("ERROR_USER_NOT_EXISTS")).thenReturn("Error: User not exists");
		when(userService.getById(adminUserId)).thenReturn(null);
		ResponseEntity<MessageResponse> response = familyController.updateUserRoleInFamily(adminUserId,
				isNewUserRoleAdmin, userIdToBeUpdated, familyId);
		String result = String.valueOf(response.getBody());
		assertTrue(result.contains("User not exists"));
	}

	@Test
	public void testUpdateUserRoleInFamilyFamilyNotFoundFail() throws ResourceNotFoundException {
		User user = createUser();
		Long adminUserId = 1L;
		boolean isNewUserRoleAdmin = true;
		Long userIdToBeUpdated = 32L;
		Long familyId = 23L;
		when(messages.get(ERROR_FAMILY_NOTFOUND)).thenReturn(FAMILY_NOT_FOUND_NAME);
		when(userService.getById(adminUserId)).thenReturn(user);
		when(familyService.getFamilyById(familyId)).thenReturn(null);
		ResponseEntity<MessageResponse> response = familyController.updateUserRoleInFamily(adminUserId,
				isNewUserRoleAdmin, userIdToBeUpdated, familyId);
		assertTrue(String.valueOf(response.getBody()).contains(FAMILY_NOT_FOUND_NAME));
	}

	@Test
	public void testUpdateUserRoleInFamilyUserHaveNoAdminRightesFail() throws ResourceNotFoundException {
		Family family = createFamily();
		User user = createUser();
		Long adminUserId = 1L;
		boolean isNewUserRoleAdmin = true;
		Long userIdToBeUpdated = 32L;
		Long familyId = 23L;
		when(messages.get(ERROR_USER_LACK_ADMIN_RIGHTS)).thenReturn(ERROR_NO_ADMIN_PREVILEGE);
		when(userService.getById(adminUserId)).thenReturn(user);
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(false);
		when(familyService.getFamilyById(familyId)).thenReturn(family);
		ResponseEntity<MessageResponse> response = familyController.updateUserRoleInFamily(adminUserId,
				isNewUserRoleAdmin, userIdToBeUpdated, familyId);
		String result = String.valueOf(response.getBody());
		assertTrue(result.contains(ERROR_NO_ADMIN_PREVILEGE));
	}

	@Test
	public void testUpdateUserRoleInFamilySuccess() throws ResourceNotFoundException {
		Family family = createFamily();
		User user = createUser();
		Long adminUserId = 1L;
		boolean isNewUserRoleAdmin = true;
		Long userIdToBeUpdated = 32L;
		Long familyId = 23L;
		when(userService.getById(adminUserId)).thenReturn(user);
		when(familyService.isUserAdminforFamily(adminUserId, family.getFamilyId())).thenReturn(true);
		when(familyService.getFamilyById(familyId)).thenReturn(family);
		ResponseEntity<MessageResponse> response = familyController.updateUserRoleInFamily(adminUserId,
				isNewUserRoleAdmin, userIdToBeUpdated, familyId);
		String result = String.valueOf(response.getBody());
		verify(familyService, times(1)).changeUserRoleInFamily(isNewUserRoleAdmin, userIdToBeUpdated, family);
	}

	private Family createFamily() {
		Family family = new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
		family.setFamilyId(1L);
		return family;
	}

	private User createUser() {
		User user = new User("user1", "email1.cc.com", "Ew22232wwwWESD", true);
		user.setDob("DOB");
		return user;
	}

	private FamilyUpdatePoJo getFamilyUpdateInfo() {
		FamilyUpdatePoJo familyUpdatePoJo = FamilyUpdatePoJo.builder().name("new Name")

				.phone("1234567890").address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600")
				.country("Country").relationWithFamily("Father").about("").build();
		return familyUpdatePoJo;
	}

	private FamilyCreatePoJo getFamilyCreateInfo() {
		FamilyCreatePoJo familyCreatePoJo = FamilyCreatePoJo.builder().name("new Name")

				.phone("1234567890").address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600")
				.country("Country").about("").relationWithFamily("Father").build();
		return familyCreatePoJo;
	}

}
