package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.ShoppingListService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ShoppingListControllerTest {

	@InjectMocks
	private ShoppingListController shopListController;

	@Mock
	UserRepository userRepo;

	@Mock
	ShoppingListService shopListService;
	@Mock
	ShoppingListEntriesRepository shopItemsListRepo;

	@Mock
	Messages messages;
	@Mock
	UserService userService;

	@Test
	public void getShoppingListByIdTestCase() {

		ShoppingList shopList = createObj();
		when(shopListService.findById(1L)).thenReturn(shopList);
		ResponseEntity<?> result = shopListController.getShoppingListById(Long.valueOf(1));
		Object response = result.getBody();
		assertEquals(shopList, response);
	}

	@Test
	public void getShoppingListByIdNullShopListTestCase() {

		when(shopListService.findById(1L)).thenReturn(null);
		when(messages.get("ERROR_SHOP_LIST_ID_INVALID")).thenReturn("Error: Invalid shopping list id :: ");
		MessageResponse response = (MessageResponse) shopListController.getShoppingListById(Long.valueOf(1)).getBody();

		assertTrue(response.getMessage().contains("Invalid shopping list id :: "));

	}

	@Test
	public void getShoppingListByFamilyIdTestCase() {

		List<ShoppingList> shopList = creatListOfShoppingList();
		when(userService.isUserInFamily(1L, 1L )).thenReturn(true);
		when(shopListService.getShoppingListForFamilyOnId(1L)).thenReturn(shopList);
		ResponseEntity<?> result = shopListController.getShoppingListByFamilyId(Long.valueOf(1),Long.valueOf(1));
		Object response = result.getBody();
		assertEquals(shopList, response);
	}

	@Test
	public void getShoppingListByFamilyIdNullShopListTestCase() {
		when(userService.isUserInFamily(1L, 1L )).thenReturn(true);
		when(shopListService.getShoppingListForFamilyOnId(1L)).thenReturn(null);
		when(messages.get("ERROR_SHOP_LIST__FAMILYID_INVALID"))
				.thenReturn("Error: Shopping list not found for family :: ");

		MessageResponse response = (MessageResponse) shopListController.getShoppingListByFamilyId(Long.valueOf(1),Long.valueOf(1))
				.getBody();

		assertTrue(response.getMessage().contains("Error: Shopping list not found for family :: "));

	}

	@Test
	public void createShoppingListTestCase() {
		ShoppingList shoppingList = createObj();
		ShoppingListRequest shopList = createPOJOObj();
		when(shopListService.saveShoppingList(1L, shopList)).thenReturn(shoppingList);

		ResponseEntity<?> result = shopListController.createShoppingList(1L, shopList);
		assertEquals(shoppingList, result.getBody());
	}

	@Test
	public void updateShoppingListTestCase() {
		ShoppingList shoppingList = createObj();
		ShoppingListUpdateRequest shopList = createUpdatePOJOObj();
		ResponseEntity responseEntity = new ResponseEntity<>(shopList, HttpStatus.OK);
		when(shopListService.updateShoppingList(1L, shopList)).thenReturn(shoppingList);

		ResponseEntity<?> result = shopListController.updateShoppingList(1L, shopList);
		assertEquals(shoppingList, result.getBody());
	}

	@Test
	public void deleteShoppingListItemsSuccessTestCase() {
		boolean response = true;
		ShoppingListEntries shopListEntry = new ShoppingListEntries("one", false, 1L, "user1");

		when(shopItemsListRepo.getByItemId(1L)).thenReturn(shopListEntry);
		when(shopListService.deleteShoppingListItemByItemId(1L)).thenReturn(response);
		when(messages.get("SHOPPINGLIST_ITEM_DELETED")).thenReturn("ShoppingList item deleted Successfully ");

		MessageResponse result = (MessageResponse) shopListController.deleteShoppingListItems(1L, 1L).getBody();
		assertTrue(result.getMessage().contains("ShoppingList item deleted Successfully "));
	}


	@Test
	public void deleteShoppingListItemsFailTestCase() {
		boolean response = false;
		when(shopItemsListRepo.getByItemId(1L)).thenReturn(null);
		when(shopListService.deleteShoppingListItemByItemId(1L)).thenReturn(response);
		when(messages.get("ERROR_SHOP_LIST_ITEM_ID_NOT_FOUND")).thenReturn("Error: Could not find shop list item id ");

		MessageResponse result = (MessageResponse) shopListController.deleteShoppingListItems(1L, 1L).getBody();

		assertTrue(result.getMessage().contains("Error: Could not find shop list item id "));

	}

	@Test
	public void deleteShoppingListSuccessTestCase() {
		boolean response = true;
		when(shopListService.deleteShoppingListByShoppingListId(1L)).thenReturn(response);
		when(messages.get("SHOPPINGLIST_DELETED")).thenReturn("ShoppingList deleted successfully ");

		MessageResponse result = (MessageResponse) shopListController.deleteShoppingList(1L, 1L).getBody();
		assertTrue(result.getMessage().contains("ShoppingList deleted successfully "));

	}

	@Test
	public void deleteShoppingListEmptyShoppingListIdTestCase() {
		Long shoppingListId = null;

		ResponseEntity<?> actual = shopListController.deleteShoppingList(shoppingListId, 1L);
		assertEquals(HttpStatus.NOT_FOUND, actual.getStatusCode());

	}

	@Test
	public void deleteShoppingListFailTestCase() {
		boolean response = false;
		when(shopListService.deleteShoppingListByShoppingListId(1L)).thenReturn(response);
		when(messages.get("ERROR_INVALID_REQUEST")).thenReturn("Error: Invalid request ");

		MessageResponse result = (MessageResponse) shopListController.deleteShoppingList(1L, 1L).getBody();

		assertTrue(result.getMessage().contains("Error: Invalid request "));

	}

	@Test
	public void createShoppingListResponseFailedTestCase() {

		ShoppingListRequest shopList = createPOJOObj();

		when(shopListService.saveShoppingList(1L, shopList)).thenReturn(null);
		when(messages.get("ERROR_SAVING_SHOPPING_LIST")).thenReturn("Error: Could not save the Shopping List!");

		MessageResponse response = (MessageResponse) shopListController.createShoppingList(1L, shopList).getBody();

		assertTrue(response.getMessage().contains("Error: Could not save the Shopping List!"));

	}

	@Test
	public void updateShoppingListResponseFailedTestCase() {

		ShoppingListUpdateRequest shopList = createUpdatePOJOObj();

		when(shopListService.updateShoppingList(1L, shopList)).thenReturn(null);
		when(messages.get("ERROR_UPDATING_SHOPPING_LIST")).thenReturn("Error: Could not update the Shopping List");

		MessageResponse response = (MessageResponse) shopListController.updateShoppingList(1L, shopList).getBody();

		assertTrue(response.getMessage().contains("Error: Could not update the Shopping List"));

	}



	@Test
	public void getShoppingListByFamilyUserFailRelationTestCase() {
		when(userService.isUserInFamily(1L, 1L )).thenReturn(false);
		when(messages.get("ERROR_USER_FAMILY_NOT_RELAION_EXISTS"))
				.thenReturn("Error: User is not associated with the family");

		MessageResponse response = (MessageResponse) shopListController.getShoppingListByFamilyId(Long.valueOf(1),Long.valueOf(1))
				.getBody();

		assertTrue(response.getMessage().contains("Error: User is not associated with the family"));

	}
	private ShoppingList createObj() {
		ShoppingList shopList = new ShoppingList("TEST Shop List", 1L, getCurrentDateTime(), true, null);
		Set<ShoppingListEntries> shopListEntries = new HashSet<>();
		ShoppingListEntries shopListEntry = new ShoppingListEntries("one", false, 1L, "user1");
		shopListEntries.add(shopListEntry);
		shopListEntry = new ShoppingListEntries("two", false, 1L, "user1");
		shopListEntries.add(shopListEntry);
		shopList.setShoppingItemsList(shopListEntries);
		return shopList;
	}

	private List<ShoppingList> creatListOfShoppingList() {
		List<ShoppingList> shopLists = new ArrayList<>();
		ShoppingList ShoppingList = createObj();
		shopLists.add(ShoppingList);
		return shopLists;
	}

	private ShoppingListRequest createPOJOObj() {
		ShoppingListRequest shopList = new ShoppingListRequest();

		Map<String, Boolean> strMap = new HashMap<>();
		strMap.put("one", false);
		strMap.put("two", false);

		shopList.setFamilyId(1L);
		shopList.setItemMap(strMap);
		shopList.setShoppinglistName("TEST Shop List");
		return shopList;
	}

	private ShoppingListUpdateRequest createUpdatePOJOObj() {
		ShoppingListUpdateRequest shopList = new ShoppingListUpdateRequest();

		Map<String, Boolean> strMap = new HashMap<>();
		strMap.put("one", false);
		strMap.put("two", false);
		strMap.put("three", true);

		shopList.setId(1L);
		shopList.setItemMap(strMap);
		shopList.setShoppingListName("TEST Shop List");
		shopList.setListStatus(true);
		return shopList;
	}

	private String getCurrentDateTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
}
