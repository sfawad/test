package com.cybersolution.fazeal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.models.NotificationsActivity;
import com.cybersolution.fazeal.service.NotificationService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class NotificationControllerTest {

	@InjectMocks
	private NotificationController notifController;

	@Mock
	private NotificationService notificationService;

	private static final Long USERID = 13L;

	@Test
	public void getNotificationsForUserTestcase() {

		List<NotificationsActivity> notifList = new ArrayList<>();
		NotificationsActivity notif = new NotificationsActivity(1L, "USER_CREATION", "USER_CREATION", 14L, 18L);
		notifList.add(notif);
		when(notificationService.getNotificationsForUsers(USERID)).thenReturn(notifList);
		ResponseEntity<?> actual = notifController.getNotificationsForUser(USERID);
		assertEquals(notifList, actual.getBody());
	}

}
