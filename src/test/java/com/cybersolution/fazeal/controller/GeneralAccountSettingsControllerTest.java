package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.models.FamilyMemberRequest;
import com.cybersolution.fazeal.models.FamilyMembers;
import com.cybersolution.fazeal.models.SystemLanguages;
import com.cybersolution.fazeal.models.SocialMediaLinks;
import com.cybersolution.fazeal.models.SocialMediaLinksRequest;
import com.cybersolution.fazeal.pojo.AddSystemLanguagePojo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.GeneralAccountSettingsService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
class GeneralAccountSettingsControllerTest {
	@InjectMocks
	private GeneralAccountSettingsController generalAccSettingsController;

	@Mock
	private Messages messages;

	@Mock
	private GeneralAccountSettingsService generalAccSettingsService;
	
	@Mock
	private UserService userService;

	@Test
	public void addSystemLanguageUserNotFoundTest() {
		Long userId = 1L;
		String systemLanguage = "English";
		when(userService.setSystemLanguage(userId, systemLanguage)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn("Error: User not found to be approved.");
		AddSystemLanguagePojo addSystemLanguagePojo = new AddSystemLanguagePojo();
		addSystemLanguagePojo.setSystemLanguage(systemLanguage);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addSystemLanguage(userId, addSystemLanguagePojo).getBody();
		assertTrue(result.getMessage().contains("Error: User not found to be approved."));
	}

	@Test
	public void addSystemLanguagesuccessTest() {
		Long userId = 1L;
		String systemLanguage = "English";
		when(userService.setSystemLanguage(userId, systemLanguage)).thenReturn(true);
		when(messages.get(AppConstants.SYSTEM_LANGUAGE_UPDATED_SUCCESSFULLY)).thenReturn("System Language is Updated successfully");
		AddSystemLanguagePojo addSystemLanguagePojo = new AddSystemLanguagePojo();
		addSystemLanguagePojo.setSystemLanguage(systemLanguage);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addSystemLanguage(userId, addSystemLanguagePojo).getBody();
		assertTrue(result.getMessage().contains("System Language is Updated successfully"));
	}

	@Test
	public void getSystemLanguagesuccessTest() {
		Long userId = 1L;
		String systemLanguage = "English";
		when( userService.getSystemLanguage(userId)).thenReturn(systemLanguage);
		AddSystemLanguagePojo addSystemLanguagePojo = new AddSystemLanguagePojo();
		addSystemLanguagePojo.setSystemLanguage(systemLanguage);
		MessageResponse result = (MessageResponse) generalAccSettingsController.getSystemLanguage(userId).getBody();
		assertTrue(result.getMessage().contains(systemLanguage));
	}
	@Test
	public void addSocialMediaLinksSuccessTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		Map<String,String> links= new HashMap<>();
		links.put("Facebook", "fb.me");
		SocialMediaLinksRequest socialMediaLinksRequest= new SocialMediaLinksRequest();
		socialMediaLinksRequest.setSocialMediaLinksMap(links);
		when(messages.get(AppConstants.SOCIAL_MEDIA_LINKS_ADDED_SUCCESSFULLY)).thenReturn("Social Media Links added successfully");
		when(generalAccSettingsService.saveSocialMediaLinks(1L,socialMediaLinksRequest)).thenReturn(socialMediaLinks);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addSocialMediaLinks(1L,socialMediaLinksRequest).getBody();
		assertTrue(result.getMessage().contains("Social Media Links added successfully"));
	}
	@Test
	public void addSocialMediaLinksFailTest() {
		Map<String,String> links= new HashMap<>();
		links.put("Facebook", "fb.me");
		SocialMediaLinksRequest socialMediaLinksRequest= new SocialMediaLinksRequest();
		socialMediaLinksRequest.setSocialMediaLinksMap(links);
		when(messages.get(AppConstants.ERROR_SAVING_SOCIAL_MEDIA_LINKS)).thenReturn("Could not add the Social media link/s");
		when(generalAccSettingsService.saveSocialMediaLinks(1L,socialMediaLinksRequest)).thenReturn(null);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addSocialMediaLinks(1L,socialMediaLinksRequest).getBody();
		assertTrue(result.getMessage().contains("Could not add the Social media link/s"));
	}
	
	@Test
	public void getSocialMediaLinksSuccessTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		ResponseEntity repsponse =generalAccSettingsController.getSocialMediaLinks(1L,1L);
	   assertEquals(repsponse.getBody(),socialMediaLinks);
	}
	
	@Test
	public void getSocialMediaLinksFailTest() {
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)).thenReturn("Could not get social media links");
		MessageResponse result = (MessageResponse)generalAccSettingsController.getSocialMediaLinks(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Could not get social media links"));
	}
	@Test
	public void getSocialMediaLinksUnEqualUserIdTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		when(messages.get(AppConstants.ERROR_INVALID_REQUEST)).thenReturn("Error: Invalid request");
		MessageResponse result = (MessageResponse)generalAccSettingsController.getSocialMediaLinks(2L,1L).getBody();
		assertTrue(result.getMessage().contains("Error: Invalid request"));
	}
	
	@Test
	public void getUsersAllSocialMediaLinksTest() {
		List<SocialMediaLinks> socialMediaLinksList= new ArrayList<>();
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		socialMediaLinksList.add(socialMediaLinks);
		when(generalAccSettingsService.getSocialMediaLinks(1L)).thenReturn(socialMediaLinksList);
		ResponseEntity repsponse =generalAccSettingsController.getUsersAllSocialMediaLinks(1L);
		   assertEquals(repsponse.getBody(),socialMediaLinksList);
		}
	
	@Test
	public void getUsersAllSocialMediaLinksFailTest() {
		List<SocialMediaLinks> socialMediaLinksList= new ArrayList<>();
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		socialMediaLinksList.add(socialMediaLinks);
		when(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)).thenReturn("Could not get social media links");
		when(generalAccSettingsService.getSocialMediaLinks(1L)).thenReturn(null);
		MessageResponse result = (MessageResponse) generalAccSettingsController.getUsersAllSocialMediaLinks(1L).getBody();
		assertTrue(result.getMessage().contains("Could not get social media links"));
		}
	
	@Test
	public void deleteSocialMediaLinkSuccessTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		when(generalAccSettingsService.deleteSocMediaLink(1L)).thenReturn(true);
		when(messages.get(AppConstants.SOCIAL_MEDIA_LINK_DELETED)).thenReturn("Social Media link deleted successfully");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteSocialMediaLink(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Social Media link deleted successfully"));
	}
	
	@Test
	public void deleteSocialMediaLinkFailTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		when(generalAccSettingsService.deleteSocMediaLink(1L)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_INVALID_REQUEST)).thenReturn("Error: Invalid request");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteSocialMediaLink(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Error: Invalid request"));
	}

	@Test
	public void deleteSocialMediaLinkWithNullSocialLinksTest() {
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINKS_NOTFOUND)).thenReturn("Could not get social media links");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteSocialMediaLink(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Could not get social media links"));
	}
	
	@Test
	public void deleteSocialMediaLinkUnEqualUserIdTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccSettingsService.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		when(messages.get(AppConstants.ERROR_SOCIAL_MEDIA_LINK_USER_ASSOCIATION)).thenReturn("Error: social media id passed in request does not have relation with the user. Please check again");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteSocialMediaLink(1L,2L).getBody();
		assertTrue(result.getMessage().contains("Error: social media id passed in request does not have relation with the user. Please check again"));
	}
	
	@Test
	public void addFamilyMemberSuccessTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		Map<String,String> familyMembersMap= new HashMap<>();
		familyMembersMap.put("Father", "maxwell");
		FamilyMemberRequest familyMemberRequest= new FamilyMemberRequest();
		familyMemberRequest.setFamilyMemberMap(familyMembersMap);
		when(messages.get(AppConstants.FAMILY_MEMBER_ADDED_SUCCESSFULLY)).thenReturn("Family member added successfully");
		when(generalAccSettingsService.saveFamilyMembers(1L, familyMemberRequest)).thenReturn(familyMembers);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addFamilyMember(1L,familyMemberRequest).getBody();
		assertTrue(result.getMessage().contains("Family member added successfully"));
	}
	
	@Test
	public void addFamilyMemberFailTest() {
		Map<String,String> familyMembersMap= new HashMap<>();
		familyMembersMap.put("Father", "maxwell");
		FamilyMemberRequest familyMemberRequest= new FamilyMemberRequest();
		familyMemberRequest.setFamilyMemberMap(familyMembersMap);
		when(messages.get(AppConstants.ERROR_SAVING_FAMILY_MEMBER)).thenReturn("Could not add the Family Members");
		when(generalAccSettingsService.saveFamilyMembers(1L, familyMemberRequest)).thenReturn(null);
		MessageResponse result = (MessageResponse) generalAccSettingsController.addFamilyMember(1L,familyMemberRequest).getBody();
		assertTrue(result.getMessage().contains("Could not add the Family Members"));
	}
	@Test
	public void getUsersAllFamilyMembersTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		List<FamilyMembers> familyMembersList=List.of(familyMembers);
		when(generalAccSettingsService.getFamilyMembers(1L)).thenReturn(familyMembersList);
		ResponseEntity repsponse =generalAccSettingsController.getUsersAllFamilyMembers(1L);
		   assertEquals(repsponse.getBody(),familyMembersList);
	}
	
	@Test
	public void getUsersAllFamilyMembersFailTest() {
		when(generalAccSettingsService.getFamilyMembers(1L)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)).thenReturn("Could not get Family Members");
		MessageResponse result = (MessageResponse) generalAccSettingsController.getUsersAllFamilyMembers(1L).getBody();
		assertTrue(result.getMessage().contains("Could not get Family Members"));
	}
	
	@Test
	public void getFamilyMemberTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		ResponseEntity repsponse =generalAccSettingsController.getFamilyMember(1L,1L);
		   assertEquals(repsponse.getBody(),familyMembers);
	}
	
	@Test
	public void getFamilyMemberFailTest() {
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)).thenReturn("Could not get Family Members");
		MessageResponse result = (MessageResponse) generalAccSettingsController.getFamilyMember(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Could not get Family Members"));
	}
	
	@Test
	public void getFamilyMemberUnequalUserIdTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		when(messages.get(AppConstants.ERROR_INVALID_REQUEST)).thenReturn("Error: Invalid request");
		MessageResponse result = (MessageResponse) generalAccSettingsController.getFamilyMember(2L,1L).getBody();
		assertTrue(result.getMessage().contains("Error: Invalid request"));
	}
	
	@Test
	public void deleteFamilyMemberSuccessTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		when(generalAccSettingsService.deleteFamilyMember(1L)).thenReturn(true);
		when(messages.get(AppConstants.FAMILY_MEMBER_DELETED)).thenReturn("Family member deleted successfully");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteFamilyMember(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Family member deleted successfully"));
	}
	
	@Test
	public void deleteFamilyMemberFailTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		when(generalAccSettingsService.deleteFamilyMember(1L)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_INVALID_REQUEST)).thenReturn("Error: Invalid request");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteFamilyMember(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Error: Invalid request"));
	}

	@Test
	public void deleteFamilyMemberWithNullSocialLinksTest() {
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_FAMILY_MEMBERS_NOTFOUND)).thenReturn("Could not get family members");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteFamilyMember(1L,1L).getBody();
		assertTrue(result.getMessage().contains("Could not get family members"));
	}
	
	@Test
	public void deleteFamilyMemberUnEqualUserIdTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsService.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		when(messages.get(AppConstants.ERROR_FAMILY_MEMBER_USER_ASSOCIATION)).thenReturn("Error: family member id passed in request does not have relation with the user. Please check again");
		MessageResponse result = (MessageResponse) generalAccSettingsController.deleteFamilyMember(1L,2L).getBody();
		assertTrue(result.getMessage().contains("Error: family member id passed in request does not have relation with the user. Please check again"));
	}
	
	@Test
	public void listAllSystemLanguagesTest() {
		SystemLanguages  systemLanguages= new SystemLanguages(1L,"English");
		List<SystemLanguages> systemLanguagesList= List.of(systemLanguages);
		when(generalAccSettingsService.findAll()).thenReturn(systemLanguagesList);
		ResponseEntity response=generalAccSettingsController.listAllSystemLanguages(1L);
		assertEquals(response.getBody(),systemLanguagesList);
	}
	

}
