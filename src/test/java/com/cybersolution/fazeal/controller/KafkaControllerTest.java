package com.cybersolution.fazeal.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.service.KafkaService;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaControllerTest {
	@InjectMocks
	KafkaController kafkaController;
	
	@Mock
    private KafkaService kafkaService;

	@Test
	void authenticateUserSuccessTestCase() {
		Notification notification = Notification.builder().build();
		ResponseEntity<Object> authenticateUser = kafkaController.authenticateUser(notification);
		assertEquals(HttpStatus.OK, authenticateUser.getStatusCode());
	}

}
