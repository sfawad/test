package com.cybersolution.fazeal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.Voting;
import com.cybersolution.fazeal.models.VotingOptions;
import com.cybersolution.fazeal.models.VotingOptionsSelection;
import com.cybersolution.fazeal.models.VotingResponse;
import com.cybersolution.fazeal.pojo.SaveVotingPojo;
import com.cybersolution.fazeal.pojo.UpdateVotingPojo;
import com.cybersolution.fazeal.pojo.UpdateVotingSelectionPojo;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.VotingService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class VotingControllerTest {

	@InjectMocks
	private VotingController votingController;

	@Mock
	Messages messages;
	@Mock
	private VotingService votingService;
	@Mock
	private UserService userService;

	@Test
	public void getVotingByFamilyTestCase() throws Exception {
		// setup
		List<Voting> votingList = new ArrayList<>();
		Voting voting = new Voting(1L, 1L, "votingTest", getCurrentDateTime());
		votingList.add(voting);
		List<VotingResponse> votingListResp = new ArrayList<>();
		VotingResponse votingResp = new VotingResponse();
		BeanUtils.copyProperties(voting, votingResp);
		votingListResp.add(votingResp);
		when(userService.isUserInFamily(1L, 1L )).thenReturn(true);
		when(votingService.getAllFamilyVotingsByFamilyId(1L)).thenReturn(votingListResp);

		// run test
		ResponseEntity<?> result = votingController.listAllVotingsByFamilyId(1L, 1L);
		Object response = result.getBody();

		// assert
		assertEquals(votingListResp, response);
	}

	@Test
	public void getVotingByFamilyTestCaseReturnEmptyList() throws Exception {
		// setup
		when(userService.isUserInFamily(1L, 1L )).thenReturn(true);
		when(votingService.getAllFamilyVotingsByFamilyId(1L)).thenReturn(Collections.emptyList());
		when(messages.get("ERROR_NO_VOTING_RECORD_FOUND")).thenReturn("Error: No voting record found");

		// run test
		ResponseEntity<?> result = votingController.listAllVotingsByFamilyId(1L,1L);
		String response = (String) result.getBody();

		// assert
		assertTrue(response.contains("No voting record"));
	}

	@Test
	public void saveVotingTest() {
		// setup
		Voting voting = new Voting(1L, 1L, "VotingTest", getCurrentDateTime());
		Set<VotingOptions> votOptsSet = new HashSet<VotingOptions>();
		VotingOptions votingOpts = new VotingOptions("apple");
		votOptsSet.add(votingOpts);
		voting.setVotingDetails(votOptsSet);
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		user.setId(1L);
		Set<Family> families = new HashSet<Family>();
		Family family = new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		SaveVotingPojo saveVotingPojo= SaveVotingPojo.builder().familyId(1L).title("VotingTest").options(List.of("apple")).build();
		when(userService.isUserInFamily(1L, 1L)).thenReturn(true);
		when(votingService.saveUserVoting(1L, 1L, "VotingTest", List.of( "apple"))).thenReturn(voting);

		// run test
		ResponseEntity<?> result = votingController.saveUserVoting(1L,saveVotingPojo );
		Object response = result.getBody();

		// assert
		assertEquals(voting, response);
	}
 
	@Test
	public void testUpdateVotingOptionsByIdSuccess() {
		Voting voting = new Voting(1L, 1L, "VotingTest", getCurrentDateTime());
		voting.setId(1L);
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		user.setId(1L);
		Set<Family> families = new HashSet<Family>();
		Family family = new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		UpdateVotingPojo updateVotingPojo= UpdateVotingPojo.builder().options(List.of( "peach")).type("A").build();
		when(votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 1L, "A")).thenReturn(voting);
		ResponseEntity<?> result = votingController.updateVotingOptionsByVoteId(1L, 1L,updateVotingPojo);
		Object response = result.getBody();
		assertEquals(voting, response);
	}

	@Test
	public void testUpdateVotingOptionsByIdFail() {

		when(votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 1L, "A")).thenReturn(null);
		when(messages.get("ERROR_USER_OR_NO_VOTING_OR_USER_NOT_VALID"))
				.thenReturn("Error: user not found, or don't have permission to updat vote, or vote not exist.");
		UpdateVotingPojo updateVotingPojo= UpdateVotingPojo.builder().options(List.of( "peach")).type("A").build();
		
		ResponseEntity<?> result = votingController.updateVotingOptionsByVoteId(1L,1L, updateVotingPojo);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains("user not found, or don't have permission to updat vote, or vote not exist"));
	}

	@Test
	public void testUpdateVotingSuccess() {
		Voting votingResp = new Voting(1L, 1L, "VotingTest", getCurrentDateTime());
		votingResp.setId(1L);
		Set<VotingOptionsSelection> votOptsSelSet = new HashSet<VotingOptionsSelection>();
		VotingOptionsSelection votingOptsSel = new VotingOptionsSelection(1L, 1L);
		votOptsSelSet.add(votingOptsSel);
		Set<VotingOptions> votOptsSetResp = new HashSet<VotingOptions>();
		VotingOptions votingOptsResp = new VotingOptions("apple");
		votingOptsResp.setId(1L);
		votingOptsResp.setVotingSelection(votOptsSelSet);
		votOptsSetResp.add(votingOptsResp);
		votingResp.setVotingDetails(votOptsSetResp);
		Voting voting = new Voting(1L, 1L, "VotingTest", getCurrentDateTime());
		voting.setId(1L);
		Set<VotingOptions> votOptsSet = new HashSet<VotingOptions>();
		VotingOptions votingOpts = new VotingOptions("apple");
		votingOpts.setId(1L);
		votOptsSet.add(votingOpts);
		voting.setVotingDetails(votOptsSet);
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		user.setId(1L);
		Set<Family> families = new HashSet<Family>();
		Family family = new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		UpdateVotingSelectionPojo updateVotingSelectionPojo=UpdateVotingSelectionPojo.builder().options("apple").build();
		when(votingService.updateVotingByIdUserIdOption(1L, 1L, "apple")).thenReturn(votingResp);
		ResponseEntity<?> result = votingController.updateVoting(1L, 1L, updateVotingSelectionPojo);
		Object response = result.getBody();
		assertEquals(votingResp, response);
	}



	@Test
	public void testUpdateVotingNullRetrunFromServiceFailed() {
		when(messages.get("ERROR_USER_NOT_EXIST_OR_NOT_IN_FAMILY_OR_ALREADY_VOTED_OR_VOTE_NOT_FOUND"))
				.thenReturn("Error: User not exist or not part of family or already voted or vote not found.");
		when(votingService.updateVotingByIdUserIdOption(1L, 1L, "apple")).thenReturn(null);
		UpdateVotingSelectionPojo updateVotingSelectionPojo=UpdateVotingSelectionPojo.builder().options("apple").build();
		ResponseEntity<?> result = votingController.updateVoting(1L, 1L, updateVotingSelectionPojo);
		String response = String.valueOf(result.getBody());
		assertTrue(response.contains("User not exist or not part of family or "));
	}

	private String getCurrentDateTime() {
		String returnTime = null;
		Date date = new Timestamp(System.currentTimeMillis());
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			returnTime = new Timestamp(formatter.parse(date.toString()).getTime()).toString();

		} catch (Exception ec) {
			ec.getMessage();
		}
		return returnTime;
	}
	
	
	@Test
	public void saveVotingTestFailUserFamilyRelation() {
		// setup
		SaveVotingPojo saveVotingPojo= SaveVotingPojo.builder().familyId(1L).title("VotingTest").options(List.of( "peach")).build();
		
		when(messages.get("ERROR_USER_FAMILY_NOT_RELAION_EXISTS")).thenReturn("User family relation not found");
		// run test
		when(userService.isUserInFamily(1L, 1L)).thenReturn(false);
		ResponseEntity<?> result = votingController.saveUserVoting(1L, saveVotingPojo);
		Object response = result.getBody();
		// assert
		assertTrue(response.toString().contains("User family relation not found"));
	}
	@Test
	public void getVotingByFamilyTestCaseFailUserFamilyRelation() throws Exception {
		// setup
		when(userService.isUserInFamily(1L, 1L )).thenReturn(false);
		when(votingService.getAllFamilyVotingsByFamilyId(1L)).thenReturn(Collections.emptyList());
		when(messages.get("ERROR_USER_FAMILY_NOT_RELAION_EXISTS")).thenReturn("User family relation not found");

		// run test
		MessageResponse result = (MessageResponse) votingController.listAllVotingsByFamilyId(1L,1L).getBody();

		// assert
		assertTrue(result.getMessage().contains("User family relation not found"));
	}
}
