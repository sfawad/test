package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cybersolution.fazeal.requestbody.ThemeBody;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.ThemeService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
class ThemeControllerTest {
	@InjectMocks
	private ThemeController themeController;

	@Mock
	private Messages messages;

	@Mock
	private ThemeService themeService;

	@Test
	void updateUserThemeTestCaseUserNotFound() {
		Long userId = 1L;
		String theme = "green";
		when(themeService.updateUserTheme(userId, theme)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn("Error: User not found to be approved.");
		ThemeBody themeBody = new ThemeBody();
		themeBody.setTheme(theme);
		MessageResponse result = (MessageResponse) themeController.updateUserTheme(userId, themeBody).getBody();
		assertTrue(result.getMessage().contains("Error: User not found to be approved."));
	}

	@Test
	void updateUserThemeTestCaseSuccess() {
		Long userId = 1L;
		String theme = "green";
		when(themeService.updateUserTheme(userId, theme)).thenReturn(true);
		when(messages.get(AppConstants.THEME_UPDATED_SUCCESSFULLY)).thenReturn("User theme updated successfully");
		ThemeBody themeBody = new ThemeBody();
		themeBody.setTheme(theme);
		MessageResponse result = (MessageResponse) themeController.updateUserTheme(userId, themeBody).getBody();
		assertTrue(result.getMessage().contains("User theme updated successfully"));
	}

	@Test
	void getUserThemeTestCaseSuccess() {
		Long userId = 1L;
		String theme = "green";
		when(themeService.getUserTheme(userId)).thenReturn(theme);
		ThemeBody themeBody = new ThemeBody();
		themeBody.setTheme(theme);
		MessageResponse result = (MessageResponse) themeController.getUserTheme(userId).getBody();
		assertTrue(result.getMessage().contains(theme));
	}

}
