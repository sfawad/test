package com.cybersolution.fazeal.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cybersolution.fazeal.service.KafkaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.LoginRequest;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.UserUpdateDetails;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.service.AuthService;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthControllerTest {
	@InjectMocks
	private AuthController authController;

	@Mock
	Messages messages;
	@Mock
	EmailService emailService;
	@Mock
	FamilyService famService;
	@Mock
	AuditLogsHelper auditLogsHelper;
	@Mock
	private AuthService authService;
	@Mock
	private HttpServletRequest httpServletRequest;
	@Mock
	private FamilyController familyController;
	@Mock
	private FamilyService familyService;
	@Mock
	private KafkaService kafkaService;

	public static final String EMAIL_ID = "test@test.com";
	public static final String PASSWORD = "123A2!@kjdsllkd";
	public static final String PHONE = "1234569087";
	public static final String USERNAME = "username";
	public static final String EMAIL_ERROR = "Error: Email id is mandatory.";
	public static final String TOKEN = "token";
	public static final String PARAM_MISSING_ERROR = "Error: Please check input parameters. One of the mandatory input parameter has null or invalid value";
	public static final String USER_USERNAME = "testUser";
	public static final String USER_PASSWORD = "Encrepted";
	public static final String USER_NOT_APPROVED_ERROR = "Error: User not found to be approved.";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void registerUserTestCaseNoUserName() {
		SignupRequest signUpRequest = new SignupRequest(null, EMAIL_ID, "", PASSWORD, false, null, null, null, "male",
				true, null, PHONE);
		when(messages.get(AppConstants.ERROR_USER_NAME_NULL)).thenReturn("Error: User name is mandatory.");
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Error: User name is mandatory."));
	}

	@Test
	public void registerUserTestCaseDuplicatedUsername() {
		SignupRequest signUpRequest = getSignupRequestInfo();
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(true);
		when(messages.get(AppConstants.ERROR_USER_NAME)).thenReturn(EMAIL_ERROR);
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals(EMAIL_ERROR));
	}

	@Test
	public void registerUserTestCaseNoEmail() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, null, "", PASSWORD, false, null, null, null, "male",
				true, null, PHONE);
		when(messages.get(AppConstants.ERROR_EMAIL_NULL)).thenReturn(EMAIL_ERROR);
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals(EMAIL_ERROR));
	}

	@Test
	public void registerUserTestCaseDuplicatedEmail() {
		SignupRequest signUpRequest = getSignupRequestInfo();
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(true);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.ERROR_EMAIL_IN_USE)).thenReturn(EMAIL_ERROR);
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals(EMAIL_ERROR));
	}

	@Test
	public void registerUserTestCaseNoDate() {
		SignupRequest signUpRequest = getSignupRequestInfo();
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.ERROR_DOB_MANDATORY)).thenReturn("Error: Date id is mandatory.");
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Error: Date id is mandatory."));
	}

	@Test
	public void registerUserTestCaseNoPhone() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, EMAIL_ID, "", PASSWORD, false, null, null, "date",
				"male", true, null, "");
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.ERROR_PHONE_NULL)).thenReturn("Error: Phone number is mandatory.");
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Error: Phone number is mandatory."));
	}

	@Test
	public void registerUserTestCaseJoinFamilyWithouTokenFailed() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, EMAIL_ID, "", PASSWORD, false, null, null, "dob",
				"male", false, "", PHONE);
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.ERROR_FAMILY_TOKEN_MANDATORY))
				.thenReturn("Error: Please provide token to join family, it is mandatory!");
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Error: Please provide token to join family, it is mandatory!"));
	}

	@Test
	public void registerUserTestCaseJoinByFamilyTokenNoFamily() {
		SignupRequest signUpRequest = signupRequestMoreInfo();
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(famService.getFamilyInfoFromToken(signUpRequest.getFamilyToken())).thenReturn(null);
		when(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)).thenReturn("Family not found for name:");
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Family not found for name"));
	}

	@Test
	public void registerUserTestCaseJoinFamilyWithouTokenUserNotCreatedFailed() {
		SignupRequest signUpRequest = signupRequestMoreInfo();
		Family family = Family.builder().token(TOKEN).build();
		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.ERROR_FAMILY_NOTFOUND)).thenReturn("Error: User not saved, server error");
		when(famService.getFamilyInfoFromToken(signUpRequest.getFamilyToken())).thenReturn(family);
		when(authService.createNewUser(signUpRequest)).thenReturn(null);
		MessageResponse result = authController.registerUser(signUpRequest, httpServletRequest).getBody();
		assertThat(result.equals("Error: User not saved, server error"));
	}

	@Test
	public void registerUserTestCaseJoinFamilyWithouTokenSuccess() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, EMAIL_ID, "", "pass", true, "fname", "lname", "dob",
				"male", false, TOKEN, PHONE);
		Family family = Family.builder().token(TOKEN).build();
		User user = User.builder().id(1L).firstName(signUpRequest.getFirstName()).lastName(signUpRequest.getLastName())
				.dob(signUpRequest.getDob()).enabled(true).gender(signUpRequest.getGender())
				.password(signUpRequest.getPassword()).build();

		when(messages.get(AppConstants.PASSWORD_EMAIL_FROM)).thenReturn("from");
		when(messages.get(AppConstants.SIGNUP_EMAIL_SUBJECT)).thenReturn("subject");
		when(messages.get(AppConstants.SIGNUP_EMAIL_SUBJECT_JOIN_FAMILY_TEXT)).thenReturn("text");
		when(messages.get(AppConstants.DOMAIN_SRC)).thenReturn("domain");

		when(authService.isUserExistsWithEmail(signUpRequest.getEmail())).thenReturn(false);
		when(authService.isUserExistsWithUsername(signUpRequest.getUsername())).thenReturn(false);
		when(messages.get(AppConstants.SIGNUP_JOIN_FAMILY_RESPONSE_MSG))
				.thenReturn("User created successfully and joined family with tok");
		when(famService.getFamilyInfoFromToken(signUpRequest.getFamilyToken())).thenReturn(family);
		when(authService.createNewUser(signUpRequest)).thenReturn(user);
		when(familyService.getFamilyInfoFromToken(signUpRequest.getFamilyToken())).thenReturn(family);
		when(familyController.requestJoinUserToFamily(user.getId(), signUpRequest.getFamilyToken())).thenReturn(null);
		MessageResponse result = (MessageResponse) authController.registerUser(signUpRequest, httpServletRequest)
				.getBody();
		assertTrue(result.getMessage().contains("User created successfully and joined family with tok"));
	}

	@Test
	public void authenticateUserTestCaseUserNotFoundOrUsernameAndPasswordCombinationWrong() {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setPassword("password");
		loginRequest.setUsername(USERNAME);
		when(authService.loginByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword()))
				.thenReturn(null);
		when(messages.get(AppConstants.USER_PASSWORD_COMBINATION_OR_USER_NOT_FOUND))
				.thenReturn("Username and password combination are not correct, or user not registered!");
		MessageResponse result = (MessageResponse) authController.authenticateUser(loginRequest, httpServletRequest)
				.getBody();
		assertTrue(result.getMessage()
				.contains("Username and password combination are not correct, or user not registered!"));
	}

	@Test
	public void authenticateUserTestCaseUserSuccess() {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setPassword("password");
		loginRequest.setUsername(USERNAME);
		JwtResponse response = new JwtResponse();
		response.setAccessToken(TOKEN);
		response.setUsername(USERNAME);
		when(authService.loginByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword()))
				.thenReturn(response);
		JwtResponse result = (JwtResponse) authController.authenticateUser(loginRequest, httpServletRequest).getBody();
		assertEquals(result, response);
	}


	@Test
	public void listAllFamiliesAndUsersByUserIdTestCaseNoUserFound() {
		Long userId = 1L;
		when(authService.getUserById(userId)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)).thenReturn("Error: User not exists");
		MessageResponse result = (MessageResponse) authController.listAllFamiliesAndUsersByUserId(userId).getBody();
		assertTrue(result.getMessage().contains("Error: User not exists"));
	}

	@Test
	public void listAllFamiliesAndUsersByUserIdTestCaseSuccess() {
		Long userId = 1L;
		User user = getUser();
		when(authService.getUserById(userId)).thenReturn(user);
		when(authService.listAllFamiliesAndUsersByUserId(user)).thenReturn(new HashMap<String, List<UserResponse>>());
		Map<String, List<UserResponse>> result = (Map<String, List<UserResponse>>) authController
				.listAllFamiliesAndUsersByUserId(userId).getBody();
		assertEquals(0, result.size());
	}



	@Test
	public void findUserByIdTestCaseNoUserFound() {
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn(USER_NOT_APPROVED_ERROR);
		Long userId = 1L;
		MessageResponse result = (MessageResponse) authController.findUserById(userId).getBody();
		assertTrue(result.getMessage().contains(USER_NOT_APPROVED_ERROR));
	}

	@Test
	public void findUserByIdTestCaseSuccess() {
		Long userId = 1L;
		User user = getUser();
		when(authService.getUserById(userId)).thenReturn(user);
		when(authService.findUserById(userId, user)).thenReturn(new UserResponse());
		UserResponse result = (UserResponse) authController.findUserById(userId).getBody();
		assertEquals(result, new UserResponse());
	}

	@Test
	public void updateUserDataTestCaseSuccess() {
		String email = "email";
		String password = "pass";
		String firstName = "fname";
		String lastName = "lname";

		User user = User.builder().email(email).password(password).firstName(firstName).lastName(lastName).build();
		UserUpdateDetails userUpdateDetails = createPOJOObj();
		when(authService.updateUserDetails(1L, userUpdateDetails)).thenReturn(user);
		ResponseEntity<?> updateUser = authController.updateUser(1L, userUpdateDetails);
		assertEquals(updateUser.getBody(), user);
	}

	@Test
	public void updateUserDataTestCaseNoUserFound() {

		UserUpdateDetails userUpdateDetails = createPOJOObj();
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn(USER_NOT_APPROVED_ERROR);
		MessageResponse result = (MessageResponse) authController.updateUser(1L, userUpdateDetails).getBody();
		assertTrue(result.getMessage().contains(USER_NOT_APPROVED_ERROR));
	}


	@Test
	public void listAllUsersSuccess() {
		User user = User.builder().id(1L).firstName("test").build();
		when(authService.findAll()).thenReturn(List.of(user));
		ResponseEntity<List<User>> listAllUsers = authController.listAllUsers(user.getId());
		assertTrue(listAllUsers.getBody().size() > 0);
	}


	@Test
	public void deleteUserTestCaseSuccess() {
		Long userId = 1L;
		when(authService.deleteUserById(userId)).thenReturn(true);
		boolean result = (boolean) authController.deleteUser(userId).getBody();
		assertTrue(result);
	}

	@Test
	public void deleteUserTestCaseFail() {
		Long userId = 1L;
		when(authService.deleteUserById(userId)).thenReturn(false);
		boolean result = (boolean) authController.deleteUser(userId).getBody();
		assertFalse(result);
	}

	@Test
	public void enableUserSucessTestCaseSucess() {
		User user = new User("testUser", EMAIL_ID, "Encrepted", false);
		Long userId = 2L;
		Long userIdToBeUpdated = 22L;

		when(authService.changeEnabledStatus(userIdToBeUpdated)).thenReturn(user);

		ResponseEntity<?> result = authController.updateUserEnabled(userId, userIdToBeUpdated);
		user.setEnabled(true);
		User response = (User) result.getBody();
		assertTrue(response.isEnabled());
		assertEquals(response, user);
	}

	@Test
	public void enableUserTestCaseUserNotFound() {
		Long userId = 2L;
		Long userIdToBeUpdated = 22L;
		when(authService.changeEnabledStatus(userIdToBeUpdated)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_USER_NOT_FOUND)).thenReturn(USER_NOT_APPROVED_ERROR);
		MessageResponse result = (MessageResponse) authController.updateUserEnabled(userId, userIdToBeUpdated)
				.getBody();
		assertTrue(result.getMessage().contains(USER_NOT_APPROVED_ERROR));
	}


	@Test
	public void forgotPasswordTestCaseSuccess() {
		String email = "test1@test.com";
		User user = new User("testUser", email, "Encrepted", true);
		when(authService.generateTokenToResetUserPassword(email)).thenReturn(user);
		when(messages.get(AppConstants.PASSWORD_EMAIL_SENT_SUCCESS))
				.thenReturn("Email sent to user with reset password link");
		when(messages.get(AppConstants.PASSWORD_EMAIL_RESET_TEXT)).thenReturn("message");
		MessageResponse result = (MessageResponse) authController.forgotPassword(email).getBody();
		assertTrue(result.getMessage().contains("Email sent to user with reset password link"));
	}

	@Test
	public void forgotPasswordTestCaseNoUserWithEmail() {
		String email = "test1@test.com";
		when(authService.generateTokenToResetUserPassword(email)).thenReturn(null);
		when(messages.get(AppConstants.ERROR_USER_NOT_EXISTS)).thenReturn("Error: User not exists");
		MessageResponse result = (MessageResponse) authController.forgotPassword(email).getBody();
		assertTrue(result.getMessage().contains("Error: User not exists"));
	}


	@Test
	public void verifyResetTokenTestCaseVerified() {
		String token = TOKEN;
		User user = getUser();
		when(authService.getUserByResetToken(token)).thenReturn(user);
		when(messages.get(AppConstants.TOKEN_VERIFIED)).thenReturn("User token verified");
		MessageResponse result = (MessageResponse) authController.verifyResetToken(token).getBody();
		assertTrue(result.getMessage().contains("User token verified"));
	}

	@Test
	public void verifyResetTokenTestCaseNotVerified() {
		String token = TOKEN;
		when(authService.getUserByResetToken(token)).thenReturn(null);
		when(messages.get(AppConstants.TOKEN_IS_NOT_FOUND)).thenReturn("Token is not valid");
		MessageResponse result = (MessageResponse) authController.verifyResetToken(token).getBody();
		assertTrue(result.getMessage().contains("Token is not valid"));
	}


	@Test
	public void resetPasswordTestCaseSuccess() {
		String token = TOKEN;
		User user = getUser();
		when(authService.getUserByResetToken(token)).thenReturn(user);
		when(authService.isTokenExpired(user)).thenReturn(false);
		when(messages.get(AppConstants.PASSWORD_UPDATED_SUCCESS)).thenReturn("Password has been updated successfully.");
		ResponseEntity<?> response = authController.resetPassword(token, "passw0rd");
		MessageResponse result = (MessageResponse) response.getBody();
		assertTrue(result.getMessage().contains("Password has been updated successfully."));
	}



	@Test
	public void resetPasswordTestCaseTokenExpired() {
		String token = TOKEN;
		User user = getUser();
		when(authService.getUserByResetToken(token)).thenReturn(user);
		when(authService.isTokenExpired(user)).thenReturn(true);
		when(messages.get(AppConstants.PASSWORD_TOKEN_EXPIRED))
				.thenReturn("Error: password token expired, please generate new token.");
		ResponseEntity<?> response = authController.resetPassword(token, TOKEN);
		MessageResponse result = (MessageResponse) response.getBody();
		assertTrue(result.getMessage().contains("Error: password token expired, please generate new token."));
	}

	@Test
	public void resetPasswordTestCaseWrongToken() {
		String token = TOKEN;
		when(authService.getUserByResetToken(token)).thenReturn(null);
		when(messages.get(AppConstants.PASSWORD_ERROR_USER_TOKEN_INVALID)).thenReturn("Error: Token is invalid!");
		ResponseEntity<?> response = authController.resetPassword(token, TOKEN);
		MessageResponse result = (MessageResponse) response.getBody();
		assertTrue(result.getMessage().contains("Error: Token is invalid!"));
	}

	private UserUpdateDetails createPOJOObj() {
		return UserUpdateDetails.builder().aboutMe("about me").dateOfBirth("09-09-2001").email("Gamil@gmail.com")
				.firstName("Firstname").gender("Female").languages("Enlish").lastName("Lastname").nickname("Nick")
				.password("jhafjkhk").build();

	}

	private SignupRequest signupRequestMoreInfo() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, EMAIL_ID, "", PASSWORD, false, null, null, "dob",
				"male", false, TOKEN, PHONE);
		return signUpRequest;
	}

	private SignupRequest getSignupRequestInfo() {
		SignupRequest signUpRequest = new SignupRequest(USERNAME, EMAIL_ID, "", PASSWORD, false, null, null, null,
				"male", true, null, PHONE);
		return signUpRequest;
	}

	private User getUser() {
		User user = new User(USER_USERNAME, EMAIL_ID, USER_PASSWORD, true);
		return user;
	}

}
