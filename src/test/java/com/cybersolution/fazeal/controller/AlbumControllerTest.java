package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.AlbumCoverPojo;
import com.cybersolution.fazeal.pojo.CreateAlbumModelPoJo;
import com.cybersolution.fazeal.pojo.UpdateAlbumModelPoJo;
import com.cybersolution.fazeal.response.MessageResponse;
import com.cybersolution.fazeal.security.services.UserDetailsImpl;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AlbumControllerTest {

	@InjectMocks
	AlbumController albumController;

	@Mock
	private AlbumService albumService;
	@Mock
	private UserService userService;
	@Mock
	private Messages messages;
	@Mock
	SecurityContext securityContext;

	public static final String ALBUM_DESCRIPTION = "AlbumDescription";
	public static final String ALBUM_NAME = "AlbumName";
	public static final String PE_PR = "PE_PR";
	public static final String STATE = "state";
	public static final String COUNTRY = "country";
	public static final String LINE1 = "line1";
	public static final String LINE2 = "line2";
	public static final String LINE12 = "line12";
	public static final String CITY2 = "city2";
	public static final String LINE22 = "line22";
	public static final String STATE2 = "state2";
	public static final String COUNTRY2 = "country2";
	public static final String PE_CU = "PE_CU";
	public static final String PE_PU = "PE_PU";
	public static final String FAMILY1_PHNO = "1234567891";
	public static final String FAMILY2_PHNO = "2234567891";
	public static final String INPUT_PARAM_MISSING = "Error: Please check input parameters. One of the mandatory input parameter has null or invalid value";
	public static final String ERR_NO_RECORD_FOUND = "No record found";

	@Test
	void testCreateAlbumAlreadyExistForAUser() {
		// setup
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumId(10L).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION)
				.date(date).userId(20L).type(PE_PR).build();
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(album);
		when(messages.get("ERROR_ALBUM_ALREADY_EXISTS")).thenReturn("Error: Album already exists");
		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date)
				.albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).familyIds(List.of(111L, 112L, 113L))
				.userIds(List.of(1L)).type(PE_PR).build();
		// run test
		MessageResponse response = (MessageResponse) albumController.createNewAlbum(albumModelPoJo).getBody();

		// assert
		assertTrue(response.getMessage().toString().contains("Album already exists"));
	}

	@Test
	void testCreatePrivateAlbumUser() {
		// setup
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.userId(20L).type(PE_PR).build();
		AlbumModel expected = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.albumId(1L).userId(20L).type(PE_PR).build();

		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(null);
		when(albumService.createPrivateAlbum(album)).thenReturn(expected);
		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date)
				.albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).type(PE_PR).userIds(List.of())
				.familyIds(List.of()).build();

		// run test
		ResponseEntity<?> response = albumController.createNewAlbum(albumModelPoJo);
		AlbumModel result = (AlbumModel) response.getBody();

		// assert
		assertEquals(result, expected);
		assertNull(result.getAlbumFamiliesList());
		assertNull(result.getAlbumUserList());
	}

	@Test
	void testCreatePublicAlbumWith2FamiliesUser() {
		// setup
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 1L, 230L);

		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.userId(20L).type(PE_PU).build();
		AlbumModel saved = AlbumModel.builder().albumId(1L).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION)
				.date(date).userId(20L).type(PE_PU).albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2))
				.build();

		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(null);
		when(albumService.createPublicToAllUserFamiliesAlbum(album, loggedUser)).thenReturn(saved);
		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date)
				.albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).familyIds(List.of(200L, 230L)).type(PE_PU)
				.build();

		// run test
		ResponseEntity<?> response = albumController.createNewAlbum(albumModelPoJo);
		AlbumModel result = (AlbumModel) response.getBody();

		// assert
		assertEquals(2, result.getAlbumFamiliesList().size());
		assertNull(result.getAlbumUserList());
	}

	@Test
	void testCreateSharedAlbumWith2FamiliesAndFriendsUser() {
		// setup
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 1L, 3L);

		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.userId(20L).type(PE_PU).build();
		AlbumModel saved = AlbumModel.builder().albumId(1L).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION)
				.date(date).userId(20L).type(PE_PU).albumFamiliesList(Set.of(albumModelFamilies))
				.albumUserList(Set.of(albumModelUser3)).build();

		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(null);
		when(albumService.createPublicToAllUserFamiliesAlbum(album, loggedUser)).thenReturn(saved);

		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date)
				.albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).familyIds(List.of(200L, 230L)).type(PE_PU)
				.build();
		// run test
		ResponseEntity<Object> response = (ResponseEntity<Object>) albumController.createNewAlbum(albumModelPoJo);
		AlbumModel result = (AlbumModel) response.getBody();

		// assert
		assertEquals(1, result.getAlbumFamiliesList().size());
		assertEquals(1, result.getAlbumUserList().size());
	}

	@Test
	void testCreateSharedAlbumWith2FamiliesAndFriendsUserEmptyListFails() {
		// setup
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 1L, 3L);

		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.userId(20L).type(PE_PU).build();
		AlbumModel saved = AlbumModel.builder().albumId(1L).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION)
				.date(date).userId(20L).type(PE_PU).albumFamiliesList(Set.of(albumModelFamilies))
				.albumUserList(Set.of(albumModelUser3)).build();

		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(null);
		when(albumService.createPublicToAllUserFamiliesAlbum(album, loggedUser)).thenReturn(saved);

		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date).userIds(List.of())
				.familyIds(List.of()).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).type(PE_CU).build();
		// run test
		ResponseEntity<Object> response = (ResponseEntity<Object>) albumController.createNewAlbum(albumModelPoJo);

		// assert
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	void testCreateSharedAlbumWith2FamiliesAndFriendsUserEmptyListNoRelationsWithFamiliesAndUsersFails() {
		// setup
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).date(date)
				.userId(20L).type(PE_PU).build();
		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of()).build();

		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.findAlbumByAlbumNameAndUserId(ALBUM_NAME, 20L)).thenReturn(null);
		when(albumService.createPublicToAllUserFamiliesAlbum(album, loggedUser)).thenReturn(null);
		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().userId(20L).date(date).userIds(List.of(1L))
				.familyIds(List.of(1L, 2L, 3L)).albumName(ALBUM_NAME).description(ALBUM_DESCRIPTION).type(PE_CU)
				.build();
		// run test
		ResponseEntity<Object> response = (ResponseEntity<Object>) albumController.createNewAlbum(albumModelPoJo);

		// assert
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	void testDeleteAlbumSuccess() {
		// setup
		boolean expected = true;
		Long id = deleteAlbum(expected);
		when(messages.get(AppConstants.ALBUM_DELETED)).thenReturn("Album deleted successfully");
		// run test
		MessageResponse result = (MessageResponse) albumController.deleteAlbumById(id).getBody();
		// verify result
		assertTrue(result.getMessage().contains("Album deleted successfully"));
	}

	@Test
	void testDeleteAlbumfailAlbumNotExist() {
		// setup
		boolean expected = false;
		Long id = deleteAlbum(expected);
		when(messages.get(AppConstants.ERROR_ALBUM_NOTFOUND)).thenReturn("Error: Album not found");
		// run test
		MessageResponse result = (MessageResponse) albumController.deleteAlbumById(id).getBody();

		// verify result
		assertTrue(result.getMessage().contains("Error: Album not found"));
	}

	@Test
	void testUpdatePrivateAlbumUserAlbumNotExist() {
		UpdateAlbumModelPoJo albumModelPoJo = UpdateAlbumModelPoJo.builder().albumName(ALBUM_NAME).date(new Date())
				.description("Desc").type("PE_PR").userIds(List.of()).familyIds(List.of()).build();
		// setup
		when(albumService.getByAlbumId(1L)).thenReturn(null);
		when(messages.get("ERROR_ALBUM_NOTFOUND")).thenReturn("Error:Album not found");
		// run test
		MessageResponse response = (MessageResponse) albumController.updateAlbum(1L, 1L, albumModelPoJo).getBody();

		// assert
		assertTrue(response.getMessage().toString().contains("Album not found"));
	}

	@Test
	void testupdateSetAlbumToPrivateAlbumUserSuccess() {
		// setup
		String albumName = "newAlbumName";
		String description = "new album description";
		String type = PE_PR;
		Date date = new Date();
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 1L, 3L);
		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();
		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName(albumName).description(description).date(date)
				.userId(20L).type(PE_PR).albumFamiliesList(Set.of(albumModelFamilies))
				.albumUserList(Set.of(albumModelUser3)).build();
		Date updateDate = new Date();
		AlbumModel saved = AlbumModel.builder().albumName(albumName).description(description).date(updateDate)
				.albumId(1L).userId(20L).type(type).build();

		UpdateAlbumModelPoJo albumModelPoJo = UpdateAlbumModelPoJo.builder().albumName(albumName).date(updateDate)
				.description(description).familyIds(List.of()).type(type).userIds(List.of()).build();
		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.getByAlbumId(1L)).thenReturn(oldAlbum);
		when(albumService.updateAlbumSetPrivateAlbum(oldAlbum, albumName, description, type, date, loggedUser))
				.thenReturn(saved);

		// run test
		ResponseEntity<Object> response = albumController.updateAlbum(1L, 20L, albumModelPoJo);

		// assert
		assertEquals(HttpStatus.OK, response.getStatusCode());

	}

	@Test
	void testupdateSetAlbumToPublicAlbumUserSuccess() {
		// setup
		String albumName = "newAlbumName";
		String description = "new album description";
		String type = PE_PU;
		Date date = new Date();
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 200L, 1L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 230L, 1L);

		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();
		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName(albumName).description(description).date(date)
				.userId(1L).type(PE_PR).build();
		Date updateDate = new Date();
		AlbumModel updated = AlbumModel.builder().albumName(albumName).description(description).date(updateDate)
				.albumId(1L).userId(20L).type(type).albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2))
				.build();
		UpdateAlbumModelPoJo albumModelPoJo = UpdateAlbumModelPoJo.builder().albumName(ALBUM_NAME).date(new Date())
				.description("Desc").type("PE_PR").userIds(List.of()).familyIds(List.of()).build();
		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.getByAlbumId(1L)).thenReturn(oldAlbum);
		doReturn(updated).when(albumService).updateAlbumSetPublicToAllUserFamiliesAlbum(updated, albumName, description,
				type, updateDate, loggedUser);
		albumModelPoJo.setType("PE_PU");
		// run test
		ResponseEntity<Object> response = albumController.updateAlbum(1L, 1L, albumModelPoJo);

		// assert
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void testupdateSetAlbumToCustomNoRelationSuccess() {
		// setup
		String albumName = "newAlbumName";
		String description = "new album description";
		String type = PE_PU;
		Date date = new Date();
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 200L, 1L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 230L, 1L);

		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();
		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName(albumName).description(description).date(date)
				.userId(1L).type(PE_PR).build();
		Date updateDate = new Date();
		AlbumModel updated = AlbumModel.builder().albumName(albumName).description(description).date(updateDate)
				.albumId(1L).userId(20L).type(type).albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2))
				.build();
		UpdateAlbumModelPoJo albumModelPoJo = UpdateAlbumModelPoJo.builder().albumName(ALBUM_NAME).date(new Date())
				.description("Desc").type("PE_PR").userIds(List.of()).familyIds(List.of()).build();
		when(userService.getById(20L)).thenReturn(loggedUser);
		when(albumService.getByAlbumId(1L)).thenReturn(oldAlbum);
		doReturn(null).when(albumService).updateAlbumSetPublicToAllUserFamiliesAlbum(updated, albumName, description,
				type, updateDate, loggedUser);
		albumModelPoJo.setType("PE_CU");
		// run test
		ResponseEntity<Object> response = albumController.updateAlbum(1L, 1L, albumModelPoJo);

		// assert
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	void testgetAlbumsIdByUserId() {
		Date date = new Date();
		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date)
				.userId(1L).type(PE_PU).albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		List<AlbumModel> albumModeList = List.of(oldAlbum);
		when(albumService.findByUserId(1L)).thenReturn(albumModeList);
		ResponseEntity<List<AlbumModel>> response = albumController.getAlbumsIdByUserId(1L);
		assertEquals(response.getBody(), albumModeList);
	}

	private CreateAlbumModelPoJo getAlbumModelPoJo() {
		Date date = new Date();
		List<Long> familyIds = List.of(1L, 2L);
		List<Long> userIds = List.of(1L, 2L);
		final Family family1 = new Family("fam1", FAMILY1_PHNO, LINE1, LINE2, "city", STATE, "zip", COUNTRY);
		final Family family2 = new Family("fam2", FAMILY2_PHNO, LINE12, LINE22, CITY2, STATE2, "zip2", COUNTRY2);
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);

		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date)
				.userId(1L).type(PE_CU).albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();
		return CreateAlbumModelPoJo.builder().albumName(ALBUM_NAME).date(date).description("Desc").familyIds(familyIds)
				.type("Type").userIds(userIds).build();
	}

	@Test
	void testSetAlbumCoberphotoNotUpdated() {
		AlbumCoverPojo albumCoverPojo = getAlbumCoverPojo();
		// setup
		when(albumService.updateAlbumCover(1L, 1L, 1L)).thenReturn(false);
		when(messages.get(AppConstants.ERROR_ALBUM_COVER_FAILED)).thenReturn("Error: Album cover not updated");
		// run test
		MessageResponse response = (MessageResponse) albumController.setAlbumCoberphoto(1L, albumCoverPojo).getBody();

		// assert
		assertTrue(response.getMessage().toString().contains("Error: Album cover not updated"));
	}

	@Test
	void testSetAlbumCoberphotoUpdatedSuccess() {
		AlbumCoverPojo albumCoverPojo = getAlbumCoverPojo();
		// setup
		when(albumService.updateAlbumCover(1L, 1L, 1L)).thenReturn(true);
		when(messages.get(AppConstants.ALBUM_COVER_UPDATED)).thenReturn("Album cover updated successfully");
		// run test
		MessageResponse response = (MessageResponse) albumController.setAlbumCoberphoto(1L, albumCoverPojo).getBody();

		// assert
		assertTrue(response.getMessage().toString().contains("Album cover updated successfully"));
	}

	private AlbumCoverPojo getAlbumCoverPojo() {
		return new AlbumCoverPojo(1L, 1L);
	}

	@Test
	void testGetAlbumByAlbumIdSuccess() {
		Long albumId = 1L;
		String userName = "LoggedInUserNameOwner";

		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(authentication.getName()).thenReturn(userName);
		User user = User.builder().id(2L).username("testUser")
				.roles(Set.of(Role.builder().name(ERole.ROLE_USER).build())).build();
		AlbumModel albumModel = AlbumModel.builder().userId(2L).albumId(1L).albumName("test album").build();
		UserDetailsImpl userDetails = UserDetailsImpl.build(user);
		when(securityContext.getAuthentication().getPrincipal()).thenReturn(userDetails);
		when(albumService.getByAlbumId(albumId)).thenReturn(albumModel);
		ResponseEntity<?> response = albumController.getAlbumByAlbumId(albumId);
		AlbumModel result = (AlbumModel) response.getBody();
		assertEquals(result, albumModel);
	}

	private Long deleteAlbum(boolean expected) {
		Long id = 1L;
		String userName = "LoggedInUserNameOwner";
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(authentication.getName()).thenReturn(userName);
		when(albumService.deleteByAlbumId(id, userName)).thenReturn(expected);
		return id;
	}

	@Test
	void testgetAlbumsByALbumNameAndUserId() {
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date).userId(1L)
				.type(PE_PU).albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		when(albumService.findAlbumByAlbumNameAndUserId("ALBUMNAME", 1L)).thenReturn(album);
		ResponseEntity<Object> response = albumController.getAlbumByAlbumName("ALBUMNAME", 1L);
		assertEquals(album, response.getBody());
	}

	@Test
	void testNullgetAlbumsByALbumNameAndUserId() {
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date).userId(1L)
				.type(PE_PU).albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		when(messages.get(AppConstants.ERROR_NO_RECORD_FOUND)).thenReturn(ERR_NO_RECORD_FOUND);
		when(albumService.findAlbumByAlbumNameAndUserId("ALBUMNAME", 1L)).thenReturn(null);
		MessageResponse response = (MessageResponse) albumController.getAlbumByAlbumName("ALBUMNAME", 1L).getBody();
		assertTrue(response.getMessage().toString().contains(ERR_NO_RECORD_FOUND));
	}

	@Test
	void testgetAlbumsContainingALbumNameAndUserId() {
		Date date = new Date();
		AlbumModel album = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date).userId(1L)
				.type(PE_PU).albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		List<AlbumModel> albumModeList = List.of(album);
		when(albumService.findAlbumIfContainsNameWithUserId("ALBUMNAME", 1L)).thenReturn(albumModeList);
		ResponseEntity<Object> response = albumController.getAlbumIfContainsName("ALBUMNAME", 1L);
		assertEquals(albumModeList, response.getBody());
	}

	@Test
	void testNullgetAlbumsContainingALbumNameAndUserId() {
		List<AlbumModel> albumList = new ArrayList<>();
		when(messages.get(AppConstants.ERROR_NO_RECORD_FOUND)).thenReturn(ERR_NO_RECORD_FOUND);
		when(albumService.findAlbumIfContainsNameWithUserId("ALBUMNAME", 1L)).thenReturn(albumList);
		MessageResponse response = (MessageResponse) albumController.getAlbumIfContainsName("ALBUMNAME", 1L).getBody();
		assertTrue(response.getMessage().toString().contains(ERR_NO_RECORD_FOUND));
	}
}
