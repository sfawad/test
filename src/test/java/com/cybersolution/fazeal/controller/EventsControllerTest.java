package com.cybersolution.fazeal.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.cybersolution.fazeal.pojo.CreateEventPojo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.models.Events;
import com.cybersolution.fazeal.models.EventsCurrentUserResponse;
import com.cybersolution.fazeal.models.EventsRelationResponse;
import com.cybersolution.fazeal.repository.EventsFamilyUserRespository;
import com.cybersolution.fazeal.repository.EventsRespository;
import com.cybersolution.fazeal.service.EventService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Messages;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EventsControllerTest {

	@InjectMocks
	private EventsController eventController;

	@Mock
	EventsRespository eventRepository;

	@Mock
	UserService usersService;

	@Mock
	FamilyService familyService;

	@Mock
	EventsFamilyUserRespository eventFamUserRepository;

	@Mock
	EventService eventService;

	@Mock
	private Messages messages;

	public static final String EVENT_NAME = "Test Event";
	public static final String DATE = "1965-10-26T00:00:00Z";
	public static final String EVENT_UPDATE_NAME = "TEST Event Update";

	@Test
	public void getByEventIdTestcase() {
		Events event = getEventsInfo();
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);
		ResponseEntity result = eventController.getEvent(Long.valueOf(1), Long.valueOf(1));
		Events response = (Events) result.getBody();
		assertEquals(response, event);

	}

	private Events getEventsInfo() {
		Events event = new Events(Long.valueOf(1), EVENT_NAME, 1L, true);
		return event;
	}

	@Test
	public void getByUserIdNullTestcase() {
		Events event = new Events();
		Long userId = event.getUserId();
		when(messages.get("ERROR_INVALID_REQUEST")).thenReturn(" Error: Invalid request");
		EventsCurrentUserResponse eventsCurrentUserResponse = new EventsCurrentUserResponse();
		when(eventService.getUserEvents(userId)).thenReturn(eventsCurrentUserResponse);
		ResponseEntity result = eventController.getUserEvents(userId);
		assertTrue(result.getBody().toString().contains(" Error: Invalid request"));

	}

	@Test
	public void getUserEventsPlainResponseByUserIdTestcase() {
		EventsRelationResponse event = EventsRelationResponse.builder().edate(DATE).event(EVENT_NAME).eend(DATE)
				.estart(DATE).uid(1L).eid(1L).build();
		List<EventsRelationResponse> events = new ArrayList<>();
		events.add(event);
		when(eventService.getUserEventsPlainResponse(1L)).thenReturn(events);
		ResponseEntity result = eventController.getUserEventsPlainResponse(Long.valueOf(1L));
		Object response = result.getBody();
		assertEquals(events, response);

	}

	@Test
	public void getUserWithFamillyEventsTestcase() {
		Events event = new Events(null, EVENT_NAME, 1L, true);
		List<Events> events = new ArrayList<>();
		events.add(event);
		when(eventService.getUserWithFamilyEvents(1L)).thenReturn(events);
		ResponseEntity result = eventController.getUserWithFamilyEvents(Long.valueOf(1L));
		Object response = result.getBody();
		assertEquals(events, response);

	}

	@Test
	public void saveEventTestCase() {
		CreateEventPojo createEventPojo = CreateEventPojo.builder().eventName(EVENT_NAME).date(DATE).startTime(DATE).endTime(DATE).isShared(true).build();
		Events event = new Events(null, EVENT_NAME, 1L, true);
		event.setDate(DATE);
		event.setEventEnd(DATE);
		event.setEventStart(DATE);
		when(eventService.saveEvent(1L,createEventPojo)).thenReturn(event);
		ResponseEntity result = eventController.saveUserEvent(Long.valueOf(1L), createEventPojo);

		Events response = (Events) result.getBody();
		assertEquals(event, response);
	}

	@Test
	public void updateEventTestCase() {

		Events event = getEventsInfo();
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);

		ResponseEntity<?> result = eventController.updateUserEvent(Long.valueOf(1), EVENT_UPDATE_NAME, DATE, DATE, DATE,
				1L, new Long[] { Long.valueOf(1) }, Boolean.FALSE);
		eventService.deleteEventsFamilyUser(Long.valueOf(1));
		eventService.saveEventsFamilyUser(event, 1L, new Long[] { Long.valueOf(1) });
		Object response = result.getBody();
		event.setShared(false);
		event.setEvent(EVENT_UPDATE_NAME);
		assertEquals(event, response);
	}

	@Test
	public void updateEventWithFamilyIdNullTestCase() {

		Events event = getEventsInfo();
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);
		when(messages.get("ERROR_INVALID_FAMILIES")).thenReturn("Error: Invalid Families");
		ResponseEntity<?> result = eventController.updateUserEvent(Long.valueOf(1), EVENT_UPDATE_NAME, DATE, DATE, DATE,
				1L, null, Boolean.TRUE);
		assertTrue(result.getBody().toString().contains("Error: Invalid Families"));
	}

	@Test
	public void updateEventFailTestCase() {

		Events event = getEventsInfo();
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);

		when(messages.get("ERROR_INVALID_EVENT")).thenReturn("Error: Not a valid event !");
		ResponseEntity<?> result = eventController.updateUserEvent(Long.valueOf(1), EVENT_UPDATE_NAME, DATE, DATE, DATE,
				2L, new Long[] { Long.valueOf(1) }, Boolean.FALSE);

		assertFalse(result.getBody().toString().contains("Error: Not a valid event !"));

	}

	@Test
	public void updateEventNullTestCase() {

		Events event = getEventsInfo();
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);

		when(messages.get("ERROR_INVALID_EVENT")).thenReturn("Error: Invalid Edit");
		ResponseEntity<?> result = eventController.updateUserEvent(null, EVENT_UPDATE_NAME, DATE, DATE, DATE, 2L,
				new Long[] { Long.valueOf(1) }, Boolean.FALSE);
		assertTrue(result.getBody().toString().contains("Error: Invalid Edit"));
	}

	@Test
	public void deleteEventTrueTestCase() {
		Events event = new Events(1L, EVENT_NAME, 1L, true);
		event.setDate(DATE);
		event.setEventEnd(DATE);
		event.setEventStart(DATE);
		boolean actual = true;
		when(eventService.findByEventId(event.getId())).thenReturn(event);
		when(eventService.deleteEvent(Long.valueOf(1))).thenReturn(actual);
		when(messages.get("EVENT_DELETED")).thenReturn("Event deleted Successfully");

		ResponseEntity<?> result = eventController.deleteEvent(Long.valueOf(1), Long.valueOf(1));

		assertTrue(result.getBody().toString().contains("Event deleted Successfully"));

	}

	@Test
	public void deleteEventFalseTestCase() {
		Events event = new Events(1L, EVENT_NAME, 1L, true);
		event.setDate(DATE);
		event.setEventEnd(DATE);
		event.setEventStart(DATE);
		when(eventService.findByEventId(event.getId())).thenReturn(event);
		boolean actual = false;
		when(eventService.deleteEvent(Long.valueOf(1))).thenReturn(actual);
		when(messages.get("ERROR_INVALID_REQUEST")).thenReturn("Error: Invalid Request");
		ResponseEntity<?> result = eventController.deleteEvent(Long.valueOf(1), Long.valueOf(1));
		assertTrue(result.getBody().toString().contains("Error: Invalid Request"));
	}



	@Test
	public void deleteEventWithemptyEventsTestCase() {
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(null);
		when(messages.get("ERROR_EVENT_NOT_FOUND")).thenReturn("Error: Event not found");
		ResponseEntity<?> result = eventController.deleteEvent(Long.valueOf(1), Long.valueOf(1));
		assertTrue(result.getBody().toString().contains("Error: Event not found"));

	}

	@Test
	public void deleteEventDifferentUserIdTestCase() {
		Events event = new Events(2L, EVENT_NAME, 4L, true);
		event.setDate(DATE);
		event.setEventEnd(DATE);
		event.setEventStart(DATE);

		when(eventService.findByEventId(Long.valueOf(2))).thenReturn(event);

		when(messages.get("ERROR_EVENT_USER_ASSOCIATION")).thenReturn(
				"Error: the Event id passed in request does not have relation with the user. Please check again");
		ResponseEntity<?> result = eventController.deleteEvent(Long.valueOf(2), Long.valueOf(3));
		assertTrue(result.getBody().toString().contains(
				"Error: the Event id passed in request does not have relation with the user. Please check again"));

	}

	@Test
	public void getByEventIdNullEventsTestcase() {
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(null);
		when(messages.get("ERROR_EVENT_NOT_FOUND")).thenReturn("Error: Event not found");

		ResponseEntity result = eventController.getEvent(Long.valueOf(1), Long.valueOf(1));
		assertTrue(result.getBody().toString().contains("Error: Event not found"));

	}

	@Test
	public void getByEventIddifferentuserIdTestcase() {
		Events event = new Events(2L, EVENT_NAME, 4L, true);
		event.setDate(DATE);
		event.setEventEnd(DATE);
		event.setEventStart(DATE);
		when(eventService.findByEventId(Long.valueOf(1))).thenReturn(event);
		when(messages.get("ERROR_INVALID_REQUEST")).thenReturn("Error: Invalid Request");

		ResponseEntity result = eventController.getEvent(Long.valueOf(1), Long.valueOf(1));
		assertTrue(result.getBody().toString().contains("Error: Invalid Request"));

	}
}
