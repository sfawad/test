package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.pojo.CreateAlbumModelPoJo;
import com.cybersolution.fazeal.pojo.UpdateAlbumModelPoJo;
import com.cybersolution.fazeal.repository.AlbumFamiliesRepository;
import com.cybersolution.fazeal.repository.AlbumRepository;
import com.cybersolution.fazeal.repository.AlbumUsersRepository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageService;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AlbumServiceImplTest {

	@InjectMocks
	private AlbumServiceImpl albumService;

	@Mock
	private AlbumRepository albumRepository;
	@Mock
	private AlbumFamiliesRepository albumFamiliesRepository;
	@Mock
	private AlbumUsersRepository albumUsersRepository;
	@Mock
	private UserRepository userRepo;
	@Mock
	private ImageRepository imageRepository;
	@Mock
	ImageService imageService;
	@Mock
	private FamilyService familyService;

	@Test
	void testCreatePrivateAlbum() {
		// setup
		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).userId(20L)
				.type("PE_PR").build();
		AlbumModel saved = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).albumId(1L)
				.userId(20L).type("PE_PR").build();
		when(albumRepository.save(album)).thenReturn(saved);

		// call service
		AlbumModel result = albumService.createPrivateAlbum(album);

		// assert result
		verify(albumRepository, atLeastOnce()).save(any());
		assertNull(result.getAlbumFamiliesList());
		assertNull(result.getAlbumUserList());
	}

	@Test
	void testCreatePublicAlbum() {
		// setup
		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 200L, 1L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 230L, 1L);
		AlbumModelFamilies albumModelFamiliesSaved = new AlbumModelFamilies(1L, 1L, 200L);
		AlbumModelFamilies albumModelFamilies2Saved = new AlbumModelFamilies(2L, 1L, 230L);

		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).albumId(1L)
				.userId(20L).type("PE_PU").build();
		AlbumModel saved = AlbumModel.builder().albumId(1L).albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2))
				.type("PE_PU").build();

		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();

		when(albumFamiliesRepository.save(albumModelFamilies)).thenReturn(albumModelFamiliesSaved);
		when(albumFamiliesRepository.save(albumModelFamilies2)).thenReturn(albumModelFamilies2Saved);

		when(albumRepository.save(album)).thenReturn(saved);
		// call service
		AlbumModel result = albumService.createPublicToAllUserFamiliesAlbum(album, loggedUser);

		// assert result
		verify(albumFamiliesRepository, times(Integer.valueOf(2))).save(any());
		assert (result.getAlbumFamiliesList().size() == 2);
	}

	@Test
	void testCreateSharedAlbumWithOnlyFamiliesWithoutUsers() {
		// setup
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 1234567891L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 1L, 2234567891L);
		List<Long> familyIds = List.of(1234567891L, 2234567891L);
		List<Long> userIds = List.of();
		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).albumId(1L)
				.userId(20L).type("PE_CU").build();
		AlbumModel savedAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU")
				.albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2)).build();

		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family2.setFamilyId(2234567891L);
		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		when(albumRepository.save(album)).thenReturn(savedAlbum);
		// call service
		AlbumModel result = albumService.createSharedToFamiliesAndUsersAlbum(familyIds, userIds, loggedUser, album);

		// assert result
		verify(albumFamiliesRepository, times(Integer.valueOf(2))).save(any());
		assert (result.getAlbumFamiliesList().size() == 2);
	}

	@Test
	void testCreateSharedAlbumWithOnlyFamiliesWithoutUsersNoRelationWithFamilies() {
		// setup
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 1234567891L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 1L, 2234567891L);
		List<Long> familyIds = List.of(1234567891L, 2234567891L);
		List<Long> userIds = List.of();
		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).albumId(1L)
				.userId(20L).type("PE_CU").build();
		AlbumModel savedAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU")
				.albumFamiliesList(Set.of(albumModelFamilies, albumModelFamilies2)).build();

		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family2.setFamilyId(2234567891L);
		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of()).build();

		when(albumRepository.save(album)).thenReturn(savedAlbum);
		// call service
		AlbumModel result = albumService.createSharedToFamiliesAndUsersAlbum(familyIds, userIds, loggedUser, album);

		// assert result
		assertNull(result);
	}

	@Test
	void testCreateSharedAlbumWithNoFamiliesWith3Users() {
		// setup
		AlbumModelUser albumModelUser1 = new AlbumModelUser(null, 11L, 1L);
		AlbumModelUser albumModelUser2 = new AlbumModelUser(null, 11L, 2L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 11L, 3L);

		List<Long> familyIds = List.of();
		List<Long> userIds = List.of(1L, 2L, 3L);
		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family2.setFamilyId(2234567891L);
		final User user1family1 = User.builder().id(1L).firstName("user1family1").build();
		final User user2family1 = User.builder().id(2L).firstName("user2family1").build();
		user1family1.setFamilies(Set.of(family1, family2));
		user2family1.setFamilies(Set.of(family1, family2));

		final User user1family2 = User.builder().id(3L).firstName("user3family1").build();
		user1family2.setFamilies(Set.of(family2));
		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();
		loggedUser.setFamilies(Set.of(family1, family2));
		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).userId(20L)
				.type("PE_CU").build();
		AlbumModel savedAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(11L).userId(20L).type("PE_CU")
				.albumUserList(Set.of(albumModelUser1, albumModelUser2, albumModelUser3)).build();

		when(albumRepository.save(album)).thenReturn(savedAlbum);
		when(userRepo.getUsersListForAFamily(1234567891L, 1L)).thenReturn(List.of(user1family1, user2family1));
		when(userRepo.getUsersListForAFamily(2234567891L, 1L))
				.thenReturn(List.of(user1family2, user1family1, user2family1));

		// call service
		AlbumModel result = albumService.createSharedToFamiliesAndUsersAlbum(familyIds, userIds, loggedUser, album);

		// assert result
		verify(albumUsersRepository, times(Integer.valueOf(3))).save(any());
		assert (result.getAlbumUserList().size() == 3);
	}

	@Test
	void testCreateSharedAlbumWith2FamiliesWith2Users() {

		// setup
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1234567891L, 1L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 1L, 3L);

		List<Long> familyIds = List.of(1234567891L);
		List<Long> userIds = List.of(3L);
		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family2.setFamilyId(2234567891L);
		final User user1family1 = User.builder().id(1L).firstName("user1family1").families(Set.of(family1, family2))
				.build();
		final User user2family1 = User.builder().id(2L).firstName("user2family1").families(Set.of(family1, family2))
				.build();
		final User user3family1 = User.builder().id(3L).firstName("user3family1").families(Set.of(family1)).build();
		final User user4family1 = User.builder().id(4L).firstName("user4family1").families(Set.of(family1)).build();

		final User user1family2 = User.builder().id(3L).firstName("user3family2").families(Set.of(family2)).build();
		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1, family2)).build();
		AlbumModel album = AlbumModel.builder().albumName("album1").description("album1").date(new Date()).userId(20L)
				.type("PE_CU").build();
		AlbumModel savedAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU").albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies)).build();

		when(albumRepository.save(album)).thenReturn(savedAlbum);
		when(userRepo.getUsersListForAFamily(2234567891L, 1L))
				.thenReturn(List.of(user1family2, user1family1, user2family1, user3family1, user4family1));
		// call service
		AlbumModel result = albumService.createSharedToFamiliesAndUsersAlbum(familyIds, userIds, loggedUser, album);

		// assert result
		verify(albumFamiliesRepository, times(Integer.valueOf(1))).save(any());
		verify(albumUsersRepository, times(Integer.valueOf(1))).save(any());
		assert (result.getAlbumFamiliesList().size() == 1);
		assert (result.getAlbumUserList().size() == 1);
	}

	@Test
	void testDeleteAlbumFail() {
		// setup
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 11L, 3L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModel albumToBeDeleted = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU").albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies)).build();
		final User owner = User.builder().id(20L).firstName("ownerOfTheAlbum").username("owner").build();
		String loggedInUser = "owner";
		Long id = 1L;

		when(userRepo.findByUsername(loggedInUser)).thenReturn(Optional.of(owner));
		when(albumRepository.getById(id)).thenReturn(albumToBeDeleted);

		// run test
		boolean result = albumService.deleteByAlbumId(id, loggedInUser);

		// verify result
		assertFalse(result);
	}

	@Test
	void testDeleteAlbumSuccess() {
		// setup
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 11L, 3L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModel albumToBeDeleted = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU").albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies)).build();
		final User owner = User.builder().id(20L).firstName("ownerOfTheAlbum").username("owner").build();
		String loggedInUser = "owner";
		Long id = 1L;

		when(userRepo.findByUsername(loggedInUser)).thenReturn(Optional.of(owner));
		when(albumRepository.getByAlbumId(id)).thenReturn(albumToBeDeleted);

		// run test
		boolean result = albumService.deleteByAlbumId(id, loggedInUser);

		// verify result
		assertTrue(result);
	}

	@Test
	void testDeleteAlbumNoAlbum() {
		// setup
		Long id = 1L;
		when(albumRepository.getById(id)).thenReturn(null);

		// run test
		boolean result = albumService.deleteByAlbumId(id, "");

		// verify result
		assertFalse(result);
	}

	@Test
	void testDeleteAlbumUserNotOwnerFail() {
		// setup
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 11L, 3L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1L, 200L);
		AlbumModel albumToBeDeleted = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU").albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies)).build();
		final User notOwner = User.builder().id(50L).firstName("notOwnerOfTheAlbum").username("owner").build();
		String loggedInUser = "owner";
		Long id = 1L;

		when(userRepo.findByUsername(loggedInUser)).thenReturn(Optional.of(notOwner));
		when(albumRepository.getById(id)).thenReturn(albumToBeDeleted);

		// run test
		boolean result = albumService.deleteByAlbumId(id, loggedInUser);

		// verify result
		assertFalse(result);
	}

	@Test
	void testUpdateAlbumSetPrivate() {
		// setup
		AlbumModelUser albumModelUser3 = new AlbumModelUser(1L, 1L, 3L);
		String albumName = "newAlbumName";
		String description = "new album description";
		String type = "PE_PR";
		Date date = new Date();
		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName("album1").description("album1")
				.date(new Date()).albumUserList(Set.of(albumModelUser3)).albumFamiliesList(new HashSet<>())
				.albumUserList(new HashSet<>()).userId(20L).type("PE_CU").build();
		AlbumModel updatedAlbum = AlbumModel.builder().albumId(1L).albumName(albumName).description(description)
				.date(date).userId(20L).type("PE_PR").albumFamiliesList(new HashSet<>()).albumUserList(new HashSet<>())
				.build();

		when(albumRepository.save(oldAlbum)).thenReturn(updatedAlbum);

		// run test
		AlbumModel result = albumService.updateAlbumSetPrivateAlbum(oldAlbum, albumName, description, type, date, null);

		// verify
		assertEquals(type, result.getType());
		assertEquals(0, result.getAlbumFamiliesList().size());
		assertEquals(0, result.getAlbumUserList().size());
	}

	@Test
	void testUpdateAlbumSetPublic() {
		// setup
		AlbumModelUser albumModelUser3 = new AlbumModelUser(1L, 1L, 3L);

		String albumName = "newAlbumName";
		String description = "new album description";
		String type = "PE_PU";
		Date date = new Date();

		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);
		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 200L, 1L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(null, 230L, 1L);
		AlbumModelFamilies albumModelFamiliesSaved = new AlbumModelFamilies(1L, 200L, 1L);
		AlbumModelFamilies albumModelFamilies2Saved = new AlbumModelFamilies(2L, 230L, 1L);

		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName("album1").description("album1")
				.date(new Date()).albumUserList(null).albumFamiliesList(new HashSet<>()).userId(20L).type("PE_CU")
				.build();

		Set<AlbumModelUser> users = new HashSet<>();
		users.add(albumModelUser3);
		oldAlbum.setAlbumUserList(users);
		Set<AlbumModelFamilies> families = new HashSet<>();
		families.add(albumModelFamiliesSaved);
		families.add(albumModelFamilies2Saved);
		AlbumModel savedAlbum = AlbumModel.builder().albumId(1L).albumName(albumName).description(description)
				.date(date).albumId(1L).albumFamiliesList(families).userId(20L).type(type).build();

		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		when(albumRepository.save(oldAlbum)).thenReturn(savedAlbum);
		when(albumFamiliesRepository.save(albumModelFamilies)).thenReturn(albumModelFamilies);
		when(albumFamiliesRepository.save(albumModelFamilies2)).thenReturn(albumModelFamilies2);

		// call service
		AlbumModel result = albumService.updateAlbumSetPublicToAllUserFamiliesAlbum(oldAlbum, albumName, description,
				type, date, loggedUser);

		// assert result
		verify(albumFamiliesRepository, times(Integer.valueOf(2))).save(any());
		assertEquals(2, result.getAlbumFamiliesList().size());
		assertEquals(type, result.getType());
	}

	@Test
	void testUpdateAlbumSetCustomShared() {
		// setup
		String albumName = "newAlbumName";
		String description = "new album description";
		String type = "PE_PR";
		Date date = new Date();

		AlbumModelFamilies albumModelFamilies = new AlbumModelFamilies(null, 1234567891L, 1L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(null, 1L, 3L);

		List<Long> familyIds = List.of(1234567891L);
		List<Long> userIds = List.of(3L);
		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family2.setFamilyId(2234567891L);
		final User user1family1 = User.builder().id(1L).firstName("user1family1").families(Set.of(family1, family2))
				.build();
		final User user2family1 = User.builder().id(2L).firstName("user2family1").families(Set.of(family1, family2))
				.build();
		final User user3family1 = User.builder().id(3L).firstName("user3family1").families(Set.of(family1)).build();
		final User user4family1 = User.builder().id(4L).firstName("user4family1").families(Set.of(family1)).build();

		final User user1family2 = User.builder().id(3L).firstName("user3family2").families(Set.of(family2)).build();

		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();

		AlbumModel oldAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.userId(20L).type("PE_PR").albumUserList(new HashSet<AlbumModelUser>())
				.albumFamiliesList(new HashSet<AlbumModelFamilies>()).build();
		AlbumModel savedAlbum = AlbumModel.builder().albumName("album1").description("album1").date(new Date())
				.albumId(1L).userId(20L).type("PE_CU").albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies)).build();
		UpdateAlbumModelPoJo albumModelPoJo = UpdateAlbumModelPoJo.builder().albumName(albumName).date(date)
				.description(description).familyIds(familyIds).type(type).userIds(userIds).build();
		when(albumRepository.save(oldAlbum)).thenReturn(savedAlbum);
		when(userRepo.getUsersListForAFamily(2234567891L, 20L))
				.thenReturn(List.of(user1family2, user1family1, user2family1, user3family1, user4family1));

		// call service
		AlbumModel result = albumService.updateAlbumSetSharedToSelectedFamiliesAndUsers(loggedUser, oldAlbum,
				albumModelPoJo);

		// assert result
		verify(albumFamiliesRepository, times(Integer.valueOf(1))).save(any());
		verify(albumUsersRepository, times(Integer.valueOf(1))).save(any());
		assertEquals(1, result.getAlbumFamiliesList().size());
		assertEquals(1, result.getAlbumUserList().size());
	}

	@Test
	void testFindAlbumByUserIdAndAlbumName() {
		// setup
		String albumName = "albumName";
		String description = "new album description";
		String type = "PE_PR";
		Date date = new Date();
		AlbumModel saved = AlbumModel.builder().albumName(albumName).description(description).date(null).albumId(1L)
				.userId(20L).type(type).build();

		when(albumRepository.findByAlbumNameAndUserId("albumName", 20L)).thenReturn(saved);
		// run test

		AlbumModel result = albumService.findAlbumByAlbumNameAndUserId("albumName", 20L);
		// assert
		assertEquals(saved, result);
	}

	private CreateAlbumModelPoJo getAlbumModelPoJo() {
		Date date = new Date();
		List<Long> familyIds = List.of(1L, 2L);
		List<Long> userIds = List.of(1L, 2L);
		final Family family1 = new Family("fam1", "1234567891L", "line1", "line2", "city", "state", "zip", "country");
		final Family family2 = new Family("fam2", "2234567891L", "line12", "line22", "city2", "state2", "zip2",
				"country2");
		family1.setFamilyId(200L);
		family2.setFamilyId(230L);

		AlbumModel oldAlbum = AlbumModel.builder().albumId(1L).albumName("album").description("desc").date(date)
				.userId(1L).type("PE_CU").albumFamiliesList(Set.of()).albumUserList(Set.of()).build();
		User loggedUser = User.builder().id(20L).firstName("Test").families(Set.of(family1, family2)).build();
		CreateAlbumModelPoJo albumModelPoJo = CreateAlbumModelPoJo.builder().albumName("AlbumName").date(date)
				.description("Desc").familyIds(familyIds).type("Type").userIds(userIds).build();
		return albumModelPoJo;
	}

	@Test
	void testUpdateAlbumCoverFalse() {
		Long albumId = 1L;
		Long userId = 1L;
		Long imageId = 1L;
		when(albumRepository.getById(albumId)).thenReturn(null);
		when(imageRepository.getById(imageId)).thenReturn(null);
		assertFalse(albumService.updateAlbumCover(albumId, userId, imageId));
	}

	@Test
	void testUpdateAlbumCoverTrue() {
		Long albumId = 1L;
		Long userId = 1L;
		Long imageId = 1L;
		ImageModel imageModel = ImageModel.builder().albumId(1L).build();
		AlbumModel albumModel = AlbumModel.builder().userId(userId).albumId(albumId).albumImages(Set.of(imageModel))
				.build();
		when(albumRepository.getById(albumId)).thenReturn(albumModel);
		when(imageRepository.getById(imageId)).thenReturn(imageModel);
		assertTrue(albumService.updateAlbumCover(albumId, userId, imageId));
	}

	@Test
	void testFindByUserId() {
		when(albumRepository.findByUserId(1L)).thenReturn(null);
		List<AlbumModel> albumList = albumService.findByUserId(1L);
		assertEquals(null, albumList);
	}

	@Test
	void testCreatePrivateAlbumException() {
		AlbumModel albumMod = getNullAlbumModel();
		when(albumRepository.save(null)).thenThrow(new RuntimeException());
		AlbumModel album = albumService.createPrivateAlbum(albumMod);
		assertEquals(null, album);
	}

	AlbumModel getNullAlbumModel() {
		return new AlbumModel(1L, null, null, null, null, null, null, null, null, null, null);
	}

	@Test
	void testGetAlbumById() {
		when(albumRepository.getById(1L)).thenReturn(getNullAlbumModel());
		AlbumModel albumReturned = albumService.getByAlbumId(1L);
		assertEquals(getNullAlbumModel(), albumReturned);
	}

	@Test
	void testFindAlbumIfContainsNameWithUserId() {
		when(albumRepository.findAlbumIfContainsNameWithUserId("", null)).thenReturn(null);
		List<AlbumModel> albumList = albumService.findAlbumIfContainsNameWithUserId("", null);
		assertEquals(null, albumList);
	}
}