package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.cybersolution.fazeal.repository.ImageUsersRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ImageModelUserServiceImplTest {
	
	@InjectMocks
	ImageModelUserServiceImpl imageModelUserServiceImpl;
	
	@Mock
	ImageUsersRepository imageUsersRepository;
	
	 @Test
	 void testDelete() {
		 Integer expected = 1;
	    when(imageUsersRepository.deleteImg(1L)).thenReturn(expected);
	    Integer returnInt = imageModelUserServiceImpl.delete(1L);
	    assertEquals(expected, returnInt);
	  }

	  @Test
	  void testGetUserImagesIdsByUserId() {
		Long[] returnList = new Long[] {1L,2L}; 
	    when(imageUsersRepository.getImageByUseryId(1L)).thenReturn(returnList);
	    Long[] actualRes = imageUsersRepository.getImageByUseryId(1L);
	    assertEquals(returnList, actualRes);
	  }
	  
	  @Test
	  void testGetUserImagesIdsByUserIdNull() {
		Long[] returnList = null; 
	    when(imageUsersRepository.getImageByUseryId(1L)).thenReturn(null);
	    Long[] actualRes = imageUsersRepository.getImageByUseryId(1L);
	    assertEquals(returnList, actualRes);
	  }

}
