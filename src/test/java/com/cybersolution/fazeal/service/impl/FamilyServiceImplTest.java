package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;

import com.cybersolution.fazeal.pojo.Notification;
import com.cybersolution.fazeal.service.KafkaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.util.ReflectionTestUtils;

import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyResponse;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.FamilyCreatePoJo;
import com.cybersolution.fazeal.pojo.FamilySaveInfoPoJo;
import com.cybersolution.fazeal.pojo.FamilyUpdatePoJo;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EmailService;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class FamilyServiceImplTest {

	@InjectMocks
	private FamilyServiceImpl familyService;

	@Mock
	private FamilyRespository familyRespository;
	@Mock
	private ImageRepository imageRepo;
	@Mock
	private FamilyUserRoleRespository familyUsersRoleRepo;
	@Mock
	private UserRepository userRepository;
	@Mock
	private RoleRepository roleRepo;
	@Mock
	private Messages messages;
	@Mock
	private EmailService emailService;
	@Mock
	private KafkaService kafkaService;

	@Test
	void testGetFamilyByIdSuccess() {
		Family expected = Family.builder().familyId(1L).name("testFamily").about("about").build();
		Long id = 1L;
		when(familyRespository.getFamilyById(id)).thenReturn(expected);
		Family result = familyService.getFamilyById(id);
		assertEquals(expected, result);
	}

	@Test
	void testGetFamilyInfoFromTokenSuccess() {
		Family expected = Family.builder().familyId(1L).name("testFamily").about("about").build();
		String token = "someToken";
		when(familyRespository.getFamilyInfoFromToken(token)).thenReturn(expected);
		Family result = familyService.getFamilyInfoFromToken(token);
		assertEquals(result, expected);
	}

	@Test
	void testSaveFamilySuccess() {
		Family expected = Family.builder().familyId(1L).name("testFamily").about("about").build();
		when(familyRespository.save(expected)).thenReturn(expected);
		Family result = familyService.save(expected);
		assertEquals(result, expected);
	}

	@Test
	void testUpdateFamilyTestCaseTrue() throws Exception {
		Family family = createFamily();
		FamilyUpdatePoJo familyUpdatePoJo = getFamilyUpdateInfo();
		FamilyUserRoles famUserRole = new FamilyUserRoles(1L, 1L, "ROLE_ADMIN", "Father");
		when(familyUsersRoleRepo.findRoleForUserInFamily(30L, 1L)).thenReturn(famUserRole);
		boolean result = familyService.updateFamilyInfo(family.getFamilyId(), familyUpdatePoJo, family, 1L);
		family.setName("INFO Update");
		assertTrue(result);
	}

	@Test
	void testUpdateFamilyTestCaseFalse() throws Exception {
		Family family = createFamily();
		FamilyUpdatePoJo familyUpdatePoJo = FamilyUpdatePoJo.builder().name("new Name").phone("1234567890")
				.address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600").country("Country")
				.relationWithFamily(null).about("").build();
		FamilyUserRoles famUserRole = new FamilyUserRoles(1L, 1L, "ROLE_ADMIN", "Father");
		when(familyUsersRoleRepo.findRoleForUserInFamily(30L, 1L)).thenReturn(famUserRole);
		boolean result = familyService.updateFamilyInfo(family.getFamilyId(), familyUpdatePoJo, family, 1L);
		family.setName("INFO Update");
		assertFalse(result);
	}

	@Test
	void testIsUserAgeValidToCreateFamilyTrue() {
		ReflectionTestUtils.setField(familyService, "minAge", 16);
		User user = createUser();
		boolean result = familyService.isUserAgeValidToCreateFamily(user);
		assertTrue(result);
	}

	@Test
	void testIsUserAgeValidToCreateFamilyFalse() {
		ReflectionTestUtils.setField(familyService, "minAge", 16);
		User user = User.builder().id(1L).enabled(true).firstName("test").dob("2015-01-04T05:00:00.000Z").build();
		boolean result = familyService.isUserAgeValidToCreateFamily(user);
		assertFalse(result);
	}

	@Test
	void testIsUserRequestToJoinFamilyAddedTrue() {
		Family family = createFamily();
		User user = User.builder().id(1L).enabled(true).firstName("test").dob("2015-01-04T05:00:00.000Z").build();
		user.setEmail("email");
		when(familyRespository.save(family)).thenReturn(family);
		boolean result = familyService.isUserRequestToJoinFamilyAdded(user, family);
		Notification notification = Notification.builder().emailId(user.getEmail()).mobileNumber("")
				.notificationType("EMAIL").messageType("JOIN_FAMILY_USER").build();
		doNothing().when(kafkaService).publishToTopic(notification);
		assertTrue(result);

	}

	@Test
	void testIsUserRequestToJoinFamilyAddedFalse() {
		Family family = createFamily();
		User user = createUser();
		when(familyRespository.save(family)).thenReturn(null);
		boolean result = familyService.isUserRequestToJoinFamilyAdded(user, family);
		assertFalse(result);
	}

	@Test
	void testIsUserAdminforFamilyFalse() {
		when(familyUsersRoleRepo.findRoleForUserInFamily(1L, 1L)).thenReturn(null);
		boolean result = familyService.isUserAdminforFamily(1L, 1L);
		assertFalse(result);
	}

	@Test
	void testIsUserAdminforFamilyTrue() {
		FamilyUserRoles familyUserRoles = FamilyUserRoles.builder().role("ROLE_ADMIN").build();
		when(familyUsersRoleRepo.findRoleForUserInFamily(1L, 1L)).thenReturn(familyUserRoles);
		boolean result = familyService.isUserAdminforFamily(1L, 1L);
		assertTrue(result);
	}

	@Test
	void testFindAllUserRolesInFamily() {
		List<FamilyUserRoles> familyUserRoles = List
				.of(FamilyUserRoles.builder().id(1L).familyId(20L).userId(39L).build());
		when(familyUsersRoleRepo.findAllUserRolesInFamily(1L)).thenReturn(familyUserRoles);
		List<FamilyUserRoles> result = familyService.getAllUserRolesInFamily(1L);
		assertTrue(result.size() > 0);
	}

	@Test
	void testFindRoleForUserInFamily() {
		FamilyUserRoles familyUserRole = FamilyUserRoles.builder().id(1L).familyId(20L).userId(39L).build();
		when(familyUsersRoleRepo.findRoleForUserInFamily(20L, 1L)).thenReturn(familyUserRole);
		FamilyUserRoles result = familyService.getRoleForUserInFamily(20L, 1L);
		assertNotNull(result);
	}

	@Test
	void testdisassociatUserFromFamilyNoUserFound() {
		String token = "famToken";
		Family family = createFamily();
		Long userIdToBeRemoved = 30L;
		when(userRepository.getById(userIdToBeRemoved)).thenReturn(null);
		boolean result = familyService.disassociatUserFromFamily(userIdToBeRemoved, token, family);
		assertFalse(result);
	}

	@Test
	void testDisassociatUserFromFamilySuccess() {
		String token = "famToken";
		Family family = createFamily();
		family.setToken(token);
		User user = createUser();
		user.setId(30L);
		User user2 = createUser();
		user2.setId(2L);
		family.setPendingUser(Set.of(user, user2));
		user.setFamilies(Set.of(family));
		Long userIdToBeRemoved = 30L;
		when(userRepository.getById(userIdToBeRemoved)).thenReturn(user);
		boolean result = familyService.disassociatUserFromFamily(userIdToBeRemoved, token, family);
		assertTrue(result);
		System.out.println(family.getPendingUser());
		assert (family.getPendingUser().size() == 1);
	}

	@Test
	void testSaveFamilyInfoSuccess() {
		FamilySaveInfoPoJo familySaveInfoPoJo = FamilySaveInfoPoJo.builder().name("new Name").phone("1234567890")
				.address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600").country("Country").build();
		Family family = Family.builder().name("new Name").phone("1234567890").address1("Addr1").address2("Addr2")
				.city("City").state("Sind").zip("74600").country("Country").pendingUser(Set.of()).build();
		when(familyRespository.save(family)).thenReturn(family);
		Family result = familyService.saveFamilyInfo(familySaveInfoPoJo);
		assertEquals(result, family);
	}

	@Test
	void testChangeUserRoleInFamilySuccess() {
		boolean isNewUserRoleAdmin = false;
		Long userIdToBeUpdated = 300L;
		Family family = createFamily();
		Role role = Role.builder().id(2L).name(ERole.ROLE_USER).build();
		when(roleRepo.getByName(ERole.ROLE_USER)).thenReturn(role);
		FamilyUserRoles familyUserRoles = FamilyUserRoles.builder().role("ROLE_USER").build();
		when(familyUsersRoleRepo.findRoleForUserInFamily(30L, 300L)).thenReturn(familyUserRoles);
		familyService.changeUserRoleInFamily(isNewUserRoleAdmin, userIdToBeUpdated, family);
		verify(familyUsersRoleRepo, times(1)).save(familyUserRoles);
	}

	@Test
	void testAssociateUserToFamilyFalse() {
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(null);
		boolean result = familyService.associateUserToFamily(userId, createFamily());
		assertFalse(result);
	}

	@Test
	void testAssociateUserToFamilyTrue() {
		ReflectionTestUtils.setField(familyService, "domain", "domain");
		Long userId = 1L;
		Family family = createFamily();
		Family family2 = createFamily();
		family2.setFamilyId(20L);
		User user = createUser();

		user.setEmail("email");
		user.setFamilies(Set.of(family2));
		family.setPendingUser(Set.of(user));
		Role role = Role.builder().name(ERole.ROLE_USER).build();
		FamilyUserRoles famUserRole = new FamilyUserRoles(1L, 1L, "ROLE_ADMIN", "Father");

		Notification notification = Notification.builder().emailId(user.getEmail()).mobileNumber("")
				.notificationType("EMAIL").messageType("JOIN_FAMILY_USER_SUCCESS").build();
		doNothing().when(kafkaService).publishToTopic(notification);

		when(familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(), userId)).thenReturn(famUserRole);
		when(userRepository.getById(userId)).thenReturn(user);
		when(roleRepo.getByName(ERole.ROLE_USER)).thenReturn(role);
		boolean result = familyService.associateUserToFamily(userId, family);
		assertTrue(result);
	}

	@Test
	void testAssociateUserToFamilyTrue2() {
		ReflectionTestUtils.setField(familyService, "domain", "domain");
		Long userId = 1L;
		Family family = createFamily();
		Family family2 = createFamily();
		family2.setFamilyId(20L);
		User user = createUser();

		user.setEmail("email");
		user.setFamilies(Set.of(family2));
		family.setPendingUser(Set.of(user));
		Role role = Role.builder().name(ERole.ROLE_USER).build();

		Notification notification = Notification.builder().emailId(user.getEmail()).mobileNumber("")
				.notificationType("EMAIL").messageType("JOIN_FAMILY_USER_SUCCESS").build();
		doNothing().when(kafkaService).publishToTopic(notification);

		when(familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(), userId)).thenReturn(null);
		when(userRepository.getById(userId)).thenReturn(user);
		when(roleRepo.getByName(ERole.ROLE_USER)).thenReturn(role);
		boolean result = familyService.associateUserToFamily(userId, family);
		assertTrue(result);
	}

	@Test
	void testCreateNewFamilySuccess() {
		Family family = Family.builder().name("new Name").phone("1234567890").address1("Addr1").address2("Addr2")
				.city("City").state("Sind").zip("74600").country("Country").about("").build();
		User user = createUser();
		FamilyCreatePoJo familyCreatePoJo = getFamilyCreateInfo();
		Role role = Role.builder().id(2L).name(ERole.ROLE_ADMIN).build();
		when(roleRepo.getByName(ERole.ROLE_ADMIN)).thenReturn(role);
		familyService.createNewFamily(familyCreatePoJo, user);
		verify(familyRespository, times(1)).save(family);
	}

	@Test
	void testGetFamilyResponseByTokenSuccess() {
		String name = "fam";
		String phone = "123";
		String address1 = "add1";
		String address2 = "add2";
		String city = "city";
		String state = "state";
		String zip = "zip";
		String country = "country";
		String about = "about";
		String token = "token";
		User user = createUser();
		String localPath = "path";
		String localPath2 = "path2";
		ImageModel user1Image = ImageModel.builder().type("c").localPath(localPath).build();
		ImageModel user2Image = ImageModel.builder().type("p").localPath(localPath2).build();
		user.setId(30L);
		User user2 = createUser();
		user2.setId(2L);

		UserResponse user1Response = UserResponse.builder().dob(user2.getDob()).email(user2.getEmail())
				.enabled(user2.isEnabled()).firstName(user2.getFirstName()).gender(user2.getGender()).id(user2.getId())
				.lastName(user2.getLastName()).phone(user2.getPhone()).username(user.getUsername())
				.profilePicture(localPath2).build();
		UserResponse user2Response = UserResponse.builder().dob(user.getDob()).email(user.getEmail())
				.enabled(user.isEnabled()).firstName(user.getFirstName()).gender(user.getGender()).id(user.getId())
				.lastName(user.getLastName()).phone(user.getPhone()).username(user.getUsername()).coverPhoto(localPath)
				.build();
		Set<UserResponse> usersList = Set.of(user1Response, user2Response);
		Family family = Family.builder().name(name).phone(phone).address1(address1).address2(address2).city(city)
				.state(state).zip(zip).country(country).pendingUser(Set.of(user, user2)).about(about).build();
		FamilyResponse familyRes = FamilyResponse.builder().familyId(family.getFamilyId()).token(family.getToken())
				.name(family.getName()).phone(family.getPhone()).address1(family.getAddress1())
				.address2(family.getAddress2()).city(family.getCity()).country(family.getCountry())
				.state(family.getState()).zip(family.getZip()).build();
		familyRes.setPendingUser(usersList);
		when(imageRepo.findByUserIdAndType(2L, new String[] { "C", "P" })).thenReturn(List.of(user2Image));
		when(imageRepo.findByUserIdAndType(30L, new String[] { "C", "P" })).thenReturn(List.of(user1Image));
		FamilyResponse result = familyService.getFamilyResponseByToken(family, token);
		assertEquals(familyRes, result);
	}

	private Family createFamily() {
		Family family = new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
		family.setFamilyId(30L);
		return family;
	}

	private User createUser() {
		return User.builder().id(1L).enabled(true).families(Set.of(createFamily())).firstName("test")
				.dob("1998-01-04T05:00:00.000Z").build();
	}

	private FamilyUpdatePoJo getFamilyUpdateInfo() {
		FamilyUpdatePoJo familyUpdatePoJo = FamilyUpdatePoJo.builder().name("new Name")

				.phone("1234567890").address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600")
				.country("Country").relationWithFamily("Father").about("").build();
		return familyUpdatePoJo;
	}

	private FamilyCreatePoJo getFamilyCreateInfo() {
		FamilyCreatePoJo familyCreatePoJo = FamilyCreatePoJo.builder().name("new Name")

				.phone("1234567890").address1("Addr1").address2("Addr2").city("City").state("Sind").zip("74600")
				.country("Country").about("").relationWithFamily("Father").build();
		return familyCreatePoJo;
	}

}
