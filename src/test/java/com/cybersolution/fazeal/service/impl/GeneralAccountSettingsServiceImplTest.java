package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cybersolution.fazeal.models.FamilyMemberRequest;
import com.cybersolution.fazeal.models.FamilyMembers;
import com.cybersolution.fazeal.models.SocialMediaLinks;
import com.cybersolution.fazeal.models.SocialMediaLinksRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.GeneralAccountSettingsFamilyRepository;
import com.cybersolution.fazeal.repository.GeneralAccountSettingsRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
class GeneralAccountSettingsServiceImplTest {
	@InjectMocks
	GeneralAccountSettingsServiceImpl generalAccSettingsServiceImpl;

	@Mock
	UserRepository userRepository;
	@Mock
	GeneralAccountSettingsRepository generalAccountSettingsRepository;

	@Mock
	GeneralAccountSettingsFamilyRepository generalAccSettingsFamRepository;
	
	@Test
	void saveSocialMediaLinksTest() {
		User user= getUser();
		
		Map<String,String> links= new HashMap<>();
		links.put("Facebook", "fb.me");
		SocialMediaLinksRequest socialMediaLinksRequest= new SocialMediaLinksRequest();
		socialMediaLinksRequest.setSocialMediaLinksMap(links);
		
		Set<SocialMediaLinks> socialMediaLinksListSet = new HashSet<>();
		SocialMediaLinks socialMediaLinks=SocialMediaLinks.builder().userId(user.getId()).socialMedia("Facebook")
				.socialMedialLink("fb.me").build();
		socialMediaLinksListSet.add(socialMediaLinks);
		user.setSocialMediaLinksList(socialMediaLinksListSet);
		when(userRepository.getById(user.getId())).thenReturn(user);
		
		when(generalAccountSettingsRepository.save(socialMediaLinks)).thenReturn(socialMediaLinks);
		SocialMediaLinks response= generalAccSettingsServiceImpl.saveSocialMediaLinks(1L, socialMediaLinksRequest);
		assertEquals(response,socialMediaLinks);
	}

	@Test
	void getSocialMediaLinksTest() {
		List<SocialMediaLinks> socialMediaLinksList= new ArrayList<>();
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		socialMediaLinksList.add(socialMediaLinks);
		when(generalAccountSettingsRepository.findByUserId(1L)).thenReturn(socialMediaLinksList);
		List<SocialMediaLinks> response= generalAccSettingsServiceImpl.getSocialMediaLinks(1L);
		assertEquals(response,socialMediaLinksList);
	}

	@Test
	void findBySocialMediaIdTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccountSettingsRepository.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		SocialMediaLinks response= generalAccSettingsServiceImpl.findBySocialMediaId(1L);
		assertEquals(response,socialMediaLinks);
	}
	@Test
	public void deleteSocMediaLinkTest() {
		SocialMediaLinks socialMediaLinks=new SocialMediaLinks(1L,1L,"facebook","fb.me");
		when(generalAccountSettingsRepository.findBySocialMediaId(1L)).thenReturn(socialMediaLinks);
		assertTrue(generalAccSettingsServiceImpl.deleteSocMediaLink(1L));
		
	}

	@Test
	public void deleteSocMediaLinkFalseTest() {
		when(generalAccountSettingsRepository.findBySocialMediaId(1L)).thenReturn(null);
		assertFalse(generalAccSettingsServiceImpl.deleteSocMediaLink(1L));
		
	}
	
	@Test
	void saveFamilyMembersTest() {
		User user= getUser();
		Set<FamilyMembers> familyMembersSet = new HashSet<>();
		FamilyMembers familyMembers= FamilyMembers.builder().relation("Father").familyMemberName("maxwell").userId(1L).build();
		Map<String,String> familyMembersMap= new HashMap<>();
		familyMembersMap.put("Father", "maxwell");
		FamilyMemberRequest familyMemberRequest= new FamilyMemberRequest();
		familyMemberRequest.setFamilyMemberMap(familyMembersMap);
		familyMembersSet.add(familyMembers);
		user.setFamilyMembersList(familyMembersSet);
		when(userRepository.getById(user.getId())).thenReturn(user);
		
		when(generalAccSettingsFamRepository.save(familyMembers)).thenReturn(familyMembers);
		FamilyMembers response= generalAccSettingsServiceImpl.saveFamilyMembers(1L, familyMemberRequest);
		assertEquals(response,familyMembers);
	}

	@Test
	void getFamilyMembersTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		List<FamilyMembers> familyMembersList=List.of(familyMembers);
		when(generalAccSettingsFamRepository.findByUserId(1L)).thenReturn(familyMembersList);
		List<FamilyMembers> response= generalAccSettingsServiceImpl.getFamilyMembers(1L);
		assertEquals(response,familyMembersList);
	}

	@Test
	void findByfamilyMemberIdTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsFamRepository.findByFamMembersId(1L)).thenReturn(familyMembers);
		FamilyMembers response= generalAccSettingsServiceImpl.findByfamilyMemberId(1L);
		assertEquals(response,familyMembers);
	}
	@Test
	public void deleteFamilyMemberTest() {
		FamilyMembers familyMembers= FamilyMembers.builder().id(1L).relation("Father").familyMemberName("maxwell").userId(1L).build();
		when(generalAccSettingsServiceImpl.findByfamilyMemberId(1L)).thenReturn(familyMembers);
		assertTrue(generalAccSettingsServiceImpl.deleteFamilyMember(1L));
		
	}

	@Test
	public void deleteFamilyMemberFalseTest() {
		when(generalAccSettingsServiceImpl.findByfamilyMemberId(1L)).thenReturn(null);
		assertFalse(generalAccSettingsServiceImpl.deleteFamilyMember(1L));
		
	}
	public User getUser() {
		return new User(1L, "userName", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "aboutme", "eng", null,  null,
				null, null, null, "theme", "eng", null, null, null);
	}

}
