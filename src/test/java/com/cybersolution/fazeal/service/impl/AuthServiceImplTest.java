package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import com.cybersolution.fazeal.controller.FamilyController;
import com.cybersolution.fazeal.helper.AuditLogsHelper;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.UserResponse;
import com.cybersolution.fazeal.pojo.UserUpdateDetails;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.security.jwt.JwtUtils;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.util.Utils;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc // need this in Spring Boot test
@MockitoSettings(strictness = Strictness.LENIENT)
class AuthServiceImplTest {
	@InjectMocks
	private AuthServiceImpl authService;
	@Mock
	AuthenticationManager authenticationManager;
	@Mock
	UserRepository userRepository;

	@Mock
	RoleRepository roleRepository;

	@Mock
	PasswordEncoder encoder;

	@Mock
	JwtUtils jwtUtils;

	@Mock
	ImageRepository imageRepo;

	@Mock
	Messages messages;

	@Mock
	EmailService emailService;

	@Mock
	private Environment environment;

	@Mock
	FamilyService famService;

	@Mock
	FamilyController familyController;

	@Mock
	FamilyUserRoleRespository familyUsersRoleRepo;

	@Mock
	AuditLogsHelper auditLogsHelper;

	@Test
	void updateUserDetailsTestCaseSuccess() {
		User oldUser = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		Long userId = 1L;
		String email = "email";
		String pas = "pass";
		String firstName = "fname";
		String lastName = "lname";
		String dateOfBirth = "s";
		String nickname = "s";
		String gender = "s";
		String aboutMe = "s";
		String languages = "s";
		when(userRepository.getById(userId)).thenReturn(oldUser);
		oldUser.setPassword(pas);
		oldUser.setFirstName(firstName);
		oldUser.setLastName(lastName);
		oldUser.setNickname(nickname);
		oldUser.setGender(gender);
		oldUser.setAboutMe(aboutMe);
		oldUser.setLanguages(languages);
		UserUpdateDetails userUpdateDetails = createPOJOObj();
		when(userRepository.save(oldUser)).thenReturn(oldUser);
		User result = authService.updateUserDetails(1L, userUpdateDetails);
		assertEquals(result, oldUser);
	}

	@Test
	void updateUserDetailsTestCaseNoUser() {
		Long userId = 1L;

		UserUpdateDetails userUpdateDetails = createPOJOObj();
		when(userRepository.getById(userId)).thenReturn(null);
		User result = authService.updateUserDetails(1L, userUpdateDetails);
		assertNull(result);
	}

	@Test
	void loginByUsernameAndPasswordTestCaseNull() {
		String username = "userName";
		String pas = "pasword";
		when(userRepository.findByUsername(username)).thenReturn(Optional.of(new User()));
		JwtResponse jwtResponse = authService.loginByUsernameAndPassword(username, pas);
		assertNull(jwtResponse);
	}

	@Test
	void changeEnabledStatusTestCaseSuccess() {
		User oldUser = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(oldUser);
		when(userRepository.save(oldUser)).thenReturn(oldUser);
		User result = authService.changeEnabledStatus(userId);
		oldUser.setEnabled(true);
		assertTrue(result.isEnabled());
	}

	@Test
	void changeEnabledStatusTestCaseNull() {
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(null);
		User result = authService.changeEnabledStatus(userId);
		assertNull(result);
	}

	@Test
	void generateTokenToResetUserPasswordTestCaseNull() {
		String email = "email";
		when(userRepository.getUserFromEmail(email)).thenReturn(null);
		User result = authService.generateTokenToResetUserPassword(email);
		assertNull(result);
	}

	@Test
	void generateTokenToResetUserPasswordTestCaseSuccess() {
		String email = "email";
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		when(userRepository.getUserFromEmail(email)).thenReturn(user);
		User result = authService.generateTokenToResetUserPassword(email);
		assertTrue(result.getResetToken().length() > 0);
	}

	@Test
	void deleteUserByIdTestCaseSuccess() {
		Long userId = 1L;
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		when(userRepository.getById(userId)).thenReturn(user);
		boolean result = authService.deleteUserById(userId);
		assertTrue(result);
	}

	@Test
	void deleteUserByIdTestCaseUserNotFound() {
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(null);
		boolean result = authService.deleteUserById(userId);
		assertFalse(result);
	}

	@Test
	void updateUserPasswordTestCaseSuccess() {
		String pas = "new Pass";
		String expected = "Encoded New Pass";
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		when(encoder.encode(pas)).thenReturn(expected);
		authService.updateUserPassword(pas, user);

		assertEquals(user.getPassword(), expected);
	}

	@Test
	void getUserByResetTokenTestCaseUserNotFound() {
		String token = "token";
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").build();
		when(userRepository.getUserFromToken(token)).thenReturn(user);
		User result = authService.getUserByResetToken(token);
		assertEquals(result, user);
	}

	@Test
	void isTokenExpiredTestCaseUserNotExpired() {
		ReflectionTestUtils.setField(authService, "resetTokenTime", 60000L);
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2000 11:22").build();
		boolean result = authService.isTokenExpired(user);
		assertFalse(result);
	}

	@Test
	void isTokenExpiredTestCaseError() {
		ReflectionTestUtils.setField(authService, "resetTokenTime", 60000L);
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("wrong date").build();
		boolean result = authService.isTokenExpired(user);
		assertTrue(result);
	}

	@Test
	void isTokenExpiredTestCaseUserExpired() {
		ReflectionTestUtils.setField(authService, "resetTokenTime", 0L);
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		boolean result = authService.isTokenExpired(user);
		assertFalse(result);
	}

	@Test
	void findUserByIdTestCaseNullImagesSuccess() {
		Long userId = 1L;
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		List<ImageModel> imgList = new ArrayList<ImageModel>();
		UserResponse expected = createUserResponse(user);
		when(imageRepo.findByUserIdAndType(userId, new String[] { "C", "P" })).thenReturn(imgList);
		UserResponse result = authService.findUserById(1L, user);
		System.out.println(result);
		assertEquals(result, expected);
	}

	@Test
	void findUserByIdTestCaseWithUserImagesSuccess() {
		Long userId = 1L;
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		UserResponse expected = createUserResponse(user);
		expected.setCoverPhoto("path");
		expected.setProfilePicture("path2");
		ImageModel imageC = ImageModel.builder().type("C").localPath("path").build();
		ImageModel imageP = ImageModel.builder().type("P").localPath("path2").build();
		List<ImageModel> imgList = List.of(imageC, imageP);
		when(imageRepo.findByUserIdAndType(userId, new String[] { "C", "P" })).thenReturn(imgList);
		UserResponse result = authService.findUserById(1L, user);
		System.out.println(result);
		assertEquals(result, expected);
	}

	@Test
	void getByIdTestCaseSuccess() {
		Long userId = 1L;
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		when(userRepository.getById(userId)).thenReturn(user);
		User result = authService.getUserById(userId);
		assertEquals(result, user);
	}

	@Test
	void findAllTestCaseSuccess() {
		User user1 = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		User user2 = User.builder().username("test").id(2L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		List<User> expected = List.of(user1, user2);
		when(userRepository.findAll()).thenReturn(expected);
		List<User> result = authService.findAll();
		assertEquals(2, result.size());
	}

	@Test
	void saveUserTestCaseSuccess() {
		User user = User.builder().username("test").id(1L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		when(userRepository.save(user)).thenReturn(user);
		User result = authService.saveUser(user);
		assertEquals(result, user);
	}

	@Test
	void isUserExistsWithEmailTestCaseSuccess() {
		String email = "email";
		when(userRepository.existsByEmail(email)).thenReturn(true);
		boolean result = authService.isUserExistsWithEmail(email);
		assertTrue(result);
	}

	@Test
	void isUserExistsWithUsernameTestCaseSuccess() {
		String userName = "test";
		when(userRepository.existsByUsername(userName)).thenReturn(true);
		boolean result = authService.isUserExistsWithUsername(userName);
		assertTrue(result);
	}

	@Test
	void createNewUserTestCaseSuccess() {
		SignupRequest signUpRequest = new SignupRequest();
		String username = "test";
		String pas = "new pass";
		String email = "email@email.com";
		String dob = "12-12-2000 11:22";
		String firstName = "fname";
		String lastName = "lname";
		String encoded = "encoded pass";
		String gender = "Male";
		String phone = "23432423";
		signUpRequest.setUsername(username);
		signUpRequest.setPassword(pas);
		signUpRequest.setDob(dob);
		signUpRequest.setEmail(email);
		signUpRequest.setFirstName(firstName);
		signUpRequest.setLastName(lastName);
		signUpRequest.setGender(gender);
		signUpRequest.setPhone(phone);
		signUpRequest.setCreateFamilyAccount(true);
		Role role = Role.builder().name(ERole.ROLE_USER).build();
		String currentDateTime = Utils.getCurrentDateTime();
		User expected = User.builder().username(username).firstName(firstName).createdAt(currentDateTime)
				.lastName(lastName).enabled(true).dob(dob).phone(phone).email(email).gender(gender).roles(Set.of(role))
				.password(encoded).build();
		when(roleRepository.findByName(ERole.ROLE_USER)).thenReturn(Optional.of(role));
		when(encoder.encode(pas)).thenReturn(encoded);
		when(userRepository.save(expected)).thenReturn(expected);
		User result = (User) authService.createNewUser(signUpRequest);
		assertEquals(result, expected);
	}

	@Test
	void listAllFamiliesAndUsersByUserIdTestCaseEmpty() {
		User user = User.builder().username("test").id(1L).password("old").families(new HashSet<>()).email("oldEmail")
				.firstName("first").lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		Map<String, List<UserResponse>> result = authService.listAllFamiliesAndUsersByUserId(user);
		assertEquals(0, result.size());
	}

	@Test
	void listAllFamiliesAndUsersByUserIdTestCase() {
		String token = "token";
		Family family = Family.builder().familyId(20L).token(token).build();
		User user = User.builder().username("test").id(1L).password("old").families(new HashSet<>()).email("oldEmail")
				.families(Set.of(family)).firstName("first").lastName("last").nickname("one")
				.updatedAt("12-12-2030 11:22").build();
		User user1 = User.builder().username("test").id(2L).password("old").email("oldEmail").firstName("first")
				.lastName("last").nickname("one").updatedAt("12-12-2030 11:22").build();
		ImageModel imageC = ImageModel.builder().type("C").localPath("path").build();
		ImageModel imageP = ImageModel.builder().type("P").localPath("path2").build();
		List<ImageModel> imgList = List.of(imageC, imageP);
		when(userRepository.getUsersListForAFamily(family.getFamilyId(), user.getId()))
				.thenReturn(List.of(user, user1));
		when(imageRepo.findByUserIdAndType(user.getId(), new String[] { "C", "P" })).thenReturn(imgList);
		when(imageRepo.findByUserIdAndType(user1.getId(), new String[] { "C", "P" })).thenReturn(imgList);
		List<UserResponse> users = new ArrayList<UserResponse>();
		UserResponse userResponse = createUserResponse(user);
		UserResponse userResponse1 = createUserResponse(user1);
		userResponse.setCoverPhoto("path");
		userResponse.setProfilePicture("path2");
		userResponse1.setCoverPhoto("path");
		userResponse1.setProfilePicture("path2");
		users.add(userResponse);
		users.add(userResponse1);
		Map<String, List<UserResponse>> expected = new HashMap<>();
		expected.put(token, users);
		Map<String, List<UserResponse>> result = authService.listAllFamiliesAndUsersByUserId(user);
		assertTrue(result.size() > 0);
		assertTrue(result.get("token").size() > 1);
	}

	private UserResponse createUserResponse(User userInFamily) {
		return UserResponse.builder().dob(userInFamily.getDob()).email(userInFamily.getEmail())
				.enabled(userInFamily.isEnabled()).families(userInFamily.getFamilies())
				.firstName(userInFamily.getFirstName()).lastName(userInFamily.getLastName())
				.gender(userInFamily.getGender()).id(userInFamily.getId()).phone(userInFamily.getPhone())
				.username(userInFamily.getUsername()).build();
	}

	private UserUpdateDetails createPOJOObj() {
		UserUpdateDetails userUpdateDetails = UserUpdateDetails.builder().aboutMe("about me").dateOfBirth("09-09-2001")
				.email("Gamil@gmail.com").firstName("Firstname").gender("Female").languages("Enlish")
				.lastName("Lastname").nickname("Nick").password("jhafjkhk").build();

		return userUpdateDetails;
	}
}