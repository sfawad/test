package com.cybersolution.fazeal.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import com.cybersolution.fazeal.pojo.Notification;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaServiceImplTest {
	@InjectMocks
	KafkaServiceImpl kafkaServiceImpl;
	@Mock
	private KafkaTemplate<String, Object> kafkaTemplate;

	@Test
	void publishToTopicSuccess() {
		Notification notification = Notification.builder().build();
		ReflectionTestUtils.setField(kafkaServiceImpl, "topicName", "kafkaTopic");
		kafkaServiceImpl.publishToTopic(notification);
		verify(kafkaTemplate, times(1)).send("kafkaTopic", notification);
	}

}
