package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.BDDMockito.Then;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cybersolution.fazeal.models.AuditLogs;
import com.cybersolution.fazeal.repository.AuditLogsRepository;
import com.cybersolution.fazeal.util.Utils;

@ExtendWith(MockitoExtension.class)
class AuditLogsServiceImplTest {

	@InjectMocks
	private AuditLogsServiceImpl auditLogsService;

	@Mock
	private AuditLogsRepository auditLogsRepo;
	

	@Test
	void testFindAuditLogsByUserId() {
		 AuditLogs auditLog = new AuditLogs(null, 1L, "user1",Utils.getCurrentDateTime(), "TEST", null, true, null);
		 List<AuditLogs> auditList = new ArrayList<AuditLogs>();
		 auditList.add(auditLog);
		 when(auditLogsRepo.findAuditLogsByUserId(1L)).thenReturn(auditList);
		 List<AuditLogs> auditList1  = auditLogsService.findAuditLogsByUserId(1L);
		 assertEquals(auditList, auditList1);
	 }
	
	@Test
	void testSave() {
		 AuditLogs auditLog = new AuditLogs(null, 1L, "user1",Utils.getCurrentDateTime(), "TEST", null, true,null);
		 when(auditLogsRepo.save(auditLog)).thenReturn(auditLog);
		 auditLogsService.save(auditLog);
		 verify(auditLogsRepo).save(auditLog);
	 }
	
	@Test
	void testSaveError() {
		 AuditLogs auditLog = new AuditLogs(null, 1L, "user1",Utils.getCurrentDateTime(), "TEST", null, true,null);
		 when(auditLogsRepo.save(auditLog)).thenThrow(new RuntimeException());
		 auditLogsService.save(auditLog);
		 verify(auditLogsRepo).save(auditLog);
	 }
	
}