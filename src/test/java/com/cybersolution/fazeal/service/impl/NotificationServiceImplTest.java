package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.cybersolution.fazeal.models.NotificationsActivity;
import com.cybersolution.fazeal.repository.NotificationsActivityRespository;
import com.cybersolution.fazeal.service.impl.NotificationServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class NotificationServiceImplTest {

  @InjectMocks
  private NotificationServiceImpl notificationServiceImpl;

  @Mock
  NotificationsActivityRespository notifsRepo;

  private static final Long USERID = 13L;
  private static final String USERCREATION = "USER_CREATION";

  @Test
  public void getNotificationsForUsersTestcase() {

    List<NotificationsActivity> notifList = new ArrayList<>();
    NotificationsActivity notif = new NotificationsActivity(1L, USERCREATION, USERCREATION, 15L, 18L);
    notifList.add(notif);

    when(notifsRepo.findNotificationsForUser(USERID)).thenReturn(notifList);
    List<NotificationsActivity> actual = notificationServiceImpl.getNotificationsForUsers(USERID);

    assertEquals(notifList, actual);
  }

}
