package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.util.AppConstants;

@ExtendWith(MockitoExtension.class)
class ThemeServiceImplTest {
	@InjectMocks
	ThemeServiceImpl themeService;

	@Mock
	UserRepository userRepository;
	
	private final String theme = "green";
	private final String defaultTheme = "default";
	@Test
	void setUserThemeTestCaseTrue() {
		Long userId = 1L;
		User user = User.builder().id(userId).firstName("test").theme(defaultTheme).build();
		when(userRepository.getById(userId)).thenReturn(user);
		assertTrue(themeService.updateUserTheme(userId, theme));
	}

	@Test
	void setUserThemeTestCaseFalse() {
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(null);
		assertFalse(themeService.updateUserTheme(userId, theme));
	}

	@Test
	void getUserThemeTestCaseSuccess() {
		Long userId = 1L;
		User user = User.builder().id(userId).firstName("test").theme(defaultTheme).theme(theme).build();
		when(userRepository.getById(userId)).thenReturn(user);
		assertEquals (theme,themeService.getUserTheme(userId));
	}

	@Test
	void getUserThemeTestCaseUserNotFound() {
		Long userId = 1L;
		when(userRepository.getById(userId)).thenReturn(null);
		assertEquals (AppConstants.DEFAULT,themeService.getUserTheme(userId));
	}

}
