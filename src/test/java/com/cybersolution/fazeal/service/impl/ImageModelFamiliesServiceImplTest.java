package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModelFamilies;
import com.cybersolution.fazeal.repository.ImageFamiliesRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ImageModelFamiliesServiceImplTest {
	
	@InjectMocks
	ImageModelFamiliesServiceImpl imageModelFamiliesServiceImpl;
	
	@Mock
	ImageFamiliesRepository imageFamiliesRepository;
	
	ImageModelFamilies imgFamily = new ImageModelFamilies(1L, 1L);
	Family family = new Family(1L, "token", null, null, null, null, null, null, null, null, null, false, false, null, null, null);
	
	  @Test
	  void testdelete() {
		  when(imageFamiliesRepository.deleteImgFamily(1L)).thenReturn(1);
		  Integer returnVal = imageModelFamiliesServiceImpl.delete(1L);
		  assertEquals(1, returnVal);
	  }

	  @Test
	  void testFindByFamId() {
	     when(imageFamiliesRepository.getById(1L)).thenReturn(imgFamily);
	    ImageModelFamilies imgFams =  imageModelFamiliesServiceImpl.findByFamId(1L);
	    assertEquals(imgFamily, imgFams);
	  }
	  
	  @Test
	  void testNullFindByFamId() {
	     when(imageFamiliesRepository.getById(1L)).thenReturn(null);
	    ImageModelFamilies imgFams =  imageModelFamiliesServiceImpl.findByFamId(null);
	    assertEquals(null, imgFams);
	  }


	  @Test
	  void fetchfamilyImages() {
		 List<ImageModelFamilies> imageList = new ArrayList<>();
		 imageList.add(imgFamily);
	    when(imageFamiliesRepository.getImageModel(Set.of(family))).thenReturn(imageList);
	    List<ImageModelFamilies> resultList = imageModelFamiliesServiceImpl.fetchfamilyImages(Set.of(family));
	    assertEquals(imageList, resultList);
	    
	  }

	  @Test
	  void testGetfamilyImagesByFamId() {
		 Long[] famIds = new Long[] {1L,2L};
	     when(imageFamiliesRepository.getImageByFamilyId(1L)).thenReturn(famIds);
	     Long[] imageFamList = imageModelFamiliesServiceImpl.getfamilyImagesByFamId(1L);
	     assertEquals(famIds,imageFamList);
	  }

	
	

	
}
