package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.models.Voting;
import com.cybersolution.fazeal.models.VotingOptions;
import com.cybersolution.fazeal.models.VotingOptionsResponse;
import com.cybersolution.fazeal.models.VotingOptionsSelection;
import com.cybersolution.fazeal.models.VotingOptionsSelectionResponse;
import com.cybersolution.fazeal.models.VotingResponse;
import com.cybersolution.fazeal.repository.VotingOptionsRepository;
import com.cybersolution.fazeal.repository.VotingRepository;

@ExtendWith(MockitoExtension.class)
class VotingServiceImplTest {

	@InjectMocks
	private VotingServiceImpl votingService;

	@Mock
	private VotingRepository votingRepo;
	@Mock
	private Messages messages;
	@Mock
	private VotingOptionsRepository votingOptionsRepo;
	@Mock
	private UserServiceImpl userService;
	private static final String votingTest = "VotingTest";
	private static final String apple = "apple";
	private static final String encrypted = "Encrypted";
	private static final String mail = "test@test.com";
	private static final String userName = "testUser";
	private static final String peach = "peach";

	@Test
	void testGetAllFamilyVotingsByFamilyReturnNull() {
		// setup
		List<Voting> expected = Collections.emptyList();
		Long familyId = 1L;
		when(votingRepo.getByFamilyId(familyId)).thenReturn(null);

		// run test
		List<VotingResponse> result = votingService.getAllFamilyVotingsByFamilyId(familyId);

		// assert
		assertTrue(result.isEmpty());
		assertEquals(expected, result);
	}

	@Test
	void testGetAllFamilyVotingsByFamilyReturnList() {
		// setup
		VotingOptionsSelection votingOptionsSelection=new VotingOptionsSelection(1L,1L);
		votingOptionsSelection.setId(1L);
		VotingOptions votingOptions= new VotingOptions(1L,"Apple",1L,Set.of(votingOptionsSelection));
		List<Voting> votingList = new ArrayList<>();
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		voting.setId(1L);
		voting.setVotingDetails(Set.of(votingOptions));
		votingList.add(voting);
		List<VotingResponse> votingListResp = new ArrayList<>();
		VotingOptionsSelectionResponse votingOptionsSelectionResponse= new VotingOptionsSelectionResponse(1L,1L,1L);
		VotingOptionsResponse votingOptionsResponse= new VotingOptionsResponse(1L,"Apple",1L,Set.of(votingOptionsSelectionResponse),Long.valueOf(1L),Double.valueOf(100d));
		VotingResponse votingResp = new VotingResponse(1L,1L, 1L, votingTest, getCurrentDateTime(),Set.of(votingOptionsResponse));
		//BeanUtils.copyProperties(voting, votingResp);
		votingListResp.add(votingResp);
			when(votingRepo.getByFamilyId(1L)).thenReturn(votingList);

		// run test
		List<VotingResponse> result = votingService.getAllFamilyVotingsByFamilyId(1L);

		// assert
		assertEquals(votingListResp, result);
	}

	private String getCurrentDateTime() {
		Date date = new Timestamp(System.currentTimeMillis());
		String time1 = "";
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			time1 = new Timestamp(formatter.parse(date.toString()).getTime()).toString();
		} catch (Exception ec) {
			ec.getMessage();
		}
		return time1;
	}

	@Test
	void testSavingUserVoting() {
		// setup
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		Set<VotingOptions> votOptsSet = new HashSet<>();
		VotingOptions votingOpts = new VotingOptions(apple);
		votOptsSet.add(votingOpts);
		voting.setVotingDetails(votOptsSet);
		User user = getUser();
		user.setId(1L);
		Set<Family> families = new HashSet<Family>();
		Family family = getFamily();
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);

		// run test
		Voting result = votingService.saveUserVoting(1L, 1L, votingTest, List.of( "apple"));

		// assert
		assertEquals(voting, result);
	}

	@Test
	void testUpdateVotingOptionsByIdWithTypeASuccess() {
		VotingOptionsSelection votingOptionsSelection=new VotingOptionsSelection(1L,1L);
		votingOptionsSelection.setId(1L);
		VotingOptions votingOptions= new VotingOptions(1L,"peach",1L,Set.of(votingOptionsSelection));
		List<Voting> votingList = new ArrayList<>();
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		voting.setId(1L);
		voting.setVotingDetails(Set.of(votingOptions));
		votingList.add(voting);
		User user = getUser();
		user.setId(1L);
		Set<Family> families = new HashSet<>();
		Family family = getFamily();
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		when(userService.getById(1L)).thenReturn(user);
		when(votingRepo.getById(1L)).thenReturn(voting);
		Voting result = votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 1L, "A");
		assertEquals(voting, result);
	}
	@Test
	void testUpdateVotingOptionsByIdWithTypeD() {
		VotingOptionsSelection votingOptionsSelection=new VotingOptionsSelection(1L,1L);
		votingOptionsSelection.setId(1L);
		VotingOptions votingOptions= new VotingOptions(1L,"peach",1L,Set.of(votingOptionsSelection));
		List<Voting> votingList = new LinkedList<Voting>(Arrays.asList());
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		voting.setId(1L);
		voting.setVotingDetails(Set.of(votingOptions));		
		votingList.addAll(Arrays.asList(voting));
		User user = getUser();
		user.setId(1L);
		Set<Family> families = new HashSet<>();
		Family family = getFamily();
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		when(userService.getById(1L)).thenReturn(user);
		when(votingRepo.getById(1L)).thenReturn(voting);
		Voting result = votingService.updateVotingOptionsByVoteId(1L, List.of( "Apple"), 1L, "d");
		assertNotNull(result);
	}

	@Test
	void testUpdateVotingOptionsByIdNoUserFail() {
		when(userService.getById(1L)).thenReturn(null);
		Voting result = votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 1L, "A");
		assertEquals(null, result);
	}

	@Test
	void testUpdateVotingOptionsByIdNoVotingFail() {
		Voting voting = new Voting(null, 1L, votingTest, getCurrentDateTime());
		User user = getUser();
		user.setId(1L);
		when(userService.getById(1L)).thenReturn(user);
		when(votingRepo.getById(1L)).thenReturn(voting);
		Voting result = votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 1L, "A");
		assertEquals(null, result);
	}

	@Test
	void testUpdateVotingOptionsByIdUserDontHavePermisionToUpdateVotingFail() {
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		voting.setId(1L);
		User user = getUser();
		user.setId(30L);
		when(userService.getById(30L)).thenReturn(user);
		when(votingRepo.getById(1L)).thenReturn(voting);
		Voting result = votingService.updateVotingOptionsByVoteId(1L, List.of( "peach"), 30L, "A");
		assertEquals(null, result);
	}

	@Test
	void testUpdateVotingByIdUserIdOptionSuccess() {
		Voting votingResp = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		votingResp.setId(1L);
		Set<VotingOptionsSelection> votOptsSelSet = new HashSet<>();
		VotingOptionsSelection votingOptsSel = new VotingOptionsSelection(1L, 1L);
		votOptsSelSet.add(votingOptsSel);
		Set<VotingOptions> votOptsSetResp = new HashSet<>();
		VotingOptions votingOptsResp = new VotingOptions(apple);
		votingOptsResp.setId(1L);
		votingOptsResp.setVotingSelection(votOptsSelSet);
		votOptsSetResp.add(votingOptsResp);

		votingResp.setVotingDetails(votOptsSetResp);
		Voting voting = new Voting(1L, 1L, votingTest, getCurrentDateTime());
		voting.setId(1L);
		Set<VotingOptions> votOptsSet = new HashSet<>();
		VotingOptions votingOpts = new VotingOptions(apple);
		votingOpts.setId(1L);
		votOptsSet.add(votingOpts);
		voting.setVotingDetails(votOptsSet);
		User user = getUser();
		user.setId(1L);
		Set<Family> families = new HashSet<>();
		Family family = getFamily();
		family.setFamilyId(1L);
		families.add(family);
		user.setFamilies(families);
		when(votingRepo.getById(1L)).thenReturn(voting);
		when(userService.getById(1L)).thenReturn(user);
		Voting result = votingService.updateVotingByIdUserIdOption(1L, 1L, apple);
		assertEquals(votingResp, result);
	}

	User getUser() {
		return new User(userName, mail, encrypted, false);
	}

	Family getFamily() {
		return new Family("INFO", "1234567890", "Addr1", "Addr2", "City", "Sind", "74600", "Country");
	}
	
	@Test
	void testCalculatePercentage() {
		double expected = 12;
		double returned = votingService.calculatePercentage(expected, 100);
		assertEquals(expected, returned);
	}
	
	@Test
	void testUpdateVotingByIdUserIdOptionFailure() {
		User user = getUser();
		Voting voting = new Voting(null, 1L, votingTest, getCurrentDateTime());
		when(userService.getById(1L)).thenReturn(user);
		when(votingRepo.getById(1L)).thenReturn(voting);
		Voting votingActual = votingService.updateVotingByIdUserIdOption(1L, 1L, apple);
		assertEquals(null, votingActual);
	}
	
	@Test
	void testUpdateVotingByIdUserIdOptionUserIdFailure() {
		when(userService.getById(1L)).thenReturn(null);
		Voting votingActual = votingService.updateVotingByIdUserIdOption(1L, 1L, apple);
		assertEquals(null, votingActual);
	}
}
