package com.cybersolution.fazeal.service.impl;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplTest {
	
	@InjectMocks
	UserServiceImpl userServiceImpl;
	
	@Mock
	UserRepository userRepository;
	
	User user = new User("user1", "email1", "password", false);
	
	@Test
	void testGetById() {
		when(userRepository.getById(1L)).thenReturn(user);
		User userRet = userServiceImpl.getById(1L);
		assertEquals(user, userRet);
	}
	
	@Test
	void testGetUsersListForAFamily() {
		when(userRepository.getUsersListForAFamily(1L, 1L)).thenReturn(List.of(user));
		List<User> usersList = userServiceImpl.getUsersListForAFamily(1L, 1L);
		assertEquals(List.of(user), usersList);
	}
	
	@Test
	void testIsUserInFamily() {
		user.setId(1L);
		when(userRepository.getAllUsersListForAFamily(1L)).thenReturn(List.of(user));
		boolean isUser = userServiceImpl.isUserInFamily(1L, 1L);
		assertTrue(isUser);
	}
	
	@Test
	public void setSystemLanguageTest() {
		String SystemLanguage="French";
		Long userId= 1L;
		User user = User.builder().id(userId).firstName("test").systemLanguage(SystemLanguage).build();
		when(userRepository.getById(1L)).thenReturn(user);
		assertTrue(userServiceImpl.setSystemLanguage(userId, SystemLanguage));
	}
	
	@Test
	public void setSystemLanguageFailTest() {
		String SystemLanguage="French";
		Long userId= 1L;
		when(userRepository.getById(1L)).thenReturn(null);
		assertFalse(userServiceImpl.setSystemLanguage(userId, SystemLanguage));
	}
	
	@Test
	public void getSystemLanguageTest() {
		String SystemLanguage="French";
		Long userId= 1L;
		User user = User.builder().id(userId).firstName("test").systemLanguage(SystemLanguage).build();
		when(userRepository.getById(1L)).thenReturn(user);
		String language= userServiceImpl.getSystemLanguage(userId);
		assertEquals(SystemLanguage, language);
	}
	@Test
	public void getSystemLanguageDefaultTest() {
		String defaultLanguage="English";
		Long userId= 1L;
		when(userRepository.getById(1L)).thenReturn(null);
		String language= userServiceImpl.getSystemLanguage(userId);
		assertEquals(defaultLanguage, language);
	}
	
	@Test
	public void setNamesSuccessTest() {
		Long userId=1L;
		String firstName="maxxw";
		String lastName="Well";
		User user = User.builder().id(userId).firstName(firstName).lastName(lastName).build();
		when(userRepository.getById(1L)).thenReturn(user);
		assertTrue(userServiceImpl.setNames(userId,"mr", firstName,lastName));
	}
	@Test
	public void setNamesFailTest() {
		Long userId=1L;
		String firstName="maxxw";
		String lastName="Well";
		when(userRepository.getById(1L)).thenReturn(null);
		assertFalse(userServiceImpl.setNames(userId,"mr", firstName,lastName));
	}
	
}
