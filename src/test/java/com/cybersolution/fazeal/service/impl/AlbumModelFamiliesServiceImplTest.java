package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.repository.AlbumFamiliesRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AlbumModelFamiliesServiceImplTest {
	
	@InjectMocks
	private AlbumModelFamiliesServiceImpl albumModelFamiliesServiceImpl;
	
	@Mock
	private AlbumFamiliesRepository albumFamiliesRepository;
	
	@Test
	void testaddFamily() {
		AlbumModelFamilies albumModelFam = new AlbumModelFamilies(1L, 1L, 1L);
		when(albumFamiliesRepository.save(albumModelFam)).thenReturn(albumModelFam);
		AlbumModelFamilies albumModelFamReturn = albumModelFamiliesServiceImpl.addFamily(albumModelFam);
		assertEquals(albumModelFam, albumModelFamReturn);
	}

	
}
