package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import com.cybersolution.fazeal.models.AlbumModel;
import com.cybersolution.fazeal.models.AlbumModelFamilies;
import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.ImageModelFamilies;
import com.cybersolution.fazeal.models.ImageModelUser;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.AlbumRepository;
import com.cybersolution.fazeal.repository.ImageFamiliesRepository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.ImageUsersRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.AlbumService;
import com.cybersolution.fazeal.service.AmazonClient;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.ImageModelFamiliesService;
import com.cybersolution.fazeal.service.ImageModelUserService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ImageServiceImplTest {

	@InjectMocks
	private ImageServiceImpl imageServiceImpl;

	@Mock
	private ImageRepository imageRepository;

	@Mock
	AlbumRepository albumRepository;

	@Mock
	UserRepository userRepository;

	@Mock
	ImageFamiliesRepository imageFamiliesRepository;

	@Mock
	ImageUsersRepository imageUsersRepository;
	@Mock
	AmazonClient amazonClient;

	@Mock
	UserService userService;

	@Mock
	ImageModelFamiliesService imageModelFamiliesService;

	@Mock
	ImageModelUserService imageModelUserService;

	@Mock
	AlbumService albumService;
	@Mock
	FamilyService familyService;
	public static final String about = "about";
	public static final String token = "token13";
	public static final String fam1 = "fam1";
	public static final String line = "line1";
	public static final String line2 = "line2";
	public static final String city = "city";
	public static final String state = "state";
	public static final String imgPath = "1/albummodel/dog.png";
	public static final String albumDesc = "AlbumDescription";
	public static final String albumName = "AlbumName";
	public static final String pePU = "PE_PU";
	public static final String user1 = "user1";
	public static final String NAME_OF_THE_FILE = "NameOfTheFile";
	public static final String FILE_TYPE = "multipart/form-data";

	Family family = new Family(1L, about, null, "123", token, fam1, line, line2, city, state, null, false, false, null,
			null, null);

	Set<Family> families = Set.of(family);
	Date date = new Date(System.currentTimeMillis());
	String[] type = { "P", "D" };

	@Test
	public void getImagesListByIdTestCase() {
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(imgPath).build();
		List<ImageModel> images = new ArrayList<>();
		images.add(imageModel);
		Long userId = imageModel.getUserId();
		when(imageRepository.findByUserId(userId)).thenReturn(images);
		List<ImageModel> result = imageServiceImpl.findByUserId(Long.valueOf(1));
		assertEquals(images, result);
	}

	@Test
	public void getImageslistUserImagesByTypeTestCase() {
		ImageModel imageModel = ImageModel.builder().userId(1L).date(date).extension("jpg").imageFamiliesList(null)
				.imageUserList(null).localPath(imgPath).build();
		List<ImageModel> images = new ArrayList<>();
		images.add(imageModel);
		Long userId = imageModel.getUserId();
		when(imageRepository.findByUserIdAndType(userId, type)).thenReturn(images);
		List<ImageModel> result = imageServiceImpl.findByUserIdAndType(Long.valueOf(1), type);
		assertEquals(images, result);
	}

	@Test(expected = NoSuchElementException.class)
	public void testListAlbumImages() throws NoSuchElementException {
		AlbumModelFamilies albumModelFamilies1 = new AlbumModelFamilies(1L, 1L, 1L);
		AlbumModelFamilies albumModelFamilies2 = new AlbumModelFamilies(1L, 2L, 2L);
		AlbumModelFamilies albumModelFamilies3 = new AlbumModelFamilies(1L, 3L, 3L);

		AlbumModelUser albumModelUser1 = new AlbumModelUser(1L, 1L, 1L);
		AlbumModelUser albumModelUser2 = new AlbumModelUser(1L, 2L, 2L);
		AlbumModelUser albumModelUser3 = new AlbumModelUser(1L, 3L, 3L);

		AlbumModel userAlbums = AlbumModel.builder().albumId(1L).albumName(albumName).description(albumDesc).date(date)
				.userId(1L).type(pePU).albumUserList(Set.of(albumModelUser1))
				.albumFamiliesList(Set.of(albumModelFamilies1)).build();

		AlbumModel familySharedAlbums = AlbumModel.builder().albumId(2L).albumName(albumName).description(albumDesc)
				.date(date).userId(2L).type(pePU).albumUserList(Set.of(albumModelUser2))
				.albumFamiliesList(Set.of(albumModelFamilies2)).build();

		AlbumModel userSharedAlbums = AlbumModel.builder().albumId(3L).albumName(albumName).description(albumDesc)
				.date(date).userId(3L).type(pePU).albumUserList(Set.of(albumModelUser3))
				.albumFamiliesList(Set.of(albumModelFamilies3)).build();

		List<AlbumModel> userAlbumsList = new ArrayList();
		List<AlbumModel> familySharedAlbumsList = new ArrayList();
		List<AlbumModel> userSharedAlbumsList = new ArrayList();

		ConcurrentHashMap albumMap = new ConcurrentHashMap<>();
		albumMap.put("userAlbums", userAlbumsList.add(userAlbums));
		albumMap.put("familySharedAlbums", familySharedAlbumsList.add(familySharedAlbums));
		albumMap.put("userSharedAlbums", userSharedAlbumsList.add(userSharedAlbums));

		final Family family1 = new Family("fam1", "1234567891", line, line, "city1", "state1", "zip1", "country1");

		family1.setFamilyId(200L);

		User loggedUser = User.builder().id(1L).firstName("Test").families(Set.of(family1)).build();

		ImageModelFamilies imageModelFamilies1 = ImageModelFamilies.builder().id(1L).familyId(200L).imagemodelId(1L)
				.build();
		ImageModelUser imageModelUser = ImageModelUser.builder().id(1L).userId(1L).imagemodelId(1L).build();

		Long[] familyImgs = { imageModelFamilies1.getImagemodelId() };
		Long[] userImgs = { imageModelUser.getImagemodelId() };

		when(userRepository.findById(Long.valueOf(1)).get()).thenReturn(loggedUser);
		when(albumRepository.findByUserId(1L)).thenReturn(userAlbumsList);
		when(imageFamiliesRepository.getImageByFamilyId(200L)).thenReturn(familyImgs);
		when(albumRepository.findAlbumsByFamilyIDS(200L, 1L)).thenReturn(familySharedAlbumsList);
		when(imageUsersRepository.getImageByUseryId(1L)).thenReturn(userImgs);
		when(albumRepository.findAlbumsByUsersIDS(1L)).thenReturn(userSharedAlbumsList);

		ConcurrentHashMap result = imageServiceImpl.getAlbumImagesWithUserId(1L);
		assertEquals(albumMap, result);
	}

	@Test
	public void deleteByImgIdTest() {
		ImageModel imageModel = ImageModel.builder().albumId(1L).date(date).extension("png").imageFamiliesList(null)
				.imageUserList(null).localPath(imgPath).build();
		imageServiceImpl.deleteByImgId(imageModel);
		verify(amazonClient, atLeastOnce()).deleteFileFromS3Bucket(any());
		verify(imageRepository, atLeastOnce()).deleteById(any());
	}

	@Test
	public void uploadImageTest() throws IOException {
		ImageModel imageModelNew = ImageModel.builder().date(date).extension("").userId(1L).userName(user1).type("C")
				.imageUserList(Set.of()).imageFamiliesList(Set.of()).description("description").build();
		List<ImageModel> imgModelList = new ArrayList<>();
		imgModelList.add(imageModelNew);
		Set<ImageModel> imagesSet = Set.of(imageModelNew);
		User user = getUser();
		when(imageRepository.findByUserIdAndType(user.getId(), type)).thenReturn(imgModelList);
		when(userService.getById(user.getId())).thenReturn(user);
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("PE_PR").albumImages(imagesSet).build();
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);

		when(albumRepository.findByAlbumNameAndUserId("test Album", 1L)).thenReturn(album);
		when(imageRepository.save(imageModelNew)).thenReturn(imageModelNew);
		when(amazonClient.uploadFile(file, 1L, "test Album")).thenReturn("UploadPath");
		ImageModel imageModel = new ImageModel();
		imageModel.setDate(date);
		imageModel.setExtension("");
		imageModel.setUserId(user.getId());
		imageModel.setUserName(user.getUsername());
		imageModel.setType("C");
		String fileName = imageServiceImpl.uploadImages(imageModel.getDate(), file, imageModel.getUserId(),
				imageModel.getType(), albumName, "description", "new file", album);
		assertNotNull(fileName);
	}

	@Test
	public void uploadImageWithTypeDTest() throws IOException {
		ImageModel imageModelNew = ImageModel.builder().date(date).extension("").userId(1L).userName(user1).type("D")
				.imageUserList(Set.of()).imageFamiliesList(Set.of()).description("description").build();
		List<ImageModel> imgModelList = new ArrayList<>();
		imgModelList.add(imageModelNew);
		Set<ImageModel> imagesSet = Set.of(imageModelNew);
		User user = getUser();
		Long count = 1L;
		when(imageRepository.findByUserIdAndType(user.getId(), type)).thenReturn(imgModelList);
		when(imageRepository.countById(1L)).thenReturn(count);
		when(imageRepository.getById(1L)).thenReturn(imageModelNew);
		amazonClient.deleteFileFromS3Bucket(imageModelNew.getLocalPath());
		imageRepository.deleteById(1L);
		verify(amazonClient, atLeastOnce()).deleteFileFromS3Bucket(any());
		verify(imageRepository, atLeastOnce()).deleteById(any());

		when(userService.getById(user.getId())).thenReturn(user);
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("PE_PR").albumImages(imagesSet).build();
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);

		when(albumRepository.findByAlbumNameAndUserId("test Album", 1L)).thenReturn(album);
		when(imageRepository.save(imageModelNew)).thenReturn(imageModelNew);
		when(amazonClient.uploadFile(file, 1L, "test Album")).thenReturn("UploadPath");
		ImageModel imageModel = new ImageModel();
		imageModel.setDate(date);
		imageModel.setExtension("");
		imageModel.setUserId(user.getId());
		imageModel.setUserName(user.getUsername());
		imageModel.setType("D");
		String fileName = imageServiceImpl.uploadImages(imageModel.getDate(), file, imageModel.getUserId(),
				imageModel.getType(), albumName, "description", "new file", album);
		assertNotNull(fileName);
	}

	@Test
	public void getImageByIdTest() {
		ImageModel imageModel = ImageModel.builder().id(1L).albumId(1L).date(date).extension("png")
				.imageFamiliesList(null).imageUserList(null).localPath(imgPath).build();
		when(imageRepository.existsById(imageModel.getId())).thenReturn(true);
		when(imageRepository.getById(imageModel.getId())).thenReturn(imageModel);
		ImageModel response = imageServiceImpl.getImageById(imageModel.getId());
		assertEquals(response, imageModel);
	}

	@Test
	public void getAlbumImagesWithUserIdTest() {
		ConcurrentHashMap albumMap = new ConcurrentHashMap<>();

		User user = getUser();
		when(userService.getById(user.getId())).thenReturn(user);
		ImageModel imageModelNew = ImageModel.builder().id(1L).date(date).extension("").userId(1L).userName(user1)
				.type("C").imageUserList(Set.of()).imageFamiliesList(Set.of()).build();
		List<ImageModel> imgModelList = new ArrayList<>();
		imgModelList.add(imageModelNew);
		Set<ImageModel> imagesSet = Set.of(imageModelNew);
		AlbumModel album = AlbumModel.builder().albumId(1L).date(date).albumName("album1").description("album1")
				.userId(20L).type("PE_PR").albumImages(imagesSet).build();
		List<AlbumModel> albums = new ArrayList<>();
		albums.add(album);
		List albumsList = new ArrayList();
		List albumsUserList = new ArrayList();
		Long[] familyAlbum = { 1L, 2L };
		Long[] userImgs = { 1L, 2L };
		when(albumService.findByUserId(user.getId())).thenReturn(albums);
		when(imageModelFamiliesService.getfamilyImagesByFamId(family.getFamilyId())).thenReturn(familyAlbum);
		when(albumService.getAlbumsByFamilyIds(family.getFamilyId(), user.getId())).thenReturn(albums);
		when(imageModelUserService.getUserImagesIdsByUserId(user.getId())).thenReturn(userImgs);
		when(albumService.getAlbumsByUserIds(user.getId())).thenReturn(albums);
		albumMap = imageServiceImpl.getAlbumImagesWithUserId(user.getId());
		assertNotNull(albumMap);
	}

	@Test
	public void filterExistTest() {
		Long idInList = 1L;
		List<Long> ids = new ArrayList<>();
		ids.add(1L);
		boolean response = imageServiceImpl.filterExist(idInList, ids);
		assertTrue(response);
	}

	@Test
	public void checkIfImageSharedToFamilyTest() {
		User user = getUser();
		Set<Long> userFamilies = Set.of(1L);
		boolean response = imageServiceImpl.checkIfImageSharedToFamily(user, userFamilies);
		assertTrue(response);
	}

	@Test
	public void imagesSharedWithUsersAndFamiliesTest() {

		List<Long> familyIds = List.of(2234567891L);
		List<Long> userIds = List.of(3L);
		final Family family1 = new Family("fam1", "1234567891", line, line2, city, state, "zip", "country");
		family1.setFamilyId(1234567891L);
		final Family family2 = new Family("fam2", "2234567891", line, line2, "city2", "state2", "zip2", "country2");
		family2.setFamilyId(2234567891L);
		String type = "TYPE";
		final User user1family2 = User.builder().id(3L).firstName("user3family2").families(Set.of(family2)).build();
		ImageModel imageModelNew = ImageModel.builder().id(1L).date(date).extension("").userId(1L).userName(user1)
				.type("C").imageUserList(Set.of()).imageFamiliesList(Set.of()).build();
		when(familyService.getAllUserRolesInFamily(2234567891L)).thenReturn(null);
		Set<Family> families = Set.of(family);
		Long imageId = imageModelNew.getId();
		Long userIdFiltered = user1family2.getId();
		Long familyId = family.getFamilyId();
		imageServiceImpl.createShareRelationWithUsers(imageId, userIdFiltered);
		imageServiceImpl.createShareRelationWithFamilies(imageId, familyId);
		boolean response = imageServiceImpl.imagesSharedWithUsersAndFamilies(familyIds, imageModelNew, userIds,
				user1family2, type);

		assertTrue(response);
	}

	public User getUser() {
		return new User(1L, user1, "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "aboutme", "eng", null, null, families, null, null,
				"theme", "eng", null, null, null);
	}

	@Test
	public void getImageByImageLocalPathTestNull() {
		String localPath = "";
		when(imageRepository.getByLocalPathAndUserId(localPath, 1L)).thenReturn(null);
		ImageModel result = imageServiceImpl.getImageByImageLocalPath(localPath, 1L);
		assertNull(result);
	}

	@Test
	public void getImageByImageLocalPathTestSuccess() {
		String localPath = "imagePath";
		ImageModel imageModel = ImageModel.builder().id(1L).build();
		when(imageRepository.getByLocalPathAndUserId(localPath, 1L)).thenReturn(imageModel);
		ImageModel result = imageServiceImpl.getImageByImageLocalPath(localPath, 1L);
		assertNotNull(result);
		assertEquals(imageModel, result);
	}

	@Test
	public void getImgByImageLocalPathTestNull() {
		String localPath = "";
		when(imageRepository.getByLocalPath(localPath)).thenReturn(null);
		ImageModel result = imageServiceImpl.getImgByImageLocalPath(localPath);
		assertNull(result);
	}

	@Test
	public void getImgByImageLocalPathTestSuccess() {
		String localPath = "imagePath";
		ImageModel imageModel = ImageModel.builder().id(1L).build();
		when(imageRepository.getByLocalPath(localPath)).thenReturn(imageModel);
		ImageModel result = imageServiceImpl.getImgByImageLocalPath(localPath);
		assertNotNull(result);
		assertEquals(imageModel, result);
	}

	@Test
	public void uploadFamilyGroupProfileOrDashboardImageGPNotSaved() throws IOException {
		ImageModel image = ImageModel.builder().date(date).extension("config").userName(user1).type("GP").build();
		ImageModel image2 = ImageModel.builder().date(date).extension("config").userName(user1).type("GP").id(1L)
				.build();
		FileInputStream inputFile = new FileInputStream("lombok.config");
		MockMultipartFile file = new MockMultipartFile("fileImage", "lombok.config", FILE_TYPE, inputFile);
		when(userService.getById(1L)).thenReturn(getUser());
		imageServiceImpl.uploadFamilyGroupProfileOrDashboardImage(file, family, "GP", date, 1L);
	}

	@Test
	public void restoreImageToAlbumFalse() {
		ImageModel image = ImageModel.builder().markedToBeDeleted(false).build();
		boolean result = imageServiceImpl.restoreImageToAlbum(image);
		assertFalse(result);
	}

	@Test
	public void restoreImageToAlbumTrue() {
		ImageModel image = ImageModel.builder().markedToBeDeleted(true).build();
		boolean result = imageServiceImpl.restoreImageToAlbum(image);
		assertTrue(result);
	}

	@Test
	public void getDeletedAlbumForUserNew() {
		Long userId = 1L;
		AlbumModel album = AlbumModel.builder().albumId(1L).build();
		when(albumService.userDontHaveDeletedAlbum(userId)).thenReturn(true);
		when(albumService.createPrivateAlbum(AlbumModel.builder().userId(userId).type(AppConstants.DELETED)
				.albumName(AppConstants.DELETED.toUpperCase()).date(any())
				.description(AppConstants.DELETED_IMAGES_ALBUM).build())).thenReturn(album);
		AlbumModel result = imageServiceImpl.getDeletedAlbumForUser(userId);
		assertNotNull(result);
	}

	@Test
	public void getDeletedAlbumForUserOld() {
		Long userId = 1L;
		AlbumModel album = AlbumModel.builder().albumId(1L).build();
		when(albumService.userDontHaveDeletedAlbum(userId)).thenReturn(false);
		when(albumService.findAlbumByAlbumNameAndUserId(AppConstants.DELETED.toUpperCase(), userId)).thenReturn(album);
		AlbumModel result = imageServiceImpl.getDeletedAlbumForUser(userId);
		assertNotNull(result);
	}

	@Test
	public void markImagedelete() {
		Long userId = 1L;
		ImageModel image = ImageModel.builder().markedToBeDeleted(true).build();
		imageServiceImpl.markImagedelete(image, userId);
		verify(imageRepository, times(1)).save(image);
	}
}
