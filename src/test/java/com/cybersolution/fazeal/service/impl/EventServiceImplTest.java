package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.cybersolution.fazeal.pojo.CreateEventPojo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.cybersolution.fazeal.models.Events;
import com.cybersolution.fazeal.models.EventsCurrentUserResponse;
import com.cybersolution.fazeal.models.EventsFamilyResponse;
import com.cybersolution.fazeal.models.EventsFamilyUser;
import com.cybersolution.fazeal.models.EventsRelation;
import com.cybersolution.fazeal.models.EventsRelationResponse;
import com.cybersolution.fazeal.models.EventsResponse;
import com.cybersolution.fazeal.models.EventsUserResponse;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.EventsFamilyUserRespository;
import com.cybersolution.fazeal.repository.EventsRelationRespository;
import com.cybersolution.fazeal.repository.EventsRespository;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.util.AppConstants;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EventServiceImplTest {

	@InjectMocks
	private EventServiceImpl eventServiceImpl;
	@Mock
	private EventsRespository eventRepo;

	@Mock
	EventsFamilyUserRespository eventsFamilyUserRespository;

	@Mock
	UserRepository userRepository;

	@Mock
	FamilyRespository familyRespository;

	@Mock
	EventsRelationRespository eventsRelationRespository;

	Date date = new Date(System.currentTimeMillis());
	Family family = new Family(1L, "about", null, "1L", "token13", "fam1", "line1", "line2", "city", "state", null,
			false, false, null, null, null);
	Set<Family> families = Set.of(family);

	@Test
	public void updateEventTestcase() {
		Events event = new Events(Long.valueOf(1), "Test Event", 1L, true);
		when(eventRepo.getById(1L)).thenReturn(event);
		when(eventRepo.save(event)).thenReturn(event);
		boolean result = eventServiceImpl.updateEvent(event.getId(), event.getEvent(), true);
		assertTrue(result);
	}

	@Test
	public void updateEventFailTestcase() {
		Events event = new Events(null, "Test Event", 1L, true);
		when(eventRepo.getById(null)).thenReturn(event);
		boolean result = eventServiceImpl.updateEvent(event.getId(), event.getEvent(), true);
		assertFalse(result);
	}

	@Test
	public void findByUserIdTestcase() {
		Events event = Events.builder().userId(1L).event("Test Event").date(null).eventEnd(null).isShared(true).build();
		List<Events> events = new ArrayList<>();
		events.add(event);
		Long userId = event.getUserId();
		when(eventRepo.findByUserId(userId)).thenReturn(events);
		List<Events> result = eventServiceImpl.findByUserId(Long.valueOf(1));
		assertEquals(events, result);

	}

	@Test
	public void findByEventIdTestcase() {
		String date1 = "1965-10-26T00:00:00Z";
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		when(eventRepo.existsById(1L)).thenReturn(true);
		when(eventRepo.getById(1L)).thenReturn(event);
		Events response = eventServiceImpl.findByEventId(1L);
		assertEquals(response, event);

	}

	@Test
	public void deleteEventTestCase() {
		String date1 = "1965-10-26T00:00:00Z";
		Long count = 1L;
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		when(eventRepo.countById(1L)).thenReturn(count);
		when(eventRepo.getById(1L)).thenReturn(event);
		eventsFamilyUserRespository.deleteById(1L);
		eventRepo.deleteById(1L);
		boolean response = eventServiceImpl.deleteEvent(count);
		assertTrue(response);
	}

	@Test
	public void deleteEventFailTestCase() {
		String date1 = "1965-10-26T00:00:00Z";
		Long count = 1L;
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		when(eventRepo.countById(null)).thenReturn(count);
		when(eventRepo.getById(null)).thenReturn(event);
		eventRepo.deleteById(1L);
		boolean response = eventServiceImpl.deleteEvent(count);
		assertFalse(response);
	}

	@Test
	public void deleteEventsFamilyUserTestCase() {
		String date1 = "1965-10-26T00:00:00Z";
		Long count = 1L;
		User loggedUser = User.builder().id(20L).firstName("Test").build();
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		Family family1 = new Family("fam1", "1234567891", "line1", "line2", "city", "state", "zip", "country");
		EventsFamilyUser eventFamilyUser = new EventsFamilyUser(loggedUser, family1, event);
		List<EventsFamilyUser> eventFamily = new ArrayList<>();
		eventFamily.add(eventFamilyUser);
		when(eventsFamilyUserRespository.findEventsByEventId(1L)).thenReturn(eventFamily);
		eventServiceImpl.deleteEventsFamilyUser(1L);
		assertTrue(Boolean.TRUE);
	}

	@Test
	public void saveEventTestCase() throws Exception {
		String date1 = "1965-10-26T00:00:00Z";
		CreateEventPojo createEventPojo = CreateEventPojo.builder().eventName("Test Events").date(date1).startTime(date1).endTime(date1).isShared(true).build();

		Events event = new Events(null, "Test Event", 1L, true);
		event.setDate(date1);
		event.setEventEnd(date1);
		event.setEventStart(date1);
		when(eventRepo.save(event)).thenReturn(event);
		Events result = eventServiceImpl.saveEvent(1L, createEventPojo);
		assertEquals(null, result);
	}

	@Test
	public void saveEventsFamilyUserTestCase() throws Exception {
		String date1 = "1965-10-26T00:00:00Z";
		User loggedUser = User.builder().id(1L).firstName("Test").build();
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		Family family1 = new Family("fam1", "1234567891", "line1", "line2", "city", "state", "zip", "country");
		EventsFamilyUser eventFamilyUser = new EventsFamilyUser(loggedUser, family1, event);
		EventsFamilyUser entity = new EventsFamilyUser(userRepository.getById(1L), familyRespository.getById(20L),
				event);
		when(eventsFamilyUserRespository.save(entity)).thenReturn(eventFamilyUser);
		eventServiceImpl.saveEventsFamilyUser(event, 1L, new Long[] { Long.valueOf(20L) });
		assertTrue(Boolean.TRUE);
	}

	@Test
	public void getUserWithFamilyEventsTestCase() throws Exception {
		String date1 = "1965-10-26T00:00:00Z";
		User loggedUser = User.builder().id(1L).firstName("Test").build();
		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		Family family1 = new Family("fam1", "1234567891", "line1", "line2", "city", "state", "zip", "country");
		EventsFamilyUser eventFamilyUser = new EventsFamilyUser(loggedUser, family1, event);
		List<Events> famEvent = new ArrayList<Events>();
		List<EventsFamilyUser> eventsRel = new ArrayList<>();
		eventsRel.add(eventFamilyUser);
		when(eventsFamilyUserRespository.findEventsByFamilyId(1L)).thenReturn(eventsRel);
		for (EventsFamilyUser eventsFamily : eventsRel) {
			famEvent.add(eventsFamily.getEvents());
		}
		List<Events> response = eventServiceImpl.getUserWithFamilyEvents(1L);
		assertEquals(famEvent, response);
	}

	@Test
	public void setEventDetailsTestCase() throws Exception {
		String date1 = "1965-10-26T00:00:00Z";
		Events event = new Events(null, null, 1L, false);
		event.setEvent("New Events");
		event.setDate(date1);
		event.setEventEnd(date1);
		event.setEventStart(date1);
		event.setShared(true);
		when(eventRepo.save(event)).thenReturn(event);
		eventServiceImpl.setEventDetails(event, date1, date1, date1, Boolean.TRUE, "New Events");
		assertTrue(Boolean.TRUE);
	}

	@Test
	public void getUserEventsPlainResponseTestCase() {
		EventsRelation eventsRelation = EventsRelation.builder().edate("14-02-22").eend("15-02-22").eid(1L).uid(1L)
				.estart("14-02-22").event("eventName").familyId(1L).familyName("My family").isShared("true").build();
		List<EventsRelation> eventsShared = new ArrayList<>();
		eventsShared.add(eventsRelation);
		String date1 = "1965-10-26T00:00:00Z";
		EventsRelationResponse event = EventsRelationResponse.builder().edate(date1).event("Test Event").eend(date1)
				.estart(date1).uid(1L).eid(1L).build();

		List<EventsRelationResponse> eventsRelList = new ArrayList<>();
		eventsRelList.add(event);
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null,
				families, null, null, "theme", "eng", null, null, null);
		String userIdCheck = "idCheck";

		String colors = AppConstants.DEFAULT_COLOR;
		String[] COLOR = { "#FF0000", "#0000FF", "#008000" };
		when(userRepository.findByUserId(user.getId())).thenReturn(user);

		when(eventsRelationRespository.findSharedEventsForFamily(family.getFamilyId())).thenReturn(eventsShared);
		when(eventsRelationRespository.findPrivateEventsForUser(user.getId())).thenReturn(eventsShared);
		List<EventsRelationResponse> response = eventServiceImpl.getUserEventsPlainResponse(1L);
		assertFalse(response.isEmpty());
	}

	@Test
	public void getUserEventsTestCase() {
		String date1 = "1965-10-26T00:00:00Z";
		User user = new User(1L, "user1", "email1.cc.com", "Ew22232wwwWESD", true, "37676", "13/2/22", "635/7/136",
				"firstName", "Lastname", "12/3/45", "male", "23458", "about me", "eng", null, null,
				families, null, null, "theme", "eng", null, null, null);
		String colors = AppConstants.DEFAULT_COLOR;
		String[] COLOR = { "#FF0000", "#0000FF", "#008000" };
		EventsResponse eventsResponse = new EventsResponse();
		eventsResponse.setDate(date1);
		eventsResponse.setEnd("14-02-22");
		eventsResponse.setId(1L);
		eventsResponse.setStart("13-02-22");
		eventsResponse.setTitle("event title");
		List<EventsResponse> eventsList1 = new ArrayList<>();

		EventsUserResponse eventsUserResponse = new EventsUserResponse();
		eventsUserResponse.setColor(colors);
		eventsUserResponse.setCurrentUser(true);
		eventsUserResponse.setEvents(eventsList1);
		eventsUserResponse.setUemail("emailId@mnms");
		eventsUserResponse.setUId(1L);
		eventsUserResponse.setUsername("UserName");
		List<EventsUserResponse> eventsUserResponseList = new ArrayList<>();
		EventsFamilyResponse eventsFamilyResp = new EventsFamilyResponse();
		eventsFamilyResp.setFamilyId(1L);
		eventsFamilyResp.setFamilyName("my Family");
		eventsFamilyResp.setUserEvents(eventsUserResponseList);
		List<EventsFamilyResponse> eventsFamilyRespList = new ArrayList<>();

		Events event = Events.builder().id(1L).userId(1L).date(date1).event("Test Event").eventStart(date1)
				.eventEnd(date1).isShared(false).build();
		List<Events> eventsList = List.of(event);
		when(eventRepo.findUnsharedEventsForUserId(user.getId())).thenReturn(eventsList);
		when(userRepository.findByUserId(user.getId())).thenReturn(user);
		EventsCurrentUserResponse eventsDetails = new EventsCurrentUserResponse();

		eventsDetails.setFamilyEvents(eventsFamilyRespList);
		List<User> usersList = List.of(user);
		when(userRepository.getAllUsersListForAFamily(family.getFamilyId())).thenReturn(usersList);
		List<EventsFamilyUser> eventsFamUser = new ArrayList<>();
		when(eventsFamilyUserRespository.findEventsByFamilyAndUserId(family.getFamilyId(), usersList.get(0).getId()))
				.thenReturn(eventsFamUser);
		EventsCurrentUserResponse response = eventServiceImpl.getUserEvents(user.getId());
		assertNotNull(response);
	}

}
