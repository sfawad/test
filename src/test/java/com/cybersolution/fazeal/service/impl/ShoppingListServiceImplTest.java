package com.cybersolution.fazeal.service.impl;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.NotificationsActivityRespository;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.repository.ShoppingListRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.ShoppingListService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ShoppingListServiceImplTest {

	@InjectMocks
	ShoppingListServiceImpl shoppingListServiceImpl;

	@Mock
	ShoppingListEntriesRepository shopItemsListRepo;
	@Mock
	ShoppingListService shopListService;
	@Mock
	ShoppingListRepository shopListRepo;

	@Mock
	NotificationsActivityRespository notifsRepo;

	@Mock
	ShoppingListService shoppingListService;
	@Mock
	UserRepository userRepo;
	@Mock
	Messages messages;

	private static final Long USERID = 1L;
	private static final Long FAMILY_ID = 1L;
	private static final String SHOPPING_LIST_NAME = "Test Shop";
	private static final boolean IS_ACTIVE = true;
	private static final String user1 = "user1";
	private static final String shopListName = "TEST Shop List";
	private static final String ONE = "one";
	private static final String mail = "email1.cc.com";
	private static final String pass = "Ew22232wwwWESD";
	private static final String TWO = "two";

	Map<String, Boolean> itemMap = new HashMap<>();

	@Test
	public void deleteShoppingListSuccessTestCase() throws Exception {
		ShoppingList shopList = createObj();
		shopList.setId(1L);
		boolean actual = true;
		when(shopListRepo.getByShoppingListId(shopList.getId())).thenReturn(shopList);
		shopListRepo.deleteById(shopList.getId());
		boolean result = shoppingListServiceImpl.deleteShoppingListByShoppingListId(1L);
		assertEquals(result, actual);
	}

	@Test
	public void deleteShoppingListFailTestCase() {
		ShoppingList shopList = createObj();
		shopList.setId(1L);
		boolean actual = false;
		when(shopListRepo.getByShoppingListId(shopList.getId())).thenReturn(null);
		boolean result = shoppingListServiceImpl.deleteShoppingListByShoppingListId(1L);
		assertEquals(result, actual);
	}

	@Test
	public void deleteByitemIdSuccessTestcase() {
		ShoppingListEntries shopListEntry = new ShoppingListEntries(ONE, false, 1L, user1);
		shopListEntry.setId(1L);
		boolean actual = true;
		when(shopItemsListRepo.getByItemId(shopListEntry.getId())).thenReturn(shopListEntry);
		shopItemsListRepo.deleteById(shopListEntry.getId());
		boolean result = shoppingListServiceImpl.deleteShoppingListItemByItemId(shopListEntry.getId());
		assertEquals(result, actual);
	}

	@Test
	public void deleteByitemIdFailTestcase() {
		ShoppingListEntries shopListEntry = new ShoppingListEntries(ONE, false, 1L, user1);
		shopListEntry.setId(1L);
		boolean actual = false;
		when(shopItemsListRepo.getByItemId(shopListEntry.getId())).thenReturn(null);
		shopItemsListRepo.deleteById(shopListEntry.getId());
		boolean result = shoppingListServiceImpl.deleteShoppingListItemByItemId(shopListEntry.getId());
		assertEquals(result, actual);
	}

	@Test
	public void getShoppingListForFamilyOnIdTest() {
		ShoppingList shopList = new ShoppingList(1L, shopListName, 1L, getCurrentDateTime(), true, null, null);
		List<ShoppingList> shoppingLists = List.of(shopList);
		when(shopListRepo.getShoppingListForFamilyOnId(1L)).thenReturn(shoppingLists);
		List<ShoppingList> shoppingList = shoppingListServiceImpl.getShoppingListForFamilyOnId(1L);
		assertEquals(shoppingList, shoppingLists);
	}

	@Test
	public void testSaveInRepository() {
		Family family = new Family(1L, "about", null, "123", "token13", "fam1", "line1", "line2", "city", "state", null,
				false, false, null, null, null);
		Set<Family> families = Set.of(family);
		User user = new User(1L, user1, mail, pass, true, "37676", "13/2/22", "635/7/136", "firstName", "Lastname",
				"12/3/45", "male", "23458", "about me", "eng", null,  null, families, null, null, null, "eng", null, null, null);
		itemMap.put("ShoopinListName", true);
		ShoppingListRequest shoppingListRequest = getTestRequest();
		shoppingListRequest.setItemMap(itemMap);
		ShoppingListEntries shopListEntrySaved = new ShoppingListEntries(ONE, false, 1L, user.getUsername());
		user.getFamilies();

		Set<ShoppingListEntries> shopListSet = new HashSet<>();
		shopListSet.add(shopListEntrySaved);

		ShoppingList shopList = new ShoppingList(1L, shopListName, 1L, getCurrentDateTime(), true, null, null);
		shopList.setShoppingItemsList(shopListSet);
		when(userRepo.getById(1L)).thenReturn(user);
		shopItemsListRepo.save(shopListEntrySaved);
		when(shopListRepo.save(ArgumentMatchers.any(ShoppingList.class))).thenReturn(shopList);
		ShoppingList response = shoppingListServiceImpl.saveShoppingList(1L, shoppingListRequest);
		verify(shopItemsListRepo, atLeastOnce()).save(ArgumentMatchers.any());
		assertEquals(response, shopList);

	}

	@Test
	public void testUpdateInRepository() {
		User user = new User(user1, mail, pass, true);
		ShoppingListUpdateRequest shoppingListUpdateRequest = createUpdatePOJOObj();
		ShoppingListEntries shopListEntrySaved = new ShoppingListEntries(ONE, false, 1L, user1);
		Set<ShoppingListEntries> shopListEntrySet = new HashSet<>();
		shopListEntrySet.add(shopListEntrySaved);
		ShoppingList shopList = new ShoppingList(1L, shopListName, 1L, getCurrentDateTime(), true, null, null);
		shopList.setShoppingItemsList(shopListEntrySet);

		when(shoppingListService.findById(shoppingListUpdateRequest.getId())).thenReturn(shopList);
		when(userRepo.getById(1L)).thenReturn(user);
		when(shopItemsListRepo.getShoppingListEntriesByListId(shoppingListUpdateRequest.getId()))
				.thenReturn(shopListEntrySet);
		shopItemsListRepo.deleteAll(shopListEntrySet);
		shopItemsListRepo.save(shopListEntrySaved);
		when(shopListRepo.save(ArgumentMatchers.any(ShoppingList.class))).thenReturn(shopList);
		ShoppingList response = shoppingListServiceImpl.updateShoppingList(1L, shoppingListUpdateRequest);
		verify(shopItemsListRepo, atLeastOnce()).save(ArgumentMatchers.any());
		verify(shopItemsListRepo, atLeastOnce()).deleteAll(ArgumentMatchers.any());

		assertEquals(response, shopList);

	}

	@Test
	public void testUpdateInRepositoryNullUser() {
		User user = new User();
		ShoppingListUpdateRequest shoppingListUpdateRequest = createUpdatePOJOObj();
		when(userRepo.getById(1L)).thenReturn(user);
		ShoppingList response = shoppingListServiceImpl.updateShoppingList(1L, shoppingListUpdateRequest);
		assertNull(response);
	}

	@Test
	public void testUpdateInRepositoryNullUserShopList() {
		ShoppingList shoppingList = new ShoppingList();
		ShoppingListUpdateRequest shoppingListUpdateRequest = createUpdatePOJOObj();
		shoppingListUpdateRequest.setId(null);
		when(shoppingListService.findById(shoppingListUpdateRequest.getId())).thenReturn(shoppingList);
		ShoppingList response = shoppingListServiceImpl.updateShoppingList(1L, shoppingListUpdateRequest);
		assertNull(response);
	}

	@Test
	public void testSaveShoppingListNullUser() {

		ShoppingListRequest shoppingListRequest = getTestRequest();

		when(userRepo.getById(1L)).thenReturn(null);

		ShoppingList response = shoppingListServiceImpl.saveShoppingList(null, shoppingListRequest);

		assertNull(response);
	}

	public ShoppingListRequest getTestRequest() {
		return ShoppingListRequest.builder().familyId(FAMILY_ID).shoppinglistName(SHOPPING_LIST_NAME)
				.familyId(FAMILY_ID).isActive(IS_ACTIVE).itemMap(itemMap).build();
	}

	private ShoppingList createObj() {
		ShoppingList shopList = new ShoppingList(shopListName, 1L, getCurrentDateTime(), true, null);
		Set<ShoppingListEntries> shopListEntries = new HashSet<>();
		ShoppingListEntries shopListEntry = new ShoppingListEntries(ONE, false, 1L, user1);
		shopListEntries.add(shopListEntry);
		shopListEntry = new ShoppingListEntries(TWO, false, 1L, user1);
		shopListEntries.add(shopListEntry);
		shopList.setShoppingItemsList(shopListEntries);
		return shopList;
	}

	private String getCurrentDateTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}

	private ShoppingListUpdateRequest createUpdatePOJOObj() {
		ShoppingListUpdateRequest shopList = new ShoppingListUpdateRequest();

		Map<String, Boolean> strMap = new HashMap<>();
		strMap.put(ONE, false);
		strMap.put(TWO, false);
		strMap.put("three", true);

		shopList.setId(1L);
		shopList.setItemMap(strMap);
		shopList.setShoppingListName(shopListName);
		shopList.setListStatus(true);
		return shopList;
	}

}
