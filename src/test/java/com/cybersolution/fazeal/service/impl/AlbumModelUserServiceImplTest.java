package com.cybersolution.fazeal.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


import com.cybersolution.fazeal.models.AlbumModelUser;
import com.cybersolution.fazeal.repository.AlbumUsersRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AlbumModelUserServiceImplTest {
	
	@InjectMocks
	AlbumModelUserServiceImpl albumModelUserServiceImpl;
	
	@Mock
	AlbumUsersRepository albumUsersRepository;
	
	@Test
	void testaddFamily() {
		AlbumModelUser albumModelUser = new AlbumModelUser(1L, 1L, 1L);
		when(albumUsersRepository.save(albumModelUser)).thenReturn(albumModelUser);
		AlbumModelUser albumModelUserReturn = albumModelUserServiceImpl.addUser(albumModelUser);
		assertEquals(albumModelUser, albumModelUserReturn);
	}

	
}
