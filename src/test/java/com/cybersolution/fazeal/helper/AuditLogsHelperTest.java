package com.cybersolution.fazeal.helper;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.cybersolution.fazeal.models.AuditLogs;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.AuditLogsService;
import com.cybersolution.fazeal.util.Utils;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuditLogsHelperTest {

  @InjectMocks
  private AuditLogsHelper auditLogsHelper;

  @Mock
  UserRepository userRepo;
  
  @Mock
  AuditLogsService auditLogsService;

  @Test
  public void testSaveAuditLogs() {
	  User user = new User("user1", "email1.cc.com", "Ew22232wwwWESD", true);
	  AuditLogs auditLog = new AuditLogs(null, 1L, "user1",Utils.getCurrentDateTime(), "TEST", null, true,null);
      when(userRepo.getById(1L)).thenReturn(user);
      auditLogsHelper.saveAuditLogs(1L, "TEST", null, true,null);
      verify(auditLogsService).save(auditLog);
      
  }
  

}
